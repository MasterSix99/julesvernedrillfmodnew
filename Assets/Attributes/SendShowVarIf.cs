﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendShowVarIf : PropertyAttribute
{
    public int id;

    public string variableToShow;
    public bool boolValue;
    public int intValue;

    public SendShowVarIf(int _id, string _variableToShowName, bool _boolValue = true)
    {
        id = _id;
        variableToShow = _variableToShowName;
        boolValue = _boolValue;
        intValue = 0;
    }

    public SendShowVarIf(int _id, string _variableToShowName, int _intValue)
    {
        id = _id;
        variableToShow = _variableToShowName;
        intValue = _intValue;
        boolValue = false;
    }
}
