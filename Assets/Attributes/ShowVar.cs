﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowVar : PropertyAttribute
{
    //public int id;
    public bool show = true;

    public static Dictionary<int, ShowVar> showVarDictionary = new Dictionary<int, ShowVar>();

    public ShowVar(int _id)
    {
        //id = _id;
        showVarDictionary.Add(_id, this);
    }
}
