﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowIfEnum : PropertyAttribute
{
    public string variableName;
    public int value;

    public ShowIfEnum(string _variableName, int _value)
    {
        variableName = _variableName;
        value = _value;
    }
}
