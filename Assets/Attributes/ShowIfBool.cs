﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
public class ShowIfBool : PropertyAttribute
{
    public string variableName;
    public bool value;

    public ShowIfBool(string _variableName, bool _value = true)
    {
        variableName = _variableName;
        value = _value;
    }
}
