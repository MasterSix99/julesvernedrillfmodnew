﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideIfEnum : PropertyAttribute
{
    public string variableName;
    public int value;

    public HideIfEnum(string _variableName, int _value)
    {
        variableName = _variableName;
        value = _value;
    }
}
