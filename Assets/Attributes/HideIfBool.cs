﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideIfBool : PropertyAttribute
{
    public string variableName;
    public bool value;

    public HideIfBool(string _variableName, bool _value = true)
    {
        variableName = _variableName;
        value = _value;
    }
}
