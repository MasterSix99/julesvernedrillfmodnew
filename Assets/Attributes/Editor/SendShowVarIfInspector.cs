﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomPropertyDrawer(typeof(SendShowVarIf))]
public class SendShowVarIfInspector : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.PropertyField(position, property, label);

        //Get the target attribute of this drawer
        SendShowVarIf sendAttribute = attribute as SendShowVarIf;
        ShowVar showAttribute;

        if (ShowVar.showVarDictionary.TryGetValue(sendAttribute.id, out showAttribute))
        {
            if (property.propertyType == SerializedPropertyType.Boolean)
            {
                if (property.boolValue == sendAttribute.boolValue)
                {
                    showAttribute.show = true;
                }
                else
                {
                    Debug.Log(property.boolValue + " - " + sendAttribute.boolValue);
                    showAttribute.show = false;
                }
            }
            else
            {
                if (property.intValue == sendAttribute.intValue)
                {
                    if (showAttribute.show != true)
                        showAttribute.show = true;
                }
                else
                {
                    if (showAttribute.show != false)
                        showAttribute.show = false;
                }
            }
        }
    }
}
