﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(HideIfEnum))]
public class HideIfEnumDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        HideIfEnum hideIfAttribute = attribute as HideIfEnum;
        SerializedProperty propertyToCheck = property.serializedObject.FindProperty(hideIfAttribute.variableName);
        if (propertyToCheck.enumValueIndex != hideIfAttribute.value)
            EditorGUI.PropertyField(position, property, label);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        HideIfEnum hideIfAttribute = attribute as HideIfEnum;
        SerializedProperty propertyToCheck = property.serializedObject.FindProperty(hideIfAttribute.variableName);
        if (propertyToCheck.enumValueIndex != hideIfAttribute.value)
            return base.GetPropertyHeight(property, label);
        else
            return -EditorGUIUtility.standardVerticalSpacing;
    }
}
