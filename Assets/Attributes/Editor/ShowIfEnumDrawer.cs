﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ShowIfEnum))]
public class ShowIfEnumDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        ShowIfEnum showIfAttribute = attribute as ShowIfEnum;
        SerializedProperty propertyToCheck = property.serializedObject.FindProperty(showIfAttribute.variableName);
        if (propertyToCheck.enumValueIndex == showIfAttribute.value)
            EditorGUI.PropertyField(position, property, label);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        ShowIfEnum showIfAttribute = attribute as ShowIfEnum;
        SerializedProperty propertyToCheck = property.serializedObject.FindProperty(showIfAttribute.variableName);
        if (propertyToCheck.enumValueIndex == showIfAttribute.value)
            return base.GetPropertyHeight(property, label);
        else
            return -EditorGUIUtility.standardVerticalSpacing;
    }
}
