﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(HideIfBool))]
public class HideIfBoolDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        HideIfBool hideIfAttribute = attribute as HideIfBool;
        SerializedProperty propertyToCheck = property.serializedObject.FindProperty(hideIfAttribute.variableName);
        if (propertyToCheck.boolValue != hideIfAttribute.value)
            EditorGUI.PropertyField(position, property, label);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        HideIfBool hideIfAttribute = attribute as HideIfBool;
        SerializedProperty propertyToCheck = property.serializedObject.FindProperty(hideIfAttribute.variableName);
        if (propertyToCheck.boolValue != hideIfAttribute.value)
            return base.GetPropertyHeight(property, label);
        else
            return -EditorGUIUtility.standardVerticalSpacing;
    }
}
