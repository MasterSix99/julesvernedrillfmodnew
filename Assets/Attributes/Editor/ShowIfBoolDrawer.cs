﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ShowIfBool))]
public class ShowIfBoolDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        ShowIfBool showIfAttribute = attribute as ShowIfBool;
        SerializedProperty propertyToCheck = property.serializedObject.FindProperty(showIfAttribute.variableName);
        if (propertyToCheck.boolValue == showIfAttribute.value)
            EditorGUI.PropertyField(position, property, label);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        ShowIfBool showIfAttribute = attribute as ShowIfBool;
        SerializedProperty propertyToCheck = property.serializedObject.FindProperty(showIfAttribute.variableName);
        if (propertyToCheck.boolValue == showIfAttribute.value)
            return base.GetPropertyHeight(property, label);
        else
            return -EditorGUIUtility.standardVerticalSpacing;
    }
}
