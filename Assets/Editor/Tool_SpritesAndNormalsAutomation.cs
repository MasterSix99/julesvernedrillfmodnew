﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class NormalAndSprites_Tool : Editor
{


    //This method ask you to have a selection. Looks inside that folder or file and takes only 2D textures, check if there is a material
    //created with their name in their path and if there is not, it will create it. Discerns normal maps (norm_ coded) and sprites (_spr coded)
    [MenuItem("Custom Tools/Create New Material")]
    static void CreateMaterials()
    {
        try
        {
            AssetDatabase.StartAssetEditing();
            var textures = Selection.GetFiltered(typeof(Texture2D), SelectionMode.DeepAssets).Cast<Texture2D>();

            foreach (Texture2D tex in textures)
            {
                string path = AssetDatabase.GetAssetPath(tex);

                if (path.ToLower().Contains("_old") == true)
                {
                    continue;
                }

                if (path.Contains("norm_") == true)
                {
                    continue;
                }

                if (path.ToLower().Contains("animat") == true)
                {
                    continue;
                }

                path = path.Substring(0, path.LastIndexOf(".")) + ".mat";

                if (AssetDatabase.LoadAssetAtPath(path, typeof(Material)) != null)
                {
                    Debug.LogWarning("This material already exists: " + path);
                    continue;
                }
                var mat = new Material(Shader.Find("Sprite (Vertex Lit)"));
                mat.mainTexture = tex;
                mat.SetFloat("_ZWrite",1.0f);
                /////
                //mat.SetFloat("_Mode", 2f);

                //mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                //mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                //mat.SetInt("_ZWrite", 0);
                //mat.DisableKeyword("_ALPHATEST_ON");
                //mat.EnableKeyword("_ALPHABLEND_ON");
                //mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                //mat.renderQueue = 3000;
                ////
                path = path.Replace("spr_", "mat_");
                Debug.Log(path);
                AssetDatabase.CreateAsset(mat, path);
                Debug.Log("Created material here:" + path);
            }

            foreach (Texture2D tex in textures)
            {
                string path = AssetDatabase.GetAssetPath(tex);

                if (path.ToLower().Contains("_old") == true)
                {
                    continue;
                }

                if (path.Contains("spr_") == true)
                {
                    continue;
                }

                if (path.ToLower().Contains("animat") == true)
                {
                    continue;
                }

                path = path.Substring(0, path.LastIndexOf(".")) + ".mat";
                path = path.Replace("norm_", "mat_");

                Material mat = (Material)AssetDatabase.LoadAssetAtPath(path, typeof(Material));

                Debug.LogWarning("The loaded material WITH NORMAL is: " + mat);

                Debug.Log(tex);

                mat.EnableKeyword("_BumpMap");
                mat.SetTexture("_BumpMap", tex);
            }
        }
        finally
        {
            AssetDatabase.StopAssetEditing();
            AssetDatabase.SaveAssets();
        }
        AssetDatabase.Refresh();
    }


    //This method look for every prefab inside the "prefab folder", and with the "pref_" tag in its path. Then, looks if it have the Models child
    //If yes, looks for every model with a renderer component and then associates them a material, if exists, that shares the same name.
    [MenuItem("Custom Tools/Auto Associate Materials")]
    static void AssociateMaterials()
    {
        AssetDatabase.StartAssetEditing();
        //GameObject[] prefabs = Selection.GetFiltered(typeof(GameObject), SelectionMode.DeepAssets) as GameObject[];
        GameObject[] prefabs = Selection.GetFiltered<GameObject>(SelectionMode.DeepAssets);
        try
        {
            foreach (GameObject pref in prefabs)
            {
                string path = AssetDatabase.GetAssetPath(pref);

                if (path.ToLower().Contains("_old"))
                    continue;

                SpriteRenderer[] sprChild = pref.GetComponentsInChildren<SpriteRenderer>();
                Debug.Log("sprChild lenght is: " + sprChild.Length);

                foreach (SpriteRenderer sprr in sprChild)
                {
                    string materialPath = "dioporco";

                    string pathT = sprr.gameObject.name;

                    if (!pathT.Contains("spr_"))
                        continue;

                    Debug.Log("The name of the object is: " + pathT);

                    string[] materialPathArray = AssetDatabase.FindAssets(pathT.Substring(pathT.IndexOf("_")));

                    pathT = sprr.gameObject.name + ".mat";

                    foreach (string str in materialPathArray)
                    {
                        string strT = AssetDatabase.GUIDToAssetPath(str);

                        Debug.Log("The first strT is: " + strT);
                        Debug.Log("The utilized pathT is: " + pathT.Substring(pathT.IndexOf("_")));
                        Debug.Log(strT.Contains(pathT.Substring(pathT.IndexOf("_"))));

                        if (!strT.Contains(pathT.Substring(pathT.IndexOf("_"))))
                        {
                            Debug.Log("PORCODIO");
                            continue;
                        }

                        materialPath = strT;
                        break;
                    }

                    sprr.material = (Material)AssetDatabase.LoadAssetAtPath(materialPath, typeof(Material));

                }
                //Debug.Log(materialPathArray);
                //Material material = AssetDatabase.LoadAssetAtPath(path.Replace(""))
            }
        }
        catch
        {
            Debug.LogWarning("PRESO UN ERRORE");
        }
        finally
        {
            AssetDatabase.StopAssetEditing();
        }
    }
}



//this importer automatically set the texture type of imported 2Dtextures. If they contains "spr_" they will be sprites, if they contains "norm_" they will be normalMaps
public class AutoSetTextureType : AssetPostprocessor
{

    void OnPreprocessTexture()
    {
        string lowerCaseAssetPath = assetPath.ToLower();
        bool isSprite = lowerCaseAssetPath.Contains("spr_");
        bool isNormal = lowerCaseAssetPath.Contains("norm_");

        TextureImporter textureImporter = (TextureImporter)assetImporter;

        if (isSprite)
        {
            textureImporter.textureType = TextureImporterType.Sprite;
            EditorUtility.SetDirty(textureImporter);
        }

        if (isNormal)
        {
            textureImporter.textureType = TextureImporterType.NormalMap;
            EditorUtility.SetDirty(textureImporter);
        }

        //textureImporter.SaveAndReimport();
    }
}






