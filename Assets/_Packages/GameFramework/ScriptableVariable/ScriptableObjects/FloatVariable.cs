﻿using System;
using UnityEngine;

namespace GameFramework
{
	[CreateAssetMenu]
	public class FloatVariable : ScriptableObject 
	{
		[SerializeField]
		private float m_baseValue = 0.0f;

		[ReadOnlyAttribute]
		[SerializeField]
		private float m_value;
        public Action onValueChangeAction;

		public float Value 
		{ 
			get { return m_value;  } 
			set {
                m_value = value;
                onValueChangeAction?.Invoke();
            }
		}
		
		public void OnEnable()
		{
			Value = m_baseValue;	
		}
        private void OnDisable()
        {
            onValueChangeAction = null;
        }

    }
}