﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameFramework
{
    public class InteractionManager2D : MonoBehaviour {

        #region VARIABLES
        /// <summary>
        /// Allowed interation layers
        /// </summary>
        [SerializeField]
        protected LayerMask allowedLayers;
        /// <summary>
        /// Determine is interactionManager is enabled or not
        /// </summary>
        [SerializeField]
        protected bool m_isEnabled = true;
        /// <summary>
        /// Point wher put pickupped GameObject
        /// </summary>
        [SerializeField]
        protected Transform m_pickPoiunt;
        /// <summary>
        /// Used to check throw direction
        /// </summary>
        [SerializeField][Tooltip("Transform utilizzato per controllare la direzione del lancio ( quindi quello che viene ruotato quando il player gira)")]
        protected Transform directionTrasform;
        /// <summary>
        /// Force applied when release Pickup
        /// </summary>
        [Header("Release and throw configurations")][SerializeField][Tooltip("Force applied on release (NOT TRHOW)")]
        protected EntityVector2Stat m_releaseForce;
        [SerializeField] [Tooltip("Force applied on Throw press")]
        protected EntityVector2Stat m_thorwForce;
        [Header("AUDIO")]
        [SerializeField] private OneShotAudio pickupSound;

        /// <summary>
        /// List of interactables near the player
        /// </summary>
        private List<Interactable2D> m_interactables = new List<Interactable2D>();
        private Rigidbody2D m_rb;
        private Pickup m_currentPickup = null;
        private Interactable2D m_currentInteractable;

        private MovementBehaviour2D movementBehaviour;

        #endregion

        #region PROPERTIES
        public bool IsEnabled
        {
            get { return m_isEnabled; }
            private set
            {
                m_isEnabled = value;
            }
        }
        public Transform PickupPoint
        {
            get { return m_pickPoiunt; }
        }
        public Rigidbody2D Rb
        {
            get { return m_rb; }
        }
        public Interactable2D CurrentInteractable
        {
            get { return m_currentInteractable; }
        }
        public Interactable2D CurrentPickup
        {
            get { return m_currentPickup; }
        }

        public EntityVector2Stat ThrowForce
        {
            get { return m_thorwForce; }
            set { m_thorwForce = value; }
        }
        public EntityVector2Stat ReleaseForce
        {
            get { return m_releaseForce; }
            set { m_releaseForce = value; }
        }

        #endregion

        #region EVENTS
        [Header("EVENTS")]
        public UnityEvent onInteractEvt;
        public UnityEvent onInteractionReleaseEvt;
        public UnityEvent onPickupEvt;
        public UnityEvent onPickupReleaseEvt;
        #endregion

        #region MONOBEHAVIOURS
        private void Awake()
        {
            m_rb = GetComponent<Rigidbody2D>();
            if (directionTrasform == null) directionTrasform = transform;
            movementBehaviour = GetComponent<MovementBehaviour2D>();
        }
        #endregion

        #region MAIN ACTIONS
        /// <summary>
        /// Check nearest interactable and interact with it
        /// </summary>
        public void Interact()
        {
            if (IsEnabled && m_currentPickup == null) {
                
                //Release current active interaction if exist
                if (m_currentInteractable != null)
                {
                    ReleaseInteraction();
                }
                else
                {
                    if (m_interactables.Count > 0)
                    {
                        Interactable2D nearest = GetNearest<Interactable2D>();
                        if (nearest != null && nearest.IsEnabled && !nearest.IsOwned)
                        {
                            m_currentInteractable = nearest;
                            if (nearest.Interact(this))
                            {
                                OnInteractionDone(nearest);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Release interaction owned
        /// </summary>
        public void ReleaseInteraction()
        {
            Debug.Log("RELEASE INTERACTION");
            m_currentInteractable.Release(this);
            m_currentInteractable = null;
            onInteractionReleaseEvt?.Invoke();
        }


        /// <summary>
        /// Check nearest pickup and pick it
        /// </summary>
        public void Pickup()
        {
            if (IsEnabled)
            {
                // Release current pickup if exist
                if(m_currentPickup != null)
                {
                    ReleasePickup(true);
                }
                else
                { 
                    if (m_interactables.Count > 0)
                    {
                        Pickup nearest = GetNearest<Pickup>();
                        
                        if (nearest != null && nearest.IsEnabled)
                        {
                            if (nearest.Interact(this))
                            {
                                onPickupEvt?.Invoke();
                                pickupSound.PlayAudio(transform.position);
                                m_currentPickup = nearest;
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Release current pickup owned (if exist)
        /// </summary>
        /// <param name="throwAway"></param>
        public void ReleasePickup(bool throwAway = false)
        {
            if(m_currentPickup != null) { 
                m_currentPickup?.Release(this);
                Vector2 _releaseForce = m_releaseForce.GetValue();
                Vector2 _throwForce = m_thorwForce.GetValue();

                float forceMultiplier = throwAway ? m_thorwForce.GetValue().x : m_releaseForce.GetValue().x;
                Vector2 throwVector = (directionTrasform.right * Mathf.Sign(directionTrasform.localScale.x)).normalized;
                
                if (throwAway) {
                    //Debug.Log("_throwForce"+_throwForce);
                    throwVector = throwVector * _throwForce;
                    //Debug.Log("throwVector"+throwVector);
                    EventManager.TriggerEvent(EventsID.PLAYERTHROW);
                }
                else
                    throwVector = throwVector + _releaseForce;

                throwVector *= m_currentPickup.Rb.mass;
                m_currentPickup.Rb.AddForce(throwVector, ForceMode2D.Impulse);
                onPickupReleaseEvt?.Invoke();
                //CHECK IF PICKUP IS DISABLED IN FEW MOMENTS BECOUSE IF DONT TRIGGER THE TRIGGEREXIT THE INTERACTION WILL BE NOT REMOVED
                StartCoroutine(CheckIfReleasedIsDisabled(m_currentPickup));
                m_currentPickup = null;
            }

        }
        #endregion

        #region CALLBACK
        /// <summary>
        /// callend when interaction is end
        /// </summary>
        /// <param name="interactable"></param>
        public void OnInteractionDone(Interactable2D interactable)
        {
            if (!interactable.IsReUsable) { 
                m_interactables.Remove(interactable);
            }
            onInteractEvt.Invoke();
        }
        
        #endregion

        #region UTILITY
        /// <summary>
        /// Get nearest Interactable or Pickup
        /// </summary>
        /// <typeparam name="T">Type to search</typeparam>
        /// <returns></returns>
        public T GetNearest<T>() where T : Interactable2D
        {
            Interactable2D nearest = null;
            float minDistance = 0;
            foreach (Interactable2D interactable in m_interactables)
            {
                if (interactable.IsEnabled && !interactable.GetType().Equals(typeof(T)))
                    continue;
                float distance = Vector3.Distance(interactable.transform.position, transform.position);
                if (Mathf.Abs(minDistance) == 0 || Mathf.Abs(distance) < Mathf.Abs(minDistance))
                {
                    nearest = interactable;
                    minDistance = distance;
                }
            }

            return nearest as T;
        }
        /// <summary>
        /// Add interaction to near interactables list
        /// </summary>
        /// <param name="interactable"></param>
        public void AddInteraction(Interactable2D interactable)
        {
            if (interactable != null && interactable.IsEnabled)
            {
                m_interactables.Add(interactable);
            }
        }
        /// <summary>
        /// Remove interaction to near interactables list
        /// </summary>
        /// <param name="interactable"></param>
        public void RemoveInteraction(Interactable2D interactable)
        {
            if (m_interactables.Contains(interactable)) { 
                m_interactables.Remove(interactable);
            }
        }
        /// <summary>
        /// Used from interactable to freeze character
        /// </summary>
        public void FreezeCharacter()
        {
            if (movementBehaviour != null)
            {
                movementBehaviour.Freeze();
            }
        }
        /// <summary>
        /// Used from interactable to Abort character freeze
        /// </summary>
        public void ReleaseCharacter()
        {
            if (movementBehaviour != null)
            {
                movementBehaviour.UnFreeze();
            }
        }
        /// <summary>
        /// Use this to check if released pickup is still active as gameobject becouse if obj is disabled after release the triggerExit will not be called and interaction is not removed
        /// </summary>
        /// <param name="interactable"></param>
        /// <returns></returns>
        IEnumerator CheckIfReleasedIsDisabled(Interactable2D interactable)
        {
            yield return new WaitForSeconds(0.1f);
            if (interactable.gameObject == null | !interactable.gameObject.activeSelf)
                RemoveInteraction(interactable);
            yield return null;
        }
        #endregion

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (Utilities.LayerIsInLayerMask(other.gameObject.layer, allowedLayers))
            {
                Interactable2D interactable = other.gameObject.GetComponent<Interactable2D>();
                if (interactable != null)
                    AddInteraction(interactable);
            }
        }
        private void OnTriggerExit2D(Collider2D other)
        {
            if (Utilities.LayerIsInLayerMask(other.gameObject.layer, allowedLayers))
            {
                Interactable2D interactable = other.gameObject.GetComponent<Interactable2D>();
                if(interactable != null)
                    RemoveInteraction(interactable);
            }
        }

    }
}
