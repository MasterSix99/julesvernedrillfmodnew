﻿using UnityEngine;
using UnityEngine.Events;

namespace GameFramework { 

    public class Interactable2D : MonoBehaviour {

        #region VARIABLES
        [SerializeField]
        protected LayerMask m_allowedMask;
        [SerializeField]
        protected InteractionInputType interactionInput;
        [SerializeField]
        protected bool m_isEnabled = true;
        [SerializeField]
        protected bool m_isReusable = true;

        private bool m_isOwned = false;
        #endregion

        #region PROPERTIES
        public bool IsEnabled
        {
            get { return m_isEnabled; }
            private set
            {
                m_isEnabled = value;
            }
        }
        public bool IsReUsable
        {
            get { return m_isReusable; }
            private set
            {
                m_isReusable = value;
            }
        }
        public bool IsOwned
        {
            get { return m_isOwned; }
        }
        public InteractionInputType InteractionInput
        {
            get { return interactionInput; }
            private set
            {
                interactionInput = value;
            }
        }
        #endregion

        #region EVENTS
        [Header("Events")]
        public UnityEvent onFocusEvent;
        public UnityEvent onLostFocusEvent;
        public InteractionEvent onInteractEvent;
        public InteractionEvent onReleaseEvent;
        #endregion

        #region MONOBEHAVIOUR
        protected virtual void Awake() { }
        protected virtual void OnDestroy()
        {
            onFocusEvent.RemoveAllListeners();
            onLostFocusEvent.RemoveAllListeners();
            onInteractEvent.RemoveAllListeners();
            onReleaseEvent.RemoveAllListeners();
        }
        #endregion

        public bool Interact(InteractionManager2D interactionManager)
        {
            if (m_isEnabled) {
                if (interactionInput.Equals(InteractionInputType.KeyDown)) m_isOwned = true;
                OnInteract(interactionManager);
                onInteractEvent?.Invoke(interactionManager);
                return true;
            }
            return false;
        }
        public bool Interact()
        {
            return Interact(null);
        }

        protected virtual void OnInteract(InteractionManager2D interactionManager){}
        protected virtual void OnInteract(){}

        public virtual void OnInteractionDone(InteractionManager2D interactionManager)
        {
            if (!m_isReusable)
                m_isEnabled = false;
        }

        public void Release(InteractionManager2D interactionManager)
        {
            m_isOwned = false;
            OnRelease(interactionManager);
            onReleaseEvent?.Invoke(interactionManager);
        }
        public void Release()
        {
            Release(null);
        }

        protected virtual void OnRelease(InteractionManager2D interactionManager){}
        protected virtual void OnRelease(){}

        /// <summary>
        /// Called from interaction manager when object has focus
        /// </summary>
        /// <param name="interactionManager"></param>
        public virtual void  OnFocus(InteractionManager2D interactionManager)
        {
            onFocusEvent?.Invoke();
        }

        /// <summary>
        /// Called from interaction manager when object has lost focus
        /// </summary>
        /// <param name="interactionManager"></param>
        public virtual void OnLostFocus(InteractionManager2D interactionManager)
        {
                onLostFocusEvent?.Invoke();
        }



    }

    [System.Serializable]
    public class InteractionEvent : UnityEvent<InteractionManager2D>
    {

    }

    public enum InteractionInputType
    {
        KeyDown,
        KeyHeld,
    }

}