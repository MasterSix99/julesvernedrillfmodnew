﻿using UnityEngine;
namespace GameFramework {

    public class Pickup : Interactable2D
    {
        #region VARIABLES
        protected bool m_isPicked = false;
        protected Rigidbody2D rb;
        protected Transform referenceTransform;
        #endregion

        #region PROPERTIES
        public bool IsPicked
        {
            get { return m_isPicked; }
            private set
            {
                m_isPicked = value;
            }
        }
        public Rigidbody2D Rb
        {
            get { return rb; }
        }
        #endregion

        protected override void Awake()
        {
            base.Awake();
            rb = GetComponent<Rigidbody2D>();
        }

        protected override void OnInteract(InteractionManager2D interactionManager)
        {
            if (IsPicked)
            {
                OnRelease(interactionManager);
            }
            else if (IsEnabled && !IsPicked) { 
                base.OnInteract(interactionManager);
                OnPickup(interactionManager);
            }
        }

        public override void OnInteractionDone(InteractionManager2D interactionManager)
        {
            if (!m_isReusable)
                m_isEnabled = false;
        }


        public virtual void OnPickup(InteractionManager2D interactionManager)
        {

            rb.velocity = Vector2.zero;
            rb.isKinematic = true;
            rb.simulated = false;
            referenceTransform = transform.parent;
            transform.parent = interactionManager.PickupPoint;
            transform.position = interactionManager.PickupPoint.position;
            IsPicked = true;
            

        }

        protected override void OnRelease(InteractionManager2D interactionManager)
        {
            transform.parent = referenceTransform;
            IsPicked = false;
            rb.isKinematic = false;
            rb.simulated = true;
        }
    }
}
