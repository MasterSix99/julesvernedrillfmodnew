﻿
// LAST ID = ALLHOLESREPAIRED = 48


public enum EventsID
{
    #region Game Flow
    NONE = 0,
    GAMESTARTED = 1,
    GAMESTATECHANGED = 2,
    PLAYERDEAD = 3,
    GAMEOVER = 4,
    GAMEWIN = 5,
    GAMEEND = 35,
    OPENMENU = 12,
    CLOSEMENU = 13,
    TOGGLEMENU = 14,
    DESTINATIONREACHED = 15,
    CALCULATESTATS = 30,
    TIMEELAPSED = 38,
    CHANGELIGHTS =  43,
    #endregion

    #region Player Lobby
    PLAYERJOINED = 7,
    PLAYERLEFT = 8,
    #endregion

    #region Player Events
    PLAYERJUMP = 39,
    PLAYERTHROW = 40,
    PLAYERREPAIR = 41,
    #endregion

    #region Machines
    DRILL_CHANGETILE = 9,
    DRIVING_STARTDRIVING = 10,
    DRIVING_STOPDRIVING = 11,
    ONGAMEOVER = 16,
    ONDRILLOVERHEAT = 17,
    DAMAGEDRILL = 18,
    DRILLDEPTHTOREACHUPDATE = 19,
    ONDRILLOVERHEATAWARE = 20,
    MINIMUMHEATREACHED = 21,
    MAXPRESSUREREACHED = 36,
    DRILLSTOPPED = 37,
    HOLEREPAIRED = 44,
    ENGINEFLUSH = 47,
    ALLHOLESREPAIRED = 48,
    #endregion

    #region Machine Statistics
    BROKENENGINE = 22,
    BROKENTRASHER = 23,
    BROKENSPAWNER = 24,
    BROKENDRIVER = 25,
    BROKENCRUSHER = 26,
    #endregion

    //R = RESOURCE
    #region Resources Statistics
    R_GATHERED = 27,
    R_USED = 28,
    R_TRASHED = 29,
    R_CRUSHED = 31,
    #endregion

    #region INFOBOX & DIALOGUE SYSTEM
    INFOBOXREQUEST= 32,
    DIALOGUESTART = 33,
    DIALOGUEEND = 34,
    BANNERINFOREQUEST = 45,
    #endregion

    //#region Tilemap
    //TILEMAP_MOVE = 17,
    //#endregion

    #region BUTTONS FIX
    UISELECTABLESELECTED = 42,
    #endregion

    ACHIEVEMENTUNLOCKED = 46,
}
