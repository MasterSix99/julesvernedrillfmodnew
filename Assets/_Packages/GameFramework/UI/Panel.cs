﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace GameFramework
{
    public class Panel : MonoBehaviour
    {

        #region VARIABLES
        [SerializeField]
        private bool openOnStart = false;
        [SerializeField]
        private bool closeOnStart = false;
        [SerializeField]
        private bool autoHide = false;
        [SerializeField] [ShowIf("autoHide",true)]
        private int autoHideTime = 0;
        /// <summary>
        /// Ui Object to select when panel is opened
        /// </summary>
        [SerializeField]
        protected GameObject m_firstSelectedObject;
        [SerializeField]
        protected bool setFirstSelectedAlsoIfAlreadyEnabled = false;
        /// <summary>
        /// Event used to open panel
        /// </summary>
        [Space] [SerializeField]
        protected EventsID m_openEvent;
        /// <summary>
        /// Event used to close panel
        /// </summary>
        [SerializeField]
        protected EventsID m_closeEvent;
        /// <summary>
        /// Used to know is timescale must be stopped when panel is opened
        /// </summary>
        [SerializeField]
        protected bool m_stopTimeScale;
        /// <summary>
        /// Determine if panel open must trigger a gameState change
        /// </summary>
        [SerializeField]
        protected bool m_changeGameState = false;
        /// <summary>
        /// New gamestate applied when panel is open if m_changeGameState is true
        /// </summary>
        [ShowIf("m_changeGameState", true)] [SerializeField]
        protected GameStateTypes m_newGameState = GameStateTypes.Pause;
        /// <summary>
        /// There the current gamestate is stored before apply new gamestate and then is restored when panel is closed
        /// </summary>
        protected GameStateTypes m_previousGameState = GameStateTypes.Running;
        /// <summary>
        /// Container object activated on open , deactivated on close
        /// </summary>
        [SerializeField]
        protected GameObject m_container;
        /// <summary>
        /// Canvas group used to perform farde (must be on container stored in m_container)
        /// </summary>
        protected CanvasGroup canvasGroup;
        /// <summary>
        /// Dotween animation used (OPTIONAL)
        /// </summary>
        [SerializeField]
        protected DOTweenAnimation m_panelAnimation;
        /// <summary>
        /// Current event system
        /// </summary>
        private MyEventSystem m_eventSystem;
        /// <summary>
        /// Stored routine for autoHiding
        /// </summary>
        private Coroutine autoHideRoutine = null;
        #endregion

        #region PROPERTIES
        public bool IsOpened { get { return m_container.activeSelf && canvasGroup.alpha > 0; } }
        #endregion

        #region EVENTS
        [SerializeField]
        private UnityEvent panelOpenedEvt;
        [SerializeField]
        private UnityEvent panelClosedEvt;
        #endregion

        #region MONHOBEHAVIOR METHODS

        protected virtual void Awake()
        {
            //Retrive references
            if (m_container == null) m_container = transform.Find("Container")?.gameObject;
            canvasGroup = m_container.GetComponent<CanvasGroup>();
            if (canvasGroup == null) canvasGroup = m_container.AddComponent<CanvasGroup>();

            
        }
        protected virtual void Start()
        {
            //Get EventSystem
            m_eventSystem = EventSystem.current as MyEventSystem;
            //Event subscription
            EventManager.StartListening(m_openEvent, OpenPanel);
            EventManager.StartListening(m_closeEvent, ClosePanel);
            //Event subscription only in case of dotween Animation
            if(m_panelAnimation != null) { 
                m_panelAnimation.onComplete.AddListener(OnPanelOpened);
                m_panelAnimation.onRewind.AddListener(OnPanelClosed);
            }

            if (m_firstSelectedObject != null && (IsOpened && setFirstSelectedAlsoIfAlreadyEnabled)) {
                Debug.Log("SETTO FIRSTSELECTED = " + m_firstSelectedObject);
                m_eventSystem.SetSelectedGameObject(m_firstSelectedObject);
            }

            if (openOnStart)
                OpenPanel();
            if (closeOnStart)
                ClosePanel();
        }

        protected virtual void OnDisable()
        {
            // event unsubscription
            EventManager.StopListening(m_openEvent, OpenPanel);
            EventManager.StartListening(m_closeEvent, ClosePanel);
            //Event unsubscription only in case of dotween Animation
            if (m_panelAnimation != null)
            {
                m_panelAnimation.onComplete.RemoveListener(OnPanelOpened);
                m_panelAnimation.onRewind.RemoveListener(OnPanelClosed);
            }
        }

        #endregion

        #region OPEN PANEL SEQUENCE
   
        /// <summary>
        /// Open the panel
        /// </summary>
        public virtual void OpenPanel()
        {
            if (!IsOpened) { 
                //Stop the autoHide routine if is running
                if(autoHideRoutine != null) StopCoroutine(autoHideRoutine);
                // Store current gameState and change gameState (all performed only if required)
                if (m_changeGameState)
                {
                    m_previousGameState = GameState.CurrentGameState;
                    GameState.CurrentGameState = m_newGameState;
                }
                //Enable Container
                m_container.SetActive(true);
                //Perform animation if required
                if (m_panelAnimation != null)
                {
                    m_panelAnimation.DOPlayForward();
                }
                //Open panel without animation if animation is not set
                else
                {
                    canvasGroup.alpha = 1;
                    OnPanelOpened();
                }
                //Stop timescale if required
                if (m_stopTimeScale)
                    Time.timeScale = 0;
            }
        }
        /// <summary>
        /// Called After panel is opened
        /// </summary>
        public virtual void OnPanelOpened()
        {
            //Check for eventSystem if not already set
            if(m_eventSystem == null)
                m_eventSystem = EventSystem.current as MyEventSystem;
            //Set selected Object if set on m_firstSelectedObject
            if (m_firstSelectedObject != null) { 
                m_eventSystem.SetSelectedGameObject(m_firstSelectedObject);
                Debug.Log("ONPANEL OPENED SETTO SELECTED GAMEOBJECT = " + m_firstSelectedObject);
            }
            if (autoHide)
                autoHideRoutine = StartCoroutine(CloseDelayed(autoHideTime));
            panelOpenedEvt?.Invoke();
        }
        #endregion

        #region CLOSE PANEL SEQUENCE
        /// <summary>
        /// Close the panel
        /// </summary>
        public virtual void ClosePanel()
        {
            if (IsOpened) { 
                // Perform Animation (Bakwards) if required
                if (m_panelAnimation != null)
                {
                    m_panelAnimation.DOPlayBackwards();
                }
                //Close panel without animation if animation is not set
                else
                {
                    canvasGroup.alpha = 0;
                    OnPanelClosed();
                }
            }
        }
        /// <summary>
        /// Called After panel is closed
        /// </summary>
        public virtual void OnPanelClosed()
        {
            //Disable container gameobject
            m_container.SetActive(false);
            //Restore previous gameState if changed
            if (m_changeGameState)
                GameState.CurrentGameState = m_previousGameState;
            //Restore timeScale if changed
            if (m_stopTimeScale)
                Time.timeScale = 1;

            panelClosedEvt?.Invoke();
        }
        #endregion

        #region COOROUTINES
        /// <summary>
        /// Close panel after specific time
        /// </summary>
        /// <param name="delay">time before close the panel</param>
        /// <returns></returns>
        protected IEnumerator CloseDelayed(float delay)
        {
            yield return new WaitForSeconds(delay);
            ClosePanel();
            yield return null;
        }
        #endregion

        #region INSPECTOR UTILITIES
        [ContextMenu("Open Panel")][Button]
        public  void OpenPanelFromInspector()
        {
            m_container.SetActive(true);
            m_container.GetComponent<CanvasGroup>().alpha = 1;
            if(m_changeGameState)
                GameState.CurrentGameState = m_previousGameState;
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }

        [ContextMenu("Close Panel")][Button]
        public void ClosePanelFromInspector()
        {
            m_container.GetComponent<CanvasGroup>().alpha = 0;
            m_container.SetActive(false);
            if (m_changeGameState)
                GameState.CurrentGameState = m_previousGameState;
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }
        #endregion
    }
}