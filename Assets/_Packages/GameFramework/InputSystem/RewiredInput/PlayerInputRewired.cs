﻿using Rewired;
using UnityEngine;
using GameFramework;

public class PlayerInputRewired : MonoBehaviour
{
    #region VARIABLES
    [SerializeField]
    private int m_rewiredPlayerId = 0;
    [SerializeField]
    [BitMask(typeof(GameStateTypes))]
    private GameStateTypes m_alowedGameStates;
    [SerializeField]
    private bool m_isEnabled = true;

    Player m_rewiredPlayer;
    #endregion

    #region PROPERTIES
    public int RewiredPlayerId
    {
        get { return m_rewiredPlayerId; }
        private set {
            m_rewiredPlayerId = value;
            
            m_rewiredPlayer = ReInput.players.GetPlayer(m_rewiredPlayerId);
        }
    }
    public bool IsEnabled
    {
        get { return m_isEnabled; }
        set
        {
            m_isEnabled = value;
        }
    }
    #endregion

    #region MONOBEHAVIOURS METHODS
    void Start()
    {
        m_rewiredPlayer = ReInput.players.GetPlayer(m_rewiredPlayerId);
    }
    #endregion

    #region METHODS
    #region GENERIC
    /// <summary>
    /// USE THIS TO ASSIGN INPUTS TO A NEW PLAYER
    /// </summary>
    /// <param name="playerId">Player id to use</param>
    public void AssignRewiredPlayer(int rewiredPlayerId)
    {
        RewiredPlayerId = rewiredPlayerId;
    }
    public bool CanExecute()
    {
        return m_isEnabled && GameState.IsStateRunning(m_alowedGameStates);
    }
    #endregion
    #region AXIS
    public float GetAxis(int actionID)
    {
        return (CanExecute() ? m_rewiredPlayer.GetAxis(actionID) :0);
    }
    public float GetAxis(string actionName)
    {
        return (CanExecute() ? m_rewiredPlayer.GetAxis(actionName) : 0);
    }
    public float GetAxisRaw(int actionID)
    {
        return (CanExecute() ? m_rewiredPlayer.GetAxisRaw(actionID) : 0);
    }
    public float GetAxisRaw(string actionName)
    {
        return (CanExecute() ? m_rewiredPlayer.GetAxisRaw(actionName) : 0);
    }
    #endregion
    #region BUTTONS
    public bool GetButtonDown(int actionID)
    {
        return (CanExecute() ? m_rewiredPlayer.GetButtonDown(actionID) : false);
    }
    public bool GetButtonDown(string actionName)
    {
        return (CanExecute() ? m_rewiredPlayer.GetButtonDown(actionName) : false);
    }
    public bool GetButton(int actionID)
    {
        return (CanExecute() ? m_rewiredPlayer.GetButton(actionID) : false);
    }
    public bool GetButton(string actionName)
    {
        return (CanExecute() ? m_rewiredPlayer.GetButton(actionName) : false);
    }
    public bool GetButtonUp(int actionID)
    {
        return (CanExecute() ? m_rewiredPlayer.GetButtonUp(actionID) : false);
    }
    public bool GetButtonUp(string actionName)
    {
        return (CanExecute() ? m_rewiredPlayer.GetButtonUp(actionName) : false);
    }
    #endregion
    #endregion
}
