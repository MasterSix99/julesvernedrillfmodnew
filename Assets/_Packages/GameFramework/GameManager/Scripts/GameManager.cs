﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameFramework;

public class GameManager : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// Current gameMode
    /// </summary>
    [Header("Game Mode Definition")] [SerializeField]
    private GameMode m_gameMode;           
    /// <summary>
    /// Variable to Check if Game Mode is active and enabled 
    /// </summary>
    private bool m_gameModeEnabled;

    [Header("Game State Definition")]                    
    [SerializeField]
    /// <summary>
    /// Defining which state is designated to be the "Game Is Running" 
    /// </summary>
    private GameStateTypes  m_runningGameState = GameStateTypes.Running;



    #endregion

    

    #region MonoBehviour

    

    public void Awake()
    {
        /// Check if there is a GameMode, if not print a
        /// Warning otherwise create a copy the GameMode.
        if (m_gameMode == null)
        {
            Debug.LogWarning("[GameManager](Awake): No GameMode for this game");
        }
        else
        {
            m_gameMode = Instantiate(m_gameMode);
        }
    }

    public void OnEnable()
    {
        /// Start the GameMode
        RestartGameMode();
        Invoke("SetGameRunning", 1f);
    }

    public void OnDisable()
    {
        /// Stop the GameMode
        if (m_gameMode != null)
        {
            m_gameModeEnabled = false;
            m_gameMode.Disable();
        }
    }

    public void Update()
    {

        if (Input.GetKeyDown(KeyCode.X))
            Debug.Log(MyEventSystem.current.currentSelectedGameObject);
        if (!GameState.IsStateRunning(m_runningGameState))
            return;

        /// Update GameMode Status
        UpdateGameMode();
    }
    #endregion

    #region Methods
    public void SetGameRunning()
    {
        GameState.CurrentGameState = GameStateTypes.Running;
    }
    /// <summary>
    /// Change the current GameMode 
    /// </summary>
    /// <param name="newGameMode">New GameMode to set</param>
    public void ChangeGameMode(GameMode newGameMode)
    {
        if(m_gameMode != null)
            m_gameMode.Disable();

        m_gameMode              = Instantiate(newGameMode);
        m_gameModeEnabled       = true;
    }

    /// <summary>
    /// Restart current GameMode
    /// </summary>
    public void RestartGameMode()
    {
        if(m_gameMode == null)
            return;

        m_gameMode.Enable();
        m_gameModeEnabled       = true;
    }

    /// <summary>
    /// Update GameModes and analyze the status.
    /// </summary>
    private void UpdateGameMode()
    {
        m_gameMode.UpdateGameModeState();
    }
    #endregion

    
}
