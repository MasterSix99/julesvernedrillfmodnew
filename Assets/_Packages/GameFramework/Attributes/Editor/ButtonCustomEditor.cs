﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;
namespace GameFramework { 
[CustomEditor(typeof(MonoBehaviour),editorForChildClasses:true,isFallback =true)]
    class ButtonCustomEditor : Editor
    {

        MethodInfo[] objectMethods;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var myScript = target;
            Type monoType = target.GetType();

            // Retreive the fields from the mono instance
            objectMethods = monoType.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Instance | BindingFlags.OptionalParamBinding);

            GUILayout.Space(20);
            // search all fields and find the attribute [Position]
            for (int i = 0; i < objectMethods.Length; i++)
            {
                ButtonAttribute attribute = Attribute.GetCustomAttribute(objectMethods[i], typeof(ButtonAttribute)) as ButtonAttribute;
                // if we detect any attribute print out the data.
                if (attribute != null)
                {
                    
                    if (GUILayout.Button(objectMethods[i].Name))
                    {
                        var methodInfo = monoType.GetMethod(objectMethods[i].Name);
                        objectMethods[i].Invoke(myScript, new object[0]);
                    }
                }
            }

        }
    }
}