﻿using System;
using UnityEngine;
namespace GameFramework
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class ButtonAttribute : PropertyAttribute
    {
        public ButtonAttribute()
        {
        }
    }
}
