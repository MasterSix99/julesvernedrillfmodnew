﻿
using System.IO;
using UnityEngine;


public enum SaveModes
{
    Json,
    Binary
}

public static class FileHelper
{

    private static string defSavePath = Application.persistentDataPath;
    private static string defFileName = "gameSav.sav";
    private static string DefCompleteSavePath { get { return string.Concat(defSavePath, "/", defFileName); } }


    public static bool FileExist(string filename = null, string filePath = null)
    {
        string path = GetFullpath(filename, filePath);
        Debug.Log(path);
        return File.Exists(path);
    }

    public static void SaveData<T>(T data, string filename = null, string filePath = null, SaveModes saveMode = SaveModes.Binary)
    {
        string savePath = GetFullpath(filename, filePath);
        Debug.Log(savePath);
        switch (saveMode)
        {
            case SaveModes.Json:
                SaveToJsonFile(data, savePath);
                break;
            case SaveModes.Binary:
                SaveToBinaryFile(data, savePath);
                break;
        }

    }




    public static void SaveToJsonFile<T>(T data, string filename = null, string filePath = null)
    {
        string savePath = GetFullpath(filename, filePath);
        System.IO.File.WriteAllText(savePath, JsonUtility.ToJson(data, true));
    }

    private static void SaveToBinaryFile<T>(T data, string savePath)
    {
        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        System.IO.FileStream stream = new System.IO.FileStream(savePath, FileMode.Create);
        bf.Serialize(stream, data);
        stream.Close();
    }




    public static void LoadData<T>(out T data, string filename = null, string filePath = null, SaveModes loadMode = SaveModes.Binary)
    {
        data = default(T);
        string loadPath = GetFullpath(filename, filePath);
        Debug.Log(loadPath);
        switch (loadMode)
        {
            case SaveModes.Json:
                LoadFromJsonFile(ref data, loadPath);
                break;
            case SaveModes.Binary:
                LoadFromBinaryFile(out data, loadPath);
                break;
        }


    }


    public static void LoadFromJsonFile<T>(ref T data, string filename = null, string filePath = null)
    {
        string savePath = GetFullpath(filename, filePath);
        if (!System.IO.File.Exists(savePath))
            SaveToJsonFile<T>(default(T), savePath);

        string jsonTxt = System.IO.File.ReadAllText(savePath);
        T json = JsonUtility.FromJson<T>(jsonTxt);
        JsonUtility.FromJsonOverwrite(jsonTxt, data);
        //data = json;
    }

    private static void LoadFromBinaryFile<T>(out T data, string loadPath)
    {
        if (File.Exists(loadPath))
        {
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            FileStream stream = new FileStream(loadPath, FileMode.Open);

            T ret = (T)bf.Deserialize(stream);
            stream.Close();
            data = ret;
            return;
        }
        else
        {
            Debug.LogError("File not exist");
        }
        data = default(T);
    }





    public static void DeleteData(string filename = null, string filePath = null)
    {

        System.IO.File.Delete(GetFullpath(filename, filePath));

    }

    private static string GetFullpath(string filename = null, string filePath = null)
    {

        if (string.IsNullOrEmpty(filename) && string.IsNullOrEmpty(filePath))
            return FileHelper.DefCompleteSavePath;
        else
        {
            string savePath = !string.IsNullOrEmpty(filePath) ? filePath : FileHelper.defSavePath;
            string saveName = !string.IsNullOrEmpty(filename) ? filename : defFileName;
            string completePath = string.Concat(savePath, "/", saveName);
            return completePath;
        }
    }

}
