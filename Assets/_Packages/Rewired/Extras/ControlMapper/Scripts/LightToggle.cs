﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightToggle : MonoBehaviour
{
    [SerializeField]
    private GameObject mainLight;
    [SerializeField]
    private GameObject startLight;
    [SerializeField]
    private AnimationCurve lightAnim;
    private float animTime = 0;
    [SerializeField]
    private float duration = 1;
    [SerializeField]
    private float lightIntensity = 15;
    private Light lightComponent;


    private void OnEnable()
    {
        EventManager.StartListening(EventsID.CHANGELIGHTS, ToggleLights);
    }

    private void Start()
    {
        lightComponent = mainLight.GetComponent<Light>();
    }

    void ToggleLights()
    {
        StartCoroutine(BetterLightFlicker());
    }

    IEnumerator BetterLightFlicker()
    {
        float eval;
        while(animTime < 1)
        {
            animTime += Time.deltaTime/duration;
            eval = lightAnim.Evaluate(animTime);
            lightComponent.intensity = eval * lightIntensity;
            yield return null;
        }
        yield return null;
    }

}
