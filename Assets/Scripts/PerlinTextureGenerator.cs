﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PerlinMode
{
    Tan,
    Sin,
    Sqrt,
    Default
}

public class PerlinTextureGenerator : MonoBehaviour
{
    public Vector2Int dimensions = new Vector2Int(256, 256);

    public float scale = 1;
    public float power;
    public Vector2Int offset;

    public PerlinMode perlinMode;

    private MeshRenderer renderer;

    private void Awake()
    {
        renderer = GetComponent<MeshRenderer>();
    }

    public void Update()
    {
        renderer.material.mainTexture = GenerateTexture();
    }

    public Texture2D GenerateTexture()
    {
        Texture2D finaltexture = new Texture2D(dimensions.x, dimensions.y);

        int x, y;
        for (y = 0; y < dimensions.y; y++)
        {
            for (x = 0; x < dimensions.x; x++)
            {
                finaltexture.SetPixel(x, y, GeneratePerlinColor(x, y));
            }
        }
        finaltexture.Apply();

        return finaltexture;
    }

    public Color GeneratePerlinColor(int x, int y)
    {
        Color finalColor;

        float perlin_x;
        float perlin_y;

        switch (perlinMode)
        {
            case PerlinMode.Tan:
                perlin_x = Mathf.Tan((float)x / dimensions.x * scale + offset.x);
                perlin_y = Mathf.Tan((float)y / dimensions.y * scale + offset.y);
                break;
            case PerlinMode.Sin:
                perlin_x = Mathf.Sin((float)x / dimensions.x * scale + offset.x);
                perlin_y = Mathf.Sin((float)y / dimensions.y * scale + offset.y);
                break;
            case PerlinMode.Sqrt:
                perlin_x = Mathf.Sqrt((float)x / dimensions.x * scale + offset.x);
                perlin_y = Mathf.Sqrt((float)y / dimensions.y * scale + offset.y);
                break;
            case PerlinMode.Default:
                perlin_x = (float)x / dimensions.x * scale + offset.x;
                perlin_y = (float)y / dimensions.y * scale + offset.y;
                break;
            default:
                perlin_x = (float)x / dimensions.x * scale + offset.x;
                perlin_y = (float)y / dimensions.y * scale + offset.y;
                break;
        }

        float perlin = Mathf.PerlinNoise(perlin_x, perlin_y);

        finalColor = new Color(perlin, perlin, perlin);

        return finalColor;
    }
}
