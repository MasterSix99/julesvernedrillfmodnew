﻿using System.Collections;
using UnityEditor;
using UnityEngine;
namespace GameFramework
{
    public class ScreenFader : MonoBehaviour
    {
        public enum FadeType
        {
            Black, Loading, GameOver,
        }


        protected static ScreenFader m_instance;

        public CanvasGroup faderCanvasGroup;
        public CanvasGroup loadingCanvasGroup;
        public CanvasGroup gameOverCanvasGroup;
        public float fadeDuration = 1f;

        [MinMax(0, 1, ShowEditRange = true)]
        public Vector2 val;


        protected bool m_IsFading;

        const int k_MaxSortingLayer = 32767;

        #region PROPERTIES
        public static ScreenFader Instance
        {
            get
            {
                if (m_instance != null)
                    return m_instance;

                m_instance = FindObjectOfType<ScreenFader>();

                if (m_instance != null)
                    return m_instance;

                Create();

                return m_instance;
            }
        }
        public static bool IsFading
        {
            get { return Instance.m_IsFading; }
        }
        #endregion

        void Awake()
        {
            if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (transform.parent != null)
                transform.parent = null;
            DontDestroyOnLoad(gameObject);
        }


        public static void Create()
        {
            /*GameObject screenFader =(GameObject)AssetDatabase.LoadAssetAtPath("Prefabs/SceneManagement/pref_ui_screenFader.prefab",typeof(GameObject)) ;

            GameObject obj = Instantiate(screenFader);
            m_instance = obj.GetComponent<ScreenFader>() ;*/
        }

        protected IEnumerator Fade(float finalAlpha, CanvasGroup canvasGroup)
        {
            //That variable make Fade Timescale Indipendet
            float timeScaleMultiplicator = Time.timeScale > 0 ? Time.deltaTime : Time.unscaledDeltaTime;
            m_IsFading = true;
            canvasGroup.blocksRaycasts = true;
            float fadeSpeed = Mathf.Abs(canvasGroup.alpha - finalAlpha) / fadeDuration;
            while (!Mathf.Approximately(canvasGroup.alpha, finalAlpha))
            {
                canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, finalAlpha,
                    fadeSpeed * timeScaleMultiplicator);
                yield return null;
            }
            canvasGroup.alpha = finalAlpha;
            m_IsFading = false;
            canvasGroup.blocksRaycasts = false;
        }

        public static void SetAlpha(float alpha)
        {
            Instance.faderCanvasGroup.alpha = alpha;
        }

        public static IEnumerator FadeSceneIn()
        {
            CanvasGroup canvasGroup;
            if (Instance.faderCanvasGroup.alpha > 0.1f)
                canvasGroup = Instance.faderCanvasGroup;
            else if (Instance.gameOverCanvasGroup.alpha > 0.1f)
                canvasGroup = Instance.gameOverCanvasGroup;
            else
                canvasGroup = Instance.loadingCanvasGroup;

            yield return Instance.StartCoroutine(Instance.Fade(0f, canvasGroup));

            canvasGroup.gameObject.SetActive(false);
        }

        public static IEnumerator FadeSceneOut(FadeType fadeType = FadeType.Black)
        {
            CanvasGroup canvasGroup;
            switch (fadeType)
            {
                case FadeType.Black:
                    canvasGroup = Instance.faderCanvasGroup;
                    break;
                case FadeType.GameOver:
                    canvasGroup = Instance.gameOverCanvasGroup;
                    break;
                default:
                    canvasGroup = Instance.loadingCanvasGroup;
                    break;
            }

            canvasGroup.gameObject.SetActive(true);

            yield return Instance.StartCoroutine(Instance.Fade(1f, canvasGroup));
        }


        #region INSPECTOR UTILITIES
        [ContextMenu("FADE IN")]
        [Button]
        public void OpenFromInspector()
        {
            loadingCanvasGroup.gameObject.SetActive(true);
            loadingCanvasGroup.alpha = 1;

#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
        #endif
        }

        [ContextMenu("FADE OUT")]
        [Button]
        public void CloseFromInspector()
        {
            loadingCanvasGroup.gameObject.SetActive(false);
            loadingCanvasGroup.alpha = 0;
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
        #endif
        }
        #endregion

    }

}