﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameFramework
{
    public class SceneController : MonoBehaviour
    {
        public static SceneController Instance
        {
            get
            {
                if (instance != null)
                    return instance;

                instance = FindObjectOfType<SceneController>();

                if (instance != null)
                    return instance;

                Create();

                return instance;
            }
        }

        public static bool Transitioning
        {
            get { return Instance.m_Transitioning; }
        }

        protected static SceneController instance;

        public static SceneController Create()
        {
            GameObject sceneControllerGameObject = new GameObject("SceneController");
            instance = sceneControllerGameObject.AddComponent<SceneController>();

            return instance;
        }

        protected Scene m_CurrentZoneScene;
        protected bool m_Transitioning;

        void Awake()
        {
            if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (transform.parent != null)
                transform.parent = null;
            DontDestroyOnLoad(gameObject);

            ScreenFader.SetAlpha(1f);
            StartCoroutine(ScreenFader.FadeSceneIn());
            m_CurrentZoneScene = SceneManager.GetActiveScene();
        }

        public static void ReloadScene()
        {
            Instance.StartCoroutine(Instance.Transition(Instance.m_CurrentZoneScene.name));
        }

        public static void ReloadSceneWithDelay(float delay)
        {
            Instance.StartCoroutine(CallWithDelay(delay, ReloadScene));
        }

        public static void LoadScene(string sceneName)
        {
            Instance.StartCoroutine(Instance.Transition(sceneName));
        }


        protected IEnumerator Transition(string newSceneName)
        {
            m_Transitioning = true;
            //PersistentDataManager.SaveAllData();
            GameState.CurrentGameState = GameFramework.GameStateTypes.OnSceneTransition;
            
            yield return StartCoroutine(ScreenFader.FadeSceneOut(ScreenFader.FadeType.Loading));
            //PersistentDataManager.ClearPersisters();
            yield return SceneManager.LoadSceneAsync(newSceneName);
            //PersistentDataManager.LoadAllData();
           
            yield return StartCoroutine(ScreenFader.FadeSceneIn());
            Time.timeScale = 1;
            m_Transitioning = false;
        }

        


        static IEnumerator CallWithDelay(float delay, System.Action call)
        {
            yield return new WaitForSeconds(delay);
            call();
        }
    }
}