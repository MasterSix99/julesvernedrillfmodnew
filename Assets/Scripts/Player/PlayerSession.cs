﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerSession
{
    #region VARIABLES

    
   
    [SerializeField]
    private int m_playerId = 0;
    private string m_playerName = "";
    private GameObject m_playerInstance; 
    private int m_rewiredPlayerId = 0;
    private bool m_isEnabled = false;
    
    #endregion

    #region PROPERTIES

    public int PlayerID
    {
        get { return m_playerId; }
    }
    public string PlayerName
    {
        get { return m_playerName; }
    }
    public bool IsEnabled
    {
        get { return m_isEnabled; }
        set
        {
            m_isEnabled = value;
        }
    }
    public GameObject PlayerInstance
    {
        get { return m_playerInstance; }
    }
    public int RewiredPlayerId
    {
        get { return m_rewiredPlayerId; }
        set { m_rewiredPlayerId = value; }
    }


    #endregion

    #region Constructor

    public PlayerSession()
    {
        if (string.IsNullOrEmpty(PlayerName)) m_playerName = "Player " + m_playerId;
    }
    public PlayerSession(int id)
    {
        m_playerId = id;
    }
    public PlayerSession(int id, bool enabled)
    {
        m_playerId = id;
        if (string.IsNullOrEmpty(PlayerName)) m_playerName = "Player " + m_playerId;
        IsEnabled = enabled;
    }
    #endregion

    #region METHODS
    #endregion

}
