﻿using UnityEngine;
using GameFramework;
using System;

[RequireComponent(typeof(ContactChecker2D),typeof(Rigidbody2D), typeof(Collider2D))]
public class MovementBehaviour2D : MonoBehaviour
{

    /*    
     *    [SerializeField][ReadOnly]
    private EntityFloatStat jumpHeightModifier;
    [SerializeField] [ReadOnly]
    private EntityFloatStat scaleModifier;
    [SerializeField][ReadOnly]
    private EntityFloatStat speedModifier;
    [SerializeField][ReadOnly]
    private EntityFloatStat throwModifier;
    [SerializeField][ReadOnly]
    private EntityFloatStat repairTimeModifier;
    */
    #region VARIABLES
    //NEEDED COMPONENTS
    /// <summary>
    /// Reference Rigidbody
    /// </summary>
    [SerializeField]
    protected Rigidbody2D m_rb;
    /// <summary>
    /// Reference collider
    /// </summary>
    [SerializeField]
    protected Collider2D m_collider;
    /// <summary>
    /// ContactChecker is used to check if player is grounded or near walls
    /// </summary>
    protected ContactChecker2D m_contactChecker;
    /// <summary>
    /// Object to flip when move
    /// </summary>
    [Tooltip("Object that scale is inverted to flip character when move right left")] [SerializeField]
    protected GameObject flipObject;
    //MOVEMENT  PARAMETERS
    /// <summary>
    /// movement speed of Gameobject
    /// </summary>
    [Space][Header("MOVEMENT PARAMETERS")][SerializeField]
    protected EntityFloatStat m_speed;
    /// <summary>
    /// speed smoothing
    /// </summary>
    //[SerializeField]
    //protected float m_MovementSmoothing =0.05f;
    /// <summary>
    /// Max movement velocity
    /// </summary>
    [SerializeField]
    protected EntityFloatStat m_maxVelocityX;
    /// <summary>
    /// Gameobject inertial speed decrease
    /// </summary>
    [SerializeField]
    protected float movementInertialDecrease = 5f;
    //JUMP  PARAMETERS
    /// <summary>
    /// Jump force
    /// </summary>
    [Space][Header("JUMP PARAMETERS")][SerializeField]
    protected EntityFloatStat m_jumpForce;
    /// <summary>
    /// Used to check if GameObject can perform a doubleJump
    /// </summary>
    [SerializeField]
    protected EntityBoolStat m_canDoubleJump;
    /// <summary>
    /// Determine max jump duration (then GameObject start to fall)
    /// </summary>
    [SerializeField]
    protected EntityFloatStat m_maxJumpDuration;
    /// <summary>
    /// used to check if jump input is pressed
    /// </summary>
    protected bool m_jumpPressed = false;
    /// <summary>
    /// Used to check jump duration
    /// </summary>
    protected float m_jumpTimer = 0;
    /// <summary>
    /// Ground layers
    /// </summary>
    [SerializeField]
    protected LayerMask m_groundMask;
    /// <summary>
    /// Wall layers 
    /// </summary>
    [SerializeField]
    protected LayerMask m_sideHitMask;
    
    //OTHER JUMP TUNING PARAMETERS
    /// <summary>
    /// Used to determine if gravity force must be applied to player 
    /// </summary>
    [Space][Header("OTHER PARAMETERS")][SerializeField]
    protected bool m_applyForceFromTop = true;
    /// <summary>
    /// Value of gravity force applied 
    /// </summary>
    [ShowIf("m_applyForceFromTop", true)] [SerializeField]
    protected EntityFloatStat m_gravityForceModifier;
    //INTERNAL PARAMTETERS
    /// <summary>
    /// Used to check if player si facing on right or left
    /// </summary>
    protected bool m_isFacingRight = false;
    /// <summary>
    /// Used to check if the player can jump
    /// </summary>
    protected bool m_canJump = true;
    /// <summary>
    /// Used to check if the player can Move
    /// </summary>
    protected bool m_canMove = true;
    /// <summary>
    /// Used to Check if movement and jump are locked or not
    /// </summary>
    [SerializeField]
    protected bool m_isFreezed = false;
    /// <summary>
    /// Determine if player is grounded
    /// </summary>
    protected bool m_isGrounded = true;
    /// <summary>
    /// if m_canDoubleJump is true this variable is used to check if doublejump is currently available
    /// </summary>
    protected bool m_hasDoubleJump = false;
    /// <summary>
    /// Used to check if movement must be updated
    /// </summary>
    protected bool m_updateMovement = false;
    /// <summary>
    /// Used to check if GameObject is moving
    /// </summary>
    protected bool m_isMoving = false;
    /// <summary>
    /// Current GameObject velocity
    /// </summary>
    protected Vector2 m_velocity = Vector2.zero;
    /// <summary>
    /// Tmp variable to store initial GameObject mass 
    /// </summary>
    protected float storedMass;
    [Space]
    [Header("AUDIO")]
    [SerializeField]
    private GenericEventMonoParameter jumpAudio;
    [SerializeField]
    private OneShotAudio jumpVOAudio;

    private RigidbodyConstraints2D initialConstraints;
    #endregion

    #region EVENTS
    public Action onFreezeEvt;
    public Action onStopFreezeEvt;
    public Action onStartMovingEvt;
    public Action onStopMovingEvt;
    public Action<bool> onIsGroundedChangeEvt;
    #endregion

    #region PROPERTIES
    /// <summary>
    /// Reference Rigidbody
    /// </summary>
    public Rigidbody2D Rb
    {
        get { return m_rb; }
    }
    /// <summary>
    /// Movement speed
    /// </summary>
    public EntityFloatStat Speed
    {
        get { return m_speed; }
        set { m_speed = value; }
    }
    public EntityFloatStat MaxVelocityX
    {
        get { return m_maxVelocityX; }
        set { m_maxVelocityX = value; }
    }
    /// <summary>
    /// Jump Force used
    /// </summary>
    public EntityFloatStat JumpForce
    {
        get { return m_jumpForce; }
        set { m_jumpForce = value; }
    }
    public EntityFloatStat MaxJumpDuration
    {
        get { return m_maxJumpDuration; }
        set { m_maxJumpDuration = value; }
    }
    public EntityFloatStat GravityForceModifier
    {
        get { return m_gravityForceModifier; }
        set { m_gravityForceModifier = value; }
    }
    public EntityBoolStat CanDoubleJump
    {
        get { return m_canDoubleJump; }
        set { m_canDoubleJump = value; }
    }

    
       
    /// <summary>
    /// Used to check if player si facing on right or left
    /// </summary>
    public bool IsFacingRight
    {
        get { return m_isFacingRight; }
        set { m_isFacingRight = value; }
    }
    /// <summary>
    /// Used to check if the player can jump
    /// </summary>
    public bool CanJump
    {
        get {
            return (!IsFreezed && (m_isGrounded || (m_canDoubleJump.GetValue() && m_hasDoubleJump)));
        }
    }
    /// <summary>
    /// Used to check if the player can Move
    /// </summary>
    public bool CanMove
    {
        get
        {
            return m_canMove && !IsFreezed;
        }
        set
        {
            m_canMove = value;
        }
    }
    /// <summary>
    /// Used to Check if movement and jump are locked or not
    /// </summary>
    public bool IsFreezed
    {
        get
        {
            return !m_canMove && !m_canJump;
        }
    }
    /// <summary>
    /// Determine if player is grounded
    /// </summary>
    public bool IsGrounded
    {
        get
        {
            return m_isGrounded;
        }
        private set
        {
            if (m_isGrounded != value)
                OnIsGroundedChange(value);
            m_isGrounded = value;
        }
    }
    #endregion

    #region MONOBEHAVIOUR METHODS
    protected virtual void Awake()
    {
        //Get required components if not already sets
        if (m_rb == null)  m_rb = GetComponent<Rigidbody2D>();
        if (m_contactChecker == null)  m_contactChecker = GetComponent<ContactChecker2D>();
        if (m_collider == null) m_collider = GetComponent<Collider2D>();
        if (flipObject == null) flipObject = this.gameObject;
    }
    private void Start()
    {
        GenerateAudioEvents();
        initialConstraints = m_rb.constraints;
    }
    public void FixedUpdate()
    {

        //GROUND CHECK
        IsGrounded = m_contactChecker.IsGrounded(m_collider, m_groundMask,0.3f,0.05f,0.05f);

        //APPLY FORCE FROM TOP
        // If hightest point of jump is reached i will start apply de force from top
        if (!m_isGrounded && m_applyForceFromTop && (!m_jumpPressed || m_jumpTimer >= m_maxJumpDuration.GetValue())) {
            m_rb.AddForce(-transform.up * m_gravityForceModifier.GetValue());
        }
        if (!IsFreezed) { 
            // MOVEMENT SECTION
            if (m_updateMovement && CanMove)//Set velocity if movement is triggered
            {
                m_rb.velocity = m_velocity;
                m_updateMovement = false;
            }else if (!m_updateMovement) //Decrease velocity if no movement triggered
            {
                float newX = Mathf.Sign(m_rb.velocity.x) * Mathf.Clamp((Mathf.Abs(m_rb.velocity.x) - (movementInertialDecrease * Time.fixedDeltaTime)), 0, m_maxVelocityX.GetValue());
                m_rb.velocity = new Vector2(newX,m_rb.velocity.y);

                if (newX <= 0)
                {
                    m_isMoving = false;
                    onStopMovingEvt?.Invoke();
                }
            }


            //JUMP SECTION
            if (m_jumpPressed) {
                m_jumpTimer += Time.deltaTime;
                if(m_jumpTimer >= m_maxJumpDuration.GetValue())
                {
                    ReleaseJump();
                }
                else
                {
                    Rb.velocity = new Vector2(m_rb.velocity.x, m_jumpForce.GetValue() * Time.fixedDeltaTime);
                }
            
            }
        }
    }
    #endregion

    #region METHODS
    
    /// <summary>
    /// Move the object in a specific direction
    /// </summary>
    /// <param name="direction">Direction where to go and basic movement force</param>
    public void Move(float direction)
    {
        if (!CanMove)
            return;   

        if (!m_isMoving)
        {
            m_isMoving = true;
            onStartMovingEvt?.Invoke();
        }
        
        if ((direction > 0 && m_isFacingRight) || (direction < 0 && !m_isFacingRight))
            flip();

        if ((direction > 0 && !(m_contactChecker.CheckRightArea(m_collider, m_sideHitMask,0.2f ,.2f, .3f).Length>0)) || (direction < 0 && !(m_contactChecker.CheckLeftArea(m_collider, m_sideHitMask,0.2f, .2f, .3f).Length>0))) {

            m_velocity = new Vector2(Mathf.Clamp((direction * Speed.GetValue() * Time.fixedDeltaTime), -m_maxVelocityX.GetValue(), m_maxVelocityX.GetValue()), m_rb.velocity.y);
            m_updateMovement = true;
            
        }
    }
    /// <summary>
    /// FLIP THE SPRITE TO THE OPPOSITE SIDE
    /// </summary>
    public void flip()
    {
        m_isFacingRight = !m_isFacingRight;
        float new_x = -flipObject.transform.localScale.x;
        flipObject.transform.localScale = new Vector3(new_x, flipObject.transform.localScale.y, flipObject.transform.localScale.z);
    }
    /// <summary>
    /// Perform a jump using AddForce
    /// </summary>
    public void Jump()
    {
        if (CanJump) { 
            m_rb.AddForce(transform.up*m_jumpForce.GetValue());
            if (m_canDoubleJump.GetValue())
                m_hasDoubleJump = !m_hasDoubleJump;
            EventManager.TriggerEvent(EventsID.PLAYERJUMP);
        }
    }
    /// <summary>
    /// Enable jump action for the maxJumpDuration time max or until ReleaseJump is called
    /// </summary>
    public void StartJump()
    {
        if (CanJump)
        {
            //if (audioSource != null) { 
            //    audioSource.clip = jumpAudio;
            //    audioSource.Play();
            //}
            jumpAudio.ChangeParameter(1.00f);
            jumpAudio.PlayAudio(transform);
            jumpVOAudio.PlayAudio(transform.position);
            //FmodManager.instance.ChangeParameter(ref jumpAudio.eventParameter, 1.00f);
            //FmodManager.instance.AttachSfx(jumpAudio.fmodEvent, transform);
            //FmodManager.instance.StartEvent(jumpAudio);

            m_jumpPressed = true;
            if (m_canDoubleJump.GetValue())
                m_hasDoubleJump = !m_hasDoubleJump;
        }
    }
    /// <summary>
    /// Release jump called with StartJump
    /// </summary>
    public void ReleaseJump()
    {
        m_jumpPressed = false;
        m_jumpTimer = 0;
    }
    /// <summary>
    /// Called when IsGrounded status change
    /// </summary>
    /// <param name="newValue"></param>
    private void OnIsGroundedChange(bool newValue)
    {
        onIsGroundedChangeEvt?.Invoke(newValue);

        if (newValue)
        {
            jumpAudio.ChangeParameter(0.00f);
            jumpAudio.PlayAudio(transform);
            //FmodManager.instance.ChangeParameter(ref jumpAudio.eventParameter, 0.00f);
            //FmodManager.instance.AttachSfx(jumpAudio.fmodEvent, transform);
            //FmodManager.instance.StartEvent(jumpAudio);
        }
    }
    /// <summary>
    /// Generate Fmod Event Instances
    /// </summary>
    private void GenerateAudioEvents()
    {
        FmodManager.instance.CreateGenericMonoEventParameterInstance(ref jumpAudio);
    }
    #endregion

    #region LOCK/UNLOCK FUNCTIONALITIES
    public void Freeze()
    {
        m_rb.constraints = RigidbodyConstraints2D.FreezeAll;
        LockMovementAndJump();
        
    }
    public void UnFreeze()
    {
        m_rb.constraints = initialConstraints;
        UnlockMovementAndJump();
        //m_rb.velocity = Vector2.zero;
    }
    /// <summary>
    /// Player can't jump and move anymore
    /// </summary>
    public void LockMovementAndJump()
    {
        LockMovement();
        LockJump();
        m_isFreezed = true;
    }
    /// <summary>
    /// Player can jump and move now
    /// </summary>
    public void UnlockMovementAndJump()
    {
        UnlockMovement();
        UnlockJump();
        m_isFreezed = false;
    }
    /// <summary>
    /// Player stop to move and can't be moved using controls
    /// </summary>
    public void LockMovement()
    {
        Rb.velocity = new Vector2(0,Rb.velocity.y);
        m_canMove = false;
    }
    /// <summary>
    /// Player can move now
    /// </summary>
    public void UnlockMovement()
    {
        m_canMove = true;
    }
    /// <summary>
    /// Player cant jump anymore
    /// </summary>
    public void LockJump()
    {
        Rb.velocity = new Vector2(Rb.velocity.x, 0);
        m_canJump = false;
    }
    /// <summary>
    /// Player Can jump now
    /// </summary>
    public void UnlockJump()
    {
        m_canJump = true;
    }
    /// <summary>
    /// Use this to lock position and avoid movement based on collision with others element
    /// </summary>
    public void LockPlayerPosition()
    {
        storedMass = Rb.mass;
        Rb.mass = Mathf.Infinity;
    }
    /// <summary>
    /// Restore player mass to unlock movement
    /// </summary>
    public void UnlockPlayerPosition()
    {
        Rb.mass = storedMass;
    }
    #endregion

}
