﻿using GameFramework;
using UnityEngine;

[RequireComponent(typeof(MovementBehaviour2D), typeof(ContactChecker2D))]
public class CharacterController2D : MonoBehaviour
{
    #region VARIABLES
    //NEEDED COMPONENTS
    /// <summary>
    /// The input wrapper
    /// </summary>
    protected PlayerInputRewired m_playerInput;
    /// <summary>
    /// Movement Wrapper
    /// </summary>
    protected MovementBehaviour2D m_movement;
    /// <summary>
    /// Interactions Wrapper
    /// </summary>
    protected InteractionManager2D m_interactionManager;
    /// <summary>
    /// Repair Wrapper
    /// </summary>
    protected RepairBehaviour m_repairBehaviour;
    /// <summary>
    /// Aminator component
    /// </summary>
    [SerializeField] protected Animator m_animator;

    /// <summary>
    /// This layer is checked to show info
    /// </summary>
    [Space] [Header("Infobox Check Settings")] [SerializeField]
    private LayerMask infoLayer;
    /// <summary>
    /// Size of overlap used to check if a info is near
    /// </summary>
    [SerializeField]
    private float infoCheckSize = 2f;
    /// <summary>
    /// Info Check Debug color
    /// </summary>
    [SerializeField]
    private Color infoCheckColor = Color.yellow;
    /// <summary>
    /// Current info shown
    /// </summary>
    private InfoBoxTrigger2D infoOwned;
    //Character generic info
    /// <summary>
    /// The player id
    /// </summary>
    [SerializeField]
    private int m_playerID = 0;
    /// <summary>
    /// The rewired id of this player
    /// </summary>
    private int m_rewiredId = 0;

    //VIBRATION CONFIGS
    [SerializeField]
    private JoystickVibrationConfig interactVibrationConfig;
    [SerializeField]
    private JoystickVibrationConfig pickupVibrationConfig;
    [SerializeField]
    private JoystickVibrationConfig pickupReleaseVibrationConfig;

    //UPGRADE VARIABLES USED TO UPGRADE VALUES OF CHILD COMPONENTS BASED ON ACHIVMENT
    [Header("MODIFIER APPLIED ON START GAME(YOU MUST USE PROPERTIES FOR UPGRADE")]
    [SerializeField][ReadOnly]
    private EntityFloatStat m_speedModifier;
    [SerializeField][ReadOnly]
    private EntityFloatStat m_maxspeedModifier;
    [SerializeField] [ReadOnly]
    private EntityFloatStat m_jumpForceModifier;
    [SerializeField][ReadOnly]
    private EntityFloatStat m_jumpDurationModifier;
    [SerializeField][ReadOnly]
    private EntityFloatStat m_gravityForceModifier;
    [SerializeField] [ReadOnly]
    private EntityBoolStat m_doubleJumpModifier;
    [SerializeField] [ReadOnly]
    private EntityFloatStat m_scaleModifier;
    [SerializeField][ReadOnly]
    private EntityVector2Stat m_releaseForceModifier;
    [SerializeField][ReadOnly]
    private EntityVector2Stat m_throwForceModifier;
    [SerializeField][ReadOnly]
    private EntityFloatStat m_repairTimeModifier;

    #endregion

    #region PROPERTIES
    public int PlayerID
    {
        get { return m_playerID; }
        set
        {
            m_playerID = value;
        }
    }
    public int RewiredId
    {
        get { return m_rewiredId; }
        set
        {
            m_rewiredId = value;
            m_playerInput.AssignRewiredPlayer(m_rewiredId);
        }
    }
    public MovementBehaviour2D MovementBehaviour
    {
        get { return m_movement; }
    }
    public PlayerInputRewired PlayerInput { get { return m_playerInput; } }
    public Animator Animator { get { return m_animator; } }

    //UPGRADE PROPERTIES USED TO UPGRADE VALUES OF CHILD COMPONENTS BASED ON ACHIVMENT
    public EntityFloatStat SpeedModifier
    {
        get { return m_speedModifier; }
        set
        {
            m_speedModifier = value;
            m_movement.Speed.SumStat(m_speedModifier);
        }
    }
    public EntityFloatStat MaxspeedModifier
    {
        get { return m_maxspeedModifier; }
        set
        {
            m_maxspeedModifier = value;
            m_movement.MaxVelocityX.SumStat(m_speedModifier);
        }
    }
    public EntityFloatStat JumpForceModifier
    {
        get { return m_jumpForceModifier; }
        set
        {
            m_jumpForceModifier = value;
            m_movement.JumpForce.SumStat(m_jumpForceModifier);
        }
    }
    public EntityFloatStat JumpDurationModifier
    {
        get { return m_jumpDurationModifier; }
        set
        {
            m_jumpDurationModifier = value;
            m_movement.MaxJumpDuration.SumStat(m_jumpDurationModifier);
        }
    }
    public EntityFloatStat GravityForceModifier
    {
        get { return m_gravityForceModifier; }
        set
        {
            m_gravityForceModifier = value;
            m_movement.GravityForceModifier.SumStat(m_gravityForceModifier);
        }
    }
    public EntityBoolStat DoubleJumpModifier
    {
        get { return m_doubleJumpModifier; }
        set
        {
            m_doubleJumpModifier = value;
            m_movement.CanDoubleJump.SumStat(m_doubleJumpModifier);
        }
    }
    public EntityVector2Stat ReleaseForceModifier
    {
        get { return m_releaseForceModifier; }
        set
        {
            m_releaseForceModifier = value;
            m_interactionManager.ReleaseForce.SumStat(m_releaseForceModifier);
        }
    }
    public EntityVector2Stat ThrowForceModifier
    {
        get { return m_throwForceModifier; }
        set
        {
            m_throwForceModifier = value;
            m_interactionManager.ThrowForce.SumStat(m_throwForceModifier);
        }
    }
    public EntityFloatStat RepairTimeModifier {
        get { return m_repairTimeModifier; }
        set {
            m_repairTimeModifier = value;
            m_repairBehaviour.RepairTime.SumStat(m_repairTimeModifier);
        }
    }
    public EntityFloatStat ScaleModifier
    {
        get { return m_scaleModifier; }
        set
        {
            m_scaleModifier = value;
            transform.localScale *= m_scaleModifier.multiplier;
            //TODO IMPLEMENT SCALE MODIFY
        }
    }
    #endregion


    #region MONOBEHAVIOUR METHODS

    private void OnDrawGizmos()
    {
        Color oldColor = Gizmos.color;
        Gizmos.color = infoCheckColor;
        Gizmos.DrawWireSphere(transform.position, infoCheckSize);
        Gizmos.color = oldColor;
    }

    private void Awake()
    {
        //GET ALL NEDED COMPONETS IF ARE NOT ALREADY SET
        if (m_playerInput == null) m_playerInput = GetComponent<PlayerInputRewired>();
        if (m_movement == null) m_movement = GetComponent<MovementBehaviour2D>();
        if (m_interactionManager == null) m_interactionManager = GetComponent<InteractionManager2D>();
        if (m_repairBehaviour == null) m_repairBehaviour = GetComponent<RepairBehaviour>();
    }

    private void Start()
    {
        m_playerInput.AssignRewiredPlayer(RewiredId);
        EventManager.StartListening<InfoBoxRequest>(EventsID.INFOBOXREQUEST, OnInfoBoxRequest);

        if (m_animator != null)
            m_animator.SetBool("isGrounded", MovementBehaviour.IsGrounded);
    }

    private void OnEnable()
    {
        // SUBSCRIBE EVENTS
        MovementBehaviour.onStartMovingEvt += OnStartMoving;
        MovementBehaviour.onStopMovingEvt += OnStopMoving;
        MovementBehaviour.onIsGroundedChangeEvt += OnIsGroundedValueChange;
        m_interactionManager.onInteractEvt.AddListener(OnInteract);
        m_interactionManager.onInteractionReleaseEvt.AddListener(OnInteractionRelease);
        m_interactionManager.onPickupEvt.AddListener(OnPickup);
        m_interactionManager.onPickupReleaseEvt.AddListener(OnReleasePickup);
        m_repairBehaviour.onRepair += OnRepair;
        m_repairBehaviour.onStopRepair += OnStopRepair;
        GameState.OnGameStateChange += OnGameStateChanged;
    }

    private void OnDisable()
    {
        //UNSUBSCRIBE EVENTS
        MovementBehaviour.onStartMovingEvt -= OnStartMoving;
        MovementBehaviour.onStopMovingEvt -= OnStopMoving;
        MovementBehaviour.onIsGroundedChangeEvt -= OnIsGroundedValueChange;
        m_interactionManager.onInteractEvt.RemoveListener(OnInteract);
        m_interactionManager.onInteractionReleaseEvt.RemoveListener(OnInteractionRelease);
        m_interactionManager.onPickupEvt.RemoveListener(OnPickup);
        m_interactionManager.onPickupReleaseEvt.RemoveListener(OnReleasePickup);
        m_repairBehaviour.onRepair -= OnRepair;
        m_repairBehaviour.onStopRepair -= OnStopRepair;
        GameState.OnGameStateChange -= OnGameStateChanged;
        EventManager.StopListening<InfoBoxRequest>(EventsID.INFOBOXREQUEST, OnInfoBoxRequest);
    }

    void Update()
    {
        //CHECK AND PERFORM MOVEMENT 
        float movement = m_playerInput.GetAxisRaw("Move Horizontal");
        if (Mathf.Abs(movement) > .7)
        {
            if (m_movement.enabled)
                m_movement.Move(movement);
        }

        //CHECK JUMP INPUTS
        if (m_playerInput.GetButtonDown("Jump") && m_movement.enabled) { 
            m_movement.StartJump();
        }
        if (m_playerInput.GetButtonUp("Jump"))
            m_movement.ReleaseJump();

        // Check first if nearest machine need to be repaired and if true it will start repair it, else perform a interaction (if interactable is near)
        if (m_playerInput.GetButtonDown("Interact"))
        {
            m_repairBehaviour.FetchMachineToRepair();
            if (m_repairBehaviour.MachineToRepair != null && m_movement.IsGrounded)
            {
                m_repairBehaviour.Repair();
            }
            else { 
                m_interactionManager.Interact();
            }
        }
        // Interaction/Repair ButtonUp management
        if (m_playerInput.GetButtonUp("Interact"))
        {
            if (m_repairBehaviour.IsRepairing)
                m_repairBehaviour.StopRepair();
            else
            {
                if (m_interactionManager.CurrentInteractable != null && m_interactionManager.CurrentInteractable.InteractionInput.Equals(InteractionInputType.KeyHeld))
                    m_interactionManager.ReleaseInteraction();
            }
        }
        // Check pickup input
        if (m_playerInput.GetButtonDown("Pickup"))
        {
            m_interactionManager.Pickup();
        }
        // Check pickup release input (this will throw pickup )
        if (m_playerInput.GetButtonUp("Pickup"))
        {
            //Throw owned pickup if exist
            if (m_interactionManager.CurrentPickup != null && m_interactionManager.CurrentPickup.InteractionInput.Equals(InteractionInputType.KeyHeld))
                m_interactionManager.ReleasePickup(true);//release pickup and throw
        }
        // Check release input (this will release pickup normally)
        if (m_playerInput.GetButtonDown("Release"))
        {
            //Release owned pickup if exist
            m_interactionManager.ReleasePickup(false);//Release Pickup
        }


        if (m_playerInput.GetButtonDown("Pause")) { 
            //Trigger event that show pause menu
            EventManager.TriggerEvent(EventsID.OPENMENU);
            //Enable ui map for this player and disable for other players
            InputUtils.SetOnlySelectedPlayerMapEnabled(m_rewiredId, "UI");
        }

        if (m_playerInput.GetButtonDown("ShowInfo"))
        {
            //check nearest collider with infobox and show it or hide if an infobox is owned.
            ToggleShowInfo();
        }
            

    }
    #endregion


    #region MOVE AND JUMP UTILITIES
    private void OnStartMoving()
    {
        if (m_animator != null)  m_animator.SetBool("running", true);
    }
    private void OnStopMoving()
    {
        if (m_animator != null)
            m_animator.SetBool("running", false);
    }
    private void OnIsGroundedValueChange(bool newValue)
    {
        if (m_animator != null)
            m_animator.SetBool("isGrounded", newValue);
    }
    #endregion

    public void ChangeHandleGrabbed(bool newValue)
    {
        if (m_animator != null)
            m_animator.SetBool("grabbingHandle", newValue);
    }
    public bool IsDrivingAndTurning()
    {
        return Animator.GetBool("drivingTurning");
    }
    public void ChangeDrivingTurning(bool newValue)
    {
        if (m_animator != null)
            m_animator.SetBool("drivingTurning", newValue);
    }

    /// <summary>
    /// Called when Gamestate is changed
    /// </summary>
    /// <param name="currentGameState"></param>
    private void OnGameStateChanged(GameStateTypes currentGameState)
    {
        if (currentGameState.Equals(GameStateTypes.Running))
        {
            // Enable Gameplay map for all players
            InputUtils.SetPlayerMapStatus(m_rewiredId, "GameplayShared", true);
        }
        else
        {
            //Disable Gameplay map for all players
            InputUtils.SetPlayerMapStatus(m_rewiredId, "GameplayShared", false);
        }
    }


    #region INTERACTIONS MANAGEMENT
    /// <summary>
    /// Performed when interact with something
    /// </summary>
    private void OnInteract()
    {
        JoystickVibration.Vibrate(m_playerInput.RewiredPlayerId, interactVibrationConfig);
        if (m_animator != null) m_animator.SetBool("running", false);
    }
    /// <summary>
    /// Performed when interaction is released
    /// </summary>
    public void OnInteractionRelease()
    {

    }
    /// <summary>
    /// Performed when pickup something
    /// </summary>
    private void OnPickup()
    {
        JoystickVibration.Vibrate(m_playerInput.RewiredPlayerId, pickupVibrationConfig);
        
        if (m_animator != null)
        {
            m_animator.SetBool("picking", true);
        }
    }
    /// <summary>
    /// Performed when a pickup is released
    /// </summary>
    private void OnReleasePickup()
    {
        JoystickVibration.Vibrate(m_playerInput.RewiredPlayerId, pickupReleaseVibrationConfig);
        
        if (m_animator != null)
            m_animator.SetBool("picking", false);
        //TODO ADD PARTICLES or souds
    }
    /// <summary>
    /// Repair function wrapper (it call repair from RepairBehaviour)
    /// </summary>
    private void OnRepair()
    {
        m_movement.LockMovementAndJump();
        // Stop running animation to prevent bugs
        if (m_animator != null) m_animator.SetBool("running", false);
    }
    /// <summary>
    /// Performed when repair end
    /// </summary>
    private void OnStopRepair()
    {
        //Debug.Log("CALL ON STOP REPAIR");
        m_movement.UnlockMovementAndJump();
        if(m_repairBehaviour.IsRepairing)
            m_repairBehaviour.StopRepair();
    }
    #endregion

    #region INFOPOPUP MANAGEMENT
    /// <summary>
    /// Show or hide Infobox if some info is near the player
    /// </summary>
    private void ToggleShowInfo()
    {
        if (infoOwned == null)
            ShowInfo();
        else
            HideInfo();
    }
    /// <summary>
    /// Chekc if infoCollider is in range (infoCheckSize) and if true show infobox with Info set on the collider
    /// </summary>
    private void ShowInfo()
    {
        Collider2D infoCol = Physics2D.OverlapCircle(transform.position, infoCheckSize, infoLayer);
        if(infoCol != null)
        {
            InfoBoxTrigger2D infoTrigger = infoCol.GetComponent<InfoBoxTrigger2D>();
            if (infoTrigger != null)
            {
                
                infoTrigger.Trigger();
                infoOwned = infoTrigger;
            }
        }
    }
    /// <summary>
    /// Hide infoBox if owned from this class
    /// </summary>
    private void HideInfo()
    {
        if (infoOwned != null)
        {
            infoOwned.TriggerExit();
            infoOwned = null;
        }
    }
    //WHEN OTHER PLAYER TRIGGER INFOBOX THIS PLAYER MUST NOT OWN INFOBOX ANYMORE (TO AVOID PROBLEM ON TRIGGER AND RELEASE)
    private void OnInfoBoxRequest(InfoBoxRequest request)
    {
        infoOwned = null;
    }
    #endregion

}
