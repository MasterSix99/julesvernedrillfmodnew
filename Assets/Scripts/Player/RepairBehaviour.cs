﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RepairBehaviour : MonoBehaviour
{
    #region EVENTS
    public Action onRepair;
    public Action onStopRepair;
    #endregion

    #region VARIABLES
    /// <summary>
    /// Layers used to check if repairable object is near
    /// </summary>
    [SerializeField] private LayerMask m_LayersAllowed;
    /// <summary>
    /// Ui of repair bar
    /// </summary>
    [SerializeField] private Slider repairBar;
    /// <summary>
    /// Repair duration
    /// </summary>
    [SerializeField] private EntityFloatStat repairTime;
    /// <summary>
    /// Size of Overlap used to check if repairable is near
    /// </summary>
    [SerializeField] private float repairCheckerSize = 1f;
    /// <summary>
    /// Determine if a collision must stop repair
    /// </summary>
    [SerializeField] private bool stopRepairOnCollision = false;
    /// <summary>
    /// The Layers allowed to stop repair
    /// </summary>
    [ShowIf("stopRepairOnCollision",true)]
    [SerializeField] private LayerMask stopRepairCollisionLayerMask = ~0;
    /// <summary>
    /// Stored machine to repair
    /// </summary>
    private MachineBehaviour machineToRepair;
    /// <summary>
    /// Timer of repair
    /// </summary>
    private float repairTimer;
    /// <summary>
    /// Overlap collider buffer
    /// </summary>
    private Collider2D[] hitColliders = new Collider2D[20];
    /// <summary>
    /// Determine if script is currently repairing something or not
    /// </summary>
    private bool isRepairing = false;

    private MovementBehaviour2D m_movement;

    [Header("AUDIO")]
    [SerializeField] private GenericEventMonoParameter repairAudio;
    [SerializeField] private OneShotAudio repairCompleteAudio;

    [Header("AESTHETICS")]
    [SerializeField] private Transform repairFeedbackSpawnpoint;
    [SerializeField] private GameObject repairFeedback;

    #endregion

    #region PROPERTIES
    public MachineBehaviour MachineToRepair
    {
        get { return machineToRepair; }
    }
    public bool IsRepairing
    {
        get { return isRepairing; }
    }
    public EntityFloatStat RepairTime
    {
        get { return repairTime; }
        set { repairTime = value; }
    }
    #endregion

    #region MONOBEHAVIOURS 
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, repairCheckerSize);
    }
    private void Awake()
    {
        if (repairBar == null) repairBar = GetComponentInChildren<Slider>();
        if (m_movement == null)
            m_movement = GetComponent<MovementBehaviour2D>();
        FmodManager.instance.CreateGenericMonoEventParameterInstance(ref repairAudio);
    }
    private void Start()
    {
        if (repairBar != null) {
            repairBar.maxValue = repairTime.GetValue();
            repairBar.value = 0;
            repairBar.gameObject.SetActive(false);
        }
    }
    private void OnDestroy()
    {
        onRepair = null;
        onStopRepair = null;
    }
    #endregion

    /// <summary>
    /// Fetch nearest machine and if found Start repairing it (NB INSTEAD MachineBehaviour we must use a IRepairable Interface)
    /// </summary>
    public void Repair()
    {
        if(isRepairing)
            StopRepair();
        FetchMachineToRepair();
        if(!machineToRepair.IsRepairing)
            StartCoroutine("RepairBuffer");
    }

    /// <summary>
    /// Stop repairing if is repairing
    /// </summary>
    public void StopRepair()
    {
        Debug.Log("call stop repair");
        StopCoroutine("RepairBuffer");
        repairAudio.StopAudio(FMOD.Studio.STOP_MODE.IMMEDIATE);
        OnRepairEnd();
    }
    /// <summary>
    /// Called after StopRepair
    /// </summary>
    private void OnRepairEnd()
    {
        Debug.Log("call on repair end");
        isRepairing = false;
        if(machineToRepair != null)
            machineToRepair.IsRepairing = false;
        repairBar.gameObject.SetActive(false);
        m_movement.UnFreeze();
        onStopRepair?.Invoke();
        //OnRepairEnd();
    }

    /// <summary>
    /// Search near broken machine (Actually if two machine are founded the one that is take is the first of array not te one nearest)
    /// </summary>
    public void FetchMachineToRepair()
    {
        machineToRepair = null;
        int founded = Physics2D.OverlapCircleNonAlloc(transform.position, repairCheckerSize, hitColliders, m_LayersAllowed);
        int i = 0;
        if (founded > 0) { 
            MachineBehaviour machineChache;
            foreach (Collider2D col in hitColliders)
            {
                if(col != null) { 
                    machineChache = col.GetComponent<MachineBehaviour>();
                    if(machineChache == null) machineChache = col.GetComponentInParent<MachineBehaviour>();
                    if (machineChache != null && machineChache.IsBroken)
                    {
                        machineToRepair = machineChache;
                        break;
                    }
                }
                i++;
            }
        }
    }

    /// <summary>
    /// Repair action
    /// </summary>
    /// <returns></returns>
    IEnumerator RepairBuffer()
    {
        if(machineToRepair != null && machineToRepair.IsBroken) {
            m_movement.Freeze();
            repairAudio.ChangeParameter(0f);
            repairAudio.PlayAudio(transform);
            isRepairing = true;
            machineToRepair.IsRepairing = true;
            repairTimer = 0;
            repairBar.gameObject.SetActive(true);
            onRepair.Invoke();
            while (repairTimer < repairTime.GetValue())
            {
                repairTimer += Time.deltaTime;
                repairBar.value = repairTimer;
                yield return new WaitForEndOfFrame();
            }
            if (repairTimer >= repairTime.GetValue())
            {
                machineToRepair.IsBroken = false;
                repairAudio.ChangeParameter(1f);
                repairCompleteAudio.PlayAudio(transform.position);
                PoolManager.GetObject(repairFeedback, repairFeedbackSpawnpoint.position);
                //Repair End event used for statistics and achivment purpose
                EventManager.TriggerEvent(EventsID.PLAYERREPAIR);
            }
            OnRepairEnd();

        }
        yield return null;
    }

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ((stopRepairOnCollision && isRepairing) && GameFramework.Utilities.LayerIsInLayerMask(collision.gameObject.layer,stopRepairCollisionLayerMask))
        {
            StopRepair();
        }
    }

}
