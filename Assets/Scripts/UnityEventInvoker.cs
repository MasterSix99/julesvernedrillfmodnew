﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityEventInvoker : MonoBehaviour
{
    [SerializeField] private UnityEngine.Events.UnityEvent unityEvent;

    public void Execute()
    {
        unityEvent?.Invoke();
    }
}
