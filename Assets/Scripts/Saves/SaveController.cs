﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveController : Singleton<SaveController>
{
    public SaveFile saveFile = new SaveFile();

    //public bool ThereIsSave { get { return SaveManager.LoadObjectBinary(GameGlobals.Saves.SavePath) != null; } }

    protected override void Awake()
    {
        base.Awake();
        saveFile = (SaveFile)SaveManager.LoadObjectBinary(GameGlobals.Saves.SavePath);
        if (saveFile == null)
            saveFile = new SaveFile();
    }

    private void OnApplicationQuit()
    {
        SaveManager.SaveObjectBinary(saveFile, GameGlobals.Saves.SavePath);
    }

    public void SaveObject(string key, object objectToSave)
    {
        if (!saveFile.saveDictionary.ContainsKey(key))
            saveFile.saveDictionary.Add(key, objectToSave);
        else
            saveFile.saveDictionary[key] = objectToSave;
    }

    public object LoadObject(string key)
    {
        if (saveFile.saveDictionary.ContainsKey(key))
            return saveFile.saveDictionary[key];
        else
            return null;
    }
}
