﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveFile
{
    public List<Achievement.AchievementSaveInfo> achievementsData = new List<Achievement.AchievementSaveInfo>();

    public Dictionary<string, object> saveDictionary = new Dictionary<string, object>();
}
