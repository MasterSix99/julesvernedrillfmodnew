﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveManager
{
    #region Methods
    #region JSon
    public static void SaveObjectJSon(object classToSave, string path)
    {
        Debug.Log(path);
        string jSonFile = JsonUtility.ToJson(classToSave);
        //Debug.Log(jSonFile);
        File.WriteAllText(path, jSonFile);
    }

    public static object LoadObjectJSon<T>(string path)
    {
        if (File.Exists(path))
        {
            string jSonFle = File.ReadAllText(path);

            T obj = JsonUtility.FromJson<T>(jSonFle);

            return obj;
        }
        return null;
    }
    #endregion
    #region Binary
    public static void SaveObjectBinary(object objectToSave, string path)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file = File.Open(path, FileMode.OpenOrCreate);
        formatter.Serialize(file, objectToSave);
        file.Close();
    }

    public static object LoadObjectBinary(string path)
    {
        if (File.Exists(path))
        {
            Debug.Log(path);
            object resultObject = null;

            FileStream file = File.Open(path, FileMode.Open);
            if (file != null)
            {
                BinaryFormatter formatter = new BinaryFormatter();
                resultObject = formatter.Deserialize(file);
                file.Close();
            }
            return resultObject;
        }
        return null;
    }
    #endregion

    public static bool SaveFileExists(string path)
    {
        return File.Exists(path);
    }

    public static void DeleteSaveFile(string path)
    {
        File.Delete(path);
    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Saves/Delete Save file")]
    public static void DeleteSaveFile()
    {
        DeleteSaveFile(GameGlobals.Saves.SavePath);
    }
#endif
#endregion
}
