﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class VirtualCameraBehaviour : MonoBehaviour
{
    private CinemachineFramingTransposer virtualCameraTransposer;

    [SerializeField] private GameFramework.FloatVariable minDistanceVar;
    [SerializeField] private float minDistanceMinValue, minDistanceMaxValue;
    [SerializeField] private float varMaxValue;

    #region Cached
    private float fraction;
    #endregion

    private void Awake()
    {
        virtualCameraTransposer = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineFramingTransposer>();
    }

    private void OnEnable()
    {
        minDistanceVar.onValueChangeAction += UpdateMinDistanceValue;
    }

    private void OnDisable()
    {
        minDistanceVar.onValueChangeAction -= UpdateMinDistanceValue;
    }

    private void UpdateMinDistanceValue()
    {
        fraction = Mathf.Clamp01(minDistanceVar.Value / varMaxValue);
        virtualCameraTransposer.m_MinimumDistance = (1 - fraction) * minDistanceMinValue + fraction * minDistanceMaxValue;
    }
}
