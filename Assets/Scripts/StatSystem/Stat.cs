﻿
public class Stat<T>
{
    public int id;
    public T value;

    public void SetStat(T newValue)
    {
        value = newValue;
        AchievementManager.Instance.UpdateAchievement(id, value);
    }
}

[System.Serializable]
public class StatInt : Stat<int>
{

}

[System.Serializable]
public class StatFloat : Stat<float>
{

}