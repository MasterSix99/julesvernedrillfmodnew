﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Progression
{
    [SerializeField] private string progressionName;
    [SerializeField] private float startValue = 0;
    [SerializeField] private float endValue = 1;
    [SerializeField] private GameFramework.FloatVariable referenceValue;
    [SerializeField] private float referenceMaxValue;
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private bool log = false;

    public float Evaluate()
    {
        float t = curve.Evaluate(referenceValue.Value / referenceMaxValue);
        t = Mathf.Clamp01(t);
        if (log)
            Debug.Log((startValue * (1 - t)) + (endValue * t));
        return (startValue * (1 - t)) + (endValue * t);
    }
}
