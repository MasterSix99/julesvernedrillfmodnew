﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// To use a "ScriptableObjectClone", do 'ScriptableObjectClone = Instantiate(ScriptableObjectClone)'.
/// It is safer than instantiating a simple ScriptableObject because it checks if itself is a clone before doing anything. If it isn't, it does nothing.
/// </summary>
public class ScriptableObjectClone : ScriptableObject {

    protected bool cloned = false;

    protected GameObject actor = null;

    /// <summary>
    /// It's just an Awake, what were you expecting?
    /// </summary>
    private void Awake()
    {
        //Debug.Log("Awake - Out");
        if (Application.IsPlaying(this))
        {
            //Debug.Log("Awake - In");
            cloned = true;
            OnAwake();
        }
    }

    private void OnEnable()
    {
        if (cloned)
            Enable();
    }

    private void OnDisable()
    {
        if (cloned)
            Disable();
    }

    /// <summary>
    /// Call Setup(GameObject _actor) after instantiating the scriptable object, and pass the "Owner" of the scriptable as a parameter.
    /// EX. The owner of a weapon scriptable object is who or what shoot with that weapon.
    /// </summary>
    /// <param name="_actor"></param>
    public void Setup(GameObject _actor)
    {
        if (cloned)
            actor = _actor;
        OnSetup();
    }

    /// <summary>
    /// Call the Update from another script, if the scriptable needs an Update.
    /// </summary>
    public void Update()
    {
        if (cloned)
            OnUpdate();
    }

    /// <summary>
    /// These functions can be overridden by classes derived from ScriptableObjectClone and can't be called from anywhere except ScriptableObjectClone's functions.
    /// </summary>
    #region Inherited functions
    protected virtual void OnAwake()
    {
        //Debug.Log("OnAwake");
    }

    protected virtual void Enable()
    {
        //Debug.Log("Enable");
    }

    protected virtual void Disable()
    {
        //Debug.Log("Disable");
    }

    protected virtual void OnUpdate()
    {
        //Debug.Log("OnUpdate");
    }

    protected virtual void OnSetup()
    {
        //Debug.Log("OnSetup");
    }
    #endregion
}
