﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingPattern : ScriptableObject
{
    protected ResourceMovement actor;

    public virtual void Initialize(ResourceMovement _actor)
    {
        actor = _actor;
        actor.onCanMoveChange += OnCanMoveChange;
    }

    protected virtual void OnDestroy()
    {
        actor.onCanMoveChange -= OnCanMoveChange;
    }

    public abstract void UpdatePattern();

    public virtual void Collide(Collision2D other)
    {

    }

    protected virtual void OnCanMoveChange(bool newValue)
    {

    }
}
