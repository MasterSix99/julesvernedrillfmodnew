﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "so_movingPattern_rotation_1", menuName = "Resources/Alive/Rotation")]
public class RotationPattern : MovingPattern
{
    [SerializeField]
    private LayerMask checkLayerMask;

    [SerializeField]
    private float torqueForce;
    [SerializeField]
    private float maxAngularVelocity = 10;
    private float direction = 1f;
    [SerializeField]
    private float checkDistance = 0.5f;
    [SerializeField]
    private float minDistFromLastPos = 0.1f;

    private Vector3 lastPos;
    private float checkPosRate = 2;
    private float checkPosTimer = 0.000f;

    public override void Initialize(ResourceMovement _actor)
    {
        base.Initialize(_actor);
        direction = Mathf.Sign(Random.Range(-1, +1));
        lastPos = actor.transform.position;
    }

    public override void UpdatePattern()
    {
        Rotate();
        if (checkPosTimer < checkPosRate)
        {
            checkPosTimer += Time.deltaTime;
        }
        else
        {
            checkPosTimer = 0.000f;
            if (Vector3.Distance(actor.transform.position, lastPos) < minDistFromLastPos)
                direction = -direction;
            lastPos = actor.transform.position;
        }
    }

    void Rotate()
    {
        actor.Rb.AddTorque(torqueForce * direction, ForceMode2D.Force);
        //Debug.Log(actor.Rb.angularVelocity);
        actor.Rb.angularVelocity = Mathf.Clamp(actor.Rb.angularVelocity, -maxAngularVelocity, maxAngularVelocity);
    }

    public override void Collide(Collision2D other)
    {
        if(GameFramework.Utilities.LayerIsInLayerMask(other.gameObject.layer, checkLayerMask))
        {
            direction = -direction;
        }
    }

    protected override void OnCanMoveChange(bool newValue)
    {
        actor.Rb.angularVelocity = 0.0000f;
    }
}
