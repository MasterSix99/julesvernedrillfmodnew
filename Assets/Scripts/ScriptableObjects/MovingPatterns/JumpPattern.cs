﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "so_movingPattern_jump_1", menuName = "Resources/Alive/Jump")]
public class JumpPattern : MovingPattern
{
    [SerializeField] private bool debugMode;
    [SerializeField] private float groundCheckDistance = 0.5f;
    [SerializeField] private float checkRadius = 0.1f;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private LayerMask wallMask;
    [SerializeField] private float minXForDirectionChange = 0.1f;

    [Header("Jump variables")]
    [SerializeField] private float jumpRange = 10;
    [SerializeField] private float jumpMinForce = 10;
    [SerializeField] private float jumpMaxForce = 10;
    [SerializeField] private bool setCountdownAtEachJump = true;
    [SerializeField] private float jumpMinCountdown = 10;
    [SerializeField] private float jumpMaxCountdown = 10;
    [SerializeField] private GameFramework.FloatVariable jumpForceMultiplier;

    [Header("Audio")]
    [SerializeField] private OneShotAudio jumpSound;

    private float jumpFinalCountdown;
    private float timer;

    private int jumpSign = -1;

    private bool hasJumped = false;

    public override void Initialize(ResourceMovement _actor)
    {
        base.Initialize(_actor);
        SetupCountdown();
        jumpSign = (int)Mathf.Sign(Random.Range(-1f, 1f));
    }

    public override void UpdatePattern()
    {
        if (IsGrounded())
        {
            if (!hasJumped)
            {
                if (DoTimer())
                {
                    Jump();
                    if (setCountdownAtEachJump)
                        SetupCountdown();
                }
            }
        }
        else if (hasJumped)
            hasJumped = false;
    }

    private bool IsGrounded()
    {
        if (debugMode)
            Debug.DrawLine(actor.transform.position, actor.transform.position + Vector3.down * (/*actor.transform.localScale.x * */actor.SpriteTransform.lossyScale.y * groundCheckDistance), Color.magenta);
        return Physics2D.CircleCast(actor.transform.position, checkRadius, Vector3.down, (/*actor.transform.localScale.x * */actor.SpriteTransform.lossyScale.y * groundCheckDistance), groundMask);
    }

    private void Jump()
    {
        //Debug.Log("Jump!");
        float jumpForce = Random.Range(jumpMinForce, jumpMaxForce);
        if (jumpForceMultiplier != null)
            jumpForce *= jumpForceMultiplier.Value;
        actor.Rb.AddForce((Quaternion.AngleAxis(Random.Range(jumpRange * jumpSign, 0), Vector3.forward) * Vector2.up) * jumpForce, ForceMode2D.Impulse);
        if (!string.IsNullOrEmpty(jumpSound.eventPath))
            jumpSound.PlayAudio(actor.transform.position);
        hasJumped = true;
    }

    public override void Collide(Collision2D other)
    {
        if (GameFramework.Utilities.LayerIsInLayerMask(other.gameObject.layer, wallMask))
        {
            if (Mathf.Abs(other.transform.position.x - actor.transform.position.x) >= minXForDirectionChange)
            {
                //Debug.Log("Colliding");
                jumpSign = -jumpSign;
            }
        }
    }

    private void SetupCountdown()
    {
        jumpFinalCountdown = Random.Range(jumpMinCountdown, jumpMaxCountdown);
    }

    private bool DoTimer()
    {
        if (timer < jumpFinalCountdown)
        {
            timer += Time.deltaTime;
            return false;
        }
        else
        {
            timer = 0.000f;
            return true;
        }
    }

    protected override void OnCanMoveChange(bool newValue)
    {
        if (newValue)
        {
            if (timer != 0.000f)
                timer = 0.000f;
        }
    }
}
