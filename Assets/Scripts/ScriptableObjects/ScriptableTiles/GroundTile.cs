﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "so_tile_ground_1", menuName = "Terrain Generation/Tiles/Ground Tile")]
public class GroundTile : Tile
{
    public enum TileType
    {
        Dirt = 0,
        Stone = 1,
        Coal = 2,
        Ruby = 3,
        lava = 4 //l'ha fatto Gianluca
    }

    public delegate void OnBlindToggle();
    public static event OnBlindToggle onBlindToggle;

    [HideInInspector]
    public Vector3Int coordinates;

    private static Tilemap tilemap = null;

    public TileType tileType;
    [SerializeField]
    private float underThresholdSpeedDecay = 0.0005f;
    [SerializeField]
    private float overThresholdSpeedDecay = 0.0005f;
    [SerializeField]
    private float underThresholdHeatPerSecond = 1f;
    [SerializeField]
    private float overThresholdHeatPerSecond = 1f;
    //[SerializeField]
    //private GameFramework.FloatVariable speedDecayer;
    [SerializeField]
    private Sprite blindSprite;
    private Sprite activeSprite;
    private static bool isBlind;

    public float UnderThresholdSpeedDecay { get { return underThresholdSpeedDecay; } }
    public float OverThresholdSpeedDecay { get { return overThresholdSpeedDecay; } }

    public float UnderThresholdHeatPerSecond { get { return underThresholdHeatPerSecond; } }
    public float OverThresholdHeatPerSecond { get { return overThresholdHeatPerSecond; } }

    public static bool IsBlind
    {
        get
        {
            return isBlind;
        }
        set
        {
            isBlind = value;
            onBlindToggle?.Invoke();
        }
    }

    public void Enable(Vector3Int _coordinates, Tilemap _tilemap)
    {
        if (Application.IsPlaying(this))
        {
            coordinates = _coordinates;
            if (tilemap == null)
                tilemap = _tilemap;

            activeSprite = sprite;

            ToggleSprite();
            onBlindToggle += ToggleSprite;
            
        }
    }

    public void Disable()
    {
        if (Application.IsPlaying(this))
        {
            onBlindToggle -= ToggleSprite;
        }
    }

    private void ToggleSprite()
    {
        if (blindSprite != null)
        {
            if (IsBlind)
            {
                sprite = blindSprite;
            }
            else
            {
                sprite = activeSprite;
            }
        }

        tilemap.RefreshTile(coordinates);
    }
}
