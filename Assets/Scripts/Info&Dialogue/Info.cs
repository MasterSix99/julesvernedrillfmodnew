﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "so_info", menuName = "InfoBox/info")]
public class Info : ScriptableObject
{
    [SerializeField]
    private Sprite infoImage;
    [SerializeField]
    private string infoText;

    public Sprite InfoImage { get { return infoImage; } }
    public string InfoText { get { return infoText; } }
    
}
