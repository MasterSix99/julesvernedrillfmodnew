﻿using GameFramework;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class DialoguePanel : Panel
{
    public TextMeshProUGUI textField;
    public string[] dialogues;

    public int currentDialogueIdx = 0;

    public UnityEvent onDialogueEnd;

    public int CurrentDialogueIdx
    {
        get { return currentDialogueIdx; }
        set {
            currentDialogueIdx = value;
            ShowCurrentDialogue();
        }
    }
    private void OnEnable()
    {
        currentDialogueIdx = 0;
        ShowCurrentDialogue();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            CurrentDialogueIdx++;
        }
    }

    public void ShowCurrentDialogue()
    {
        Debug.Log("currentDialogueIdx =" + currentDialogueIdx + "dialogues.Length" + dialogues.Length);
        if (currentDialogueIdx >= dialogues.Length)
            onDialogueEnd.Invoke();
        else
            textField.text = dialogues[currentDialogueIdx];
    }



}
