﻿/**
 * @author Fabrizio Coppolecchia
 *
 * Manage the infobox Panel
 * 
 * @date - 2019/02/12
 */

using TMPro;
using UnityEngine;
using GameFramework;
using UnityEngine.UI;



public class InfoBox : Panel
{
    /// <summary>
    /// The text component
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI txtInfo;
    /// <summary>
    /// The image component
    /// </summary>
    [SerializeField]
    private Image imageField;

    [SerializeField]
    private Animator animator;
    private float animationLen;
    private bool checkForAnimationEnd = true;
    private float timer;


    protected override void Awake()
    {
        base.Awake();
        EventManager.StartListening<InfoBoxRequest>(EventsID.INFOBOXREQUEST, OnInfoBoxRequest);

        //Store the animation Len (for now the animation is only one so i don't need to check fi is open animation or close... remember to refactor this script if needed
        AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;
        foreach (AnimationClip clip in clips)
        {
            animationLen += clip.length;
        }

        HideInfo();
    }

    private void Update()
    {
        //If Panel is opening i need to start monitoring for animation end to show info image and text , i do this incresing timer until when is greather than animation len
        if(IsOpened && checkForAnimationEnd)
        {
            timer += Time.deltaTime;
            if(timer > animationLen)
            {
                ShowInfo();
                checkForAnimationEnd = false;
                timer = 0;
            }
        }
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        EventManager.StopListening<InfoBoxRequest>(EventsID.INFOBOXREQUEST, OnInfoBoxRequest);
    }

    public void ShowInfo()
    {
        txtInfo.gameObject.SetActive(true);
        imageField.gameObject.SetActive(true);
    }
    public void HideInfo()
    {
        txtInfo.gameObject.SetActive(false);
        imageField.gameObject.SetActive(false);
    }

    /// <summary>
    /// Use the infoBoxRequest to perform a open/closure of infobox
    /// </summary>
    /// <param name="infoBoxRequest">the request info</param>
    public void OnInfoBoxRequest(InfoBoxRequest infoBoxRequest)
    {
        InfoBoxRequest request = infoBoxRequest;
        
        if (request.mustBeOpened)
        {
            HideInfo();
            checkForAnimationEnd = true;
            //Replace PlaceHolder allowed (placeholders are between {})
            string text = InputUtils.GetReplacedInputPlaceHolder(infoBoxRequest.infoDefinition.InfoText);
            //Set text to textComponent
            txtInfo.text = (string)text;
            //Set image to ImageComponent
            if (imageField != null)
                imageField.sprite = infoBoxRequest.infoDefinition.InfoImage;
            //Open Infobox
            OpenPanel();
            //Close infobox timed if required
            if (request.duration > 0)
            {
                StopAllCoroutines();
                StartCoroutine(CloseDelayed(request.duration + animationLen));
            }
        }
        else
        {
            ClosePanel();

        }

    }

    
}
