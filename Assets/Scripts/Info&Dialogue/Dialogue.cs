﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="so_dialogue",menuName ="DialogueSystem/Dialogue")]
public class Dialogue : ScriptableObject {

    [SerializeField]
    private DialogueEntry[] dialogueLines;
    public DialogueEntry[] DialogueLines { get {return dialogueLines;} }
}

[System.Serializable]
public struct DialogueEntry
{
    public string speakerName;
    public string dialogueLine;
}
