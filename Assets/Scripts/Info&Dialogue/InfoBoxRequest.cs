﻿/**
 * @author Fabrizio Coppolecchia
 *
 * Definition of a request handled from infobox
 * 
 * @date - 2019/02/12
 */

using UnityEngine;

public struct InfoBoxRequest
{
    [SerializeField]
    public bool mustBeOpened;
    public Info infoDefinition;
    public float duration;

    public InfoBoxRequest(bool _mustBeOpened, Info _infoDefinition, float _duration = -1)
    {
        /// <summary>
        /// Additional check to control if infobox must be opened or closed
        /// </summary>
        this.mustBeOpened = _mustBeOpened;
        /// <summary>
        /// Definition of a info
        /// </summary>
        this.infoDefinition = _infoDefinition;
        /// <summary>
        /// The infobox will be opened as much time as written here
        /// </summary>
        this.duration = _duration;
    }
}
