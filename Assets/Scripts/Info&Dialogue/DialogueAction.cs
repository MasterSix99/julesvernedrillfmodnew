﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class DialogueAction
{

    public DialogueAction(Dialogue _dialogue, UnityEvent _callback)
    {
        dialogue = _dialogue;
        callback = _callback;
    }
    public Dialogue dialogue;
    public UnityEvent callback;
}
