﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TestClass
{
    [System.Serializable]
    public class TestNestedClass
    {
        public int intero = 0;

        public void Increase()
        {
            intero++;
        }

        public void DebugInfo()
        {
            Debug.Log("nested: " + intero);
        }
    }

    public int intero = 3;

    public float decimale = 0.5f;

    public TestNestedClass nestedClass = new TestNestedClass();

    public void Increase()
    {
        intero++;
        decimale += 0.1f;
        nestedClass.Increase();
    }

    public void DebugInfo()
    {
        Debug.Log("intero: " + intero);
        Debug.Log("decimale: " + decimale);
        nestedClass.DebugInfo();
    }
}
