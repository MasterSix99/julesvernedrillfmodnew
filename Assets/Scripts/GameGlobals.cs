﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameGlobals
{
    public static class Saves
    {
        public static string SavePath { get { return System.IO.Path.Combine(Application.persistentDataPath, "achievementsSave.txt"); } }
    }
}
