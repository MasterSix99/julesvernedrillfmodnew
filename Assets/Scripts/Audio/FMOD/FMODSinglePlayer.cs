﻿using UnityEngine;

public abstract class FMODSinglePlayer : MonoBehaviour
{
    [SerializeField] protected Transform emitterTransform;

    [Tooltip("The first audio from oneShotAudios is played")] [SerializeField] private bool playOnEvent = false;
    [ShowIf("playOnEvent")] [SerializeField] private EventsID eventToListen;

    private void OnEnable()
    {
        if (playOnEvent)
            EventManager.StartListening(eventToListen, PlayAudio);
    }

    private void OnDisable()
    {
        if (playOnEvent)
            EventManager.StopListening(eventToListen, PlayAudio);
    }

    public abstract void PlayAudio();
}
