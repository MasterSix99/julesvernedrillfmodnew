﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODMultipleOneShotPlayer : FMODSinglePlayer
{
    [SerializeField] private OneShotAudio[] oneShots;

    public override void PlayAudio()
    {
        oneShots[0].PlayAudio(emitterTransform.position);
    }

    public void PlayAudio(int index)
    {
        oneShots[index].PlayAudio(emitterTransform.position);
    }
}
