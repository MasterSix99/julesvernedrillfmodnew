﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;

public class FMODBusUtils : MonoBehaviour
{
    [SerializeField] private string busPath;

    private Bus bus = new Bus();

    private void Awake()
    {
        FmodManager.instance.SetBus(ref bus, busPath);
    }

    public void SetBus(float value)
    {
        bus.setVolume(value);
    }
}
