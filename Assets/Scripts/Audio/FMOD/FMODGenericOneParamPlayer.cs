﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODGenericOneParamPlayer : FMODSinglePlayer
{
    [SerializeField] private GenericEventMonoParameter audioEvent;

    [SerializeField] private bool updateAtFloatVarChange;
    [SerializeField] private bool playOnStart;
    [ShowIf("updateAtFloatVarChange")] [SerializeField] private GameFramework.FloatVariable floatVarForUpdate;
    [ShowIf("updateAtFloatVarChange")] [SerializeField] private bool hasThresholdForUpdate = false;
    [ShowIf("hasThresholdForUpdate")] [SerializeField] private float thresholdForUpdate;
    [ShowIf("updateAtFloatVarChange")] [SerializeField] private bool normalizeValue = false;
    [ShowIf("normalizeValue")] [SerializeField] private float maxValue;

    private void Awake()
    {
        FmodManager.instance.CreateGenericMonoEventParameterInstance(ref audioEvent);
    }

    private void OnEnable()
    {
        if (updateAtFloatVarChange)
        {
            floatVarForUpdate.onValueChangeAction += UpdateWithFloatVar;
        }
    }

    private void Start()
    {
        if (playOnStart)
            PlayAudio();
    }

    private void OnDisable()
    {
        if (updateAtFloatVarChange)
        {
            floatVarForUpdate.onValueChangeAction -= UpdateWithFloatVar;
        }
    }

    private void OnDestroy()
    {
        StopAudio(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        ReleaseAudio();
    }

    public override void PlayAudio()
    {
        if (emitterTransform != null)
            audioEvent.PlayAudio(emitterTransform);
        else
            audioEvent.PlayAudio();
    }

    public void StopAudio(FMOD.Studio.STOP_MODE stopMode)
    {
        audioEvent.StopAudio(stopMode);
    }

    public void ReleaseAudio()
    {
        audioEvent.Release();
    }

    public bool IsPlaying()
    {
        return audioEvent.IsPlaying();
    }

    private void UpdateWithFloatVar()
    {
        UpdateParameter(floatVarForUpdate.Value);
    }

    private void UpdateParameter(float newValue)
    {
        if (!hasThresholdForUpdate || newValue > thresholdForUpdate)
        {
            audioEvent.ChangeParameter(normalizeValue ? (newValue - thresholdForUpdate / maxValue - thresholdForUpdate) : newValue);
        }
    }

}
