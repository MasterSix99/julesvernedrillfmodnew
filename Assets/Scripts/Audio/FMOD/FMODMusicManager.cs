﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODMusicManager : MonoBehaviour
{
    [SerializeField] private GenericEventMonoParameter musicEvent;

    [SerializeField] private GameFramework.FloatVariable currentDepth;
    [SerializeField] private float maxDepthForMaxParameter;

    private bool areHoles = false;

    private void Awake()
    {
        FmodManager.instance.CreateGenericMonoEventParameterInstance(ref musicEvent);
    }

    private void Start()
    {
        musicEvent.PlayAudio();
    }

    private void OnDestroy()
    {
        musicEvent.StopAudio(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        musicEvent.Release();
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.ONDRILLOVERHEAT, OnDrillOverHeat);
        EventManager.StartListening(EventsID.ALLHOLESREPAIRED, OnHolesRepaired);
        currentDepth.onValueChangeAction += UpdateParameterValue;
    }

    private void OnDisable()
    {
        EventManager.StartListening(EventsID.ONDRILLOVERHEAT, OnDrillOverHeat);
        EventManager.StartListening(EventsID.ALLHOLESREPAIRED, OnHolesRepaired);
        currentDepth.onValueChangeAction -= UpdateParameterValue;
    }

    private void Update()
    {
        float pValue;
        musicEvent.eventParameter.getValue(out pValue);
        //Debug.Log("P value: " + pValue.ToString("#.##"));
    }

    private void UpdateParameterValue()
    {
        if (!areHoles)
        {
            musicEvent.ChangeParameter(Mathf.Clamp01(currentDepth.Value / maxDepthForMaxParameter) * 2);
        }
    }

    private void OnDrillOverHeat()
    {
        areHoles = true;
        musicEvent.ChangeParameter(2);
    }

    private void OnHolesRepaired()
    {
        areHoles = false;
        UpdateParameterValue();
    }
}
