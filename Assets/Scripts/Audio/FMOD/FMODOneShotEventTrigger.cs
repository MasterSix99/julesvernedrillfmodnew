﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODOneShotEventTrigger : MonoBehaviour
{
    [System.Serializable]
    public class EventData
    {
        public OneShotAudio audio;
        public EventsID[] eventsToFollow;

        private Transform playingTransform = null;

        public void Init(Transform in_playingTransform)
        {
            playingTransform = in_playingTransform;
        }

        public void Subscribe()
        {
            int i;
            for (i = 0; i < eventsToFollow.Length; i++)
            {
                EventManager.StartListening(eventsToFollow[i], Play);
            }
        }
        
        public void Unsubscribe()
        {
            int i;
            for (i = 0; i < eventsToFollow.Length; i++)
            {
                EventManager.StopListening(eventsToFollow[i], Play);
            }
        }

        private void Play()
        {
            audio.PlayAudio(playingTransform.position);
        }
    }

    [SerializeField]
    private EventData[] audioEvents;

    #region Monobehaviour Methods
    private void Awake()
    {
        int i;
        for (i = 0; i < audioEvents.Length; i++)
        {
            audioEvents[i].Init(transform);
        }
    }

    private void OnEnable()
    {
        int i;
        for (i = 0; i < audioEvents.Length; i++)
        {
            audioEvents[i].Subscribe();
        }
    }

    private void OnDisable()
    {
        int i;
        for (i = 0; i < audioEvents.Length; i++)
        {
            audioEvents[i].Unsubscribe();
        }
    }
    #endregion
}
