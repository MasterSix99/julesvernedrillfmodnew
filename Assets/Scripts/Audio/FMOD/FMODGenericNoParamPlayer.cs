﻿using UnityEngine;

public class FMODGenericNoParamPlayer : FMODSinglePlayer
{
    [SerializeField] private GenericEvent audioEvent;

    private void Awake()
    {
        FmodManager.instance.CreateGenericEnventInstance(ref audioEvent);
    }

    private void OnDestroy()
    {
        StopAudio(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        ReleaseAudio();
    }

    public override void PlayAudio()
    {
        if (emitterTransform != null)
            audioEvent.PlayAudio(emitterTransform);
        else
            audioEvent.PlayAudio();
    }

    public void StopAudio(FMOD.Studio.STOP_MODE stopMode)
    {
        audioEvent.StopAudio(stopMode);
    }

    public void ReleaseAudio()
    {
        audioEvent.Release();
    }

    public bool IsPlaying()
    {
        return audioEvent.IsPlaying();
    }
}
