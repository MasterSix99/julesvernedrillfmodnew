﻿using UnityEngine;

public class FMODOneShotAudioPlayer : FMODSinglePlayer
{
    [SerializeField] private OneShotAudio oneShotAudio;

    public override void PlayAudio()
    {
        oneShotAudio.PlayAudio(emitterTransform.position);
    }
}
