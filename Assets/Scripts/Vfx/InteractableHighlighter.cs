﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableHighlighter : MonoBehaviour
{
    [SerializeField] private Material mat;
    [SerializeField] private float MinIntensity = 0;
    [SerializeField] private float MaxIntensity = 1;
    [SerializeField] private float blinkSpeed = 1;

    private bool forward = true;
    private float t = 0;
    float lerpedIntensity;

    // Start is called before the first frame update
    //void Awake()
    //{
    //    mat = GetComponent<Material>();
    //}

    private void Start()
    {
        if (mat == null)
            Debug.LogError("mat variable is not set");
    }

    // Update is called once per frame
    void Update()
    {
        UpdateIntensityValue();
    }

    private void UpdateIntensityValue()
    {
        if (mat == null)
            return;
        if (forward)
        {
            t += Time.deltaTime * blinkSpeed;
            if (t >= 1)
            {
                t = Mathf.Clamp01(t);
                forward = false;
            }
        }
        else
        {
            t -= Time.deltaTime * blinkSpeed;
            if (t < 0)
            {
                t = Mathf.Clamp01(t);
                forward = true;
            }
        }

        lerpedIntensity = (1 - t) * MinIntensity + t * MaxIntensity;
        mat.SetFloat("_EmissionPower", lerpedIntensity);
    }
}


//   _EmissionPower("Emission Power", Float) = 1.0