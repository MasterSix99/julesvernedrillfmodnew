﻿using UnityEngine;

[System.Serializable]
public class JoystickVibrationConfig
{
    [Header("ENABLED STATUS")]
    public bool isEnabled = true;
    [Space]
    [SerializeField]
    private float leftVibrationLevel = 0;
    [SerializeField]
    private float leftVibrationTime = 0;
    [Space]
    [SerializeField]
    private float rightVibrationLevel = 0;
    [SerializeField]
    private float rightVibrationTime = 0;
    [SerializeField]
    private int vibrationPriority = 0;



    public bool IsEnabled { get { return isEnabled; } set { isEnabled = value; } }

    public float LeftVibrationLevel { get { return Globals.vibration? leftVibrationLevel:0; } set { leftVibrationLevel = value; } }
    public float LeftVibrationTime { get { return Globals.vibration ? leftVibrationTime : 0; } set { leftVibrationTime = value; } }

    public float RightVibrationLevel { get {return Globals.vibration ? rightVibrationLevel : 0; } set { rightVibrationLevel = value; } }
    public float RightVibrationTime { get { return Globals.vibration ? rightVibrationTime : 0; } set { rightVibrationTime = value; } }
    public int VibrationPriority { get { return vibrationPriority; } set { vibrationPriority = value; } }


    public JoystickVibrationConfig(float _leftVibrationLevel, float _leftVibrationTime, float _rightVibrationLevel, float _rightVibrationTime, int _vibrationPriority)
    {
        LeftVibrationLevel = _leftVibrationLevel;
        LeftVibrationTime = _leftVibrationTime;
        RightVibrationLevel = _rightVibrationLevel;
        RightVibrationTime = _rightVibrationTime;
        VibrationPriority = _vibrationPriority;
    }

}
