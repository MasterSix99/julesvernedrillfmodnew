﻿using UnityEngine;
using Rewired;
using UnityEngine.Events;

public class OnButtonPressExecuteEvent : MonoBehaviour
{
    #region Variables
    public bool useAnyButton;
    [ShowIf("useAnyButton", false)]
    public string rewiredAction;
    #endregion

    #region Events
    public UnityEvent onPressed;
    #endregion

    // Update is called once per frame
    void Update()
    {
        int playerCount = ReInput.players.playerCount;
        int i = 0;
        for(i = 0; i < playerCount; i++)
        {
            if ((useAnyButton && ReInput.players.GetPlayer(i).GetAnyButtonDown()) || (!useAnyButton && ReInput.players.GetPlayer(i).GetButtonDown(rewiredAction)))
            {
                onPressed?.Invoke();
            }
        }
    }
}
