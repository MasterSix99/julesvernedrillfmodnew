﻿using Rewired;
using System.Collections.Generic;

public static class JoystickVibration {
    internal class Vibration
    {
        private int id = 0;
        private float leftVibrationLevel;
        private float leftVibrationDuration;
        private float rightVibrationLevel;
        private float rightVibrationDuration;
        private int vibrationPriority;

        private float initTimeStamp;

        public int Id
        {
            get { return id; }
        }
        public int VibrationPriority
        {
            get { return vibrationPriority; }
        }


        public Vibration(int id, JoystickVibrationConfig _joystickVibrationConfig)
        {
            this.id = id;
            this.leftVibrationLevel = _joystickVibrationConfig.LeftVibrationLevel;
            this.leftVibrationDuration = _joystickVibrationConfig.LeftVibrationTime; ;
            this.rightVibrationLevel = _joystickVibrationConfig.RightVibrationLevel;
            this.rightVibrationDuration = _joystickVibrationConfig.RightVibrationTime;
            this.vibrationPriority = _joystickVibrationConfig.VibrationPriority;
            initTimeStamp = UnityEngine.Time.time;
        }
        public Vibration(int id, float leftVibration, float leftVibrationDuration, float rightVibration, float rightVibrationDuration, int _vibrationPriority)
        {
            this.id = id;
            this.leftVibrationLevel = leftVibration;
            this.leftVibrationDuration = leftVibrationDuration;
            this.rightVibrationLevel = rightVibration;
            this.rightVibrationDuration = rightVibrationDuration;
            this.vibrationPriority = _vibrationPriority;
            initTimeStamp = UnityEngine.Time.time;
        }

        public bool isVibrating()
        {
            float maxVibrationTime = leftVibrationDuration > rightVibrationDuration ? leftVibrationDuration : rightVibrationDuration;
            //If global vibration time is 0 the vibration is applied constantly, so must be treated as end (becouse is not timed)
            if (leftVibrationDuration == 0 && rightVibrationDuration == 0)
                return true;
            else if(maxVibrationTime > (UnityEngine.Time.time - initTimeStamp))
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }


    }


    private static List<Vibration> vibrations = new List<Vibration>();



    public static void Vibrate(Player player, JoystickVibrationConfig _joystickVibrationConfig)
    {
        int playerID = player.id;
        int vibrationIndex = -1;
        bool isVibrating = VibrationExist(player.id, out vibrationIndex);

        //Used to check if current vibration must stop 
        bool stopCurrentVibration = false;
        if (player != null &&  _joystickVibrationConfig.IsEnabled && (_joystickVibrationConfig.LeftVibrationLevel >0 || _joystickVibrationConfig.RightVibrationLevel >0))
        {
            if (isVibrating)
            {
                //If a vibration is currently performing and the vibration priority is highter thant new vibration i will stop the function execution
                if (vibrations[vibrationIndex].isVibrating() && vibrations[vibrationIndex].VibrationPriority > _joystickVibrationConfig.VibrationPriority)
                    return;
                else { 
                    //Add new vibration to list
                    vibrations[vibrationIndex] = new Vibration(playerID, _joystickVibrationConfig);
                    stopCurrentVibration = true;
                }
            }
            else
            {
                //Add new vibration to list
                if (vibrationIndex >= 0)
                    vibrations[vibrationIndex] = new Vibration(playerID, _joystickVibrationConfig);
                else
                    vibrations.Add(new Vibration(playerID, _joystickVibrationConfig));
            }

            //TODO IL CHECK FATTO PRIMA DI QUA AGGIUNGE UNA VIBRAZIONE ANCHE SE LA VIBRAZIONE NON é SUPPORTATA.. DA FIXARE

            foreach (Joystick j in player.controllers.Joysticks)
            {
                //If vibration is not supported , avoid this joystick and check next joystick
                if (!j.supportsVibration) continue;
                //If previously calcolated stopCurrentVibration is true i stop vibration for all joystick of player
                if (stopCurrentVibration)
                    j.StopVibration();

                if (j.vibrationMotorCount > 0)
                {
                    if (_joystickVibrationConfig.LeftVibrationTime > 0)
                        j.SetVibration(0, _joystickVibrationConfig.LeftVibrationLevel, _joystickVibrationConfig.LeftVibrationTime);//Timed Vibration
                    else
                        j.SetVibration(0, _joystickVibrationConfig.LeftVibrationLevel);//Continuos Vibration
                }
                if (j.vibrationMotorCount > 1)
                {
                    if (_joystickVibrationConfig.RightVibrationTime > 0)
                        j.SetVibration(0, _joystickVibrationConfig.RightVibrationLevel, _joystickVibrationConfig.RightVibrationTime);//Timed Vibration
                    else
                        j.SetVibration(1, _joystickVibrationConfig.RightVibrationLevel);//Continuos Vibration
                }
            }
        }
    }


    // VIBRATE USING COMPONENT OWNED PLAYER AND PARAMETERS
    public static void Vibrate(int playerId, JoystickVibrationConfig _joystickVibrationConfig)
    {
        if (_joystickVibrationConfig.isEnabled)
        {
            Player rewiredPlayer = ReInput.players.GetPlayer(playerId);
            Vibrate(rewiredPlayer, _joystickVibrationConfig);
        }
    }


    

    //SET COMPONENT OWNED VIBRATION PARAMS
    public static void StopVibrations(Player player)
    {
        if (player != null)
        {
            foreach (Joystick j in player.controllers.Joysticks)
            {
                if (!j.supportsVibration) continue;
                j.StopVibration();
            }
        }
    }


    private static bool VibrationExist(int vibrationId,out int vibrationIndex)
    {
        bool ret = false;
        vibrationIndex = -1;

        int i = 0;
        int len = vibrations.Count;
        for(i = 0; i < len; i++)
        {
            if(vibrations[i].Id == vibrationId)
            {
                ret = true;
                vibrationIndex = i;
                break;
            }
        }
        return ret;
    }
}
