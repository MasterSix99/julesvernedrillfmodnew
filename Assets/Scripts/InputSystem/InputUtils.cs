﻿/**
 * @author Fabrizio Coppolecchia
 *
 * Common Utility function's for rewired
 * 
 * @date - 2019/01/03
 */


using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputUtils 
{

    private static string commandPlaceHolderStartSequence = "{";
    private static string commandPlaceHolderEndSequence = "}";

    /// <summary>
    /// Enable or disable a rewired map for all controller of each players
    /// </summary>
    /// <param name="status"></param>
    /// <param name="mapName"></param>
    public static void SetAllPlayersMapStatus(bool status,string mapName)
    {
        foreach(Player rewiredPlayer in ReInput.players.AllPlayers)
        {
            rewiredPlayer.controllers.maps.SetMapsEnabled(status, mapName);
        }
    }


    /// <summary>
    /// Disable UI map for specific rewired player if the player is not in lobby, else the map will be Enabled
    /// </summary>
    /// <param name="player">the PlayerSassion that contain rewiredId</param>
    public static void UpdateUiMapAssignment(PlayerSession player)
    {
        if (player.IsEnabled)
        {
            //Debug.Log("1 - ABILITO LA MAPPA DELLA UI PER IL PLAYER " + player.PlayerID + " rewiredPlayer " + player.RewiredPlayerId);
            ReInput.players.GetPlayer(player.RewiredPlayerId).controllers.maps.SetMapsEnabled(true, "UI");
        }
        else
        {
            if (player.RewiredPlayerId > -1) { 
                //Debug.Log("1 - DISABILITO LA MAPPA DELLA UI PER IL PLAYER " + player.PlayerID + " rewiredPlayer " + player.RewiredPlayerId);
                ReInput.players.GetPlayer(player.RewiredPlayerId).controllers.maps.SetMapsEnabled(false, "UI");
            }
        }
    }
    /// <summary>
    /// Disable UI map for all rewired player that are not in Lobby and enable UI map for all the ones that are in
    /// </summary>
    /// <param name="players">the player list to check</param>
    public static void UpdateUiMapAssignment(List<PlayerSession> players)
    {
        foreach (PlayerSession player in players)
        {
            if (player.IsEnabled)
            {
                Debug.Log(ReInput.players.GetPlayer(player.RewiredPlayerId).isPlaying);
                ReInput.players.GetPlayer(player.RewiredPlayerId).controllers.maps.SetMapsEnabled(true, "UI");
                //Debug.Log("2 - ABILITO LA MAPPA DELLA UI PER IL PLAYER " + player.PlayerID + " rewiredPlayer " + player.RewiredPlayerId);
            }
            else
            {
                //Debug.Log(ReInput.players.GetPlayer(player.RewiredPlayerId).isPlaying);
                if (player.RewiredPlayerId > -1) { 
                    //Debug.Log("2 - DISABILITO LA MAPPA DELLA UI PER IL PLAYER " + player.PlayerID + " rewiredPlayer " + player.RewiredPlayerId);
                    //ReInput.players.GetPlayer(player.RewiredPlayerId).controllers.maps.SetMapsEnabled(false, "UI");
                }
            }
        }
            
    }
    /// <summary>
    /// Disable UI map for all rewired player that are not in Lobby and enable UI map for all the ones that are in NB: Player are retrived from DataManager
    /// </summary>
    public static void UpdateUiMapAssignment()
    {
        foreach (PlayerSession player in DataManager.Instance.Players)
        {
            if (player.IsEnabled)
                ReInput.players.GetPlayer(player.RewiredPlayerId).controllers.maps.SetMapsEnabled(true, "UI");
            else
                ReInput.players.GetPlayer(player.RewiredPlayerId).controllers.maps.SetMapsEnabled(false, "UI");
        }
    }

    /// <summary>
    /// Enable map for first player and disable for others
    /// </summary>
    /// <param name="mapName">The map name</param>
    public static void SetOnlyFirstPlayerMapEnabled(string mapName)
    {
        List<PlayerSession> players = DataManager.Instance.Players;
        for (int i = 0; i < players.Count; i++)
        {
            if( i == 0 )
                ReInput.players.GetPlayer(players[0].RewiredPlayerId).controllers.maps.SetMapsEnabled(true, mapName);
            else
                ReInput.players.GetPlayer(players[0].RewiredPlayerId).controllers.maps.SetMapsEnabled(false, mapName);
        }
    }

    /// <summary>
    /// Set Specific map of specific player enabled and disabled for other players
    /// </summary>
    /// <param name="player">The PlayerSession</param>
    /// <param name="mapName">The map name</param>
    public static void SetOnlySelectedPlayerMapEnabled(PlayerSession player,string mapName)
    {
        List<PlayerSession> players = DataManager.Instance.Players;
        for (int i = 0; i < players.Count; i++)
        {
            if (player.Equals(players[i]))
                ReInput.players.GetPlayer(players[0].RewiredPlayerId).controllers.maps.SetMapsEnabled(true, mapName);
            else
                ReInput.players.GetPlayer(players[0].RewiredPlayerId).controllers.maps.SetMapsEnabled(false, mapName);
        }
    }
    /// <summary>
    /// Set Specific map of specific player enabled and disabled for other players
    /// </summary>
    /// <param name="selectedPlayer">Rewired Player</param>
    /// <param name="mapName">The map name</param>
    public static void SetOnlySelectedPlayerMapEnabled(Player selectedPlayer, string mapName)
    {
        foreach (Player rewiredPlayer in ReInput.players.AllPlayers)
        {
            if (selectedPlayer.Equals(rewiredPlayer))
                rewiredPlayer.controllers.maps.SetMapsEnabled(true, mapName);
            else
                rewiredPlayer.controllers.maps.SetMapsEnabled(false, mapName);
        }
    }
    /// <summary>
    /// Set Specific map of specific player enabled and disabled for other players
    /// </summary>
    /// <param name="rewiredPlayerID">The rewired player id</param>
    /// <param name="mapName">The map name</param>
    public static void SetOnlySelectedPlayerMapEnabled(int rewiredPlayerID, string mapName)
    {
        foreach (Player rewiredPlayer in ReInput.players.AllPlayers)
        {
            if (rewiredPlayer.id == rewiredPlayerID)
                rewiredPlayer.controllers.maps.SetMapsEnabled(true, mapName);
            else
                rewiredPlayer.controllers.maps.SetMapsEnabled(false, mapName);
        }
    }
    /// <summary>
    /// Change Player Map status
    /// </summary>
    /// <param name="rewiredPlayerID">the rewired player id</param>
    /// <param name="mapName">The map name</param>
    /// <param name="status">The new status</param>
    public static void SetPlayerMapStatus(int rewiredPlayerID, string mapName, bool status)
    {
        if(ReInput.players != null) { 
            foreach (Player rewiredPlayer in ReInput.players.AllPlayers)
            {
                if (rewiredPlayer.id == rewiredPlayerID) { 
                    rewiredPlayer.controllers.maps.SetMapsEnabled(status, mapName);
                    break;
                }
            }
        }
    }




    /// <summary>
    /// Get input definition from last active controller
    /// <para>get last used controller and get key definition, if definition not found and last controller is keybord this means that definition can be on mouse, it will check from it and vice versa</para>
    /// </summary>
    /// <param name="playerIndex"></param>
    /// <returns></returns>
    public static KeyIdentifier GetInputKeyIdentifier( int playerId, string action,  ControllerType[] controllerTypesAllowed = null,string categoryName = "StandardPlayer",string layoutName = "Default")
    {
        if (controllerTypesAllowed == null)
            controllerTypesAllowed = new ControllerType[] { ControllerType.Joystick, ControllerType.Keyboard };
        KeyIdentifier ret = new KeyIdentifier();
        ret.keyText = "-> KEY NOT FOUND ->";
        Player player = ReInput.players.GetPlayer(playerId);
        // Get the last input controller
        Controller m_lastInputController = player.controllers.GetLastActiveController();

        if (m_lastInputController == null)
        { // player hasn't used any controllers yet , set default one
            if (player.controllers.joystickCount > 0)
            {  // try to get the first joystick in player
                m_lastInputController = player.controllers.Joysticks[0];
            }
            else
            {  // no joysticks assigned for this player, just get keyboard
                m_lastInputController = player.controllers.Keyboard;
            }
        }
        // check if last input is between the allowed ones , if not i will return keyboard
        int controllerTypeAllowed = Array.IndexOf(controllerTypesAllowed, m_lastInputController.type);
        if (controllerTypeAllowed < 0)
        {
            m_lastInputController = player.controllers.Keyboard;
        }

        /*Debug.Log("m_lastInputController = " + m_lastInputController);
        Debug.Log("categoryName = " + categoryName);
        Debug.Log("layoutName = " + layoutName);*/


        ControllerMap map = player.controllers.maps.GetMap(m_lastInputController, categoryName, layoutName);
        if (map == null)
            throw new Exception("UKNOWN MAP : Controller type =" + m_lastInputController.type + ",  CATEGORY =" + categoryName + " layout = " + layoutName);
        ActionElementMap actionElementMap = map.GetFirstButtonMapWithAction(action);

        //this two check are used becouse if i try to get keyboard or mouse action but last moved is the one that does not have
        //the action i need to check to the other one
        if (actionElementMap == null && m_lastInputController.type.Equals(ControllerType.Mouse))
        {
            map = player.controllers.maps.GetMap(player.controllers.Keyboard, categoryName, layoutName);
            actionElementMap = map.GetFirstButtonMapWithAction(action);
        }
        //if i need input description from mouse but keyboard is last active and the map returned is null, i will check also from mouse
        else if (actionElementMap == null && m_lastInputController.type.Equals(ControllerType.Keyboard))
        {
            map = player.controllers.maps.GetMap(player.controllers.Mouse, categoryName, layoutName);
            actionElementMap = map.GetFirstButtonMapWithAction(action);
        }
        if (actionElementMap != null)
            ret.keyText = actionElementMap.elementIdentifierName;

        return ret;
    }


    //CONVERT ALL PLACEHOLDERS TO COMMANDS
    public static string GetReplacedInputPlaceHolder(string text, int playerId = 0, ControllerType[] controllerTypesAllowed = null)
    {
        if (controllerTypesAllowed == null)
            controllerTypesAllowed = new ControllerType[] { ControllerType.Joystick, ControllerType.Keyboard };
        string retText = text;

        List<string> commands = GetAllCommands(text, commandPlaceHolderStartSequence, commandPlaceHolderEndSequence);

        for (int i = 0; i < commands.Count; i++)
        {
            string inputCommand = commands[i];
            string commandPlaceHolder = string.Concat(commandPlaceHolderStartSequence, commands[i], commandPlaceHolderEndSequence);

            ControllerType[] allowedControllers = { ControllerType.Joystick, ControllerType.Keyboard };
            retText = retText.Replace(commandPlaceHolder, GetInputKeyIdentifier(playerId, inputCommand, allowedControllers, "GameplayShared").keyText);
        }
        return retText;
    }



    //FIND ALL WORLD SURROUNDED BY CHOSEN PLACEHOLDER
    public static List<string> GetAllCommands(string str, string commandCharStart, string commandCharEnd)
    {
        if (String.IsNullOrEmpty(commandCharStart) || String.IsNullOrEmpty(commandCharEnd))
            throw new ArgumentException("the start char to find may not be empty", "commandCharStart");
        if (String.IsNullOrEmpty(commandCharEnd))
            throw new ArgumentException("the end char to find may not be empty", "commandCharEnd");


        List<string> commands = new List<string>();

        int startCommandPos = 0;
        int endCommandPos = 0;
        string currentCommand = "";
        int startPos = 0;
        while (startCommandPos >= 0 && endCommandPos >= 0)
        {
            //GET START PLACEHOLDER POSITION, IF -1 NO MORE COMMANDS ARE IN STRING, I RETURN THE LIST FOUNDED
            startCommandPos = str.IndexOf(commandCharStart, startPos, StringComparison.Ordinal);
            if (startCommandPos == -1)
                return commands;
            //GET END PLACEHOLDER POSITION, IF -1 NO MORE COMMANDS ARE IN STRING, I RETURN THE LIST FOUNDED
            endCommandPos = str.IndexOf(commandCharEnd, startPos, StringComparison.Ordinal);
            if (endCommandPos == -1)
                return commands;
            //FIND THE CURRENT COMMAND
            currentCommand = str.Substring(startCommandPos + 1, (endCommandPos - startCommandPos - 1));
            startPos = endCommandPos + 1;
            //ADD TO LIST
            if (!commands.Contains(currentCommand))
                commands.Add(currentCommand);
        }

        return commands;
    }

    [System.Serializable]
    public struct KeyIdentifier
    {
        Controller type;
        public string keyText;
        public Sprite keySprite;
    }


}
