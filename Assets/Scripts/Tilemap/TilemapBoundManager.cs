﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapBoundManager : MonoBehaviour
{
    #region Classes
    /// <summary>
    /// Used to define an element's bounds inside the tilemap.
    /// </summary>
    [System.Serializable]
    private struct Bounds
    {
        public int up;
        public int right;
        public int down;
        public int left;

        public Bounds(int _up, int _right, int _down, int _left)
        {
            up = _up;
            right = _right;
            down = _down;
            left = _left;
        }
    }
    #endregion

    #region Variables
    [SerializeField]
    private Grid grid;
    [SerializeField]
    private TilemapGenerator[] tilemapGenerators;

    [Header("Target")]
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Bounds targetBounds;
    //[SerializeField]
    //private Bounds gridBounds;

    #region Cached
    private float northLimit;
    private float eastLimit;
    private float southLimit;
    private float westLimit;
    private float northMinDistance;
    private float eastMinDistance;
    private float southMinDistance;
    private float westMinDistance;
    #endregion
    #endregion

    private void Update()
    {
        MoveWithTarget();
    }

    /// <summary>
    /// Moves tilemaps if the target bounds gets outside the map.
    /// </summary>
    private void MoveWithTarget()
    {        
        int i;
        for (i = 0; i < tilemapGenerators.Length; i++)
        {
            northLimit = grid.transform.position.y + tilemapGenerators[i].MyTilemap.localBounds.max.y;
            eastLimit = grid.transform.position.x + tilemapGenerators[i].MyTilemap.localBounds.max.x;
            southLimit = grid.transform.position.y + tilemapGenerators[i].MyTilemap.localBounds.min.y;
            westLimit = grid.transform.position.x + tilemapGenerators[i].MyTilemap.localBounds.min.x;

            northMinDistance = target.position.y + targetBounds.up * grid.cellSize.y;
            eastMinDistance = target.position.x + targetBounds.right * grid.cellSize.x;
            southMinDistance = target.position.y + targetBounds.down * grid.cellSize.y;
            westMinDistance = target.position.x + targetBounds.left * grid.cellSize.x;


            Debug.Log("North: " + northMinDistance);
            Debug.Log("East: " + eastMinDistance);
            Debug.Log("South: " + southMinDistance);
            Debug.Log("West: " + westMinDistance);

            if (DebugManager.Instance != null)
            {
                if (DebugManager.Instance.DebugMode)
                {
                    Vector3 start = new Vector3(eastLimit, northLimit, 0);
                    Vector3 end = new Vector3(westLimit, northLimit, 0);
                    Debug.DrawLine(start, end, i == 0 ? Color.magenta : Color.cyan);
                    start = new Vector3(westLimit, northLimit, 0);
                    end = new Vector3(westLimit, southLimit, 0);
                    Debug.DrawLine(start, end, i == 0 ? Color.magenta : Color.cyan);
                    start = new Vector3(westLimit, southLimit, 0);
                    end = new Vector3(eastLimit, southLimit, 0);
                    Debug.DrawLine(start, end, i == 0 ? Color.magenta : Color.cyan);
                    start = new Vector3(eastLimit, southLimit, 0);
                    end = new Vector3(eastLimit, northLimit, 0);
                    Debug.DrawLine(start, end, i == 0 ? Color.magenta : Color.cyan);

                    start = new Vector3(eastMinDistance, northMinDistance, 0);
                    end = new Vector3(westMinDistance, northMinDistance, 0);
                    Debug.DrawLine(start, end, Color.yellow);
                    start = new Vector3(westMinDistance, northMinDistance, 0);
                    end = new Vector3(westMinDistance, southMinDistance, 0);
                    Debug.DrawLine(start, end, Color.yellow);
                    start = new Vector3(westMinDistance, southMinDistance, 0);
                    end = new Vector3(eastMinDistance, southMinDistance, 0);
                    Debug.DrawLine(start, end, Color.yellow);
                    start = new Vector3(eastMinDistance, southMinDistance, 0);
                    end = new Vector3(eastMinDistance, northMinDistance, 0);
                    Debug.DrawLine(start, end, Color.yellow);
                }
            }

            int range;
            int j;
            if (southMinDistance < southLimit)
            {
                range = Mathf.FloorToInt(southLimit - southMinDistance);
                
                for (j = 0; j < range; j++)
                {
                    tilemapGenerators[i].Move(TilemapGenerator.Direction.South);
                }
            }
            else if (northMinDistance > northLimit)
            {
                range = Mathf.FloorToInt(northMinDistance - northLimit);
                for (j = 0; j < range; j++)
                {
                    tilemapGenerators[i].Move(TilemapGenerator.Direction.North);
                }
            }

            if (eastMinDistance > eastLimit)
            {
                range = Mathf.FloorToInt(eastMinDistance - eastLimit);
                for (j = 0; j < range; j++)
                {
                    tilemapGenerators[i].Move(TilemapGenerator.Direction.East);
                }
            }
            else if (westMinDistance < westLimit)
            {
                range = Mathf.FloorToInt(westLimit - westMinDistance);
                for (j = 0; j < range; j++)
                {
                    tilemapGenerators[i].Move(TilemapGenerator.Direction.West);
                }
            }
            tilemapGenerators[i].MyTilemap.CompressBounds();
        }
    }
}
