﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Perlin;

[CreateAssetMenu(fileName = "so_genMtd_random_1", menuName = "Terrain Generation/Generation Methods/Random Method")]
public class RandomMethod : GenerationMethod
{
    [SerializeField] private bool randomValue = true;
    [SerializeField] private bool randomDepth = false;
    [SerializeField] private int minDepth = 0;
    [SerializeField] private int maxDepth = 5;

    /// <summary>
    /// Generates a grid with random Coordinates.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public override Coordinate[,] GenerateGrid(Vector2Int start, Vector2Int end)
    {
        int minX = Mathf.Min(start.x, end.x);
        int maxX = Mathf.Max(start.x, end.x);
        int minY = Mathf.Min(start.y, end.y);
        int maxY = Mathf.Max(start.y, end.y);

        Coordinate[,] resultGrid = null;
        resultGrid = new Coordinate[maxX - minX + 1, maxY - minY + 1];

        int x, y;
        for (x = 0; x < resultGrid.GetLength(0); x++)
        {
            for (y = 0; y < resultGrid.GetLength(1); y++)
            {
                resultGrid[x, y] = new Coordinate(randomValue ? Random.Range(0f, 1f) : 0f, randomDepth ? Random.Range(minDepth, maxDepth + 1) : 0);
            }
        }

        return resultGrid;
    }
}
