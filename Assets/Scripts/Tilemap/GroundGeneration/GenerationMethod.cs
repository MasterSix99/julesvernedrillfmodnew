﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenerationMethod : ScriptableObjectClone
{
    protected enum DataFlow
    {
        Random,
        RoundRobin
    }

    /// <summary>
    /// Overridden by inheriting classes that defines what criterion to follow for generating a perlin map.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="offset"></param>
    /// <param name="scale"></param>
    /// <returns></returns>
    public abstract Perlin.Coordinate[,] GenerateGrid(Vector2Int start, Vector2Int end);
}
