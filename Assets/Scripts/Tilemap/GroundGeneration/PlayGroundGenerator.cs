﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using static Perlin;

public class PlayGroundGenerator : TilemapGenerator
{
    [SerializeField] private bool destroyAtGetTile;

    #region Methods
    /// <summary>
    /// Places all the tiles (GroundTile) according to the value of each Coordinate of the input grid and enables them.
    /// If there are some pre-placed tiles, they are cloned and the clones are enabled. 
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="grid"></param>
    protected override void GenerateGround(Vector2Int start, Vector2Int end, out Coordinate[,] grid)
    {
        int xMin = Mathf.Min(start.x, end.x);
        int xMax = Mathf.Max(start.x, end.x);
        int yMin = Mathf.Min(start.y, end.y);
        int yMax = Mathf.Max(start.y, end.y);

        grid = gridGenerator.GenerateGrid(start, end);

        Vector3Int tilePosition = Vector3Int.zero;
        Tile currentPosTile;
        GroundTile currentPosGroundTile;

        int x, y;
        for (x = 0; x <= xMax - xMin; x++)
        {
            for (y = 0; y <= yMax - yMin; y++)
            {
                tilePosition.Set(xMin + x, yMin + y, tilePosition.z);
                if (MyTilemap.GetTile(tilePosition) == null || overwritePrePlacedTiles)
                {
                    ChooseTile(grid[x, y], out currentPosTile);
                    if (currentPosTile)
                    {
                        currentPosTile = Instantiate(currentPosTile);

                        currentPosGroundTile = (GroundTile)currentPosTile;
                        currentPosGroundTile.Enable(tilePosition, MyTilemap);
                        MyTilemap.SetTile(tilePosition, currentPosGroundTile);
                        //currentPosTile.Enable();
#if UNITY_EDITOR
                        CountSpawnedTile(currentPosGroundTile.tileType);
#endif
                    }
                }
                else
                {
                    currentPosGroundTile = MyTilemap.GetTile<GroundTile>(tilePosition);
                    currentPosGroundTile = Instantiate(currentPosGroundTile);
                    currentPosGroundTile.Enable(tilePosition, MyTilemap);
                    MyTilemap.SetTile(tilePosition, currentPosGroundTile);
                }
            }
        }
    }

    /// <summary>
    /// Returns the GroundTile in the input position.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public GroundTile GetTile(Vector3 position)
    {
        Vector3 pos = position - MyTilemap.transform.position/* + (Vector3)detectionOffset*/;
        Vector3Int intPos = new Vector3Int(Mathf.FloorToInt(pos.x / grid.cellSize.x), Mathf.FloorToInt(pos.y / grid.cellSize.y), Mathf.FloorToInt(pos.z));
        if (destroyAtGetTile)
            RemoveTile(intPos);
        return MyTilemap.GetTile<GroundTile>(intPos);
    }

    /// <summary>
    /// Disables and removes a single GroundTile from the input position.
    /// </summary>
    /// <param name="position"></param>
    protected override void RemoveTile(Vector3Int position)
    {
        if (MyTilemap.GetTile<GroundTile>(position) != null)
            MyTilemap.GetTile<GroundTile>(position).Disable();
        else
            Debug.LogWarning("Tile at position: " + position + " doesn't exist");
        base.RemoveTile(position);
    }
    #endregion

#if UNITY_EDITOR
    private void CountSpawnedTile(GroundTile.TileType type)
    {
        switch (type)
        {
            case GroundTile.TileType.Coal:
                DebugManager.Instance.spawnedCoal++;
                break;
            case GroundTile.TileType.Dirt:
                DebugManager.Instance.spawnedDirt++;
                break;
            case GroundTile.TileType.Stone:
                DebugManager.Instance.spawnedStone++;
                break;
            case GroundTile.TileType.Ruby:
                DebugManager.Instance.spawnedGem++;
                break;
            case GroundTile.TileType.lava:
                DebugManager.Instance.spawnedLava++;
                break;
        }
    }
#endif
}
