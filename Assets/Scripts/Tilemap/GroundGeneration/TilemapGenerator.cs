﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using static Perlin;

public class TilemapGenerator : MonoBehaviour
{
    public enum Direction
    {
        North = 0,
        East = 1,
        South = 2,
        West = 3
    }

    [System.Serializable]
    protected struct Bounds
    {
        public int up;
        public int right;
        public int down;
        public int left;

        public Bounds(int _up, int _right, int _down, int _left)
        {
            up = _up;
            right = _right;
            down = _down;
            left = _left;
        }
    }

    [System.Serializable]
    public class PlacementData
    {
        public Tile tile;
        public bool useRange;
        public bool useDepth;
        //[ShowIf("isFirst")]
        //public AnimationCurve minRangeCurve;
        [ShowIf("notLast")]
        public AnimationCurve rangeProgressionCurve;
        public float minRange, maxRange;
        public int depth;

        //public bool isFirst = false;
        [HideInInspector] public bool notLast = false;
    }

    #region Variables
    [Header("Components")]
    protected Grid grid;
    [Header("Generation")]
    [SerializeField] protected GridGenerator gridGenerator;
    [SerializeField] private PlacementData[] placements;
    [SerializeField] private Vector2Int offset;
    [SerializeField] private int size;
    [SerializeField] protected bool overwritePrePlacedTiles = false;
    [SerializeField] protected Progression placementsProgression;
    private float minPlacementValue = 0f;
    private float maxPlacementValue = 2f;

    //[Header("Target")]
    //[SerializeField]
    //private Transform target;
    //[SerializeField]
    //private Bounds targetBounds;

    protected Bounds gridBounds;

    [Header("Debug")]
    [SerializeField] private GameObject quad;
    private MeshRenderer quadRenderer;
    private Texture2D texture;

    #region Cached
    private float northLimit;
    private float eastLimit;
    private float southLimit;
    private float westLimit;
    private float northMinDistance;
    private float eastMinDistance;
    private float southMinDistance;
    private float westMinDistance;
    #endregion
    #endregion

    #region Properties
    public Tilemap MyTilemap { get; private set; }
    #endregion

    #region Monobehaviour Methods
    private void Awake()
    {
        Init();
    }

    private void OnValidate()
    {
        int i;
        for (i = 0; i < placements.Length; i++)
        {
            //placements[i].isFirst = (i == 0);
            placements[i].notLast = (i != placements.Length - 1);
        }
    }

    //public void Update()
    //{
    //    if (target)
    //        MoveWithTarget();
    //}
    #endregion

    #region Methods

    #region Initialization
    /// <summary>
    /// Initializes the tilemap generator.
    /// </summary>
    private void Init()
    {
        gridGenerator.Init();

        grid = GetComponentInParent<Grid>();
        MyTilemap = GetComponent<Tilemap>();
        //tilemap.ClearAllTiles();

        Coordinate[,] gridToBuild;
        BuildGround(out gridToBuild);

        if (quad)
            quadRenderer = quad.GetComponent<MeshRenderer>();

        if (DebugManager.Instance.DebugMode && quadRenderer)
        {
            texture = GenerateTexture(gridToBuild);
            quadRenderer.material.mainTexture = texture;
        }
    }
    #endregion

    #region Generation
    /// <summary>
    /// Places all the tiles (Tile) according to the value of each Coordinate of the input grid.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="grid"></param>
    protected virtual void GenerateGround(Vector2Int start, Vector2Int end, out Coordinate[,] grid)
    {
        int xMin = Mathf.Min(start.x, end.x);
        int xMax = Mathf.Max(start.x, end.x);
        int yMin = Mathf.Min(start.y, end.y);
        int yMax = Mathf.Max(start.y, end.y);

        grid = gridGenerator.GenerateGrid(start, end);

        Vector3Int tilePosition = Vector3Int.zero;
        Tile currentPosTile;

        int x, y;
        
        for (x = 0; x <= xMax - xMin; x++)
        {
            for (y = 0; y <= yMax - yMin; y++)
            {
                tilePosition.Set(xMin + x, yMin + y, tilePosition.z);
                if (MyTilemap.GetTile(tilePosition) == null || overwritePrePlacedTiles)
                {
                    ChooseTile(grid[x, y], out currentPosTile);
                    if (currentPosTile)
                    {
                        currentPosTile = Instantiate(currentPosTile);
                        MyTilemap.SetTile(tilePosition, currentPosTile);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Creates the grid of coordinates.
    /// </summary>
    /// <param name="_grid"></param>
    public void BuildGround(out Coordinate[,] _grid)
    {
        GenerateGround(offset, Vector2Int.one * size + offset, out _grid);
        gridBounds = new Bounds(offset.y + _grid.GetLength(1) - 1, offset.x + _grid.GetLength(0) - 1, offset.y, offset.x);
    }
    #endregion

    #region GridMovement

    /// <summary>
    /// Moves the tilemap, adding and removing lines from it.
    /// </summary>
    /// <param name="direction"></param>
    public void Move(Direction direction)
    {
        //int cyclesIndex;
        switch (direction)
        {
            case Direction.North:
                //for (cyclesIndex = 0; cyclesIndex < linesForEachMovement; cyclesIndex++)
                //{
                Remove(Direction.South);
                Add(Direction.North);
                //}
                break;
            case Direction.East:
                //for (cyclesIndex = 0; cyclesIndex < linesForEachMovement; cyclesIndex++)
                //{
                Remove(Direction.West);
                Add(Direction.East);
                //}
                break;
            case Direction.South:
                //for (cyclesIndex = 0; cyclesIndex < linesForEachMovement; cyclesIndex++)
                //{
                Remove(Direction.North);
                Add(Direction.South);
                //}
                break;
            case Direction.West:
                //for (cyclesIndex = 0; cyclesIndex < linesForEachMovement; cyclesIndex++)
                //{
                Remove(Direction.East);
                Add(Direction.West);
                //}
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Adds a line of terrain in the desired direction.
    /// </summary>
    /// <param name="direction"></param>
    private void Add(Direction direction)
    {
        Vector2Int start;
        Vector2Int end;

        switch (direction)
        {
            case Direction.North:
                gridBounds.up++;
                start = new Vector2Int(gridBounds.left, gridBounds.up);
                end = new Vector2Int(gridBounds.right, gridBounds.up);
                break;
            case Direction.East:
                gridBounds.right++;
                start = new Vector2Int(gridBounds.right, gridBounds.down);
                end = new Vector2Int(gridBounds.right, gridBounds.up);
                break;
            case Direction.South:
                gridBounds.down--;
                start = new Vector2Int(gridBounds.left, gridBounds.down);
                end = new Vector2Int(gridBounds.right, gridBounds.down);
                break;
            case Direction.West:
                gridBounds.left--;
                start = new Vector2Int(gridBounds.left, gridBounds.down);
                end = new Vector2Int(gridBounds.left, gridBounds.up);
                break;
            default:
                start = Vector2Int.zero;
                end = Vector2Int.zero;
                break;
        }
        Coordinate[,] outGrid;
        GenerateGround(start, end, out outGrid);
    }

    /// <summary>
    /// Removes a line of terrain at the desired direction.
    /// </summary>
    /// <param name="direction"></param>
    private void Remove(Direction direction)
    {
        Vector3Int position = Vector3Int.zero;

        int i;
        switch (direction)
        {
            case Direction.North:
                for (i = gridBounds.left; i <= gridBounds.right; i++)
                {
                    position.Set(i, gridBounds.up, position.z);
                    RemoveTile(position);
                }
                gridBounds.up--;
                break;
            case Direction.East:
                for (i = gridBounds.down; i <= gridBounds.up; i++)
                {
                    position.Set(gridBounds.right, i, position.z);
                    RemoveTile(position);
                }
                gridBounds.right--;
                break;
            case Direction.South:
                for (i = gridBounds.left; i <= gridBounds.right; i++)
                {
                    position.Set(i, gridBounds.down, position.z);
                    RemoveTile(position);
                }
                gridBounds.down++;
                break;
            case Direction.West:
                for (i = gridBounds.down; i <= gridBounds.up; i++)
                {
                    position.Set(gridBounds.left, i, position.z);
                    RemoveTile(position);
                }
                gridBounds.left++;
                break;
        }
    }

    /// <summary>
    /// Removes a single tile from the input position.
    /// </summary>
    /// <param name="position"></param>
    protected virtual void RemoveTile(Vector3Int position)
    {
        MyTilemap.SetTile(position, null);
    }
    #endregion

    #region Tile detection
    /// <summary>
    /// Returns the tile to place according to the coordinate received as input.
    /// </summary>
    /// <param name="coordinate"></param>
    /// <param name="outTile"></param>
    protected void ChooseTile(Coordinate coordinate, out Tile outTile)
    {
        float minValue;
        float maxValue;

        float progressionValue = placementsProgression.Evaluate();

        int i;
        for (i = 0; i < placements.Length; i++)
        {
            if (placements[i].useRange)
            {
                minValue = (i > 0) ? placements[i - 1].rangeProgressionCurve.Evaluate(progressionValue) : minPlacementValue;
                maxValue = (i < placements.Length - 1) ? placements[i].rangeProgressionCurve.Evaluate(progressionValue) : maxPlacementValue;
                if (coordinate.value < minValue || coordinate.value >= maxValue)
                    continue;
            }
            if (placements[i].useDepth)
                if (coordinate.depth != placements[i].depth)
                    continue;

            outTile = placements[i].tile;
            return;
        }
        outTile = null;
    }
    #endregion

    #region Debug
    /// <summary>
    /// Generates a texture to apply on a quad, for DEBUG purposes.
    /// </summary>
    /// <param name="perlinGrid"></param>
    /// <returns></returns>
    private Texture2D GenerateTexture(Coordinate[,] perlinGrid)
    {
        Texture2D resultTexture = new Texture2D(perlinGrid.GetLength(0), perlinGrid.GetLength(1));

        int x, y;
        float color;
        for (x = 0; x < resultTexture.width; x++)
        {
            for (y = 0; y < resultTexture.height; y++)
            {
                color = perlinGrid[x, y] != null ? perlinGrid[x, y].value : 0;
                resultTexture.SetPixel(x, y, new Color(color, color, color));
            }
        }
        resultTexture.Apply();
        return resultTexture;
    }
    #endregion
    #endregion
}
