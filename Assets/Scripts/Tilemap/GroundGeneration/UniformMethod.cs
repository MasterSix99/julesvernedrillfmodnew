﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Perlin;

[CreateAssetMenu(fileName = "so_genMtd_uniform_1", menuName = "Terrain Generation/Generation Methods/Uniform Method")]
public class UniformMethod : GenerationMethod
{
    /// <summary>
    /// Generates a grid with the same Coordinate's value and depth for each cell.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public override Coordinate[,] GenerateGrid(Vector2Int start, Vector2Int end)
    {
        int minX = Mathf.Min(start.x, end.x);
        int maxX = Mathf.Max(start.x, end.x);
        int minY = Mathf.Min(start.y, end.y);
        int maxY = Mathf.Max(start.y, end.y);

        Coordinate[,] resultGrid = null;
        resultGrid = new Coordinate[maxX - minX, maxY - minY];

        int x, y;
        for (x = 0; x < resultGrid.GetLength(0); x++)
        {
            for (y = 0; y < resultGrid.GetLength(1); y++)
            {
                resultGrid[x, y] = new Coordinate(0f);
            }
        }

        return resultGrid;
    }
}
