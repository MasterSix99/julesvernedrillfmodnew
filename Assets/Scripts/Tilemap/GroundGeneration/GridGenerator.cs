﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GridGenerator
{
    [SerializeField]
    private GenerationMethod generationMethod;

    /// <summary>
    /// Clones the generationMethod and setups it.
    /// </summary>
    public void Init()
    {
        generationMethod = Object.Instantiate(generationMethod);
        generationMethod.Setup(null);
    }

    /// <summary>
    /// Generates a grid of Coordinate with the given start and end position.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public Perlin.Coordinate[,] GenerateGrid(Vector2Int start, Vector2Int end)
    {
        return generationMethod.GenerateGrid(start, end);
    }
}
