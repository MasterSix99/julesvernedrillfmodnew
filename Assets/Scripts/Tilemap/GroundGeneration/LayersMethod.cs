﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Perlin;

[CreateAssetMenu(fileName = "so_genMtd_layer_1", menuName = "Terrain Generation/Generation Methods/Layers' Method")]
public class LayersMethod : GenerationMethod
{
    [System.Serializable]
    public struct LayerData
    {
        public float minValue;
        public float maxValue;
        [Space(5)]
        public float scale;
        [HideInInspector]
        public Vector2 offset;
    }

    /// <summary>
    /// Amount of layers we want to overlap
    /// </summary>
    public LayerData[] layers;
    public bool fillEmpties;
    //[ShowIf("fillEmpties")]
    public Coordinate[] emptiesValues;
    [SerializeField][ShowIf("fillEmpties")]
    private DataFlow emptiesValuesFlow;

    protected override void OnAwake()
    {
        int i;
        for (i = 0; i < layers.Length; i++)
        {
            layers[i].offset = new Vector2(Random.Range(0f, 999999f), Random.Range(0f, 999999f));
        }
    }

    /// <summary>
    /// Generates multiple grids and composes a final grid with just specific values from the previous ones (overlaps more grids). The result cells have different depth values.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    public override Coordinate[,] GenerateGrid(Vector2Int start, Vector2Int end)
    {
        Coordinate[,] resultGrid = null;

        Coordinate[,] perlin;
        int i;
        for (i = 0; i < layers.Length; i++)
        {
            perlin = Generate(start, end, layers[i].offset, layers[i].scale, i);

            if (resultGrid == null)
            {
                resultGrid = new Coordinate[perlin.GetLength(0), perlin.GetLength(1)];
            }
            CopyRange(perlin, ref resultGrid, i);
        }
        if (fillEmpties)
            FillEmpties(ref resultGrid);
        return resultGrid;
    }

    /// <summary>
    /// Copies coordinates with a value between a min and a max from a given grid to another given one.
    /// Min and max values are taken from "layers" array, with and index passed as parameter
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <param name="layerRange"></param>
    public void CopyRange(Coordinate[,] from, ref Coordinate[,] to, int layerRange)
    {
        int x, y;
        
        for (x = 0; x < from.GetLength(0); x++)
        {
            for (y = 0; y < from.GetLength(1); y++)
            {
                if (to[x, y] == null)
                {
                    if (from[x, y].value >= layers[layerRange].minValue && from[x, y].value < layers[layerRange].maxValue)
                    {
                        to[x, y] = from[x, y];
                    }
                }
            }
        }
    }

    /// <summary>
    /// Given a grid of Coordinate, if some cells are null, fills them with new Coordinates.
    /// </summary>
    /// <param name="gridToFill"></param>
    public void FillEmpties(ref Coordinate[,] gridToFill)
    {
        int x, y;

        switch (emptiesValuesFlow)
        {
            case DataFlow.Random:
                for (x = 0; x < gridToFill.GetLength(0); x++)
                {
                    for (y = 0; y < gridToFill.GetLength(1); y++)
                    {
                        if (gridToFill[x, y] == null)
                        {
                            gridToFill[x, y] = emptiesValues[Random.Range(0, emptiesValues.Length)];
                        }
                    }
                }
                break;
            case DataFlow.RoundRobin:
                int index = 0;
                for (x = 0; x < gridToFill.GetLength(0); x++)
                {
                    for (y = 0; y < gridToFill.GetLength(1); y++)
                    {
                        if (gridToFill[x, y] == null)
                        {
                            gridToFill[x, y] = emptiesValues[index];
                            if (index >= emptiesValues.Length - 1)
                                index = 0;
                            else
                                index++;
                        }
                    }
                }
                break;
        }
    }
}
