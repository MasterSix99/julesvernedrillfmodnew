﻿using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilemapMovement : MonoBehaviour
{
    [SerializeField]
    protected bool canMove = true;
    [Header("Movement infos")]
    [SerializeField] protected FloatVariable currentSpeed;
    [SerializeField] protected Transform drillTransform;

    [SerializeField] private FloatVariable depthVar;
    [SerializeField] private FloatVariable depthToReach;

    private Vector3 startPosition;
    private float deltaDepth;

    private void Start()
    {
        startPosition = transform.position;
        depthVar.Value = 0f;
    }

    private void Update()
    {
        if (canMove)
        {
            Move();
        }
    }

    /// <summary>
    /// Translates the object following the drill's up vector.
    /// </summary>
    private void Move()
    {
        transform.Translate(drillTransform.up * currentSpeed.Value * Time.deltaTime, Space.World);
        deltaDepth = transform.position.y - startPosition.y;
        if (depthVar.Value != deltaDepth)
            depthVar.Value = deltaDepth;
        if (depthVar.Value >= depthToReach.Value)
            EventManager.TriggerEvent(EventsID.DESTINATIONREACHED);
    }
}
