﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrillTrailGenerator : MonoBehaviour
{
    [SerializeField]
    private Transform drill;
    [SerializeField]
    private GameFramework.FloatVariable speed;

    private void Update()
    {
        transform.rotation = drill.transform.rotation;
        transform.Translate(-transform.up * (speed.Value/5) * Time.deltaTime, Space.World);
    }
}
