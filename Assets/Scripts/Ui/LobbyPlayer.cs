﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.Events;

public class LobbyPlayer : MonoBehaviour
{
    #region VARIABLES
    [SerializeField]
    private int m_playerIdAllowed = 1;
    private bool m_isOwned;
    #endregion

    #region PROPERTIES
    public int PlayerIdAllowed
    {
        get { return m_playerIdAllowed; }
    }
    public bool IsOwned
    {
        get { return m_isOwned; }
        private set
        {
            bool lastVal = m_isOwned;
            m_isOwned = value;
            if(m_isOwned != lastVal) {
                if (m_isOwned)
                    playerConnectedEvent?.Invoke();
                else
                    playerLeftEvent?.Invoke();
            }
        }
    }
    #endregion

    #region EVENTS
    [SerializeField]
    private UnityEvent playerConnectedEvent;
    [SerializeField]
    private UnityEvent playerLeftEvent;
    #endregion

    #region MONOBEHAVIOURS

    private void Start()
    {
        foreach(PlayerSession player in DataManager.Instance.Players)
        {
            if (player.PlayerID == m_playerIdAllowed && player.IsEnabled)
                OnPlayerJoined(player);
        }
    }

    private void OnEnable()
    {
        EventManager.StartListening<PlayerSession>(EventsID.PLAYERJOINED, OnPlayerJoined);
        EventManager.StartListening<PlayerSession>(EventsID.PLAYERLEFT, OnPlayerLeft);
    }

    private void OnDisable()
    {
        EventManager.StopListening<PlayerSession>(EventsID.PLAYERJOINED, OnPlayerJoined);
        EventManager.StopListening<PlayerSession>(EventsID.PLAYERLEFT, OnPlayerLeft);
    }
    #endregion


    #region METHODS
    private void OnPlayerJoined(PlayerSession player)
    {
        if(player.PlayerID == m_playerIdAllowed)
            IsOwned = true;
    }
    private void OnPlayerLeft(PlayerSession player)
    {
        if (player.PlayerID == m_playerIdAllowed)
            IsOwned = false;
    }
    #endregion
}
