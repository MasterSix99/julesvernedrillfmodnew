﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AchievementButton : MonoBehaviour, IPointerEnterHandler, ISelectHandler
{
    [SerializeField]
    private Achievement achievement;
    public Sprite imgLocked;
    public Sprite imgUnlocked;
    public Sprite imgSelected;
    public Sprite frameLocked;
    public Sprite frameUnLocked;
    public Sprite frameSelected;

    [Space]
    [SerializeField]
    private Button btn;
    [SerializeField]
    private Image imageAchievement;
    [SerializeField]
    private Image imageFrame;

    public Action PointerEnterEvent;

    public Achievement Achievement
    {
        get { return achievement; }
        set {
            achievement = value;
            imgLocked = achievement.uiInfo.lockedSprite;
            imgUnlocked = achievement.uiInfo.unlockedSprite;
            imgSelected = achievement.uiInfo.selectedSprite;
            frameLocked = achievement.uiInfo.frameLockedSprite;
            frameUnLocked = achievement.uiInfo.frameUnlockedSprite;
            frameSelected = achievement.uiInfo.frameSelectedSprite;
            UpdateButton();
        }
    }

    private void Awake()
    {
        if(btn == null)
            btn = GetComponent<Button>();
        if(imageAchievement == null)
            imageAchievement = GetComponent<Image>();

    }

    private void OnDestroy()
    {
        btn.onClick.RemoveAllListeners();
    }

    void UpdateButton()
    {
        if (achievement.achievementDataToSave.selected)
        {
            imageAchievement.sprite = imgSelected;
            imageFrame.sprite = frameSelected;
        }
        else if (achievement.achievementDataToSave.unlocked)
        {
            imageAchievement.sprite = imgUnlocked;
            imageFrame.sprite = frameUnLocked;
        }
        else
        {
            imageAchievement.sprite = imgLocked;
            imageFrame.sprite = frameLocked;
        }
        
    }

    public void OnSelectionChanged()
    {
        UpdateButton();

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        PointerEnterEvent?.Invoke();
    }

    public void OnSelect(BaseEventData eventData)
    {
        PointerEnterEvent?.Invoke();
    }
}
