﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class GameSettings
{
    public int resolutionWidth;
    public int resolutionHeight;
    public int qualityIndex;
    public bool isFullScreen;
    public float musicLevel;
    public float sfxLevel;
} 

public class MenuOptions : MonoBehaviour
{
    [SerializeField]
    private TMPro.TMP_Dropdown dropdownResolution;
    [SerializeField]
    private TMPro.TMP_Dropdown dropdownQuality;
    [SerializeField]
    private Toggle fullScreenToggle;
    [SerializeField]
    private Slider sliderMusic;
    [SerializeField]
    private Slider sliderSfx;

    private Resolution[] resolutions;
    private string[] qualityNames;

    private GameSettings settings;


    // Start is called before the first frame update
    void Start()
    {
        LoadSettings();
        //Populate resolutions dropdown and set selected one
        resolutions = Screen.resolutions;
        List<string> resolutionsList = new List<string>();
        int currentResolutionIndex = 0;
        int i = 0;
        int len = resolutions.Length;
        for(i = 0; i < len; i++)
        {
            resolutionsList.Add(resolutions[i].width + "x" + resolutions[i].height);
            if (Screen.currentResolution.width == resolutions[i].width && Screen.currentResolution.height == resolutions[i].height)
                currentResolutionIndex = i;
        }
        dropdownResolution.ClearOptions();
        dropdownResolution.AddOptions(resolutionsList);
        dropdownResolution.value = currentResolutionIndex;
        dropdownResolution.RefreshShownValue();

        //Populate and set 
        qualityNames = QualitySettings.names;
        List<string> qualityOptions = new List<string>();
        int currentQualityIndex = 0;
        i = 0;
        len = qualityNames.Length;
        for (i = 0; i < len; i++)
        {
            qualityOptions.Add(qualityNames[i]);
            if (QualitySettings.GetQualityLevel() == i)
                currentQualityIndex = i;
        }
        dropdownQuality.ClearOptions();
        dropdownQuality.AddOptions(qualityOptions);
        dropdownQuality.value = currentQualityIndex;
        dropdownQuality.RefreshShownValue();

        //Initialize fullscreen toggle
        fullScreenToggle.isOn = Screen.fullScreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        Screen.SetResolution(resolutions[resolutionIndex].width, resolutions[resolutionIndex].height, Screen.fullScreen);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void SetMusicVolume(float volume)
    {

    }

    public void SetSfxVolume(float volume)
    {

    }

    public void SaveSettings()
    {
        GameSettings gSettings = new GameSettings();
        // Uso Screen.width/height invece di Screen.Resolution.width/height perchè il secondo non funziona in windowed mode mentre il primo si
        gSettings.resolutionWidth = Screen.width/*Screen.currentResolution.width*/;
        gSettings.resolutionHeight = Screen.height/*Screen.currentResolution.height*/;
        /*Debug.LogError("RES W salvata = " + gSettings.resolutionWidth);
        Debug.LogError("RES H salvata = " + gSettings.resolutionHeight);*/
        gSettings.qualityIndex = dropdownQuality.value;
        gSettings.isFullScreen = fullScreenToggle.isOn;
        gSettings.musicLevel = sliderMusic.value;
        gSettings.sfxLevel = sliderSfx.value;
        SaveController.Instance.SaveObject("GameSettings", gSettings);
    }

    public void LoadSettings()
    {
        var loaded = SaveController.Instance.LoadObject("GameSettings");
        if(loaded != null) { 
            settings = (GameSettings)loaded;
            Screen.SetResolution(settings.resolutionWidth, settings.resolutionHeight, settings.isFullScreen);
            QualitySettings.SetQualityLevel(settings.qualityIndex);
        }
    }

}
