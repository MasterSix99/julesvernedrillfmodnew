﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.Events;

public class Lobby : MonoBehaviour
{
    #region VARIABLES
    public int maxPlayers;

    [SerializeField]
    private List<PlayerSession> m_players;
    private List<Player> m_rewiredPlayers;
    private bool allPlayerJoyned;
    #endregion

    #region PROPERTIES

    #endregion

    #region EVENTS
    [SerializeField]
    private UnityEvent<PlayerSession> playerJoynedEvent;
    private UnityEvent<PlayerSession> playerDisconnectedEvent;
    #endregion

    #region MONOBEHAVIOUR
    private void Awake()
    {
    }

    private void OnEnable()
    {
        if (DataManager.Instance) {
            m_players = new List<PlayerSession>(DataManager.Instance.Players);
            InputUtils.SetOnlyFirstPlayerMapEnabled("UI");
        }
        else
            m_players = new List<PlayerSession>();

        ReInput.ControllerPreDisconnectEvent += OnControllerPreDisconnected;
        EventManager.StartListening<PlayerSession>(EventsID.PLAYERJOINED, OnPlayerJoined);
        EventManager.StartListening<PlayerSession>(EventsID.PLAYERLEFT, OnPlayerLeft);

        InputUtils.UpdateUiMapAssignment(m_players);
    }

    private void OnDisable()
    {
        ReInput.ControllerDisconnectedEvent -= OnControllerPreDisconnected;
        EventManager.StopListening<PlayerSession>(EventsID.PLAYERJOINED, OnPlayerJoined);
        EventManager.StopListening<PlayerSession>(EventsID.PLAYERLEFT, OnPlayerLeft);
    }

    void Update()
    {
        if (!allPlayerJoyned) { 
            foreach(Player rewiredPlayer in ReInput.players.Players)
            {
                if (!allPlayerJoyned && rewiredPlayer.GetButtonDown("Join Lobby")){
                    JoinLobby(rewiredPlayer);
                }
            }
        }
        foreach(PlayerSession player in m_players)
        {
            if (player.IsEnabled) {
                Player rewiredPlayer = ReInput.players.GetPlayer(player.RewiredPlayerId);
                if(rewiredPlayer.GetButtonLongPressDown("Exit Lobby"))
                {
                    ExitLobby(rewiredPlayer);
                }
            }
        }
    }
    #endregion

    #region LOBBY MANAGEMENT
    /// <summary>
    /// Join lobby using PlayerSession
    /// </summary>
    /// <param name="player">the player that must join</param>
    public void JoinLobby(PlayerSession player)
    {
        if (player != null)
        {
            EventManager.TriggerEvent<PlayerSession>(EventsID.PLAYERJOINED, player);
            playerJoynedEvent?.Invoke(player);
        }
    }
    /// <summary>
    /// Join Lobby using rewiredPlayer, this function assign joined rewired player id to first sessionPlayer and then move old rewiredPlayerID to next player
    /// </summary>
    /// <param name="rewiredPlayer">the rewiredPlayer</param>
    private void JoinLobby (Player rewiredPlayer)
    {
        if (CheckAllPlayerJoyned() || rewiredPlayer == null || IsPlayerJoined(rewiredPlayer))
            return;


        PlayerSession availablePlayer = GetFirstAvailablePlayerSlot();

        int tmpId = availablePlayer.RewiredPlayerId;
        availablePlayer.RewiredPlayerId = rewiredPlayer.id;
        foreach(PlayerSession player in m_players)
        {
            if (player.RewiredPlayerId == rewiredPlayer.id && player != availablePlayer)
                player.RewiredPlayerId = tmpId;
        }
        if (availablePlayer != null)
        {
            JoinLobby(availablePlayer);
        }

    }
    /// <summary>
    /// Called When player has joined the game
    /// </summary>
    /// <param name="player"></param>
    private void OnPlayerJoined(PlayerSession player)
    {
        if(player != null)
        {
            int playersCount = GetPlayerJoinedCount();
            player.IsEnabled = true;
            
            if(playersCount < 1)
                InputUtils.UpdateUiMapAssignment(m_players);
        }
    }
    
    public void ExitLobby(PlayerSession player)
    {
        player.IsEnabled = false;
        EventManager.TriggerEvent<PlayerSession>(EventsID.PLAYERLEFT, player);
        playerDisconnectedEvent?.Invoke(player);
    }
    private void ExitLobby(Player rewiredPlayer)
    {
        PlayerSession player = GetPlayer(rewiredPlayer);
        ExitLobby(player);
    }
    private void OnPlayerLeft(PlayerSession player)
    {
        if (player != null)
        {
            player.IsEnabled = false;
            InputUtils.UpdateUiMapAssignment(player);
            player.RewiredPlayerId = -1;

        }
    }
    #endregion

    #region CHECKS
    /// <summary>
    /// Check if rewiredPlayer is associated to a joined player
    /// </summary>
    /// <param name="rewiredPlayer"></param>
    /// <returns></returns>
    private bool IsPlayerJoined(Player rewiredPlayer)
    {
        
        foreach (PlayerSession player in m_players)
        {
            if (player.RewiredPlayerId == rewiredPlayer.id && player.IsEnabled)
                return true;
        }
        return false;
    }
    /// <summary>
    /// Check if all players have joined
    /// </summary>
    /// <returns></returns>
    private bool CheckAllPlayerJoyned()
    {
        if (m_players.Count < maxPlayers)
            return false;

        foreach(PlayerSession player in m_players)
        {
            if (!player.IsEnabled)
                return false;
        }

        return true;
    }
    #endregion

    #region GET PLAYER
    /// <summary>
    /// Get first player that is not joined
    /// </summary>
    /// <returns></returns>
    private PlayerSession GetFirstAvailablePlayerSlot()
    {
        for(int i = 0; i< m_players.Count; i++)
        {
            if (!m_players[i].IsEnabled)
                return m_players[i];
        }
        
        return null;
    }
    /// <summary>
    /// Return a PlayerSession with the rewiredPlayer sent by param 
    /// </summary>
    /// <param name="rewiredPlayer"></param>
    /// <returns></returns>
    private PlayerSession GetPlayer(Player rewiredPlayer)
    {
        foreach(PlayerSession player in m_players)
        {
            if (player.RewiredPlayerId== rewiredPlayer.id)
                return player;
        }
        return null;
    }
    /// <summary>
    /// Get rewired player associated to controller
    /// </summary>
    /// <param name="controllerType"></param>
    /// <param name="controllerId"></param>
    /// <returns></returns>
    private Player GetRewiredPlayer(ControllerType controllerType, int controllerId)
    {
        foreach (Player rewiredPlayer in ReInput.players.Players)
        {
            if (rewiredPlayer.controllers.GetController(controllerType,controllerId) != null)
            {
                return  rewiredPlayer;
            }
        }
        return null;
    }

    private int GetPlayerJoinedCount()
    {
        int joinedCount = 0;
        foreach (PlayerSession pSession in m_players)
        {
            if (pSession.IsEnabled)
                joinedCount++;
        }
        return joinedCount;
    }
    #endregion

    #region CONTROLLERS CHECKS
    private void OnControllerPreDisconnected(ControllerStatusChangedEventArgs controllerArgs)
    {
        Debug.Log("LOBBY -> ONControllerPreDisconnected");
        Player owner = GetRewiredPlayer(controllerArgs.controllerType, controllerArgs.controllerId);
        if (owner != null)
            ExitLobby(owner);
    }
    #endregion


}
