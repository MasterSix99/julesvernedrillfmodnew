﻿using UnityEngine;
using UnityEngine.UI;

public class IntegrityBar : MonoBehaviour
{
    [SerializeField] private Slider integritySlider;
    [SerializeField] private GameObject[] brokenElements;

    private int currentBrokenIndex = 0;

    /// <summary>
    /// Updates the value of the slider.
    /// </summary>
    /// <param name="newValue"></param>
    public void UpdateValue(float newValue)
    {
        integritySlider.value = newValue;
    }

    /// <summary>
    /// Enables the next "broken" element on the slider.
    /// </summary>
    public void Break()
    {
        if (currentBrokenIndex <= brokenElements.Length - 1)
        {
            brokenElements[currentBrokenIndex].SetActive(true);
            currentBrokenIndex++;
        }
    }
}
