﻿using GameFramework;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class AchievementPanel : Panel
{
    [Header("Needed References And Configuration")]
    [SerializeField]
    private GameObject achievementContainer;
    [SerializeField]
    private GameObject achievementTemplateObject;
    [SerializeField]
    private GameObject allAchievementSelectedObj;
    [SerializeField]
    private float allAchievementSelectedTextShowTime = 3;

    [Header("Details references")]
    [SerializeField]
    private TMPro.TextMeshProUGUI detailsTitleLabel;
    [SerializeField]
    private Image detailsImage;
    [SerializeField]
    private TMPro.TextMeshProUGUI detailsDescriptionLabel;
    [SerializeField]
    private TMPro.TextMeshProUGUI detailsProgressAmountlabel;
    [SerializeField]
    private TMPro.TextMeshProUGUI detailsProgressNeededLabel;
    [SerializeField]
    private TMPro.TextMeshProUGUI detailsRewardLabel;
    //public ScrollRect scrollView;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        //Clear container
        foreach (Transform child in achievementContainer.transform)
        {
            Destroy(child.gameObject);
        }

        
        foreach (Achievement achievement in AchievementManager.Instance.Achievements)
        {
            GameObject go;
            AchievementButton btnA;
            Button btnUi;
            go = Instantiate(achievementTemplateObject, achievementContainer.transform);
            if(go != null)
            {
                btnA = go.GetComponent<AchievementButton>();
                btnA.Achievement = achievement;
                btnUi = go.GetComponent<Button>();
                btnUi.onClick.AddListener(()=> { Toggle(btnA); });
                btnA.PointerEnterEvent += () => ShowDetails(btnA);
            }
        }

        
        

    }

    private void OnEnable()
    {
        /*MyEventSystem eventSystem = EventSystem.current as MyEventSystem;
        eventSystem.SetSelectedGameObject(achievementContainer.transform.GetChild(0).gameObject);
        Debug.Log("SET SELECTED OBJECT = " + achievementContainer.transform.GetChild(0).gameObject.name);
        Debug.Log("SELECTED OBJECT = " + eventSystem.currentSelectedGameObject.name);*/
    }

    public override void OnPanelOpened()
    {
        m_firstSelectedObject = achievementContainer.transform.GetChild(0).gameObject;
        base.OnPanelOpened();
    }


        /// <summary>
        /// Toggle Achievement Selection
        /// </summary>
        /// <param name="achievementBtn">The achievement button that contains achievement</param>
        public void Toggle(AchievementButton achievementBtn)
    {
        if (achievementBtn.Achievement.achievementDataToSave.unlocked) { 
            if (achievementBtn.Achievement.achievementDataToSave.selected)
                Deselect(achievementBtn);
            else
                Select(achievementBtn);
        }
    }
    /// <summary>
    /// Select an achievement if max achievement selection is not reached
    /// </summary>
    /// <param name="achievementBtn">The achievement button that contains achievement</param>
    private void Select(AchievementButton achievementBtn)
    {
        if (achievementBtn.Achievement.achievementDataToSave.unlocked)
        {
            bool achievementLimitReached = AchievementManager.Instance.CheckAllAchievementsSelected();
            if (achievementLimitReached)
            {
                ShowAllAchievementSelectedPanel();
                Debug.Log("ACHIEVEMENT SELECTION LIMIT REACHED");
            }
            else
            {
                AchievementManager.Instance.SelectAchievement(achievementBtn.Achievement);
                achievementBtn.OnSelectionChanged();
            }
        }
       
    }
    /// <summary>
    /// Deselect an achievement
    /// </summary>
    /// <param name="achievementBtn">The achievement button that contains achievement</param>
    private void Deselect(AchievementButton achievementBtn)
    {
        if (achievementBtn.Achievement.achievementDataToSave.unlocked)
        {
            AchievementManager.Instance.DeselectAchievement(achievementBtn.Achievement);
            achievementBtn.OnSelectionChanged();
        }
    }

    /// <summary>
    /// Show AchievementDetails
    /// </summary>
    /// <param name="achievementBtn">The achievement button that contains achievement</param>
    public void ShowDetails(AchievementButton achievementBtn)
    {
        detailsTitleLabel.text = achievementBtn.Achievement.achievementDataToSave.AchievementName;
        detailsImage.sprite = achievementBtn.Achievement.uiInfo.selectedSprite;
        detailsDescriptionLabel.text = achievementBtn.Achievement.description;
        detailsProgressAmountlabel.text = achievementBtn.Achievement.achievementDataToSave.achievementCompletion.ToString();
        //detailsProgressNeededLabel.text = achievementBtn.Achievement.achievementDataToSave.conditionCompletions.ToString();
        detailsRewardLabel.text = achievementBtn.Achievement.effectDescription;
    }

    public void ShowAllAchievementSelectedPanel()
    {
        StopAllCoroutines();
        allAchievementSelectedObj?.SetActive(true);
        StartCoroutine(PerformActionAfterDelay(allAchievementSelectedTextShowTime, new Action(() => allAchievementSelectedObj?.SetActive(false))));
    }

    IEnumerator PerformActionAfterDelay(float delay,Action action)
    {
        yield return new WaitForSeconds(delay);
        action?.Invoke();
    }
}
