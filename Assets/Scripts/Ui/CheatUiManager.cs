﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameFramework;
using TMPro;

/// <summary>
/// LISTA CHEATS da fare:
/// - Non perdi mai
/// - Crea risorse
/// - Aggiungere / togliere secondi al timer
/// - 
/// </summary>
public class CheatUiManager : Panel
{
    /// <summary>
    /// Textarea where used cheats should be shown
    /// </summary>
    [Space][SerializeField]
    private TextMeshProUGUI cheathListTextArea;

    /// <summary>
    /// Parse text recived from inputBox and trigger right cheat
    /// </summary>
    /// <param name="text">the cheath</param>
    public void ParseText(string text)
    {
        if (text.Contains("playerImmortality"))
        {
            SetPlayerImmortality(1,true);
        }
    }

    private void SetPlayerImmortality(int playerId, bool immortal)
    {
        Debug.Log("SET IMMORTALITY");
    }

    private void SpawnResources()
    {
        Debug.Log("SpawnResources");
    }
}
