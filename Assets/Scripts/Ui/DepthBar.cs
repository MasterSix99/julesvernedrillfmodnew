﻿using GameFramework;
using UnityEngine;
using UnityEngine.Events;

//[ExecuteInEditMode]
public class DepthBar : MonoBehaviour
{
    #region EVENTS 
    public UnityEvent<float> onValueChange;
    #endregion

    [SerializeField]
    private Transform indicator;
    [SerializeField]
    private Transform indicatorArea;

    [SerializeField]
    private float m_minVal = 0;
    [SerializeField]
    private float m_maxVal = 1;
    [SerializeField]
    private FloatVariable m_curentValue;

    //private float previousLerpValue = 0;
    [SerializeField]
    private float t = 5;
    float indicatorStartPoint;
    float indicatorEndPoint;

    #region PROPERTIES
    /// <summary>
    /// Current value setted
    /// </summary>
    public FloatVariable CurrentValue
    {
        get { return m_curentValue; }
        set {
            m_curentValue.Value = Mathf.Clamp(value.Value,MinVal,MaxVal);
            onValueChange.Invoke(m_curentValue.Value);
        }
    }
    /// <summary>
    /// Min val usable
    /// </summary>
    public float MinVal
    {
        get { return m_minVal; }
        set
        {
            m_minVal = value;
        }
    }
    /// <summary>
    /// Max val usable
    /// </summary>
    public float MaxVal
    {
        get { return m_maxVal; }
        set
        {
            m_maxVal = value;
        }
    }
    #endregion



    private void Awake()
    {
        indicatorArea = indicatorArea ?? transform.Find("IndicatorArea");
    }

    // Start is called before the first frame update
    void Start()
    {
        indicatorStartPoint = indicatorArea.localPosition.x - (indicatorArea.localScale.x /2);
        indicatorEndPoint = indicatorArea.localPosition.x + (indicatorArea.localScale.x / 2);
        if (m_curentValue == null) Debug.LogError("Flaotvariable CurrentValue is not set on " + gameObject.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
            CurrentValue.Value += MaxVal / 10;

        UpdateIndicator();
    }

    /// <summary>
    /// Update indicator position based on CurrentValue
    /// </summary>
    void UpdateIndicator()
    {
        m_curentValue.Value = Mathf.Clamp(m_curentValue.Value, MinVal, MaxVal);
        //Caluculate desired indicator position
        float desiredIndicatorPosX = indicatorStartPoint + (CurrentValue.Value / MaxVal * (indicatorEndPoint - indicatorStartPoint));
        // lerp to desired position
        float newIndicatorPosX = Mathf.Lerp(indicator.localPosition.x, desiredIndicatorPosX, Time.deltaTime * t);
        //assign current lerped position
        if(!float.IsNaN(newIndicatorPosX))
            indicator.localPosition = new Vector3(newIndicatorPosX, indicator.localPosition.y);
    }

   


}
