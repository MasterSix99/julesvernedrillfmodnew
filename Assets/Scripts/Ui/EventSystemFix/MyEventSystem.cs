﻿
using UnityEngine;
using UnityEngine.EventSystems;

public class MyEventSystem : EventSystem
{

    public bool avoidEmptySelectedGameObject;
    private GameObject lastSelectedObject;

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if(currentSelectedGameObject == null)
        {
            SetSelectedGameObject(lastSelectedObject);
        }
        else //SE NON è NULL ALLORA CONTROLLO CHE NON SIA CAMBIATO IL CURRENTSELECTED, SE è CAMBIATO AGGIORNO IL LASTSELECTED
        {
            if (currentSelectedGameObject != lastSelectedObject)
                lastSelectedObject = currentSelectedGameObject;
                
        }
    }
}
