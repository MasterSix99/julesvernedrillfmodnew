﻿using Rewired;
using UnityEngine;
using UnityEngine.UI;
public class UiBackButton : MonoBehaviour
{
    private Button button;
    private void Awake()
    {
        button = GetComponent<Button>();
    }
    private void Update()
    {
        foreach (PlayerSession player in DataManager.Instance.Players)
        {
            if (player.IsEnabled)
            {
                Player rewiredPlayer = ReInput.players.GetPlayer(player.RewiredPlayerId);
                if (rewiredPlayer.GetButtonDown("UICancel"))
                {
                    Debug.Log("CLICK");
                    button.OnSubmit(new UnityEngine.EventSystems.BaseEventData(MyEventSystem.current));
                }
            }
        }
    }
}
