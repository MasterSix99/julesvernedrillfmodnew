﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AchievementVisualizer : GameFramework.Panel
{
    [SerializeField]
    private Image achievementImage;
    [SerializeField]
    private TextMeshProUGUI achievementName;
    [SerializeField]
    private TextMeshProUGUI achievementDescription;

    private List<Achievement> achievementsToShow = new List<Achievement>();

    private void OnEnable()
    {
        EventManager.StartListening<Achievement>(EventsID.ACHIEVEMENTUNLOCKED, OnAchievementUnlocked);
    }

    protected override void OnDisable()
    {
        EventManager.StopListening<Achievement>(EventsID.ACHIEVEMENTUNLOCKED, OnAchievementUnlocked);
        base.OnDisable();
    }

    private void ShowAchievement(Achievement unlockedAchievement)
    {
        PopulateAchievementView(achievementsToShow[0]);
        base.OpenPanel();
    }

    private void OnAchievementUnlocked(Achievement unlockedAchievement)
    {
        achievementsToShow.Add(unlockedAchievement);
        if (!IsOpened)
        {
            ShowAchievement(unlockedAchievement);
        }
    }

    public override void OnPanelClosed()
    {
        base.OnPanelClosed();

        achievementsToShow.RemoveAt(0);
        if (achievementsToShow.Count > 0)
        {
            ShowAchievement(achievementsToShow[0]);
        }
    }

    private void PopulateAchievementView(Achievement achievementForUpdating)
    {
        Debug.Log(achievementForUpdating.achievementDataToSave.AchievementName);
        achievementImage.sprite = achievementForUpdating.uiInfo.frameUnlockedSprite;
        achievementName.text = achievementForUpdating.achievementDataToSave.AchievementName;
        achievementDescription.text = achievementForUpdating.description;
    }
}
