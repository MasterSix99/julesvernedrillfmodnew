﻿using DG.Tweening;
using TMPro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoBanner : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textField;
    [SerializeField]
    private DOTweenAnimation tweenAnimation;
    [SerializeField]
    private List<string> randomTexts;
    private Queue<string> conditionalTexts;

    private string lastText;
    // Start is called before the first frame update
    void Start()
    {
        Canvas canvas = GetComponent<Canvas>();
        if (canvas.worldCamera == null) canvas.worldCamera = Camera.main;

        if (textField == null)
            textField = GetComponentInChildren<TextMeshProUGUI>();
        if (tweenAnimation == null && textField != null)
            tweenAnimation = textField.GetComponent<DOTweenAnimation>();

        conditionalTexts = new Queue<string>();
        tweenAnimation.onComplete.AddListener(ShowNextInfo);
        EventManager.StartListening<string>(EventsID.BANNERINFOREQUEST, AddText);
    }

    private void OnDestroy()
    {
        tweenAnimation.onComplete.RemoveAllListeners();
    }

    public void ShowNextInfo()
    {
        string nextInfo = GetNextInfo();

        tweenAnimation.DORewind();
        textField.text = nextInfo;
        tweenAnimation.DOPlayForward();
        lastText = nextInfo;
    }

    private string GetNextInfo()
    {
        string next = "";
        if (conditionalTexts.Count > 0)
            next = conditionalTexts.Dequeue();
        else
        {
            int rnd = Random.Range(0, randomTexts.Count);
            next = randomTexts[rnd];
            if(randomTexts.Count > 1) { 
                while(next == lastText)
                {
                    rnd = Random.Range(0, randomTexts.Count);
                    next = randomTexts[rnd];
                }
            }
        }

        return next;
    }

    public void AddText(string text)
    {
        Debug.Log("AGGIUNGO UN TESTO AL BANNER");
        conditionalTexts.Enqueue(text);
    }
}
