﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsBinder : MonoBehaviour
{
    private StatsManager statsManager;

    [SerializeField]
    private TMPro.TextMeshProUGUI scoreText;
    [Space]
    [SerializeField]
    private TMPro.TextMeshProUGUI depthReachedText;
    [SerializeField]
    private TMPro.TextMeshProUGUI gameLengthText;
    [SerializeField]
    private TMPro.TextMeshProUGUI topSpeedText;
    [SerializeField]
    private TMPro.TextMeshProUGUI mediumSpeedText;
    [SerializeField]
    private TMPro.TextMeshProUGUI machinesBrokenText;
    [Header("Resources")]
    [SerializeField]
    private TMPro.TextMeshProUGUI coalGatheredText;
    [SerializeField]
    private TMPro.TextMeshProUGUI coalTrashedText;
    [SerializeField]
    private TMPro.TextMeshProUGUI coalUsedText;
    [Space]
    [SerializeField]
    private TMPro.TextMeshProUGUI dirtGatheredText;
    [SerializeField]
    private TMPro.TextMeshProUGUI dirtTrashedText;
    [SerializeField]
    private TMPro.TextMeshProUGUI dirtUsedText;
    [Space]
    [SerializeField]
    private TMPro.TextMeshProUGUI gemGatheredText;
    [SerializeField]
    private TMPro.TextMeshProUGUI gemTrashedText;
    [SerializeField]
    private TMPro.TextMeshProUGUI gemUsedText;
    [Space]
    [SerializeField]
    private TMPro.TextMeshProUGUI stoneGatheredText;
    [SerializeField]
    private TMPro.TextMeshProUGUI stoneTrashedText;
    [SerializeField]
    private TMPro.TextMeshProUGUI stoneUsedText;

    private void Awake()
    {
        statsManager = FindObjectOfType<StatsManager>();
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.GAMEEND, UpdateTexts);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.GAMEEND, UpdateTexts);
    }

    /// <summary>
    /// Updates all the texts with the corresponding value in the StatsManager.
    /// </summary>
    public void UpdateTexts()
    {
        scoreText.text = statsManager.Score.ToString("#");

        depthReachedText.text = statsManager.DepthReached.ToString("#");
        gameLengthText.text = statsManager.GameTime.ToString("#");
        topSpeedText.text = statsManager.TopSpeed.ToString("#");
        mediumSpeedText.text = statsManager.AverageSpeed.ToString("#");
        machinesBrokenText.text = statsManager.MachinesBroken.ToString();

        //RESOURCES SECTION
        coalGatheredText.text = statsManager.CoalGathered.ToString();
        coalTrashedText.text = statsManager.CoalTrashed.ToString();
        coalUsedText.text = statsManager.CoalUsed.ToString();

        dirtGatheredText.text = statsManager.DirtGathered.ToString();
        dirtTrashedText.text = statsManager.DirtTrashed.ToString();
        dirtUsedText.text = statsManager.DirtUsed.ToString();

        gemGatheredText.text = statsManager.GemGathered.ToString();
        gemTrashedText.text = statsManager.GemTrashed.ToString();
        gemUsedText.text = statsManager.GemUsed.ToString();

        stoneGatheredText.text = statsManager.StoneGathered.ToString();
        stoneTrashedText.text = statsManager.StoneTrashed.ToString();
        stoneUsedText.text = statsManager.StoneUsed.ToString();
    }
}
