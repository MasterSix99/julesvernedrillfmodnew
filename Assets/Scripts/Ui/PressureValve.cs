﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameFramework;


public class PressureValve : MachineBehaviour
{
    [SerializeField]
    private EngineBehaviour engine;
    [SerializeField]
    private FloatVariable currentPressureVal;
    private Transform playerParent;
    //[SerializeField]
    //private Transform indicator;
    //[SerializeField]
    //private SpriteRenderer background;
    //[SerializeField]
    //private SliderColors[] sliderColors;

    


    //[SerializeField]
    //private float minIndicatorRotation = -90;
    //[SerializeField]
    //private float maxIndicatorRotation = 90;
    [Header("View")]
    [SerializeField]
    private Transform handleTransform;
    [SerializeField]
    private Transform gearTransform;
    [SerializeField]
    private float handleAnimationHeight = -5;
    [SerializeField]
    private float handleAnimationDuration = 1;
    [SerializeField]
    private float handleBackAnimationDuration = 0.5f;
    [SerializeField]
    private float gearRotationSpeed = 70;
    [SerializeField]
    private Transform playerHandleTransform;
    [SerializeField]
    private Transform endAnim;
    [SerializeField]
    private Transform startAnim;
    //[SerializeField]
    //private float minVal = 0;
    //[SerializeField]
    //private float maxVal = 100;

    [Header("Audio")]
    [SerializeField] private GenericEvent controlToggleAudio;

    private float speedMineralCoefficient = 1;
    private bool maxPressionReached = false;
    private Vector3 handleLerpStartPosition;
    private Vector3 handleLerpEndPosition;
    private float handleLerpTime = 0.0f;

    private void Awake()
    {
        if (engine == null)
            Debug.LogError("ERROR: pressurevalve (" + gameObject.name + ") does not have engine assigned");
    }
    protected override void Start()
    {
        base.Start();
        handleLerpStartPosition = handleTransform.position;
        //handleLerpEndPosition = handleLerpStartPosition + Vector3.up * handleAnimationHeight;
        //handleLerpEndPosition = new Vector2(handleLerpStartPosition.x, handleLerpStartPosition.y - handleAnimationHeight);
        handleLerpEndPosition = endAnim.position;
        //if (currentPressureVal != null)
        //    currentPressureVal.onValueChangeAction += UpdateIndicator;
        FmodManager.instance.CreateGenericEnventInstance(ref controlToggleAudio);

        engine.OnBreakUnityEvt.AddListener(OnBreak);
    }
    //private void OnDisable()
    //{
    //    if (currentPressureVal != null)
    //        currentPressureVal.onValueChangeAction -= UpdateIndicator;
    //}

    protected virtual void OnDestroy()
    {
        engine.OnBreakUnityEvt.RemoveListener(OnBreak);
    }

    private void Update()
    {
        if (isOwned)
        {
            if (handleLerpTime < 1)
            {
                handleLerpTime += Time.deltaTime / handleAnimationDuration;
                if (handleLerpTime >= 1)
                    handleLerpTime = Mathf.Clamp01(handleLerpTime);
                handleTransform.position = Vector3.Lerp(startAnim.position, endAnim.position, handleLerpTime);
                //valveTransform.rotation *= Quaternion.Euler(new Vector3(0,0, valveRotationSpeed * Time.deltaTime));
                gearTransform.Rotate(Vector3.forward, gearRotationSpeed * Time.deltaTime);
            }
        }
        else
        {
            if (handleLerpTime > 0.0000f)
            {
                handleLerpTime -= Time.deltaTime / handleBackAnimationDuration;
                if (handleLerpTime < 0.0000f)
                    handleLerpTime = Mathf.Clamp01(handleLerpTime);
                handleTransform.position = Vector3.Lerp(startAnim.position, endAnim.position, handleLerpTime);
                gearTransform.Rotate(Vector3.forward, -gearRotationSpeed * Time.deltaTime);
            }
        }
    }



    //[GameFramework.Button]
    //public void UpdateIndicator()
    //{
    //    float rotationTotalRange = 0;
    //    //CALCOLO LA DISTANZA TOTALE TRA ROT MIN E ROT MAX ES: -90 +90   DISTANZA = 180   
    //    if (minIndicatorRotation < 0 || maxIndicatorRotation < 0) //SE UNO DEI DUE VALORI E' NEGATIVO SOMMO I VALORI ASSOLUTI PER TROVARE LA DISTANZA
    //        rotationTotalRange = Mathf.Abs(maxIndicatorRotation) + Mathf.Abs(minIndicatorRotation);
    //    else //SE SONO TUTTI E DUE POSITIVI SOTTRAGGO il minimo al massimo
    //        rotationTotalRange = maxIndicatorRotation - minIndicatorRotation;

    //    //CALCOLO IL MOLTIPLICATORE DA USARE SUL VALORE DI PRESSIONE PER OTTENERE IL VALORE DI ROTAZIONE ES: 
    //    //rotationTotalRange = 180 , MASSIMA PRESSIONE = 100 , MOLTIPLICATORE = 180/100 =1.8f 
    //    //(moltiplicherò la pressione per questo valore per avere la rotazione)
    //    float rotationMultiplier = rotationTotalRange / maxVal;

    //    float rotation = 0;
    //    //SE VA VERSO IL NEGATIVO SOTTRAGGO SENNO AGGIUNGO
    //    if (maxIndicatorRotation < minIndicatorRotation)
    //    {
    //        rotation = minIndicatorRotation - (currentPressureVal.Value * rotationMultiplier);
    //    }
    //    else
    //    {
    //        rotation = minIndicatorRotation + (currentPressureVal.Value * rotationMultiplier);
    //    }

    //    indicator.localRotation = Quaternion.Euler(new Vector3(0, 0, rotation));

    //    UpdateSliderColor(currentPressureVal.Value);
    //}

    //void UpdateSliderColor(float val)
    //{
    //    if (sliderColors.Length > 0)
    //    {
    //        for (int i = sliderColors.Length - 2; i >= 0; i--)
    //        {

    //            // IF CICLED UNTIL END POSITION AND LIFE IS LESS THAN LOWER POSITION OF ARRAY THE COLOR IS THAT
    //            if (i == 0 && val <= sliderColors[i].value)
    //            {
    //                background.color = sliderColors[i].color;
    //                return;
    //            }
    //            // ELSE I CHECK THE COLOR RANGE
    //            else if (val <= sliderColors[i].value && val > sliderColors[i - 1].value)
    //            {
    //                background.color = sliderColors[i].color;
    //                return;
    //            }

    //        }
    //        // IF NO OPTION CHOSEN I USE MAX LIFE COLOR
    //        if (sliderColors.Length > 0)
    //            background.color = sliderColors[sliderColors.Length - 1].color;
    //    }
    //}

    public override void GainControl(InteractionManager2D interactionManager2D) {

        //handleLerpTime = 0.0000f;
        if (interactionManager2D != null)
        {
            controlToggleAudio.PlayAudio(transform);
            isOwned = true;
            owner = interactionManager2D;
            PlayerInputRewired ownerInput = interactionManager2D.GetComponent<PlayerInputRewired>();
            Debug.Log("CharController is valid");

            
            interactionManager2D.GetComponent<CharacterController2D>().ChangeHandleGrabbed(true);

            playerParent = interactionManager2D.transform.parent;
            interactionManager2D.transform.position = playerHandleTransform.position;
            interactionManager2D.transform.SetParent(playerHandleTransform);
            interactionManager2D.FreezeCharacter();

            if(engine != null)
                engine.TogglePressureReleaser();
        }
    }

    public override void ReleaseControl(InteractionManager2D interactionManager2D) {
        controlToggleAudio.TriggerCue();

        isOwned = false;
        InteractionManager2D interactor = interactionManager2D == null ? owner : interactionManager2D;
        if (interactor != null)
        {
            PlayerInputRewired ownerInput = interactor.GetComponent<PlayerInputRewired>();
            interactor.ReleaseCharacter();
            interactor.transform.SetParent(playerParent);
            interactor.GetComponent<CharacterController2D>().ChangeHandleGrabbed(false);
            if (engine != null)
                engine.TogglePressureReleaser();
        }
    }

    protected override void OnBreak()
    {
        base.OnBreak();
        //throw new NotImplementedException();
    }

    protected override void OnRepair()
    {
        //throw new NotImplementedException();
    }
}


[System.Serializable]
public class SliderColors : IComparable
{
    public float value;
    public Color color;

    // ICOMPARABLE IMPLEMENTATION
    public int CompareTo(object obj)
    {
        if (obj is SliderColors)
        {
            return this.value.CompareTo((obj as SliderColors).value);  // compare user names
        }
        throw new ArgumentException("Object is not a SliderColors");
    }
}