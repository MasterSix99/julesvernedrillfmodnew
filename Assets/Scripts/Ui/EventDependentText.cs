﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EventDependentText : MonoBehaviour
{
    [System.Serializable]
    public class StringInfo
    {
        public EventsID eventForChange;
        public string newText;

        private TextMeshProUGUI textComponent;

        /// <summary>
        /// Sets textComponent.
        /// </summary>
        /// <param name="inTextToBind"></param>
        public void SetText(TextMeshProUGUI inTextToBind)
        {
            textComponent = inTextToBind;
        }

        /// <summary>
        /// Updates textComponent.
        /// </summary>
        public void UpdateText()
        {
            textComponent.text = newText;
        }
    }

    [SerializeField] private StringInfo[] infos;
    [SerializeField] private TextMeshProUGUI textToBind;



    //private Dictionary<EventsID, string> infosDictionary = new Dictionary<EventsID, string>();

    private void Awake()
    {
        int i;
        for (i = 0; i < infos.Length; i++)
        {
            infos[i].SetText(textToBind);
            //infosDictionary.Add(infos[i].eventForChange, infos[i].newText);
        }
    }

    private void OnEnable()
    {
        int i;
        for (i = 0; i < infos.Length; i++)
        {
            EventManager.StartListening(infos[i].eventForChange, infos[i].UpdateText);
        }
    }

    private void OnDisable()
    {
        int i;
        for (i = 0; i < infos.Length; i++)
        {
            EventManager.StopListening(infos[i].eventForChange, infos[i].UpdateText);
        }
    }
}
