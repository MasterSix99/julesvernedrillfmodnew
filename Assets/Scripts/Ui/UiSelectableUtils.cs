﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UiSelectableUtils : MonoBehaviour, ISelectHandler, IDeselectHandler ,IPointerEnterHandler,IPointerExitHandler
{
    public Selectable selectable;
    //public TMPro.TextMeshProUGUI textField;
    public UnityEvent hoverEvt;
    public UnityEvent deselectEvt;
    EventSystem m_eventSystem;


    private void Awake()
    {
        if (selectable == null)
            selectable = GetComponent<Selectable>();
    }

    void Start()
    {
        m_eventSystem = EventSystem.current as MyEventSystem;
    }

    void OnEnable()
    {
        EventManager.StartListening<GameObject>(EventsID.UISELECTABLESELECTED, DeselectByEvent);
    }

    void OnDisable()
    {
        EventManager.StopListening<GameObject>(EventsID.UISELECTABLESELECTED, DeselectByEvent);
    }




    public void OnSelect(BaseEventData eventData)
    {
        EventManager.TriggerEvent<GameObject>(EventsID.UISELECTABLESELECTED, gameObject);
        hoverEvt?.Invoke();
    }

    public void OnDeselect(BaseEventData eventData)
    {
        deselectEvt?.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        hoverEvt?.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        deselectEvt?.Invoke();
    }

    private void DeselectByEvent(GameObject obj)
    {
        if(obj != gameObject) { 
            deselectEvt?.Invoke();
            //NON FUNZIONA, TROVA UN MODO PER DESELEZIONARE
            selectable.OnDeselect(new BaseEventData(m_eventSystem));
        }
    }
}
