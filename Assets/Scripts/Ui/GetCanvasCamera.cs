﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetCanvasCamera : MonoBehaviour
{
    [SerializeField]
    private string cameraName = "MainCamera";
    // Start is called before the first frame update
    void Awake()
    {
        Canvas canvas = GetComponent<Canvas>();
        if (canvas.renderMode.Equals(RenderMode.WorldSpace)) {
            GameObject cameraObj = GameObject.Find(cameraName);
            if(cameraObj != null) {
                Camera camera = cameraObj.GetComponent<Camera>();
                if(camera != null)
                    canvas.worldCamera = camera;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
