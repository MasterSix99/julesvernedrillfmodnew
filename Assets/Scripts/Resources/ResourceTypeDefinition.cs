﻿using UnityEngine;

[CreateAssetMenu(fileName = "so_templateResourceType", menuName = "Resources/new Type")]
public class ResourceTypeDefinition : ScriptableObject
{
    #region VARIABLES
    [SerializeField]
    private string resourceName;
    [SerializeField]
    private string description;
    #endregion

    #region PROPERTIES
    public string Name
    {
        get { return resourceName; }
        set { resourceName = value; }
    }
    public string Description
    {
        get { return description; }
        set { description = value; }
    }
    #endregion
}
