﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceMovement : MonoBehaviour
{
    public System.Action<bool> onCanMoveChange;

    [Header("Moving Resource variables")]
    
    
    [SerializeField]
    private Transform spriteTransform;
    [SerializeField]
    private MovingPattern[] resourcePatterns;

    private bool canMove = false;

    
    

    private Rigidbody2D rb;

    public bool CanMove
    {
        get
        {
            return canMove;
        }
        set
        {
            if (canMove != value)
            {
                canMove = value;
            }
            onCanMoveChange?.Invoke(canMove);
        }
    }
    public Rigidbody2D Rb { get { return rb; } }
    public Transform SpriteTransform { get { return spriteTransform; } }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        int i;
        for (i = 0; i < resourcePatterns.Length; i++)
        {
            resourcePatterns[i] = Instantiate(resourcePatterns[i]);
            resourcePatterns[i].Initialize(this);
        }
        
    }

    void Update()
    {
        if (CanMove)
        {
            UpdatePatterns();
        }
    }

    void UpdatePatterns()
    {
        int i;
        for (i = 0; i < resourcePatterns.Length; i++)
        {
            resourcePatterns[i].UpdatePattern();
        }
    }

    public void EnableMovement()
    {
        CanMove = true;

    }

    public void DisableMovement()
    {
        CanMove = false;
    }

    //private bool DoActivationTimer()
    //{
    //    //Debug.Log(Mathf.Clamp(activationCoefficient / drillDepth.value, 0, activationCoefficient) + activationCountdownOffset);
    //    if (activationTimer < Mathf.Clamp(activationCoefficient / drillDepth.Value, 0, activationCoefficient) + activationCountdownOffset)
    //    {
    //        activationTimer += Time.deltaTime;
    //        return false;
    //    }
    //    else
    //    {
    //        activationTimer = 0.000f;
    //        return true;
    //    }
    //}


    void OnCollisionEnter2D(Collision2D other)
    {
        int i;
        for (i = 0; i < resourcePatterns.Length; i++)
        {
            resourcePatterns[i].Collide(other);
        }
    }






}
