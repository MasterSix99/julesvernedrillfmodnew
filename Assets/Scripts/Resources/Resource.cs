﻿using UnityEngine;
using GameFramework;

[RequireComponent(typeof(Rigidbody2D))]
public class Resource : MonoBehaviour
{
    #region VARIABLES
    public ResourceDefinition resourceInfo;

    protected Rigidbody2D rb;
    protected SpriteRenderer spriteRenderer;
    protected ResourceMovement movement;

    private bool dimensionsAreForced = false;
    private bool isGrabbed = false;

    [Header("Spawn")]
    [SerializeField]
    private bool spawnAtStart = false;

    [Header("Activation stats")]
    [SerializeField]
    private float activationCoefficient;
    [SerializeField]
    private float activationMinOffset = 0;
    [SerializeField]
    private float activationMaxOffset = 1;
    [SerializeField]
    private bool resetActivationOffsetEachTime = true;
    [SerializeField]
    private FloatVariable activationTimeMultiplier;
    [SerializeField]
    private FloatVariable drillDepth;
    [SerializeField]
    private float activationCountdownOffset = 0f;
    [SerializeField]
    private Sprite activeSprite;
    private Sprite inactiveSprite = null;
    [SerializeField]
    private Material activeMaterial;
    private Material inactiveMaterial = null;
    private float activationTimer = 0f;
    private bool isActivated = false;

    //[Header("Upgradables")]
    //private static float extraCoefficient;

    [Header("Audio")]
    [SerializeField] private OneShotAudio activationSound;
    [SerializeField] private OneShotAudio deathSound;
    [SerializeField] private OneShotAudio hitSound;
    [SerializeField] private float squareVelocityForHitAudio = 64;
    #endregion

    public Rigidbody2D Rb { get { return rb; } }
    public bool IsGrabbed
    {
        get
        {
            return isGrabbed;
        }
        set
        {
            isGrabbed = value;
            OnIsGrabbedChange(isGrabbed);
        }
    }

    private bool IsActivated
    {
        get
        {
            return isActivated;
        }
        set
        {
            isActivated = value;
            if (isActivated)
            {
                movement.CanMove = true;
                if (!string.IsNullOrEmpty(activationSound.eventPath))
                    activationSound.PlayAudio(transform.position);
                OnActivate();
            }
            else
            {
                movement.CanMove = false;
                OnDeactivate();
            }
        }
    }

    #region METHODS MONOBEHAVIOUR
    public void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        movement = GetComponent<ResourceMovement>();
        resourceInfo = Instantiate(resourceInfo);
        inactiveSprite = spriteRenderer.sprite;
        inactiveMaterial = spriteRenderer.material;
    }
    public void OnEnable()
    {
        if (!dimensionsAreForced)
            resourceInfo.SetPhysicalDimensions(rb, transform);
        activationCountdownOffset = Random.Range(activationMinOffset, activationMaxOffset);
    }

    private void OnDisable()
    {
        if (dimensionsAreForced)
            dimensionsAreForced = false;
    }

    private void Start()
    {
        if (spawnAtStart)
            AdviseSpawn();
    }

    private void Update()
    {
        if (!IsGrabbed)
        {
            if (!IsActivated)
            {
                if (DoActivationTimer())
                    IsActivated = true;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!string.IsNullOrEmpty(hitSound.eventPath) && Rb.velocity.sqrMagnitude > squareVelocityForHitAudio)
            hitSound.PlayAudio(transform.position);
    }
    #endregion

    #region METHODS
    public void AdviseSpawn()
    {
        EventManager.TriggerEvent(EventsID.R_GATHERED, resourceInfo.Type.Name);
    }

    public void Destroy()
    {
        if (resourceInfo.DeathParticle != null)
            PoolManager.GetObject(resourceInfo.DeathParticle, transform.position);
        if (!string.IsNullOrEmpty(deathSound.eventPath))
            deathSound.PlayAudio(transform.position);
        gameObject.SetActive(false);
    }

    private void OnIsGrabbedChange(bool newValue)
    {
        if (!newValue)
        {
            activationTimer = 0.000f;

            if (resetActivationOffsetEachTime)
                activationCountdownOffset = Random.Range(activationMinOffset, activationMaxOffset);
        }
        else
        {
            IsActivated = false;
        }
    }

    [Button]
    public void RecalculatePhysicalDimensions()
    {
        resourceInfo.SetPhysicalDimensions(rb, transform);
    }

    public void ForceCalculateStats(float mass, float size, float fuel)
    {
        resourceInfo.SetPhysicalDimensions(rb, transform, mass, size, fuel);
        dimensionsAreForced = true;
    }

    private bool DoActivationTimer()
    {
        //Debug.Log(Mathf.Clamp(activationCoefficient / drillDepth.value, 0, activationCoefficient) + activationCountdownOffset);
        float upgradeActMultiplier = activationTimeMultiplier != null ? activationTimeMultiplier.Value : 1;
        if (activationTimer < (Mathf.Clamp(activationCoefficient / drillDepth.Value, 0, activationCoefficient) + activationCountdownOffset) * upgradeActMultiplier)
        {
            activationTimer += Time.deltaTime;
            return false;
        }
        else
        {
            activationTimer = 0.000f;
            return true;
        }
    }

    private void OnActivate()
    {
        spriteRenderer.material = activeMaterial;
        spriteRenderer.sprite = activeSprite;
    }

    private void OnDeactivate()
    {
        spriteRenderer.material = inactiveMaterial;
        spriteRenderer.sprite = inactiveSprite;
    }
    #endregion
}
