﻿using UnityEngine;


public struct ResourcePhysicalInfo
{
    #region VARIABLES
    private float mass;
    private float size;
    #endregion

    #region PROPERTIES
    public float Mass
    {
        get { return mass; }
        set { mass = value; }
    }

    public float Size
    {
        get { return size; }
        set { size = value; }
    }
    #endregion

    #region CONSTRUCTOR
    public ResourcePhysicalInfo(float mass, float size )
    {
        this.mass = mass;
        this.size = size;
    }
    #endregion
}

[CreateAssetMenu(fileName ="so_templateResource", menuName ="Resources/New Resource Definition")]
public class ResourceDefinition : ScriptableObject
{
    #region VARIABLES
    [SerializeField]
    private ResourceTypeDefinition type;
    [Header("Fuel")]
    [SerializeField]
    private bool isFontOfFuel = false;
    [ShowIf("IsFontOfFuel", true)]
    [SerializeField]
    private float minFuel = 0;
    [SerializeField]
    private float maxFuel = 1;
    private float fuel = 0;

    [Space]
    [SerializeField][Tooltip("Multiply this for sum of 20d20")]
    private float coefficent = 1;
    [SerializeField]
    private float minMass = 0;
    [SerializeField]
    private float maxMass = 1;
    [SerializeField]
    private float minScale = 0;
    [SerializeField]
    private float maxScale = 2;

    [Space]
    [SerializeField]
    private GameObject deathParticle;

    #endregion

    #region PROPERTIES
    public ResourceTypeDefinition Type
    {
        get { return type; }
        set { type = value; }
    }
    
    public float MinMass
    {
        get { return minMass; }
        set { minMass = value; }
    }
    public float MaxMass
    {
        get { return maxMass; }
        set { maxMass = value; }
    }
    public float MinScale
    {
        get { return minScale; }
        set { minScale = value; }
    }
    public float MaxScale
    {
        get { return maxScale; }
        set { maxScale = value; }
    }
    public bool IsFontOfFuel
    {
        get { return isFontOfFuel; }
    }
    public float MinFuel
    {
        get { return minFuel; }
        set { minFuel = value; }
    }
    public float MaxFuel
    {
        get { return maxFuel; }
        set { maxFuel = value; }
    }
    public float Fuel
    {
        get { return fuel; }
        set { fuel = value; }
    }
    public GameObject DeathParticle
    {
        get { return deathParticle; }
    }

    #endregion

    #region METHODS
    public ResourcePhysicalInfo GetPhysicalDimensions()
    {
        float mass = GetMass();
        float size = GetSize(mass);
        
        return new ResourcePhysicalInfo(mass, size);
    }

    public void SetPhysicalDimensions(Rigidbody2D rb, Transform transform)
    {
        ResourcePhysicalInfo dimensions = GetPhysicalDimensions();
        fuel = GetFuel(dimensions.Mass);
        rb.mass = dimensions.Mass * dimensions.Size;
        transform.localScale = new Vector3(dimensions.Size, dimensions.Size, dimensions.Size);
    }

    public void SetPhysicalDimensions(Rigidbody2D rb, Transform transform, float _mass, float _size, float _fuel)
    {
        rb.mass = Mathf.Clamp(_mass, MinMass, MaxMass);
        float clampedSize = Mathf.Clamp(_size, MinScale, MaxScale);
        transform.localScale = new Vector3(clampedSize, clampedSize, clampedSize);
        fuel = Mathf.Clamp(_fuel, MinFuel, MaxFuel);
    }

    //Get weight using d20 formula

    /// <summary>
    /// Calculate mass , sum 20d20 and multiply by coefficent, then clamp to min and max mass setted
    /// </summary>
    /// <returns></returns>
    private float GetMass() {
        float currentVal = 0;
        for(int i = 0; i<= 10; i++){
            currentVal += Random.Range(1, 20);
        }
        currentVal *= coefficent;
        currentVal = Mathf.Clamp(currentVal,MinMass, MaxMass);

       
        return currentVal;
    }

    /// <summary>
    /// Size is directly proportional to mass, so the funciton calculate mass percentage based on min and max mass and then apply same percentage value to scale
    /// </summary>
    /// <param name="mass"></param>
    /// <returns></returns>
    private float GetSize(float mass)
    {
        float massPercentage = mass * 100 / (maxMass - minMass);
        float size = (maxScale - MinScale) /100 * massPercentage;
        return size;
    }

    private float GetFuel(float mass)
    {
        //Debug.Log("GetSize mass param = " + mass);
        float massPercentage = mass * 100 / (maxMass - minMass);
        float fuelAmount = (maxFuel - MinFuel) / 100 * massPercentage;

        return fuelAmount;
    }
    #endregion

    void OnValidate()
    {
        if (System.Math.Abs(MaxMass) <= 0) {
            Debug.LogWarning("WARNING! Resource " + name + " can't have maxMass set to 0 , system is restoring a basic value of 1");
            maxMass = 1;
        }
        if (System.Math.Abs(MaxScale) <= 0)
        {
            Debug.LogWarning("WARNING! Resource " + name + " can't have maxScale set to 0 ,system is restoring a basic value of 2");
            maxScale = 2;
        }
        if (System.Math.Abs(MaxFuel) <= 0)
        {
            Debug.LogWarning("WARNING! Resource " + name + " can't have maxScale set to 0 ,system is restoring a basic value of 2");
            maxFuel = 1;
        }
    }

}



