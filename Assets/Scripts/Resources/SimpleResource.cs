﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleResource : MonoBehaviour
{
    [SerializeField] private GameObject correspondingResource;

    public GameObject CorrespondingResource { get { return correspondingResource; } }
}
