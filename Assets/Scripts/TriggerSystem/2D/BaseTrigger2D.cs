﻿using UnityEngine;
using GameFramework;

public class BaseTrigger2D :Entity
{
    [Space]
    [Header("Trigger params")]
    [SerializeField]
    protected bool m_useOnce = true;
    [SerializeField] protected bool activateOnTrigger = true;
    [SerializeField] protected LayerMask m_allowedLayers;
    [SerializeField] protected bool activateOnGameStart = false;
    [SerializeField] protected bool activateOnEvent = false;
    [SerializeField] protected EventsID eventToListen;
   

    protected bool m_triggered = false;

    public bool CanUse { get {return (IsEnabled && (!m_useOnce || (m_useOnce && !m_triggered))); }  }

    protected override void Awake()
    {
        base.Awake();
        //triggerAudio.Init(transform, GetComponent<Rigidbody>());
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        //triggerAudio.OnEnable();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        //triggerAudio.OnDisable();
    }

    public virtual void Trigger(Collider2D other)
    {
        m_triggered = true;
    }

    public virtual void Trigger()
    {
        m_triggered = true;
    }

    public virtual void TriggerExit(Collider2D other)
    {

    }

    public virtual void TriggerExit()
    {
    }


}