﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using GameFramework;



public class DialogueTrigger2D : BaseTrigger2D
{
    [Space]
    [SerializeField]
    private Dialogue dialogue;
    [Space]
    public UnityEvent onDialogueFinishEvt;

    #region MONOBEHAVIOURS METHODS
    protected override void OnEnable()
    {
        if (activateOnGameStart)
            SceneManager.sceneLoaded += Trigger;
        if (activateOnEvent)
            EventManager.StartListening(eventToListen, Trigger);
    }
    protected override void OnDisable()
    {
        if (activateOnGameStart)
            SceneManager.sceneLoaded -= Trigger;
        if (activateOnEvent)
            EventManager.StopListening(eventToListen, Trigger);
    }
    #endregion

    public override void Trigger(Collider2D other)
    {
        Debug.Log("CHIAMATO TRIGGER " + CanUse);
        if (CanUse &&  Utilities.LayerIsInLayerMask(m_allowedLayers,other.gameObject))
        {
            base.Trigger(other);
            StartDialogue();
        }

    }
    public override void Trigger()
    {
        StartDialogue();
    }
    private void Trigger(Scene scene, LoadSceneMode mode)
    {
        StartDialogue();
    }


    public void StartDialogue()
    {
        StartCoroutine(StartDialogueRoutine());
        /*DialogueAction dialogueAction = new DialogueAction(dialogue, onDialogueFinishEvt);
        //THIS LISTENER WILL TRIGGER THE UNITY EVENT ON PANEL CLOSE  (DialogueSystem class) AND AFTER EVENT INVOKE IT WILL UNSUBSCRIBE ALL EVENTS OF END DIALOGUE
        EventManager.StartListening(EventsIds.onEndDialogue, () => { if (onDialogueFinishEvt != null) onDialogueFinishEvt.Invoke(); EventManager.StopAllListener(EventsIds.onEndDialogue); });
        EventManager.TriggerEvent(EventsIds.dialogueStart, dialogueAction);*/
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
            
        if (activateOnTrigger && Utilities.LayerIsInLayerMask(collision.gameObject.layer, m_allowedLayers))
        {
            Trigger(collision);
        }
    }

    IEnumerator StartDialogueRoutine()
    {
        float attemptTimer = 5f;
        float timer = 0;
        //WAIT UNTIL RIGHT GAME STATE (MAX WAIT DEFINED BY attemptTimer)
        while (!GameState.IsStateRunning(GameStateTypes.Running))
        {
            if (timer >= attemptTimer)
                break;
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        DialogueAction dialogueAction = new DialogueAction(dialogue, onDialogueFinishEvt);
        //THIS LISTENER WILL TRIGGER THE UNITY EVENT ON PANEL CLOSE  (DialogueSystem class) AND AFTER EVENT INVOKE IT WILL UNSUBSCRIBE ALL EVENTS OF END DIALOGUE
        EventManager.TriggerEvent(EventsID.DIALOGUESTART, dialogueAction);
    }
}