﻿/**
 * @author Fabrizio Coppolecchia
 *
 * This trigger is used to show a info Throught InfoBox
 * 
 * @date - 2019/02/12
 */
using GameFramework;
using UnityEngine;

public class InfoBoxTrigger2D : BaseTrigger2D
{
    /// <summary>
    /// The info definition
    /// </summary>
    [SerializeField]
    private Info info;

    protected override void Start()
    {
        base.Start();
        if (activateOnGameStart)
            OpenInfoBox();
    }

    public override void Trigger(Collider2D other)
    {
        if (CanUse)
        {
            base.Trigger(other);
            OpenInfoBox();
        }
    }

    public override void Trigger()
    {
        if (CanUse)
        {
            base.Trigger();
            OpenInfoBox();
        }
    }

    public override void TriggerExit(Collider2D other)
    {
        InfoBoxRequest request = new InfoBoxRequest();
        request.mustBeOpened = false;
        EventManager.TriggerEvent<InfoBoxRequest>(EventsID.INFOBOXREQUEST, request);
    }
    public override void TriggerExit()
    {
        InfoBoxRequest request = new InfoBoxRequest();
        request.mustBeOpened = false;
        EventManager.TriggerEvent<InfoBoxRequest>(EventsID.INFOBOXREQUEST, request);
    }


    public void OpenInfoBox()
    {
        InfoBoxRequest request = new InfoBoxRequest(true,info,-1);
        request.infoDefinition = info;
        request.mustBeOpened = true;

        EventManager.TriggerEvent<InfoBoxRequest>(EventsID.INFOBOXREQUEST, request);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (activateOnTrigger && Utilities.LayerIsInLayerMask(other.gameObject.layer, m_allowedLayers))
        {
            Trigger(other);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (activateOnTrigger && Utilities.LayerIsInLayerMask(other.gameObject.layer, m_allowedLayers))
        {
            TriggerExit(other);
        }
    }
}