﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameFramework;

public class PhysicButtonBehaviour : Interactable2D
{
    [SerializeField] private float height;
    [SerializeField] private Transform buttonSprite;
    [SerializeField] private float movementSpeed;

    private bool isPressed;
    private bool isMoving = false;
    private bool moveBackwards = false;
    private Vector3 startPosition;

    [Header("Audio")]
    [SerializeField] private GenericEvent buttonAudio;

    private void Start()
    {
        startPosition = buttonSprite.localPosition;
        
        FmodManager.instance.CreateGenericEnventInstance(ref buttonAudio);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isPressed && Utilities.LayerIsInLayerMask(collision.gameObject.layer, m_allowedMask))
        {
            isPressed = true;
            Interact();
        }
        //buttonSprite.Translate(Vector3.down * height, Space.Self);
        StartCoroutine(MoveButton(false));

        buttonAudio.PlayAudio(transform);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (isPressed && Utilities.LayerIsInLayerMask(collision.gameObject.layer, m_allowedMask))
        {
            isPressed = false;
            Release();
        }
        //buttonSprite.Translate(Vector3.up * height, Space.Self);
        if (!isMoving)
            StartCoroutine(MoveButton(true));
        else
            moveBackwards = true;

        buttonAudio.TriggerCue();
    }

    private IEnumerator MoveButton(bool backwards)
    {
        isMoving = true;

        Vector3 startLerp, endLerp;

        if (!backwards)
        {
            startLerp = startPosition;
            endLerp = startPosition - buttonSprite.up * height;
        }
        else
        {
            startLerp = startPosition - buttonSprite.up * height;
            endLerp = startPosition;
        }

        float t = 0.000f;
        do
        {
            t += movementSpeed * Time.deltaTime;
            t = Mathf.Clamp01(t);
            buttonSprite.localPosition = Vector3.Lerp(startLerp, endLerp, t);
            yield return null;
        }
        while (t < 1);
        isMoving = false;

        if (moveBackwards)
        {
            moveBackwards = false;
            StartCoroutine(MoveButton(true));
        }
    }
}
