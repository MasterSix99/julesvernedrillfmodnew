﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameFramework;
using Rewired;

public class DriverBehaviour : MachineBehaviour
{

    #region VARIABLES
    [Tooltip("speed of rotation")]
    [SerializeField]
    private EntityFloatStat rotationSpeed;
    [SerializeField]
    private float resetRotationSpeed;
    [Tooltip("smoothness of rotation")]
    [SerializeField]
    private float damping = 2f;
    [SerializeField]
    private Transform drillTransform;
    [SerializeField]
    private Transform wheelTransform;
    [SerializeField]
    private EntityFloatStat maxRotDegree;
    private Quaternion newRotation;
    [Tooltip("how many seconds you have once you reach the rotation limit until the machine breaks")]
    [SerializeField]
    private EntityFloatStat breakConditionThreshold;
    private float breakTimer;
    private PlayerInputRewired mahcineInput;
    private CharacterController2D drivingCharacter = null;
    [SerializeField]
    private Transform playerDrivingTransform;
    [Header("Audio")]
    [SerializeField] private GenericEvent interactConstantAudio;

    [Header("Vibration")]
    [SerializeField]
    private JoystickVibrationConfig driveVibration;

    #region Cached
    private Vector3 desiredRotation;
    #endregion

    #endregion

    #region PROPERTIES
    public Quaternion DrillRotation
    {
        get { return newRotation; }
        private set { newRotation = value; }
    }

    #endregion


    private void Awake()
    {
        mahcineInput = GetComponent<PlayerInputRewired>();
        if (drillTransform == null)
            drillTransform = FindObjectOfType<DrillBehaviour>()?.transform;
        FmodManager.instance.CreateGenericEnventInstance(ref interactConstantAudio);
    }

    protected override void Start()
    {
        base.Start();

        if(rotationSpeed.baseValue == 0)
        {
            rotationSpeed.SetBaseValue(2f);
        }
        if(maxRotDegree.baseValue == 0)
        {
            maxRotDegree.SetBaseValue(30f);
        }
        
        
    }
    protected override void ConstantUpdate()
    {
        if (!isOwned)
        {
            if (desiredRotation.z > 1.0f && desiredRotation.z < 180.0f)
            {
                desiredRotation.z += (-resetRotationSpeed * Time.deltaTime);
                ActuateRotation();
            }
            else if (desiredRotation.z < 359.0f && desiredRotation.z > 180.0f)
            {
                desiredRotation.z += (resetRotationSpeed * Time.deltaTime);
                ActuateRotation();
            }
        }
    }

    protected override void OnUpdate()
    {
        if (drillTransform != null && mahcineInput.IsEnabled)
        {

            float rotationInput = mahcineInput.GetAxis("Move Horizontal");
            if (Mathf.Abs(rotationInput) > .1)
            {
                desiredRotation = drillTransform.rotation.eulerAngles;
                desiredRotation.z += (Mathf.Sign(rotationInput) * rotationSpeed.GetValue() * Time.deltaTime);
                if (desiredRotation.z <= 180 && desiredRotation.z > maxRotDegree.GetValue())
                    desiredRotation.z = maxRotDegree.GetValue();
                else if (desiredRotation.z > 180 && desiredRotation.z < 360 - maxRotDegree.GetValue())
                    desiredRotation.z = 360 - maxRotDegree.GetValue();
                ActuateRotation();
                wheelTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, maxRotDegree.GetValue() * rotationInput));

                if (!drivingCharacter.IsDrivingAndTurning())
                    drivingCharacter.ChangeDrivingTurning(true);
            }
            else if(drivingCharacter.IsDrivingAndTurning())
                drivingCharacter.ChangeDrivingTurning(false);
        }
        CheckBreakCondition();
    }

    private void ActuateRotation()
    {
        newRotation = Quaternion.RotateTowards(drillTransform.rotation, Quaternion.Euler(desiredRotation), damping * Time.deltaTime);
        drillTransform.rotation = DrillRotation;
    }

    //bool that returns true whenever you reach maximum rotation in both direction of the drill.
    private bool BreakCondition()
    {
        if (desiredRotation.z == maxRotDegree.GetValue() || desiredRotation.z == 360 - maxRotDegree.GetValue()){
            return true;
        }
        else{
            return false;
        }
    }

    private void CheckBreakCondition()
    {
        if (BreakCondition())
        {
            breakTimer += Time.deltaTime;
            if(breakTimer >= breakConditionThreshold.GetValue())
            {
                
                IsBroken = true;
                breakTimer = 0f;
            }
        }
        else
        {
            if(breakTimer != 0f)
            breakTimer = 0f;
        }
    }
    
    protected override void OnBreak()
    {
        base.OnBreak();
        ReleaseControl(Owner);
        Debug.Log("driving machine is broken!");
        EventManager.TriggerEvent(EventsID.BROKENDRIVER);
        
    }

    protected override void OnRepair()
    { 

    }



    public override void GainControl(InteractionManager2D interactionManager2D)
    {
        if (interactionManager2D != null && !IsBroken)
        {
            isOwned = true;
            
            

            drivingCharacter = interactionManager2D.GetComponent<CharacterController2D>();
            if (drivingCharacter.MovementBehaviour.IsGrounded)
            {
                PlayerInputRewired ownerInput = drivingCharacter.PlayerInput;
                //APPLY VIBRATION
                Player rPlayer = ReInput.players.GetPlayer(ownerInput.RewiredPlayerId);
                if (ownerInput != null && rPlayer != null)
                {
                    JoystickVibration.Vibrate(rPlayer, driveVibration);
                }

                interactionManager2D.FreezeCharacter();
                interactionManager2D.GetComponent<Rigidbody2D>().simulated = false;
                interactionManager2D.transform.position = playerDrivingTransform.position;
                drivingCharacter.Animator.SetBool("driving", true);

                if (mahcineInput != null && ownerInput != null)
                {
                    mahcineInput.AssignRewiredPlayer(ownerInput.RewiredPlayerId);
                    mahcineInput.IsEnabled = true;
                }
                owner = interactionManager2D;
                interactConstantAudio.PlayAudio(transform);
                EventManager.TriggerEvent(EventsID.DRIVING_STARTDRIVING);
            }
        }
    }


    public override void ReleaseControl(InteractionManager2D interactionManager2D)
    {
        if (interactionManager2D != null)
        {


            //Debug.Log("ReleaseControl");
            isOwned = false;
            //Debug.Log("ReleaseControl success");
            PlayerInputRewired ownerInput = interactionManager2D.GetComponent<PlayerInputRewired>();

            //STOP VIBRATION
            Player rPlayer = ReInput.players.GetPlayer(ownerInput.RewiredPlayerId);
            if (ownerInput != null && rPlayer != null)
            {
                JoystickVibration.StopVibrations(rPlayer);
            }

            interactionManager2D.ReleaseCharacter();
            mahcineInput.AssignRewiredPlayer(0);
            mahcineInput.IsEnabled = false;

            interactionManager2D.GetComponent<Rigidbody2D>().simulated = true;
            interactionManager2D.GetComponentInChildren<Animator>().SetBool("driving", false);

            if (drivingCharacter != null && drivingCharacter.IsDrivingAndTurning())
                drivingCharacter.ChangeDrivingTurning(false);

            if (interactConstantAudio.IsPlaying())
                interactConstantAudio.StopAudio(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

            drivingCharacter = null;
            owner = null;
            EventManager.TriggerEvent(EventsID.DRIVING_STOPDRIVING);
        }
    }


}
