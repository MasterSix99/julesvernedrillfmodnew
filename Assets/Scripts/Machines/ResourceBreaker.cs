﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceBreaker : SlotEquippedMachine
{
    [System.Serializable]
    public class ResourceResultInfo
    {
        [System.Serializable]
        public class ResultPercentage
        {
            public GameObject resource;
            public int weightPercentage = 100;
        }

        public string resourceName;
        public ResultPercentage[] resultInfos = new ResultPercentage[0];

        public GameObject GetRandomResult()
        {
            float value = Random.Range(1, 101);
            int lastPercentage, currentPercentage = 0;
            int i;
            for (i = 0; i < resultInfos.Length; i++)
            {
                lastPercentage = currentPercentage;
                currentPercentage += resultInfos[i].weightPercentage;

                if (lastPercentage < value && value <= currentPercentage)
                {
                    return resultInfos[i].resource;
                }
            }
            return null;
        }
    }

    [System.Serializable]
    public class QuantityPerCrushedInfo
    {
        public int quantity = 2;
        public EntityIntStat weightPercentage;
    }

    private Animator workingAnimator;

    [SerializeField] private LayerMask targetLayerMask;

    [Header("Processing infos")]
    [SerializeField] private EntityFloatStat breakingTime;
    [SerializeField] private ResourceResultInfo[] results;
    [SerializeField] private QuantityPerCrushedInfo[] quantitiesWeights;

    [Space()]
    [SerializeField] private float timeBetweenSpawns = 0.1f;

    [Header("Machine infos")]
    [SerializeField] private float valuesPercentage = 47.5f;
    [SerializeField] [Tooltip("If the scale is less than the minimum just 1 piece is created")] private float minScaleToDivide = 0.4f;
    private List<Resource> resourcesToBreak = new List<Resource>();
    private List<Resource> resultResources = new List<Resource>();

    [Header("Spawn infos")]
    [SerializeField] private Transform spawnTransform;
    [SerializeField] private bool applyForceOnSpawn = true;
    [SerializeField] [ShowIf("applyForceOnSpawn")] private Vector2 minSpawnForce;
    [SerializeField] [ShowIf("applyForceOnSpawn")] private Vector2 maxSpawnForce;

    [Header("Energy infos")]
    [SerializeField] private EntityIntStat maxActivationsBeforeBreak;
    [SerializeField] private UnityEngine.UI.Slider energySlider;

    [Header("Bonuses")]
    [SerializeField] private GameFramework.FloatVariable currentTime;
    [SerializeField] private EntityFloatStat timeIncreaseForGem;

    [Header("Aesthetic")]
    [SerializeField] private GameObject workingVfx;
    [SerializeField] private GameObject brokenVfx;
    [SerializeField] private int animationRepetitions = 20;
    //[SerializeField] private DG.Tweening.DOTweenAnimation workingAnimation;

    //[Header("Audio")]
    //[SerializeField] private GenericEvent constantWorkingAudio;

    [Header("Components")]
    private Collider2D interactCollider;

    private Dictionary<string, ResourceResultInfo> resultsDictionary = new Dictionary<string, ResourceResultInfo>();

    private int currentActivations = 0;
    private bool isCheckingResources = false;
    private bool hasStarted = false;

    #region Cached
    private WaitForSeconds spawnWaiting;
    #endregion

    private int CurrentActivations
    {
        get
        {
            return currentActivations;
        }
        set
        {
            currentActivations = value;
            energySlider.value = 1 - (float)currentActivations / maxActivationsBeforeBreak.GetValue();
        }
    }

    protected override void Awake()
    {
        base.Awake();
        CurrentActivations = 0;
        spawnWaiting = new WaitForSeconds(timeBetweenSpawns);
        interactCollider = GetComponent<Collider2D>();
        workingAnimator = GetComponentInChildren<Animator>();
        int i;
        for (i = 0; i < results.Length; i++)
        {
            resultsDictionary.Add(results[i].resourceName, results[i]);
        }

        //FmodManager.instance.CreateGenericEnventInstance(ref constantWorkingAudio);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GameFramework.Utilities.LayerIsInLayerMask(collision.gameObject.layer, targetLayerMask))
        {
            Resource triggeredResource = collision.GetComponent<Resource>();
            resourcesToBreak.Add(triggeredResource);
            EventManager.TriggerEvent(EventsID.R_CRUSHED, triggeredResource.resourceInfo.Type.Name);
            triggeredResource.Destroy();
        }
    }

    public void OnInteract()
    {
        if (!IsBroken)
        {
            if (!isCheckingResources && !hasStarted)
            {
                isCheckingResources = true;
                StartCoroutine(InteractCoroutine());
            }
        }
    }

    private IEnumerator InteractCoroutine()
    {
        interactCollider.enabled = true;
        yield return new WaitForSeconds(0.1f);
        interactCollider.enabled = false;
        isCheckingResources = false;

        if (resourcesToBreak.Count > 0)
        {
            hasStarted = true;
            CurrentActivations++;
            workingAnimator.SetTrigger("bite");
            //workingAnimation.DOPlay();
            workingVfx.SetActive(true);
            //constantWorkingAudio.PlayAudio(transform);
            StartNewProcess(breakingTime.GetValue(), OnEndProcessing);
        }
    }

    private void OnEndProcessing()
    {
        float newMass;
        float newSize;
        float newFuel;
        GameObject spawnedResource;
        float quantity;

        int i, j;
        for (i = 0; i < resourcesToBreak.Count; i++)
        {
            newMass = resourcesToBreak[i].Rb.mass * valuesPercentage / 100;
            newSize = resourcesToBreak[i].transform.localScale.y * valuesPercentage / 100;
            newFuel = resourcesToBreak[i].resourceInfo.Fuel * valuesPercentage / 100;
            quantity = resourcesToBreak[i].transform.localScale.y >= minScaleToDivide ? GetQuantity() : 1;
            for (j = 0; j < quantity; j++)
            {
                spawnedResource = PoolManager.GetObject(resultsDictionary[resourcesToBreak[i].resourceInfo.Type.Name].GetRandomResult(), false);
                spawnedResource.transform.SetParent(resourcesToBreak[i].transform.parent);
                resultResources.Add(spawnedResource.GetComponent<Resource>());
                resultResources[resultResources.Count - 1].ForceCalculateStats(newMass, newSize, newFuel);
                resourcesToBreak[i].AdviseSpawn();
            }
            if (resourcesToBreak[i].resourceInfo.Type.Name == "Gem")
                currentTime.Value += timeIncreaseForGem.GetValue();
        }
        resourcesToBreak.Clear();
        //workingAnimation.DORewind();
        workingVfx.SetActive(false);
        //constantWorkingAudio.TriggerCue();

        StartCoroutine(ThrowNewResourcesCoroutine());
    }

    private IEnumerator ThrowNewResourcesCoroutine()
    {
        int randomIndex;
        while(resultResources.Count > 1)
        {
            randomIndex = Random.Range(0, resultResources.Count);
            Spawn(resultResources[randomIndex].Rb);
            resultResources.RemoveAt(randomIndex);
            yield return spawnWaiting;
        }
        Spawn(resultResources[0].Rb);
        resultResources.Clear();
        if (currentActivations >= maxActivationsBeforeBreak.GetValue())
        {
            IsBroken = true;
        }
        hasStarted = false;
    }

    private void Spawn(Rigidbody2D resource)
    {
        resource.transform.position = spawnTransform.position;
        resource.gameObject.SetActive(true);

        if (applyForceOnSpawn)
        {
            resource.velocity = new Vector2(Random.Range(minSpawnForce.x, maxSpawnForce.x), Random.Range(minSpawnForce.y, maxSpawnForce.y));
        }
    }

    private int GetQuantity()
    {
        
        int lastPercentage, currentPercentage = 0;
        int maxValue = 0;
        int i;
        for (i = 0; i < quantitiesWeights.Length; i++)
        {
            maxValue += quantitiesWeights[i].weightPercentage.GetValue();
        }
        int value = Random.Range(1, maxValue + 1);
        for (i = 0; i < quantitiesWeights.Length; i++)
        {
            lastPercentage = currentPercentage;
            currentPercentage += quantitiesWeights[i].weightPercentage.GetValue();

            if (lastPercentage < value && value <= currentPercentage)
            {
                return quantitiesWeights[i].quantity;
            }
        }
        return 1;
    }

    protected override void OnBreak()
    {
        base.OnBreak();
        brokenVfx.SetActive(true);
        EventManager.TriggerEvent(EventsID.BROKENCRUSHER);
    }

    protected override void OnRepair()
    {
        brokenVfx.SetActive(false);
        CurrentActivations = 0;
    }

    private void UpgradeFirstQuantity(StatModifier modifier)
    {
        if (quantitiesWeights.Length > 0)
            quantitiesWeights[0].weightPercentage.AddModifier(modifier);
    }

    private void UpgradeSecondQuantity(StatModifier modifier)
    {
        if (quantitiesWeights.Length > 1)
            quantitiesWeights[1].weightPercentage.AddModifier(modifier);
    }

    private void UpgradeThirdQuantity(StatModifier modifier)
    {
        if (quantitiesWeights.Length > 2)
            quantitiesWeights[2].weightPercentage.AddModifier(modifier);
    }
}
