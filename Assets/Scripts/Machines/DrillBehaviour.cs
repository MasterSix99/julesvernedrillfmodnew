﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class DrillBehaviour : MachineBehaviour
{
    #region Variables
    [SerializeField] private GameFramework.FloatVariable speedDecayValue;
    [SerializeField] private GameFramework.FloatVariable heatIncreaseValue;
    [SerializeField] private GameFramework.FloatVariable speedValue;
    [SerializeField] private float speedThresholdForValueSwitch = 0f;
    private bool? isOverSpeedThreshold = null;

    [Header("Ground detection infos")]
    [SerializeField] private Transform tileDetector;
    [SerializeField] private float detectionRate = 0.1f;
    private float detectionTimer;
    private GroundTile currentTile = null;

    

    [Header("Components")]
    private PlayGroundGenerator groundGenerator;
    
    [Header("Vibrations")][Space]
    [SerializeField]
    private float speedToReachBeforeVibration = 10;
    [SerializeField]
    private float rotationToReachBeforeVibration = 90;
    [SerializeField]
    JoystickVibrationConfig speedVibration;
    [SerializeField]
    JoystickVibrationConfig rotationVibration;

    //Generated on start, chached to avoid too much garbage collector, also this 2 vibrations are not on inspector to make easier setting (using only rotationVibration variable)
    private JoystickVibrationConfig leftRotationVibration;
    private JoystickVibrationConfig rightRotationVibration;

    private bool? IsOverSpeedThreshold
    {
        get
        {
            return isOverSpeedThreshold;
        }
        set
        {
            isOverSpeedThreshold = value;
            UpdateCurrentTileValues(isOverSpeedThreshold.Value);
        }
    }

    #region Cached
    private GroundTile newTile;
    #endregion
    #endregion

    #region Monobehviour Methods

    private void Awake()
    {
        groundGenerator = FindObjectOfType<PlayGroundGenerator>();

        //Generate chached vibrations
        leftRotationVibration = new JoystickVibrationConfig(rotationVibration.LeftVibrationLevel, rotationVibration.LeftVibrationTime, 0, 0,rotationVibration.VibrationPriority);
        rightRotationVibration = new JoystickVibrationConfig(rotationVibration.RightVibrationLevel, rotationVibration.RightVibrationTime, 0, 0, rotationVibration.VibrationPriority);
    }

    protected override void Start()
    {
        base.Start();
    }

    #endregion

    #region Methods

    private bool speedVibrationInProgress = false;
    private bool leftRotationVibrationInProgress = false;
    private bool rightRotationVibrationInProgress = false;
    protected override void OnUpdate()
    {
        TimedSetCurrentTile();
        //Vibrate if speed reach and exceed the limit 
        if (speedValue.Value > speedToReachBeforeVibration)
        {
            foreach(Player player in ReInput.players.AllPlayers)
                JoystickVibration.Vibrate(player, speedVibration);
            speedVibrationInProgress = true;
        }
        else if(speedVibrationInProgress)
        {
            foreach (Player player in ReInput.players.AllPlayers)
                JoystickVibration.StopVibrations(player);
            speedVibrationInProgress = false;
        }

        float angle = transform.localEulerAngles.z;
        angle = (angle > 180) ? angle - 360 : angle;
        //Vibrate if rotation exceed the limit (on both sides)
        if (angle >= rotationToReachBeforeVibration)
        {
            foreach (Player player in ReInput.players.AllPlayers)
                JoystickVibration.Vibrate(player, leftRotationVibration);
            leftRotationVibrationInProgress = true;

        }
        else if (leftRotationVibrationInProgress)
        {
            foreach (Player player in ReInput.players.AllPlayers)
                JoystickVibration.StopVibrations(player);
            leftRotationVibrationInProgress = false;
        }
        if(angle <= -rotationToReachBeforeVibration)
        {
            foreach (Player player in ReInput.players.AllPlayers)
                JoystickVibration.Vibrate(player, rightRotationVibration);
        }
        else if (rightRotationVibrationInProgress)
        {
            foreach (Player player in ReInput.players.AllPlayers)
                JoystickVibration.StopVibrations(player);
            rightRotationVibrationInProgress = false;
        }
    }

    protected override void ConstantUpdate()
    {
        if (speedValue.Value <= speedThresholdForValueSwitch)
        {
            if (!IsOverSpeedThreshold.HasValue || IsOverSpeedThreshold.Value)
            {
                IsOverSpeedThreshold = false;
            }
        }
        else
        {
            if (!IsOverSpeedThreshold.HasValue || !IsOverSpeedThreshold.Value)
            {
                IsOverSpeedThreshold = true;
            }
        }
    }

    protected override void OnBreak()
    {
        base.OnBreak();
    }

    protected override void OnRepair()
    {

    }

    #region Env

    /// <summary>
    /// Calls UpdateCurrentTile() after a timer is passed.
    /// </summary>
    private void TimedSetCurrentTile()
    {
        if (detectionTimer < detectionRate)
            detectionTimer += Time.deltaTime;
        else
        {
            detectionTimer = 0.0000f;
            UpdateCurrentTile();
        }
    }

    /// <summary>
    /// Updates the tile the drill is currently above;
    /// </summary>
    /// <param name="newTile"></param>
    private void UpdateCurrentTile()
    {
        newTile = groundGenerator.GetTile(tileDetector.position);
        if (newTile != null)
        {
            if (currentTile == null || currentTile.tileType != newTile.tileType)
            {
                currentTile = newTile;
                UpdateCurrentTileValues();
                //TE L HO COMMENTATO IO (FABRY) POI FIXALO CHE DAVA NULL
                EventManager.TriggerEvent(EventsID.DRILL_CHANGETILE, currentTile.tileType);
            }
        }
        else
            Debug.LogError("Drill is outside the game grid!");
    }

    #endregion

    private void UpdateCurrentTileValues()
    {
        UpdateCurrentTileValues(speedValue.Value > speedThresholdForValueSwitch);
    }

    private void UpdateCurrentTileValues(bool over)
    {
        if (currentTile != null)
        {
            //Debug.Log("Updating tile values --- Is Over: " + over);
            if (over)
            {
                speedDecayValue.Value = currentTile.OverThresholdSpeedDecay;
                heatIncreaseValue.Value = currentTile.OverThresholdHeatPerSecond;
            }
            else
            {
                speedDecayValue.Value = currentTile.UnderThresholdSpeedDecay;
                heatIncreaseValue.Value = currentTile.UnderThresholdHeatPerSecond;
            }
        }
    }

    //private void OnSpeedThresholdSwitch(bool over)
    //{
    //    UpdateCurrentTileValues(over);
    //}

    #endregion
}
