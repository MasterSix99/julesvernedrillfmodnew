﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TrashBehaviour : SlotEquippedMachine
{


    #region Variables
    #region Privates
    [Header("Upgradables")]
    [SerializeField] private EntityFloatStat extraProcessTime;
    [SerializeField] private EntityFloatStat binScale;
    [SerializeField] private Transform binTransform;
    [Space()]
    [SerializeField] private LayerMask targetLayerMask;
    [SerializeField]
    private float maximumSizeAccepted;
    private bool isWorking = false;
    private float deactivationCountdown = 3f;
    private float deactivationCurrentCountdown = 0f;

    [Header("Energy")]
    [SerializeField] private bool hasEnergy = false;
    [ShowIf("hasEnergy")] [SerializeField] private int maxEnergy = 10;
    [ShowIf("hasEnergy")] [SerializeField] private int energyConsumptionPerDisposal = 1;
    private int energy = 0;

    [Header("UI")]
    [SerializeField] private Slider energyBar;

    [Header("Audio")]
    [SerializeField] private GenericEvent constantWorkingAudio;
    //[SerializeField] private GameObject[] processBarsGameObjects;
    //private ProcessBar[] processBars;
    [Header("Animations")]
    [SerializeField] private DOTweenAnimation shake;
    [SerializeField] private DOTweenAnimation rotatingsaw;



    #endregion

    #region Cached
    //int processingSessionsIndex;
    #endregion
    #endregion

    #region Properties

    private int Energy
    {
        get
        {
            return energy;
        }
        set
        {
            energy = value;

            UpdateEnergyUI();
        }
    }

    #endregion

    #region Monobehaviour methods

    protected override void Awake()
    {
        base.Awake();
        Init();
    }

    private void Start()
    {
        binTransform.localScale = new Vector3(binScale.GetValue(), binTransform.localScale.y, binTransform.localScale.z);
    }

    private void OnDestroy()
    {
        if (constantWorkingAudio.IsPlaying())
            constantWorkingAudio.StopAudio(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        constantWorkingAudio.Release();
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        if (isWorking)
        {

            deactivationCurrentCountdown -= Time.deltaTime;
            if (deactivationCurrentCountdown <= 0.0000f)
            {
                Deactivate();
            }
        }
    }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    Debug.Log("COLLIDED");
    //    ManageTrigger(collision);
    //}

    //private void OnTriggerStay2D(Collider2D collision)
    //{
    //    ManageTrigger(collision);
    //}

    #endregion

    #region Methods

    public void ManageTrigger(Collider2D collision)
    {
        if (!IsBroken && (!hasEnergy || (Energy - energyConsumptionPerDisposal >= 0)))
        {
            if (GameFramework.Utilities.LayerIsInLayerMask(collision.gameObject.layer, targetLayerMask))
            {
                Resource resource = collision.GetComponent<Resource>();
                if (!IsFull() && resource.transform.localScale.y < maximumSizeAccepted)
                {
                    StartProcessing(resource);
                }
                else
                {
                    IsBroken = true;
                }
            }
        }
    }

    private void Init()
    {
        if (hasEnergy)
            Energy = maxEnergy;
        else if (energyBar != null)
            energyBar.gameObject.SetActive(false);

        FmodManager.instance.CreateGenericEnventInstance(ref constantWorkingAudio);
    }

    private void StartProcessing(Resource resource)
    {
        StartProcessing(resource, null);
    }

    private void StartProcessing(Resource resource, System.Action onComplete)
    {
        ConsumeEnergy(energyConsumptionPerDisposal);
        if (!isWorking)
        {
            Activate();
        }
        deactivationCurrentCountdown = deactivationCountdown;

        if (resource)
        {
            resource.Destroy();
            StartNewProcess(GetProcessingTime(resource.resourceInfo.Type.Name) + extraProcessTime.GetValue(), onComplete);
            EventManager.TriggerEvent(EventsID.R_TRASHED, resource.resourceInfo.Type.Name);
        }
        else
        {
            StartNewProcess("unknown", onComplete);
        }
    }

    public void AddEnergy(int amount)
    {
        if (hasEnergy)
        {
            if (Energy < maxEnergy)
            {
                Energy += amount;
                if (Energy > maxEnergy)
                    Energy = maxEnergy;
            }
        }
    }

    public void ConsumeEnergy(int amount)
    {
        if (hasEnergy)
        {
            if (Energy > 0)
            {
                Energy -= amount;
                if (Energy < 0)
                    Energy = 0;
            }
        }
    }

    private void UpdateEnergyUI()
    {
        if (hasEnergy)
        {
            if (energyBar)
                energyBar.value = (float)Energy / maxEnergy;
        }
    }

    private void Activate()
    {
        isWorking = true;
        shake.DOPlay();
        rotatingsaw.DOPlay();
        constantWorkingAudio.PlayAudio(transform);
    }

    private void Deactivate()
    {
        isWorking = false;
        shake.DOPause();
        rotatingsaw.DOPause();
        constantWorkingAudio.StopAudio(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    protected override void OnBreak()
    {
        base.OnBreak();
        EventManager.TriggerEvent(EventsID.BROKENTRASHER);
        if (DebugManager.Instance.DebugMode)
            GetComponentInChildren<SpriteRenderer>().color = Color.red;
    }

    protected override void OnRepair()
    {
        if (DebugManager.Instance.DebugMode)
            GetComponentInChildren<SpriteRenderer>().color = Color.white;
    }
    #endregion

}
