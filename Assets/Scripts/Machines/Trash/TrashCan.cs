﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashCan : MonoBehaviour
{
    private TrashBehaviour trashBehaviour;

    private void Awake()
    {
        trashBehaviour = GetComponentInParent<TrashBehaviour>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        trashBehaviour.ManageTrigger(collision);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        trashBehaviour.ManageTrigger(collision);
    }
}
