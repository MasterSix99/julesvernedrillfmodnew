﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineFlusher : MonoBehaviour
{
    [SerializeField]
    private LayerMask resourceLayerMask;

    [SerializeField]
    private List<Resource> enteredResources;

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.ENGINEFLUSH, FlushResources);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.ENGINEFLUSH, FlushResources);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Stone" || other.tag == "Dirt")
        {
            Resource resource = other.gameObject.GetComponent<Resource>();
            enteredResources.Add(resource);
        }

    }

    void FlushResources()
    {
        foreach(Resource res in enteredResources)
        {
            res.Destroy();
            EventManager.TriggerEvent(EventsID.R_TRASHED, res.resourceInfo.Type.Name);
        }
        enteredResources.Clear();
    }
}
