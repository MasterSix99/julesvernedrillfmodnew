﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotEquippedMachine : MachineBehaviour
{
    [System.Serializable]
    protected class ProcessBar
    {
        private bool free = true;

        private Slider slider;
        private Image image;

        public bool Free { get { return free; } }

        public ProcessBar(Slider _slider, Image _image)
        {
            slider = _slider;
            image = _image;
        }

        public void Reset()
        {
            slider.value = 0.0000f;
            image.sprite = null;
            free = true;
        }

        public void Init(Sprite imageSprite)
        {
            image.sprite = imageSprite;
            free = false;
        }

        public void UpdateSliderValue(float newValue)
        {
            slider.value = newValue;
        }

    }

    [System.Serializable]
    protected class ProcessingSession
    {
        private float processingTime;

        private float timer = 0;

        private bool isReverse = false;

        private ProcessBar processBar = null;



        private Action onComplete = null;

        //public ProcessingSession(float _processingTime, ProcessBar _processBar, Sprite _processBarSprite)
        //{
        //    processingTime = _processingTime;
        //    timer = 0.0000f;
        //    if (_processBar != null)
        //    {
        //        processBar = _processBar;
        //        processBar.Init(_processBarSprite);
        //    }
        //}

        public ProcessingSession(float _processingTime, ProcessBar _processBar, Sprite _processBarSprite, Action _onComplete,bool _isReverse)
        {
            processingTime = _processingTime;
            timer = 0.0000f;
            isReverse = _isReverse;
            if (_processBar != null)
            {
                processBar = _processBar;
                processBar.Init(_processBarSprite);
            }

            if (_onComplete != null)
                onComplete = _onComplete;
        }

        public bool PassTime()
        {
                timer += Time.deltaTime;
            if (processBar != null)
            {
                if (!isReverse)
                {
                    processBar.UpdateSliderValue(timer / processingTime);
                }
                else
                {
                    processBar.UpdateSliderValue(1 - (timer / processingTime));
                }
            }
                

            bool result = ResourceIsProcessed();
            if (result)
                onComplete?.Invoke();
            return result;
        }

        public void ResetUI()
        {
            if (processBar != null)
                processBar.Reset();
        }

        public bool ResourceIsProcessed()
        {
            return timer >= processingTime;
        }
    }

    [System.Serializable]
    protected class ProcessingInfo
    {
        public string name;
        public float processingTime;
        public Sprite sprite;
    }

    #region Variables
    [SerializeField] private ProcessingInfo[] processingInfos;
    [SerializeField] private GameObject processParticle;
    [Space]
    [SerializeField] private GameObject[] processBarsGameObjects;
    [SerializeField] private int disposalLimit = 3;
    [SerializeField] private bool reverseLoading = false;
    [SerializeField] private bool sessionsInConstantUpdate = false;

    private List<ProcessingSession> processingSessions = new List<ProcessingSession>();
    private ProcessBar[] processBars;
    private Dictionary<string, ProcessingInfo> processingInfosDictionary = new Dictionary<string, ProcessingInfo>();

    #region Cached
    private int processingSessionsIndex;
    #endregion
    #endregion

    protected virtual void Awake()
    {
        Init();
    }

    protected override void OnUpdate()
    {
        if (!sessionsInConstantUpdate)
            UpdateProcessingSessions();
    }

    protected override void ConstantUpdate()
    {
        if (sessionsInConstantUpdate)
            UpdateProcessingSessions();
    }

    protected override void OnBreak()
    {
        base.OnBreak();
    }

    protected override void OnRepair()
    {

    }

    private void Init()
    {
        int i;
        if (processBarsGameObjects.Length > 0)
        {
            processBars = new ProcessBar[disposalLimit];
            
            int processBarIndex = 0;
            for (i = 0; i < processBarsGameObjects.Length; i++)
            {
                processBars[processBarIndex] = new ProcessBar(processBarsGameObjects[i].GetComponentInChildren<Slider>(), processBarsGameObjects[i].GetComponentInChildren<Image>());
                if (processBarIndex < processBars.Length - 1)
                    processBarIndex++;
                else
                    break;
            }
        }
        
        for (i = 0; i < processingInfos.Length; i++)
        {
            processingInfosDictionary.Add(processingInfos[i].name, processingInfos[i]);
        }
    }


    /// <summary>
    /// Call this method if you want to start a new process.
    /// </summary>
    /// <param name="resourceName"></param>
    protected void StartNewProcess(string resourceName)
    {
        if (!processParticle.activeSelf)
            processParticle.SetActive(true);

        StartNewProcess(resourceName, null);
    }

    /// <summary>
    /// Call this method if you want to start a new process with a callback called at the end of the process.
    /// </summary>
    /// <param name="resourceName"></param>
    /// <param name="onComplete"></param>
    protected void StartNewProcess(string resourceName, Action onComplete)
    {
        string key = processingInfosDictionary.ContainsKey(resourceName) ? resourceName : "unknown";
        processingSessions.Add(new ProcessingSession(processingInfosDictionary[key].processingTime, GetFirstFreeProcessBar(), processingInfosDictionary[key].sprite, onComplete,reverseLoading));
    }

    /// <summary>
    /// Call this method if you want to start a new process with a callback called at the end of the process.
    /// </summary>
    /// <param name="resourceName"></param>
    /// <param name="onComplete"></param>
    protected void StartNewProcess(float processingTime, Action onComplete, Sprite processSprite = null)
    {
        if (!processParticle.activeSelf)
            processParticle.SetActive(true);
        processingSessions.Add(new ProcessingSession(processingTime, GetFirstFreeProcessBar(), processSprite, onComplete,reverseLoading));
    }

    protected float GetProcessingTime(string resourceName)
    {
        string key = processingInfosDictionary.ContainsKey(resourceName) ? resourceName : "unknown";
        return processingInfosDictionary[key].processingTime;
    }

    private void UpdateProcessingSessions()
    {
        if (processingSessions.Count > 0)
        {
            for (processingSessionsIndex = 0; processingSessionsIndex < processingSessions.Count; processingSessionsIndex++)
            {
                if (processingSessions[processingSessionsIndex].PassTime())
                {
                    processingSessions[processingSessionsIndex].ResetUI();
                    processingSessions.RemoveAt(processingSessionsIndex);
                    processingSessionsIndex--;
                    if(processingSessions.Count <1)
                        processParticle.SetActive(false);
                }
            }
        }
    }

    protected bool IsFull()
    {
        return processingSessions.Count >= disposalLimit;
    }

    private ProcessBar GetFirstFreeProcessBar()
    {
        ProcessBar resultBar = null;

        int i;
        for (i = 0; i < processBars.Length; i++)
        {
            if (processBars[i].Free)
            {
                resultBar = processBars[i];
                break;
            }
        }
        return resultBar;
    }
}
