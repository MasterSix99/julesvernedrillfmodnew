﻿using GameFramework;
using UnityEngine;
using UnityEngine.Events;

public abstract class MachineBehaviour : MonoBehaviour
{
    [SerializeField] protected bool isBreakable = true;
    [SerializeField] protected JoystickVibrationConfig breakVibration;
    private bool isBroken = false;
    [SerializeField]
    [ReadOnly]
    protected bool isOwned = false;
    [Header("Audio")]
    [SerializeField] private OneShotAudio breakAudio;
    [Header("Debug")]
    [SerializeField] protected KeyCode breakKey;

    protected bool isRepairing = false;

    [Header("Events")]
    [SerializeField] public UnityEvent OnBreakUnityEvt;
    [SerializeField] public UnityEvent OnRepairUnityEvt;

    [Header("Generic upgradables")]
    [SerializeField] private EntityFloatStat machineSize;

    protected InteractionManager2D owner;

    public bool IsBroken
    {
        get
        {
            return isBroken;
        }
        set
        {
            if (isBreakable)
            {
                isBroken = value;
                if (isBroken)
                {
                    OnBreak();
                    if (OnBreakUnityEvt != null)
                        OnBreakUnityEvt.Invoke();
                    //onbreakevent
                }
                else
                {
                    OnRepair();
                    if (OnRepairUnityEvt != null)
                        OnRepairUnityEvt.Invoke();
                }
                //onrepairevent
            }
        }

    }
    public bool IsOwned {
        get { return isOwned; }
    }

    public InteractionManager2D Owner
    {
        get { return owner; }
    }

    public bool IsRepairing
    {
        get { return isRepairing; }
        set { isRepairing = value; }
    }

    #region Monobehaviour Methods
    protected virtual void Start()
    {
        float scale = machineSize.GetValue();
        transform.localScale = new Vector3(scale, scale, scale);
    }

    private void Update()
    {
        if (Input.GetKeyDown(breakKey))
        {
            IsBroken = !IsBroken;
        }
        ConstantUpdate();
        if (!isBroken)
        {
            OnUpdate();
        }
        //if (debugMode)
        //{
        //    DebugRepair();
        //}

    }
    #endregion

    #region Methods
    public virtual void GainControl(InteractionManager2D interactionManager2D)
    {
        if (interactionManager2D != null && !IsBroken && !IsOwned)
        {
            isOwned = true;
            owner = interactionManager2D;
            interactionManager2D.FreezeCharacter();
            interactionManager2D.Rb.simulated = false;
        }
    }
    public virtual void ReleaseControl(InteractionManager2D interactionManager2D)
    {
        InteractionManager2D interactor = interactionManager2D == null ? owner : interactionManager2D;
        if (IsOwned && interactor != null) { 
            isOwned = false;
            owner = null;
            interactor.ReleaseCharacter();
            interactor.Rb.simulated = true;
        }
    }

    protected virtual void OnBreak() {
        foreach (Rewired.Player player in Rewired.ReInput.players.AllPlayers)
        {
            JoystickVibration.Vibrate(player, breakVibration);
        }
        if (breakAudio.eventPath != null)
        {
            breakAudio.PlayAudio(transform.position);
            Debug.Log("Playing break audio");
        }

        ReleaseControl(owner);
    }



    protected abstract void OnRepair();

    protected virtual void OnUpdate()
    {

    }

    protected virtual void ConstantUpdate()
    {

    }

    //private void DebugRepair()
    //{
    //    if (Input.GetKeyDown(KeyCode.R))
    //    {
    //        IsBroken = false;
    //    }
    //}


    // INSPECTOR UTILITIES
    /*[Button]
    public void SetBroken()
    {
        if (!IsBroken)
            IsBroken = true;
    }
    [Button]
    public void Repair()
    {
        if (IsBroken)
        {
            IsBroken = false;
        }
    }*/
    #endregion
}
