﻿using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HullBehaviour : MonoBehaviour
{
    [Header("Heat increase stats")]
    [SerializeField] private FloatVariable heatIncreasePerSecond;
    //[SerializeField] private float heatIncreaseCoefficient = 1;
    [SerializeField] private FloatVariable heatValue;
    [SerializeField] private EntityFloatStat heatValueMax;
    private float currentMaxHeatValue;
    [SerializeField] private float heatValueMin;
    [SerializeField] private float timeForRepairingBeforeDamage;
    [SerializeField] private float heatDecreaseSteps;
    [SerializeField] private float dirtOffset = 0;
    [SerializeField] private FloatVariable actualSpeed;


    [SerializeField] private HullSpawner[] holes;

    [Header("UI")]
    [SerializeField] IntegrityBar integrityBar;

    [Header("Audio")]
    [SerializeField] GenericEventMonoParameter dirtFlowSound;
    [SerializeField] GenericEvent alarmSound;

    [Header("Alarm Light")]
    [SerializeField] GameObject alarmLight;

    private bool isCheckingHeat = false;
    private List<HullSpawner> availableHoles = new List<HullSpawner>();
    private List<HullSpawner> previuslyBrokenHoles = new List<HullSpawner>();

    private GroundTile.TileType currentTile;

    #region Properties

    public float HeatValue
    {
        get { return heatValue.Value; }
        private set
        {
            heatValue.Value = Mathf.Clamp(value, heatValueMin, currentMaxHeatValue);
            integrityBar.UpdateValue(heatValue.Value / heatValueMax.GetValue());

            if (heatValue.Value >= currentMaxHeatValue)
            {
                if (!isCheckingHeat)
                    StartCoroutine(HeatCheckCoroutine());
            }
        }
    }

    #endregion

    private void OnEnable()
    {
        EventManager.StartListening<GroundTile.TileType>(EventsID.DRILL_CHANGETILE, OnCurrentTileChange);
        EventManager.StartListening(EventsID.HOLEREPAIRED, OnHoleRepaired);
    }

    private void OnDisable()
    {
        EventManager.StopListening<GroundTile.TileType>(EventsID.DRILL_CHANGETILE, OnCurrentTileChange);
        EventManager.StopListening(EventsID.HOLEREPAIRED,OnHoleRepaired);
    }

    private void Start()
    {
        FmodManager.instance.CreateGenericMonoEventParameterInstance(ref dirtFlowSound);
        FmodManager.instance.CreateGenericEnventInstance(ref alarmSound);
        currentMaxHeatValue = heatValueMax.GetValue();
    }

    private void OnDestroy()
    {
        dirtFlowSound.StopAudio(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        dirtFlowSound.Release();
    }

    private void Update()
    {
        if(actualSpeed.Value > 1.0f)
        {
            UpdateHeatValue();
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            HeatValue += 5;
        }
        else if (Input.GetKeyDown(KeyCode.K))
        {
            HeatValue -= 5;
        }
    }


    private void OverHeat()
    {
        integrityBar.Break();
        UpdateAvailableHoles();

        if (availableHoles.Count > 0)
        {
            int randomIndex = Random.Range(0, availableHoles.Count);
            List<int> indexes = new List<int>();
            indexes.Add(randomIndex);

            int i;
            for (i = 0; i < availableHoles.Count; i++)
            {
                if (i != randomIndex && previuslyBrokenHoles.Contains(availableHoles[i]))
                    indexes.Add(i);
            }
            if (!previuslyBrokenHoles.Contains(availableHoles[randomIndex]))
                previuslyBrokenHoles.Add(availableHoles[randomIndex]);

            BreakHoles(indexes);
        }

        EventManager.TriggerEvent(EventsID.ONDRILLOVERHEAT);
        Debug.Log("Over heat");
    }

    private void BreakHoles(List<int> indexes)
    {
        int i;
        for (i = 0; i < indexes.Count; i++)
        {
            availableHoles[indexes[i]].IsBroken = true;
            availableHoles[indexes[i]] = null;
        }

        for (i = 0; i < availableHoles.Count; i++)
        {
            if (availableHoles[i] == null)
            {
                availableHoles.RemoveAt(i);
                i--;
            }
        }

        UpdateDirtFlowAudio();

        if(alarmLight.activeInHierarchy == false)
        {
            alarmLight.SetActive(true);
            alarmSound.PlayAudio(alarmLight.transform);
        }
    }

    private void UpdateHeatValue()
    {
        if ((HeatValue > heatValueMin && heatIncreasePerSecond.Value < 0) || (HeatValue < currentMaxHeatValue && heatIncreasePerSecond.Value > 0))
            HeatValue += heatIncreasePerSecond.Value/* * heatIncreaseCoefficient*/ * Time.deltaTime;

        if (DebugManager.Instance.DebugMode)
        {
            if (Input.GetKeyDown(KeyCode.H))
            {
                HeatValue += 5;
            }
            if (Input.GetKeyDown(KeyCode.G))
            {
                HeatValue -= 5;
            }
        }
    }

    private IEnumerator HeatCheckCoroutine()
    {
        isCheckingHeat = true;
        EventManager.TriggerEvent(EventsID.ONDRILLOVERHEATAWARE);
        yield return new WaitForSeconds(timeForRepairingBeforeDamage);
        if (HeatValue >= currentMaxHeatValue)
        {
            currentMaxHeatValue -= heatValueMax.GetValue() / heatDecreaseSteps;
            HeatValue = heatValueMin;

            OverHeat();

            if (currentMaxHeatValue <= heatValueMin)
            {
                EventManager.TriggerEvent(EventsID.MINIMUMHEATREACHED);
            }
        }
        isCheckingHeat = false;
    }

    private void UpdateAvailableHoles()
    {
        availableHoles.Clear();

        for (int i = 0; i < holes.Length; i++)
        {
            if (!holes[i].IsBroken)
                availableHoles.Add(holes[i]);
        }
    }

    private void OnCurrentTileChange(GroundTile.TileType newTile)
    {
        currentTile = newTile;
    }

    private void OnHoleRepaired()
    {
        Debug.Log("check siren called");
        UpdateAvailableHoles();

        UpdateDirtFlowAudio();

        if (availableHoles.Count == holes.Length)
        {
            EventManager.TriggerEvent(EventsID.ALLHOLESREPAIRED);
            if (alarmLight.activeInHierarchy == true)
            {
                alarmLight.SetActive(false);
                alarmSound.TriggerCue();
            }
        }
    }

    private void UpdateDirtFlowAudio()
    {
        dirtFlowSound.ChangeParameter((float)(holes.Length - availableHoles.Count) / holes.Length * 3);

        if (!dirtFlowSound.IsPlaying())
            dirtFlowSound.PlayAudio(transform);
    }
}