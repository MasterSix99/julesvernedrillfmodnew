﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnGroup
{
    [System.Serializable]
    public class ResourceInfo
    {
        public GameObject resource;
        public int weightPercentage;
    }

    public GroundTile.TileType correspondingTile;

    [Space(10)]
    public float quantityPerSecond = 1;
    public float kilometersPerQuantity = 10;
    public bool divideQuantityInTime = false;
    public ResourceInfo[] resources;
}
