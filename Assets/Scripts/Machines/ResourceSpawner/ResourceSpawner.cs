﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameFramework;
using DG.Tweening;

public class ResourceSpawner : MachineBehaviour
{
    #region Variables

    [Header("Generation infos")]
    [SerializeField] private Transform spawnTransform;
    [SerializeField] private Transform resourcesParent;
    [SerializeField] private SpawnGroup[] spawnGroups;
    [SerializeField] private int minGatherMultiplier = 1;
    [SerializeField] private bool addForceOnSpawn = true;
    [SerializeField] private bool checkSpeedToSpawn = true;
    [SerializeField] private bool hasFixedSpawnGroup = false;
    [SerializeField] [ShowIf("hasFixedSpawnGroup")] private GroundTile.TileType fixedSpawnType = GroundTile.TileType.Dirt;
    [SerializeField] private bool hasFixedGatherFrequency = false;
    [SerializeField] [ShowIf("hasFixedGatherFrequency")] private float fixedGatherFrequency = 2;
    [SerializeField] private Vector2 minForce;
    [SerializeField] private Vector2 maxForce;
    [SerializeField] private FloatVariable drillSpeed;
    [Tooltip("max resources that can be in the checkbox until the machine breaks")]
    [SerializeField] private EntityIntStat maxResourcesAroundSpawner;
    [Tooltip("the height of the checkbox")]
    [SerializeField] private float checkBoxY;
    [Tooltip("the width of the checkbox")]
    [SerializeField] private float checkBoxX;
    [Tooltip("the angle of the box")]
    // now needs to be implemented in the gizmo, if needed
    private float boxAngle = 0;
    [Tooltip("the layer that the checkbox needs to read - should be the resouces layer")]
    [SerializeField] private LayerMask _breakCheckLayer;
    [Tooltip("how frequently the check for break condition should happen")]
    [SerializeField] private float checkTimeCycle = 3;
    [SerializeField]
    private int maxQueuedResources = 0;
    private List<GameObject> queuedResources = new List<GameObject>();

    [Header("Visual")]
    [SerializeField]
    private Transform queueStartTransform;
    [SerializeField]
    private float distanceBetweenResources;
    [ShowIf("canBoostSpawnRate")]
    [SerializeField]
    private Transform leverTransform;
    [SerializeField]
    private float rotationSpeed;
    [SerializeField]
    private float leverMovementRotation;
    private Quaternion leverStartRotation;
    private Quaternion leverEndRotation;
    [SerializeField]
    private DOTweenAnimation mill;

    [Header("Boost")]
    [SerializeField]
    private float defaultSpawnRate = 0.0000f;
    [SerializeField]
    private bool canBoostSpawnRate = false;
    [ShowIf("canBoostSpawnRate")] [SerializeField]
    private float boostedSpawnRate = 0.0000f;
    private bool isBoostedSpawnRate = false;

    private Collider2D[] inZoneCollider = new Collider2D[] { };
    private float checkTimer;

    [Header("Upgradables")]
    [SerializeField] private EntityFloatStat extraSpawnRate;

    [Header("Audio")]
    [SerializeField] private OneShotAudio spawnAudio;
    [Header("Vibration")]
    [SerializeField]
    private JoystickVibrationConfig spawnVibration;

    private float finalGatherFrequency;
    private float gatherFrequency;
    private float kilometersPerQuantity;
    private int currentGatherMultiplier = 0;
    private float gatherTimer;
    private bool divideQuantityInTime;

    private float spawnRate = 0.0000f;
    private float spawnTimer = 0.0000f;

    private SpawnGroup.ResourceInfo[] currentSpawnList;
    
    private GroundTile.TileType spawnType;

    #region Cached
    private GameObject resourceToSpawn;
    private GameObject spawnedResource;
    private Vector2 finalForce = Vector2.zero;
    #endregion

    #endregion

    #region Properties
    public bool IsBoostedSpawnRate
    {
        get
        {
            return isBoostedSpawnRate;
        }
        set
        {
            isBoostedSpawnRate = value;
            if (canBoostSpawnRate)
            {
                SwitchSpawnRate();
                StartCoroutine(MoveLever());
            }
        }
    }
    #endregion

    #region Monobehaviour Methods

    protected override void Start()
    {
        base.Start();
        inZoneCollider = new Collider2D[maxResourcesAroundSpawner.GetValue()];
        if (hasFixedSpawnGroup)
        {
            ChangeCurrentSpawnType(fixedSpawnType);
            
        }
        if (hasFixedGatherFrequency)
            finalGatherFrequency = fixedGatherFrequency;

        //Visual
        //leverTransform.rotation = Quaternion.Euler(0, 0, -leverMovementRotation);
        //leverStartRotation = leverTransform.rotation;
        spawnRate = defaultSpawnRate;
        IsBoostedSpawnRate = false;
        
    }

    private void OnEnable()
    {
        if (!hasFixedSpawnGroup)
            EventManager.StartListening<GroundTile.TileType>(EventsID.DRILL_CHANGETILE, ChangeCurrentSpawnType);
    }

    private void OnDisable()
    {
        if (!hasFixedSpawnGroup)
            EventManager.StopListening<GroundTile.TileType>(EventsID.DRILL_CHANGETILE, ChangeCurrentSpawnType);
    }

    protected override void OnUpdate()
    {
        MachineUpdate();
        CheckBreakConditionCycle();
    }

    private void OnDrawGizmos()
    {
        Color oldColor = Gizmos.color;
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(spawnTransform.position, new Vector2(checkBoxX, checkBoxY));
        Gizmos.color = oldColor;
    }
    #endregion

    #region Methods

    public void ToogleBoostState()
    {
        IsBoostedSpawnRate = !IsBoostedSpawnRate;
    }

    protected void MachineUpdate()
    {
        if ((!checkSpeedToSpawn || drillSpeed.Value >= 0.1f) && !queuedResourcesAreMax())
        {
            if (!hasFixedGatherFrequency)
                GetFinalGatherFrequency();

            if (!divideQuantityInTime)
            {
                if (gatherTimer < 1)
                    gatherTimer += Time.deltaTime;
                else
                {
                    gatherTimer = 0.0000f;
                    if (finalGatherFrequency >= 1)
                    {
                        resourceToSpawn = GetResourceToSpawn();
                        int i = 0;
                        while (i < Mathf.FloorToInt(finalGatherFrequency))
                        {
                            EnqueueResource(resourceToSpawn);
                            i++;
                        }
                    }
                }
            }
            else
            {
                if (gatherTimer < 1 / finalGatherFrequency)
                    gatherTimer += Time.deltaTime;
                else
                {
                    gatherTimer = 0.0000f;
                    EnqueueResource(GetResourceToSpawn());
                }
            }
        }
        else if (gatherTimer != 0.00f)
        {
            gatherTimer = 0f;
        }

        UpdateSpawn();
    }

    private GameObject GetResourceToSpawn()
    {
        GameObject result = null;

        int randomI = Random.Range(1, 101);
        int currentPercentageAmount = 0;
        int lastPercentageAmount;
        int i;
        for (i = 0; i < currentSpawnList.Length; i++)
        {
            lastPercentageAmount = currentPercentageAmount;
            currentPercentageAmount += currentSpawnList[i].weightPercentage;
            //Debug.Log("Last: " + lastPercentageAmount + ", current: " + currentPercentageAmount);
            if (randomI > lastPercentageAmount && randomI <= currentPercentageAmount)
            {
                result = currentSpawnList[i].resource;
                break;
            }
        }

        return result;
    }

    private void UpdateSpawn()
    {
        
        if (queuedResources.Count > 0)
        {   
            spawnTimer += Time.deltaTime;
            mill.DOPlay();
            if (spawnTimer >= spawnRate)
            {
                DequeueResource();
                spawnTimer = 0.0000f;
            }
        }
        else if (spawnTimer != 0.0000f)
            spawnTimer = 0.0000f;
    }

    public void SwitchSpawnRate()
    {
        spawnRate = isBoostedSpawnRate ? boostedSpawnRate : defaultSpawnRate;
    }

    private bool queuedResourcesAreMax()
    {
        return queuedResources.Count >= maxQueuedResources;
    }

    private void EnqueueResource(GameObject inResource)
    {
        //Debug.Log("Enqueuing");
        queuedResources.Add(PoolManager.GetObject(inResource));
        UpdateQueuedResourcesPositions();
    }

    private void DequeueResource()
    {
        //Debug.Log("Dequeuing");
        Spawn(queuedResources[0]);
        queuedResources[0].SetActive(false);
        queuedResources.RemoveAt(0);
        UpdateQueuedResourcesPositions();
    }

    private void UpdateQueuedResourcesPositions()
    {
        int i;
        for (i = 0; i < queuedResources.Count; i++)
        {
            queuedResources[i].transform.position = queueStartTransform.position + (queueStartTransform.up * (distanceBetweenResources * (queuedResources.Count - i - 1)));
            //queuedResources[i].transform.rotation = queueStartTransform.rotation;
            queuedResources[i].transform.SetParent(queueStartTransform);
        }
    }

    private void Spawn(GameObject simpleResource)
    {
        spawnedResource = PoolManager.GetObject(simpleResource.GetComponent<SimpleResource>().CorrespondingResource, spawnTransform.position, spawnTransform.rotation);
        Resource _resource = spawnedResource.GetComponent<Resource>();
        _resource.AdviseSpawn();
        //AUDIO
        spawnAudio.PlayAudio(transform.position);

        if (resourcesParent != null)
            spawnedResource.transform.SetParent(resourcesParent);

        if (addForceOnSpawn)
        {
            finalForce.Set(Random.Range(minForce.x, maxForce.x), Random.Range(minForce.y, maxForce.y));
            _resource.Rb.velocity = finalForce;
        }
        foreach (Rewired.Player player in Rewired.ReInput.players.AllPlayers)
            JoystickVibration.Vibrate(player, spawnVibration);
        mill.DOPause();
    }

    private void ChangeCurrentSpawnType(GroundTile.TileType newType)
    {
        spawnType = newType;
        int i;
        for (i = 0; i < spawnGroups.Length; i ++)
        {
            if (spawnType == spawnGroups[i].correspondingTile)
            {
                gatherFrequency = spawnGroups[i].quantityPerSecond;
                kilometersPerQuantity = spawnGroups[i].kilometersPerQuantity;
                divideQuantityInTime = spawnGroups[i].divideQuantityInTime;
                currentSpawnList = spawnGroups[i].resources;
                break;
            }
        }
    }

    private void GetFinalGatherFrequency()
    {
        //floorGatherMultipler = Mathf.FloorToInt(tmpSpeed / kilometersPerQuantity);
        //if (gatherFrequency != finalGatherFrequency / currentGatherMultiplier)
        //{
        //    currentGatherMultiplier = Mathf.Clamp(floorGatherMultipler, minGatherMultiplier, floorGatherMultipler);
        //    finalGatherFrequency = currentGatherMultiplier * gatherFrequency;
        //}
        currentGatherMultiplier = Mathf.FloorToInt(drillSpeed.Value / kilometersPerQuantity) + minGatherMultiplier;

        float gatherExtraMultiplier = extraSpawnRate.GetValue();
        if (finalGatherFrequency != currentGatherMultiplier * gatherFrequency * gatherExtraMultiplier)
        {
            finalGatherFrequency = currentGatherMultiplier * gatherFrequency * gatherExtraMultiplier;
        }
    }

    private void CheckBreakConditions()
    {
        inZoneCollider = Physics2D.OverlapBoxAll(spawnTransform.position, new Vector2(checkBoxX, checkBoxY),boxAngle,_breakCheckLayer);
        if(inZoneCollider.Length >= maxResourcesAroundSpawner.GetValue())
        {
            IsBroken = true;
        }
        
    }

    private void CheckBreakConditionCycle()
    {
        checkTimer += Time.deltaTime;
        if(checkTimer>= checkTimeCycle)
        {
            CheckBreakConditions();
            checkTimer = 0f;
        }
    }

    private IEnumerator MoveLever()
    {
        leverStartRotation = leverTransform.localRotation;
        leverEndRotation = Quaternion.Euler(0, 0, IsBoostedSpawnRate ? leverMovementRotation : -leverMovementRotation);
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime * rotationSpeed;
            if (t >= 1)
                t = Mathf.Clamp01(t);
            leverTransform.localRotation = Quaternion.Lerp(leverStartRotation, leverEndRotation, t);
            yield return null;
        }
        yield return null;
    }

    protected override void OnBreak()
    {
        base.OnBreak();
        EventManager.TriggerEvent(EventsID.BROKENSPAWNER);
    }

    protected override void OnRepair()
    {

    }

    #endregion
}
