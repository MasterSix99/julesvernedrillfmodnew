﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HullSpawner : MachineBehaviour
{
    #region Variables
    [Header("Core")]
    [SerializeField] private GameObject patchGameObject;
    [SerializeField] private GameObject animatorObject;
    [SerializeField] private Transform spawnTransform;
    [SerializeField] private GameObject resourceToSpawn;
    [SerializeField] private Transform resourcesParent;
    [SerializeField] private float gatherFrequency = 2;
    [SerializeField] private bool addForceOnSpawn = true;
    [SerializeField] private Vector2 minForce;
    [SerializeField] private Vector2 maxForce;
    private bool brokenBefore = false;
    private float gatherTimer = 0.0000f;

    [Header("Audio")]
    [SerializeField] private OneShotAudio spawnAudio;
    [Header("Vibration")]
    [SerializeField]
    private JoystickVibrationConfig spawnVibration;

    #region Cached
    private Vector2 finalForce = Vector2.zero;
    private Resource spawnedResource;
    #endregion
    #endregion

    protected override void OnUpdate()
    {
        
    }

    protected override void ConstantUpdate()
    {
        if (IsBroken)
            MachineUpdate();
    }

    protected override void OnBreak()
    {
        base.OnBreak();
        gatherTimer = 0.0000f;
        if (!brokenBefore)
        {
            brokenBefore = true;
            animatorObject.SetActive(true);
        }
        else
        {
            patchGameObject.SetActive(true);
        }
    }

    protected override void OnRepair()
    {
        if (patchGameObject.activeInHierarchy)
            patchGameObject.SetActive(false);
        else if (animatorObject.activeInHierarchy)
            animatorObject.SetActive(false);
        EventManager.TriggerEvent(EventsID.HOLEREPAIRED);
    }

    #region Methods

    private void MachineUpdate()
    {
        if (gatherTimer < gatherFrequency)
            gatherTimer += Time.deltaTime;
        else
        {
            gatherTimer = 0.0000f;
            Spawn(resourceToSpawn);
        }
    }

    private void Spawn(GameObject resource)
    {
        spawnedResource = PoolManager.GetObject(resource, spawnTransform.position, spawnTransform.rotation).GetComponent<Resource>();
        EventManager.TriggerEvent(EventsID.R_GATHERED, spawnedResource.resourceInfo.Type.Name);
        //AUDIO
        spawnAudio.PlayAudio(transform.position);

        if (resourcesParent != null)
            spawnedResource.transform.SetParent(resourcesParent);

        if (addForceOnSpawn)
        {
            finalForce.Set(Random.Range(minForce.x, maxForce.x), Random.Range(minForce.y, maxForce.y));
            spawnedResource.Rb.velocity = finalForce;
        }
        foreach (Rewired.Player player in Rewired.ReInput.players.AllPlayers)
            JoystickVibration.Vibrate(player, spawnVibration);
    }

    #endregion
}
