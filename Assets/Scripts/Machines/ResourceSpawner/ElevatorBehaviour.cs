﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ElevatorBehaviour : MonoBehaviour
{
    [SerializeField]
    private float goingUpTime, upStillTime, goingDownTime;

    [Header("Audio")]
    [SerializeField]
    private GenericEvent elevatorAudio;

    [Header("Movement")]
    [SerializeField] private DOTweenAnimation forwardsMovementTween;
    [SerializeField] private DOTweenAnimation backwardsMovementTween;

    private bool isMoving = false;

    private void Start()
    {
        FmodManager.instance.CreateGenericEnventInstance(ref elevatorAudio);
    }

    public void StartElevator()
    {
        if (!isMoving)
        {
            isMoving = true;
            PlayElevatorAudio();
            forwardsMovementTween.DOPlay();
        }
    }

    public void StopMovementForward()
    {
        elevatorAudio.TriggerCue();
    }

    public void StartMovementBackward()
    {
        backwardsMovementTween.DOPlay();
    }

    public void PlayElevatorAudio()
    {
        elevatorAudio.PlayAudio(transform);
    }

    public void StopMovementBackward()
    {
        isMoving = false;
        elevatorAudio.TriggerCue();
        forwardsMovementTween.DORewind();
        backwardsMovementTween.DORewind();
    }
}
