﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameFramework;
using DG.Tweening;
using UnityEngine.Events;

public class EngineBehaviour : SlotEquippedMachine
{
    /*
    TEST FOR UPGRADES
    public EntityIntStat testInt;
    public EntityFloatStat testFloat;
    public EntityBoolStat testBool;
    public EntityVector2Stat testVecto2;
    */
    [SerializeField] private UnityEvent OnEngineStart;

    #region VARIABLES
    [Tooltip("is the engine operating?")]
    [SerializeField]
    protected bool isOperating;
    [Tooltip("is the engine burning?")]
    [SerializeField]
    protected bool isBurning;
    protected bool startEngine;
    protected bool isReleasingPressure = false;
    [Header("Audio")]
    [SerializeField]
    protected GenericEventMonoParameter engineConstantSound;
    [SerializeField] [Tooltip("Goes from 0 to 1")]
    protected float audioActivationThreshold = 0.33f;
    [Header("Animation")]
    //[SerializeField] private Animator animator;
    [SerializeField]
    private DOTweenAnimation[] OuterPistons;
    [SerializeField]
    private DOTweenAnimation[] InnerPistons;
    [SerializeField]
    private DOTweenAnimation[] MainPiston;
    [SerializeField]
    private DOTweenAnimation[] HorizontalPiston;
    [SerializeField]
    private GameObject movementVfx;
    [SerializeField]
    private AnimatorGroup animatorGroup;

    private bool animationStart = false;
    private bool isAudio = false;

    #region SPEED VARIABLES
    [Header("Speed Variables")]
    [Tooltip("Speed output of the machine, this will move the drill")]
    [SerializeField]
    private EntityFloatStat actualSpeed;
    private float lastActualSpeed;
    //scriptable variable for speed output
    [SerializeField]
    private FloatVariable drillSpeed;
    [Tooltip("minimum value of speed, this should stay at 0")]
    [SerializeField]
    private float minSpeed = 0;
    [Tooltip("maximum value of speed")]
    [SerializeField]
    private EntityFloatStat maxSpeed;
    //the speed modifier value
    private float speedModifier;
    [Header("Speed Increase Modifier")]
    [Tooltip("slow speed modifier")]
    [SerializeField]
    private float slowSpeedModifier;
    [Tooltip("mid speed modifier")]
    [SerializeField]
    private float midSpeedModifier;
    [Tooltip("high speed modifier")]
    [SerializeField]
    private float highSpeedModifier;
    [Tooltip("decay value of speed")]
    [SerializeField]
    public FloatVariable speedDecayValue;
    [SerializeField]
    private float initialDrillSpeed;
    [SerializeField]
    private float speedGameOverCondition;
    #endregion

    #region PRESSURE VARIABLES
    [Header("Pressure Variables")]
    [Tooltip("engine's pressure")]
    [SerializeField]
    private EntityFloatStat actualPressure;
    //Pressure Variable Output
    [SerializeField]
    private FloatVariable enginePressure;
    [Tooltip("minimum value of pressure, this should be 0")]
    [SerializeField]
    private float minPressure = 0;
    [Tooltip("maximum value of pressure")]
    [SerializeField]
    private EntityFloatStat maxPressure;
    //defines all the kinds of pressure zone you can have
    private enum PressureZones
    {
        slow,
        normal,
        fast
    }
    //the actual pressure of the engine
    private PressureZones activePressureZone;
    //variables to define various states of pressure
    [Header("Pressure Limits")]
    [Tooltip("pressure's slow limit")]
    [SerializeField]
    private float pressureSlowLimit;
    [Tooltip("pressure's mid limit")]
    [SerializeField]
    private float pressureNormalLimit;
    [Tooltip("pressure's fast limit")]
    [SerializeField]
    private float pressureFastLimit;
    [Header("Pressure Increase Constants")]
    [Tooltip("pressure's low constant")]
    [SerializeField]
    private float pressureIncreaseLowConstant;
    [Tooltip("pressure's mid constant")]
    [SerializeField]
    public float pressureIncreaseMidConstant;
    [Tooltip("pressure's high constant")]
    [SerializeField]
    public float pressureIncreaseHighConstant;
    [Tooltip("amount of pressure released on demand")]
    [SerializeField]
    private float pressureLossOnRelease;
    [Tooltip("indicates how much pressure is increased each second, this cannot be 0")]
    [SerializeField]
    private float pressureIncreaseValue;
    private float lastPressureValue;
    [SerializeField]
    private ParticleSystem pressureParticle;
    [SerializeField]
    private GameObject engineSprite;
    private Vector2 engineStartPos;
    [SerializeField]
    private DOTweenAnimation drillSprite;


    #endregion

    #region TEMPERATURE VARIABLES
    [Header("temperature variables")]
    //this is the engine's temperature output
    [Tooltip("engine's temperature")]
    [SerializeField]
    protected float temperature;
    //temperature output
    [SerializeField]
    private FloatVariable engineTemperature;
    //minimum temperature 
    [Tooltip("minimum reachable temperature")]
    [SerializeField]
    private float minTemperature = 0;
    //maximum temperature
    [Tooltip("maximum reachable temperature")]
    [SerializeField]
    private float maxTemperature;
    //defines all the kinds of temperature you can have.
    protected enum TemperatureZones
    {
        low,
        mid,
        high
    }
    //actual active zone of temperature
    [Tooltip("actual active zone of temperature")]
    protected TemperatureZones activeTemperatureZone;
    //decay value of temperature
    [Tooltip("defines the temperature's constant decay value")]
    [SerializeField]
    protected float temperatureDecayValue;
    //variables to define various states of temperature
    [Tooltip("defines the amount of temperature lost when you feed the engine a non-fuel resource")]
    [SerializeField]
    protected float temperatureLossOnEngineBreak;
    [Tooltip("defines the amount of non-resources that you can put inside the engine before it breaks")]
    [SerializeField]
    protected int nonResourceLimit;
    [Header("Temperature Limits")]
    [Tooltip("defines 1st limit of temperature")]
    [SerializeField]
    protected float temperatureLowLimit;
    [Tooltip("defines 2nd limit of temperature")]
    [SerializeField]
    protected float temperatureMidLimit;
    [Tooltip("defines 3rd limit of temperature")]
    [SerializeField]
    protected float temperatureHighLimit;
    //burner's cooldown time sent to temperature 
    [Tooltip("defines how much time the engine should cooldown if a non-resource element is put inside the engine")]
    protected float coolDownTime;
    //this is the layer of resources to enable burner method.
    [Tooltip("this is the layer of resources to enable burner process")]
    [SerializeField]
    private LayerMask resourceLayerMask;
    [Tooltip("maxium size that can go into engine")]
    [SerializeField]
    private float maximumSizeAccepted;

    [SerializeField]
    private SpriteRenderer[] engineHeatSprite;
    private SpriteRenderer tmp;
    private Color newHeatColor;

    [SerializeField]
    private EntityFloatStat fuelModifier;
    #endregion
    #endregion

    #region PROPERTIES

    public float Pressure
    {
        get { return actualPressure.GetValue(); }
        private set
        {
            //Debug.Log(value);
            lastPressureValue = actualPressure.GetValue();
            actualPressure.SetBaseValue(Mathf.Clamp(value, minPressure, maxPressure.GetValue()));
            enginePressure.Value = actualPressure.GetValue();
            //Debug.Log("the drills pressure is : " + actualPressure.GetValue());
            if (lastPressureValue != actualPressure.GetValue())
                OnPressureChange();
        }
    }

    public float Speed
    {
        get { return actualSpeed.GetValue(); }
        private set
        { 
            lastActualSpeed = actualSpeed.GetValue();
            actualSpeed.SetBaseValue(Mathf.Clamp(value, minSpeed, maxSpeed.GetValue()));
            drillSpeed.Value = actualSpeed.GetValue();
        }
    }

    public float Temperature
    {
        get { return temperature; }
        private set { temperature = Mathf.Clamp(value, minTemperature, maxTemperature); animatorGroup.SetFloat("temperature", temperature); }
    }

    public bool Operating
    {
        get { return isOperating; }
        protected set { isOperating = value; }
    }

    public bool Burning
    {
        get { return isBurning; }
        protected set { isBurning = value; }
    }

    #endregion

    #region Monobehaviour Methods

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.GAMEEND, OnGameEnd);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.GAMEEND, OnGameEnd);
    }

    protected override void Start()
    {
        base.Start();

        drillSpeed.Value = initialDrillSpeed;
        newHeatColor = new Color(1, 1, 1, 0);
        //AUDIO
        GenerateAudioEvents();
        engineConstantSound.ChangeParameter(0f);
        engineConstantSound.PlayAudio(transform);
        engineStartPos = engineSprite.transform.localPosition;
        
    }

    private void OnDestroy()
    {
        engineConstantSound.StopAudio(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        engineConstantSound.Release();
    }

    //engine methods that should never stop running, even if the engine is broken
    protected override void ConstantUpdate()
    {
        //temperature methods
        CheckStatus();
        CheckTemperatureZone();
        ConstantTemperatureLoss();
        //pressure methods
        CheckPressureIncreaseValue();
        CheckPressureZone();
        // speed methods
        ConstantSpeedDecay();
        CheckSpeedModifier();

        drillSpeed.Value = Speed;
        engineTemperature.Value = Temperature;
        enginePressure.Value = Pressure;
    }

    //Update functioning only when MachineBehaviour.isBroken == false.
    protected override void OnUpdate()
    {
        base.OnUpdate();
        //temperature methods
        //temperature is triggered with burner, once is broken the burner should not be able to function.
        //pressure methods
        PressureGainer();
                if (isReleasingPressure)
            PressureRelease();
        //speed methods
        SpeedIncreaser();
        //UpdateConstantAudio();
    }
    #endregion

    #region TEMPERATURE METHODS
    //returns Status of engine On/Off
    private void CheckStatus()
    {
        //to do : in properties
        isOperating = temperature > 0f ? true : false;
    }

    //changes temperature zone based on temperature
    private void CheckTemperatureZone()
    {
        if (temperature <= temperatureLowLimit)
        {
            activeTemperatureZone = TemperatureZones.low;
        }
        else if (temperature <= temperatureMidLimit)
        {
            activeTemperatureZone = TemperatureZones.mid;
        }
        else if (temperature <= temperatureHighLimit)
        {
            activeTemperatureZone = TemperatureZones.high;
        }
    }

    //constant decay of temperature
    private void ConstantTemperatureLoss()
    {
        if (Temperature > 0f && !isBurning)
        {
            Temperature -= temperature * temperatureDecayValue;
        }
    }

    //check if the resource has energy to burn, if it does then sends energy to, else calls the coolDown iteration;
    private void Burner(Resource resource)
    {
        float energy = resource.resourceInfo.Fuel;
        if (resource.resourceInfo.IsFontOfFuel)
        {
            StartNewProcess(energy,null);
            StartCoroutine(WarmUp(energy));
        }
        else if(!resource.resourceInfo.IsFontOfFuel && isBreakable)
        {
            IsBroken = true;
            StartCoroutine(TemperatureLossCoroutine());
        }
    }

    //increases temperature based on energyToBurn.
    IEnumerator WarmUp(float energyToBurn)
    {
        
        float timer = 0;
        isBurning = true;
        while (timer < energyToBurn)
        {
            timer += Time.deltaTime;
            Temperature += fuelModifier.GetValue()*Time.deltaTime;  
            newHeatColor.a = (Temperature / maxTemperature);
            for(int i = 0; i < engineHeatSprite.Length; i++)
            {
                engineHeatSprite[i].color = newHeatColor;
            }
            yield return null;
        }
        isBurning = false;
        yield return null;
    }

    //removes a fixed amount of temperature, stops if the engine is repaired
    IEnumerator TemperatureLossCoroutine()
    {
        float x = Temperature;
        while(Temperature > x - temperatureLossOnEngineBreak && IsBroken)
        {
            Temperature -= Time.deltaTime;
            newHeatColor.a = (Temperature / 5);
            for (int i = 0; i < engineHeatSprite.Length; i++)
            {
                engineHeatSprite[i].color = newHeatColor;
            }

        }
        yield return null;
    }
    #endregion

    #region PRESSURE METHODS

    //changes pressure zone based on actual pressure
    private void CheckPressureZone()
    {
        //Debug.Log("value to check is :  "+actualPressure.GetValue());
        if (actualPressure.GetValue() <= pressureSlowLimit)
        {
            //Debug.Log("slow");
            activePressureZone = PressureZones.slow;
        }
        else if (actualPressure.GetValue() <= pressureNormalLimit)
        {
            //Debug.Log("mid");
            activePressureZone = PressureZones.normal;
        }
        else if (actualPressure.GetValue() <= pressureFastLimit)
        {
            //Debug.Log("mid");
            activePressureZone = PressureZones.fast;
        }
    }

    //changes  pressure zone based on engine's temperature
    private void CheckPressureIncreaseValue()
    {
        switch (activeTemperatureZone)
        {
            case TemperatureZones.low:
                pressureIncreaseValue = pressureIncreaseLowConstant;
                break;
            case TemperatureZones.mid:
                pressureIncreaseValue = pressureIncreaseMidConstant;
                break;
            case TemperatureZones.high:
                pressureIncreaseValue = pressureIncreaseHighConstant;
                break;
            default:
                break;
        }
    }

    //increases pressure over time 
    private void PressureGainer()
    {
        if (!IsBroken)
        {
            //Pressure += Time.deltaTime * pressureIncreaseValue;
            Pressure = actualPressure.baseValue + Time.deltaTime * pressureIncreaseValue;
            pressureParticle.startSpeed = (Pressure*0.2f);
            ShakeEngine();
            if (Pressure >= maxPressure.GetValue())
            {
                actualPressure.SetBaseValue(0);
                Debug.Log("Pressure calls gameover");
                IsBroken = true;
                
                EventManager.TriggerEvent(EventsID.MAXPRESSUREREACHED);
            }
        }
        
    }


    public void TogglePressureReleaser()
    {
        isReleasingPressure = !isReleasingPressure;
        animatorGroup.SetBool("boost", isReleasingPressure);
    }

    private void ShakeEngine()
    {
        //Debug.Log("should be shaking");
        if (startEngine)
            engineSprite.transform.localPosition = engineStartPos + Random.insideUnitCircle/(100-((65*Pressure)/100));
    }

    //releses pressure and gives speed;
    private void PressureRelease()
    {
        if(Pressure>= pressureLossOnRelease)
        {
            Pressure -= (pressureLossOnRelease *Time.deltaTime);
            pressureParticle.startSpeed = Pressure / maxPressure.baseValue;
            ShakeEngine();
            Speed = actualSpeed.baseValue+((Pressure/2)*Time.deltaTime);
            //pressure animation release starts here
        }
    }
    //called when actual pressure value changes
    private void OnPressureChange()
    {
        float percentage = actualPressure.GetValue() / maxPressure.GetValue();
        if (percentage >= audioActivationThreshold)
        {
            if (!isAudio)
                isAudio = true;
            engineConstantSound.ChangeParameter(percentage * 3);
        }
        else if (isAudio)
        {
            isAudio = false;
            engineConstantSound.ChangeParameter(0f);
        }

        //animator.SetFloat("heartSpeed", percentage);
    }
    #endregion

    #region SPEED METHODS

    private void CheckSpeedModifier()
    {
        switch (activePressureZone)
        {
            case PressureZones.slow:
                speedModifier = slowSpeedModifier;
                break;
            case PressureZones.normal:
                speedModifier = midSpeedModifier;
                break;
            case PressureZones.fast:
                speedModifier = highSpeedModifier;
                break;
            default:
                break;
        }
    }

    //constant increase of speed
    private void SpeedIncreaser()
    {
        Speed = actualSpeed.baseValue + Time.deltaTime * speedModifier;
        if (!startEngine && Speed > speedGameOverCondition)
        {
            StartEngine();
        }
        if (!animationStart && !startEngine && Speed != 0.0f)
        {
            animationStart = true;
            StartCoroutine(EngineAnimations());
        }
    }

    //constant decay of speed
    private void ConstantSpeedDecay()
    {
        if (Speed > speedGameOverCondition)
        {
            Speed = actualSpeed.baseValue - actualSpeed.GetValue() * speedDecayValue.Value;
            
        }
        else if(startEngine && GameState.CurrentGameState == GameStateTypes.Running)
        {
            //Debug.Log("engine calls gameover");
            
            //EventManager.TriggerEvent(EventsID.DRILLSTOPPED);
        }
        
    }

    IEnumerator EngineAnimations()
    {
        if (movementVfx)
            movementVfx.SetActive(true);

        for (int i = 0; i < OuterPistons.Length; i++)
        {
            OuterPistons[i].DOPlay();
        }
        
        yield return new WaitForSeconds(1.0f);
        for (int i = 0; i < InnerPistons.Length; i++)
        {
            InnerPistons[i].DOPlay();
        }
        yield return new WaitForSeconds(1.0f);
        for (int i = 0; i < MainPiston.Length; i++)
        {
            MainPiston[i].DOPlay();
        }
        yield return new WaitForSeconds(1.0f);
        for (int i = 0; i < HorizontalPiston.Length; i++)
        {
            HorizontalPiston[i].DOPlay();
        }
        yield return null;
    }

    #endregion

    #region CUSTOM METHODS

    private void OnGameEnd()
    {
        Speed = 0;
        for (int i = 0; i < OuterPistons.Length; i++)
        {
            OuterPistons[i].DOPause();
        }
        for (int i = 0; i < InnerPistons.Length; i++)
        {
            InnerPistons[i].DOPause();
        }
        for (int i = 0; i < MainPiston.Length; i++)
        {
            MainPiston[i].DOPause();
        }

    }

    #endregion

    #region BASIC MACHINE METHODS
    //what should happen when the machine is broken.
    protected override void OnBreak()
    {
        base.OnBreak();
        //temperature = 0f;
        //animation of main piston glass broken starts here
        EventManager.TriggerEvent(EventsID.BROKENENGINE);
        Debug.Log("engine's OnBreak called");
    }

    //what should happen when the machine is repaired.
    protected override void OnRepair()
    {
        EventManager.TriggerEvent(EventsID.ENGINEFLUSH);
    }
    #endregion

    #region TRIGGER
    //the trigger reads the resource given, then calls Burner.
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(GameState.CurrentGameState == GameStateTypes.Running)
        {
            if (GameFramework.Utilities.LayerIsInLayerMask(other.gameObject.layer, resourceLayerMask) && !IsBroken && !IsFull())
            {
                Resource resource = other.gameObject.GetComponent<Resource>();
                if (resource != null && resource.transform.localScale.y < maximumSizeAccepted)
                {
                    
                    resource.Destroy();
                    Burner(resource);
                    EventManager.TriggerEvent(EventsID.R_USED, resource.resourceInfo.Type.Name);
                }
            }
        }
        
    }
    #endregion

    #region AUDIO METHODS
    private void GenerateAudioEvents()
    {
        FmodManager.instance.CreateGenericMonoEventParameterInstance(ref engineConstantSound);
    }

    //private void UpdateConstantAudio()
    //{
    //    if (!FmodManager.instance.IsPlaying(engineConstantSound.fmodEvent))
    //    {
    //        engineConstantSound.PlayAudio(transform);
    //    }
    //}
    #endregion

    private void StartEngine()
    {
        startEngine = true;
        //drillSprite.DOPlay();
        EventManager.TriggerEvent(EventsID.CHANGELIGHTS);
        OnEngineStart?.Invoke();
    }
}
