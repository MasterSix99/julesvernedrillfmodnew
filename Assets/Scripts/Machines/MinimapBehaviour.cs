﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapBehaviour : MonoBehaviour
{
    [SerializeField] private bool keepUpRotation = true;
    [SerializeField] private Transform minimapCameraTr;
    //[SerializeField] private Transform cameraRotationTransform;

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.DRIVING_STARTDRIVING, UnblindMinimap);
        EventManager.StartListening(EventsID.DRIVING_STOPDRIVING, BlindMinimap);
    }

    private void Start()
    {
        BlindMinimap();
    }

    private void Update()
    {
        UpdateCameraTransform();
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.DRIVING_STARTDRIVING, UnblindMinimap);
        EventManager.StopListening(EventsID.DRIVING_STOPDRIVING, BlindMinimap);
    }

    private void UpdateCameraTransform()
    {
        if (minimapCameraTr.rotation != Quaternion.identity)
            minimapCameraTr.rotation = Quaternion.identity;
        if (keepUpRotation && transform.rotation != Quaternion.identity)
            transform.rotation = Quaternion.identity;
    }

    private void BlindMinimap()
    {
        GroundTile.IsBlind = true;
    }

    private void UnblindMinimap()
    {
        GroundTile.IsBlind = false;
    }
}
