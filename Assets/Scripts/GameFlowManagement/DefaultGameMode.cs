﻿using Cinemachine;
using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="so_gameMode_default",menuName ="GameModes/Default")]
public class DefaultGameMode : GameMode
{
    /// <summary>
    /// The players Prefabs
    /// </summary>
    [SerializeField]
    private GameObject[] playersPrefab;
    /// <summary>
    /// Determine the depthToReach win condition 
    /// </summary>
    [SerializeField]
    private FloatVariable depthToReach;

    /// <summary>
    /// Determine if a timer is used
    /// </summary>
    [SerializeField]
    private bool useTimer = true;
    /// <summary>
    /// Determine the time elapsed
    /// </summary>
    [SerializeField][ShowIf("useTimer")]
    private FloatVariable elapsedGameTime;
    /// <summary>
    /// Determine the Max time of timer
    /// </summary>
    [Tooltip("indicates how many minutes of game before gameover")][SerializeField][ShowIf("useTimer")]
    private float gameSessionStartTime;
    [Tooltip("indicates how much time a game can last (upgrades included)")] [SerializeField] [ShowIf("useTimer")]
    private float gameSessionMaxTime;
    /// <summary>
    /// Determine the joystick vibration on gameOver
    /// </summary>
    [SerializeField]
    private JoystickVibrationConfig gameOverVibration;
    /// <summary>
    /// CinemachineTargetGroup referenece
    /// </summary>
    private CinemachineTargetGroup chineMachineTargetGroup;

    public override void Enable()
    {
        //Spawn Enabled players
        SpawnPlayers();
        //Start the game
        //GameState.CurrentGameState = GameStateTypes.Running;
        
        if (depthToReach == null) Debug.LogWarning("WARNING, The FloatVariable depthToReach has not been valorized, no win condition set!");
        else
        {
            OnUpdateDepthToReach();
        }
        //Event subscription
        EventManager.StartListening(EventsID.DESTINATIONREACHED,OnGameWin);
        EventManager.StartListening(EventsID.DRILLSTOPPED, OnGameOver);
        EventManager.StartListening(EventsID.MAXPRESSUREREACHED, OnGameOver);
        EventManager.StartListening(EventsID.TIMEELAPSED, OnGameOver);
        EventManager.StartListening(EventsID.MINIMUMHEATREACHED, OnGameOver);
        

        //Assign UdateDepthToReach on var value change
        depthToReach.onValueChangeAction += OnUpdateDepthToReach;
        //elapsedGameTime.Value = 0;
        elapsedGameTime.Value = gameSessionStartTime;
    }

    public override void Disable()
    {
        //Stop the game
        GameState.CurrentGameState = GameStateTypes.Ready;
        //Event unsubscription
        EventManager.StopListening(EventsID.DESTINATIONREACHED, OnGameWin);
        EventManager.StopListening(EventsID.DRILLSTOPPED, OnGameOver);
        EventManager.StopListening(EventsID.MAXPRESSUREREACHED, OnGameOver);
        EventManager.StopListening(EventsID.TIMEELAPSED, OnGameOver);
        EventManager.StopListening(EventsID.MINIMUMHEATREACHED, OnGameOver);
        
        depthToReach.onValueChangeAction -= OnUpdateDepthToReach;
    }
    


    public override void UpdateGameModeState()
    {
        if (useTimer) { 
            elapsedGameTime.Value -= Time.deltaTime;
            elapsedGameTime.Value = Mathf.Clamp(elapsedGameTime.Value, 0.0f , gameSessionMaxTime);
            if (elapsedGameTime.Value <= (0.0f))
            {
                EventManager.TriggerEvent(EventsID.TIMEELAPSED);
            }
        }
    }

    public void IncreaseTime(StatModifier modifier)
    {
        StatModifierFloat castedModifier = (StatModifierFloat)modifier;
        Debug.Log("before: " + elapsedGameTime.Value);
        elapsedGameTime.Value += castedModifier.ValueToSum;
        Debug.Log("after: " + elapsedGameTime.Value);
    }

    /// <summary>
    /// Use this to update Depth Bar 
    /// </summary>
    public void OnUpdateDepthToReach()
    {
        DepthBar db = FindObjectOfType<DepthBar>();
        if (db != null) db.MaxVal = depthToReach.Value;
    }

    /// <summary>
    /// Called when WinCondition is reached 
    /// </summary>
    private void OnGameWin()
    {
        Debug.Log("you win the game!");
        GameState.CurrentGameState = GameStateTypes.GameWin;
        EventManager.TriggerEvent(EventsID.CALCULATESTATS);
        EventManager.TriggerEvent(EventsID.GAMEEND);
        EventManager.TriggerEvent(EventsID.GAMEWIN);
    }

    /// <summary>
    /// Called when Lose condition event is reached 
    /// </summary>
    private void OnGameOver()
    {
        Debug.Log("you lose the game");
        GameState.CurrentGameState = GameStateTypes.GameOver;
        EventManager.TriggerEvent(EventsID.CALCULATESTATS);
        EventManager.TriggerEvent(EventsID.GAMEEND);
        EventManager.TriggerEvent(EventsID.GAMEOVER);

        foreach (Rewired.Player player in Rewired.ReInput.players.AllPlayers)
        {
            JoystickVibration.StopVibrations(player);
            JoystickVibration.Vibrate(player, gameOverVibration);
        }
    }

    private void SpawnPlayers()
    {
        //FIND SPAWN POINTS
        GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint");
        //FIND CAMERA TARGETGROUP
        chineMachineTargetGroup = FindObjectOfType<CinemachineTargetGroup>();

        //Retrive Players from DataManager , if no player is active a number of player defined in "defaultActivePlayers" are activated
        List<PlayerSession> joinedPlayers = GetJoinedPlayers();
        int joinedPlayersCount = joinedPlayers.Count;
        if (joinedPlayersCount == 0)
        {
            for(int j = 0; j < DataManager.Instance.defaultActivePlayers;j++)
            {
                DataManager.Instance.Players[j].IsEnabled = true;
                DataManager.Instance.Players[j].RewiredPlayerId = j;
            }
        }

        int i = 0;
        //Get drill reference (used to spawn players in)
        DrillBehaviour drill = FindObjectOfType<DrillBehaviour>();
        //Get enabled PlayerSessions and instantiate players
        foreach (PlayerSession playerSession in DataManager.Instance.Players)
        {
            if (playerSession.IsEnabled)
            {
                if (i < spawnPoints.Length && i < playersPrefab.Length )
                {
                    Transform spawnPoint = spawnPoints[i].transform;
                    //INSTANTIATE PLAYER AND SET ID
                    GameObject playerObj = Instantiate(playersPrefab[i],spawnPoint.position, spawnPoint.rotation);
                    if (drill != null)
                        playerObj.transform.parent = drill.transform;
                    CharacterController2D characterController = playerObj.GetComponent<CharacterController2D>();
                    characterController.PlayerID = playerSession.PlayerID;
                    characterController.RewiredId = playerSession.RewiredPlayerId;
                    //SET CAMERA TARGETS
                    chineMachineTargetGroup.AddMember(playerObj.transform, 1, 0);
                }
                else
                {
                    Debug.LogError("NOT ENOUGHT PLAYER SPAWNPOINTS IN SCENE");
                }
            }
            i++;
        }
        CinemachineVirtualCamera camera = FindObjectOfType<CinemachineVirtualCamera>();
    }

    /// <summary>
    /// Retrive joined players stored in DataManager.Players
    /// </summary>
    /// <returns></returns>
    private List<PlayerSession> GetJoinedPlayers()
    {
        List<PlayerSession> players = new List<PlayerSession>();
        foreach (PlayerSession playerSession in DataManager.Instance.Players)
        {
            if (playerSession.IsEnabled)
            {
                players.Add(playerSession);
            }
        }
        return players;
    }
}
