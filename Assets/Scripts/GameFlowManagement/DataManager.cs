﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DataManager : Singleton<DataManager>
{
    #region VARIABLES
    /// <summary>
    /// Max allowed players on game
    /// </summary>
    public int maxPlayers = 4;
    /// <summary>
    /// Default active player on level start (used when start directly from gameScene in development)
    /// </summary>
    public int defaultActivePlayers = 1;
    /// <summary>
    /// List of players joined
    /// </summary>
    [Space] [SerializeField]
    private List<PlayerSession> m_players;
    #endregion

    #region PROPERTIES
    public List<PlayerSession> Players
    {
        get { return m_players; }
    }

    #endregion

    #region MONOBEHAVIOUR
    protected override void Awake()
    {
        base.Awake();
        //Initialize Player list
        m_players = new List<PlayerSession>();

        //Populate PlayerList with inactive Players
        for (int i = 0; i < maxPlayers; i++)
        {
            PlayerSession playerSession = new PlayerSession(i, false);
            playerSession.RewiredPlayerId = i;
            m_players.Add(playerSession);
        }
    }

    #endregion

    #region PLAYER SESSION
    /// <summary>
    /// Called when Player has joined
    /// </summary>
    /// <param name="player"></param>
    private void OnPlayerJoined(PlayerSession player)
    {
        EnablePlayerSession(player,true);
    }
    /// <summary>
    /// Called when Pleyer Leave lobby
    /// </summary>
    /// <param name="player">Player left</param>
    private void OnPlayerLeft(PlayerSession player)
    {
        DisablePlayerSession(player);
    }
    /// <summary>
    /// Initialize Player sending rewiredId and enabling rewiredPlayer (NB: the player is enabled only if "alsoEnabled" variable is set to true
    /// </summary>
    /// <param name="player">player to enable</param>
    /// <param name="alsoEnable"></param>
    private void EnablePlayerSession(PlayerSession player,bool alsoEnable)
    {
        foreach(PlayerSession playerInList in m_players)
        {
            if(playerInList.PlayerID == player.PlayerID)
            {
                player.IsEnabled = alsoEnable;
                player.RewiredPlayerId = playerInList.RewiredPlayerId;
                Rewired.ReInput.players.GetPlayer(player.RewiredPlayerId).isPlaying = true;
            }
        }
    }
    /// <summary>
    /// Disable Player from session
    /// </summary>
    /// <param name="playerID">Used to identify player</param>
    private void DisablePlayerSession(int playerID)
    {
        foreach(PlayerSession player in m_players)
        {
            if(player.PlayerID == playerID)
            {
                player.IsEnabled = false;
                player.RewiredPlayerId = -1;
            }
        }
    }
    /// <summary>
    /// Disable Player from session
    /// </summary>
    /// <param name="player">Player to disable</param>
    private void DisablePlayerSession(PlayerSession player)
    {
        foreach (PlayerSession playerInList in m_players)
        {
            if (playerInList.PlayerID == player.PlayerID)
            {
                player.IsEnabled = false;
                player.RewiredPlayerId = -1;
            }
        }
    }
    /// <summary>
    /// Get player associated with specific rewiredPlayer
    /// </summary>
    /// <param name="rewiredPlayer"></param>
    /// <returns></returns>
    public PlayerSession GetPlayer(Rewired.Player rewiredPlayer)
    {
        foreach(PlayerSession player in m_players)
        {
            if (player.RewiredPlayerId == rewiredPlayer.id)
                return player;
        }
        return null;
    }
    #endregion

    #region CONTROLLERS MANAGEMENT
    //TODO Implement controllers checks
    #endregion

    #region INSPECTOR UTILITIES
    [GameFramework.Button]
    private void PrintRewiredPlayerInfo()
    {
        foreach (Rewired.Player player in Rewired.ReInput.players.AllPlayers)
        {
            Debug.Log("##############");
            Debug.Log("playerName = " + player.name);
            Debug.Log("playerid = " + player.id);
            Debug.Log("Keyboard assigned = " + player.controllers.hasKeyboard);
            Debug.Log("controllers assigned = " + player.controllers.joystickCount);
        }
    }
    #endregion
}
