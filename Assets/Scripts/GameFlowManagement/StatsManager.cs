﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameFramework;

public class StatsManager : MonoBehaviour
{

    #region Achievements Stats
    [Header("Achievement stats")]
    //broken
    [SerializeField] private StatInt s_TotalMachinesBroken = new StatInt();
    [SerializeField] private StatInt s_EngineBroken = new StatInt();
    [SerializeField] private StatInt s_TrashBroken = new StatInt();
    [SerializeField] private StatInt s_SpawnerBroken = new StatInt();
    [SerializeField] private StatInt s_DriverBroken = new StatInt();
    [SerializeField] private StatInt s_CrusherBroken = new StatInt();
    //speed
    [SerializeField] private StatFloat s_DrillSpeed = new StatFloat();
    //gathered
    [SerializeField] private StatInt s_CoalGathered = new StatInt();
    [SerializeField] private StatInt s_DirtGathered = new StatInt();
    [SerializeField] private StatInt s_StoneGathered = new StatInt();
    [SerializeField] private StatInt s_GemGathered = new StatInt();
    //used
    [SerializeField] private StatInt s_CoalUsed = new StatInt();
    [SerializeField] private StatInt s_DirtUsed = new StatInt();
    [SerializeField] private StatInt s_StoneUsed = new StatInt();
    [SerializeField] private StatInt s_GemUsed = new StatInt();
    //crushed
    [SerializeField] private StatInt s_CoalCrushed = new StatInt();
    [SerializeField] private StatInt s_DirtCrushed = new StatInt();
    [SerializeField] private StatInt s_StoneCrushed = new StatInt();
    [SerializeField] private StatInt s_GemCrushed = new StatInt();
    //trashed
    [SerializeField] private StatInt s_CoalTrashed = new StatInt();
    [SerializeField] private StatInt s_DirtTrashed = new StatInt();
    [SerializeField] private StatInt s_StoneTrashed = new StatInt();
    [SerializeField] private StatInt s_GemTrashed = new StatInt();
    //onboard
    [SerializeField] private StatInt s_CoalOnBoard = new StatInt();
    [SerializeField] private StatInt s_DirtOnBoard = new StatInt();
    [SerializeField] private StatInt s_StoneOnBoard = new StatInt();
    [SerializeField] private StatInt s_GemOnBoard = new StatInt();
    //player
    [SerializeField] private StatInt s_PlayerJumps = new StatInt();
    [SerializeField] private StatInt s_PlayerRepairs = new StatInt();
    [SerializeField] private StatInt s_PlayerThrows = new StatInt();
   //drill
    [SerializeField] private StatFloat s_DepthReached = new StatFloat();
    //time 
    [SerializeField] private StatFloat s_TimeElapsed = new StatFloat();
    //score
    [SerializeField] private StatFloat s_Score = new StatFloat();




    #endregion


    #region Variables
    #region Machines Stats
    private int engineBreak;
    private int trashBreak;
    private int spawnerBreak;
    private int driverBreak;
    private int crusherBreak;

    #endregion

    #region Drill Stats
    [Space(10)]
    [SerializeField]
    private FloatVariable score;
    [SerializeField]
    private FloatVariable actualSpeed;
    [SerializeField]
    private float drivingTime;
    [SerializeField]
    private bool isDriving;
    [SerializeField]
    private float averageSpeed;
    [SerializeField]
    private float averagePressure;
    [SerializeField]
    private FloatVariable actualPressure;
    private float tempPressure;
    [SerializeField]
    private int sampleCount;
    [SerializeField]
    private float countTimer;
    [SerializeField]
    private FloatVariable actualDepthReached;
    [SerializeField]
    private float depthReached;
    [SerializeField]
    private float topSpeed;
    [Header("game time")]
    [SerializeField]
    private FloatVariable gameTime;
    [SerializeField]
    private bool isGameOver;

    #endregion

    #region Coal Stats
    [SerializeField]
    private int coalGathered;
    [SerializeField]
    private int coalUsed;
    [SerializeField]
    private int coalTrashed;
    [SerializeField]
    private int coalCrushed;
    #endregion

    #region Dirt Stats
    [SerializeField]
    private int dirtGathered;
    [SerializeField]
    private int dirtUsed;
    [SerializeField]
    private int dirtTrashed;
    [SerializeField]
    private int dirtCrushed;
    #endregion

    #region Stone Stats
    [SerializeField]
    private int stoneGathered;
    [SerializeField]
    private int stoneUsed;
    [SerializeField]
    private int stoneTrashed;
    [SerializeField]
    private int stoneCrushed;
    #endregion

    #region Gem Stats
    [SerializeField]
    private int gemGathered;
    [SerializeField]
    private int gemUsed;
    [SerializeField]
    private int gemTrashed;
    [SerializeField]
    private int gemCrushed;
    #endregion

    #region Score Points Multipliers
    [Header("Score Points Multipliers")]
    [SerializeField]
    private float gemMultiplier;
    [SerializeField]
    private float dirtMultiplier;
    [SerializeField]
    private float coalMultiplier;
    [SerializeField]
    private float stoneMultiplier;
    [SerializeField]
    private float brokenMachineMultiplier;
    [SerializeField]
    private float depthReachedMultiplier;
    [SerializeField]
    [Tooltip("This value should be between 0 and 1")]
    private float averageSpeedMultiplier;

    [SerializeField] private EntityFloatStat scoreBonusSum = new EntityFloatStat { baseValue = 1 };
    [SerializeField] private EntityFloatStat scoreBonusMultiplier = new EntityFloatStat { baseValue = 1 };
    [SerializeField] private EntityFloatStat brokenMachinesDivisor = new EntityFloatStat { baseValue = 1 };
    #endregion

    #region StatsRefresh
    [SerializeField] private float refreshRate = 3;
    private float refreshTimer = 0.0000f;
    #endregion

    #endregion

    #region Properties
    public float Score { get { return score.Value; } }

    public float DepthReached { get { return depthReached; } }
    public float GameTime { get { return gameTime.Value; } }
    public float TopSpeed { get { return topSpeed; } }
    public float AverageSpeed { get { return averageSpeed; } }
    public int MachinesBroken { get { return engineBreak + trashBreak + spawnerBreak + driverBreak + crusherBreak; } }

    //RESOURCES SECTION
    public int CoalGathered { get { return coalGathered; } }
    public int CoalTrashed { get { return coalTrashed; } }
    public int CoalUsed { get { return coalUsed; } }

    public int DirtGathered { get { return dirtGathered; } }
    public int DirtTrashed { get { return dirtTrashed; } }
    public int DirtUsed { get { return dirtUsed; } }

    public int GemGathered { get { return gemGathered; } }
    public int GemTrashed { get { return gemTrashed; } }
    public int GemUsed { get { return gemUsed; } }

    public int StoneGathered { get { return stoneGathered; } }
    public int StoneTrashed { get { return stoneTrashed; } }
    public int StoneUsed { get { return stoneUsed; } }

    private int CoalOnBoard { get { return Mathf.Clamp(coalGathered - coalUsed - coalCrushed - coalTrashed, 0, coalGathered); } }
    private int DirtOnBoard { get { return Mathf.Clamp(dirtGathered - dirtUsed - dirtCrushed - dirtTrashed, 0, dirtGathered); } }
    private int GemOnBoard { get { return Mathf.Clamp(gemGathered - GemUsed - gemCrushed - GemTrashed, 0, gemGathered); } }
    private int StoneOnBoard { get { return Mathf.Clamp(stoneGathered - StoneUsed - stoneCrushed - stoneTrashed, 0, stoneGathered); } }
    #endregion

    #region MonoBehaviour Methods
    private void OnEnable()
    {
        EventManager.StartListening(EventsID.BROKENENGINE, OnEngineBreak);
        EventManager.StartListening(EventsID.BROKENTRASHER, OnTrasherBreak);
        EventManager.StartListening(EventsID.BROKENDRIVER, OnDriverBreak);
        EventManager.StartListening(EventsID.BROKENSPAWNER, OnSpawnerBreak);
        EventManager.StartListening(EventsID.BROKENCRUSHER, OnCrusherBreak);
        EventManager.StartListening(EventsID.DRIVING_STARTDRIVING, ToggleDriveCheck);
        EventManager.StartListening(EventsID.DRIVING_STOPDRIVING, ToggleDriveCheck);
        EventManager.StartListening(EventsID.CALCULATESTATS, OnStatsCalculate);
        EventManager.StartListening<string>(EventsID.R_GATHERED, OnResourceGathered);
        EventManager.StartListening<string>(EventsID.R_USED, OnResourceUsed);
        EventManager.StartListening<string>(EventsID.R_TRASHED, OnResourceTrashed);
        EventManager.StartListening<string>(EventsID.R_CRUSHED, OnResourceCrushed);
        EventManager.StartListening(EventsID.GAMEEND, OnGameEnd);
        EventManager.StartListening(EventsID.PLAYERJUMP, OnPlayerJump);
        EventManager.StartListening(EventsID.PLAYERTHROW, OnPlayerThrow);
        EventManager.StartListening(EventsID.PLAYERREPAIR, OnPlayerRepair);

    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.BROKENENGINE, OnEngineBreak);
        EventManager.StopListening(EventsID.BROKENTRASHER, OnTrasherBreak);
        EventManager.StopListening(EventsID.BROKENDRIVER, OnDriverBreak);
        EventManager.StopListening(EventsID.BROKENSPAWNER, OnSpawnerBreak);
        EventManager.StopListening(EventsID.BROKENCRUSHER, OnCrusherBreak);
        EventManager.StopListening(EventsID.DRIVING_STARTDRIVING, ToggleDriveCheck);
        EventManager.StopListening(EventsID.DRIVING_STOPDRIVING, ToggleDriveCheck);
        EventManager.StopListening(EventsID.CALCULATESTATS, OnStatsCalculate);
        EventManager.StopListening<string>(EventsID.R_GATHERED, OnResourceGathered);
        EventManager.StopListening<string>(EventsID.R_USED, OnResourceUsed);
        EventManager.StopListening<string>(EventsID.R_TRASHED, OnResourceTrashed);
        EventManager.StopListening<string>(EventsID.R_CRUSHED, OnResourceCrushed);
        EventManager.StopListening(EventsID.GAMEEND, OnGameEnd);
        EventManager.StopListening(EventsID.PLAYERJUMP, OnPlayerJump);
        EventManager.StopListening(EventsID.PLAYERTHROW, OnPlayerThrow);
        EventManager.StopListening(EventsID.PLAYERREPAIR, OnPlayerRepair);
    }

    private void Start()
    {
        score.Value = 0.0000f;
    }

    private void Update()
    {
        if (!isGameOver)
        {
            OnDrivingTime();
            PressureAverageCalculator();
            TopSpeedCalculator();
            //on value change, need to calculate achievements check

            if (refreshTimer < refreshRate)
            {
                refreshTimer += Time.deltaTime;
            }
            else
            {
                refreshTimer = 0.0000f;
                StatsUpdate();
                AchievementsUpdate();
            }
        }
        
        
        
    }
    #endregion

    #region Achievements Methods

    private void AchievementsUpdate()
    {
        //the actual speed of the drill
        s_DrillSpeed.SetStat(actualSpeed.Value);
        //time elapsed from start
        s_TimeElapsed.SetStat(gameTime.Value);

        s_DepthReached.SetStat(DepthReached);
    }

    protected void AchievementsSetValue()
    {
        
        //all the resources usage
    }

    protected void OnPlayerJump()
    {
        s_PlayerJumps.SetStat(s_PlayerJumps.value + 1);
    }

    protected void OnPlayerThrow()
    {
        s_PlayerThrows.SetStat(s_PlayerThrows.value + 1);
    }

    protected void OnPlayerRepair()
    {
        s_PlayerRepairs.SetStat(s_PlayerRepairs.value + 1);
    }



    #endregion

    private void StatsUpdate()
    {
        DepthReachedCalculator();
    }

    #region Machine Stats Methods
    protected void OnEngineBreak()
    {
        engineBreak++;
        s_EngineBroken.SetStat(engineBreak);
        s_TotalMachinesBroken.SetStat(MachinesBroken);
        ScoreCalculator();
    }

    protected void OnTrasherBreak()
    {
        trashBreak++;
        s_TrashBroken.SetStat(trashBreak);
        s_TotalMachinesBroken.SetStat(MachinesBroken);
        ScoreCalculator();
    }

    protected void OnSpawnerBreak()
    {
        spawnerBreak++;
        s_SpawnerBroken.SetStat(spawnerBreak);
        s_TotalMachinesBroken.SetStat(MachinesBroken);
        ScoreCalculator();
    }

    protected void OnDriverBreak()
    {
        driverBreak++;
        s_DriverBroken.SetStat(driverBreak);
        s_TotalMachinesBroken.SetStat(MachinesBroken);
        ScoreCalculator();
    }

    protected void OnCrusherBreak()
    {
        crusherBreak++;
        s_CrusherBroken.SetStat(crusherBreak);
        s_TotalMachinesBroken.SetStat(MachinesBroken);
        ScoreCalculator();
    }

    
    
    #endregion

    #region Drill Stats Methods
    //add time when a player is using  the drivermachine
    protected void OnDrivingTime()
    {
        if (isDriving)
            drivingTime += Time.deltaTime;
    }
    //needs to be called when starting and ending driving interaction
    protected void ToggleDriveCheck()
    {
        isDriving = !isDriving;
    }

    protected void SpeedAverageCalculator()
    {
        averageSpeed = depthReached / Time.time;
    }

    protected void PressureAverageCalculator()
    {
        countTimer += Time.deltaTime;
        if (countTimer >= 10.0f)
        {
            tempPressure += actualPressure.Value;
            sampleCount++;
            countTimer = 0;
        }
        averagePressure = tempPressure / sampleCount;
    }

    //call this on gameover/gamewin
    protected void DepthReachedCalculator()
    {
        depthReached = actualDepthReached.Value;
    }

    protected void OnStatsCalculate()
    {
        DepthReachedCalculator();
        SpeedAverageCalculator();
        ScoreCalculator();
        finalScoreCalculator();
        //the score as stat
        s_Score.SetStat(score.Value);
    }

    protected void ScoreCalculator()
    {
        score.Value = (depthReachedMultiplier*actualDepthReached.Value + ((coalUsed * coalMultiplier) - (CoalTrashed * coalMultiplier * 0.5f) -
            (gemTrashed * gemMultiplier * 0.5f) + (dirtTrashed * dirtMultiplier) + (StoneTrashed * stoneMultiplier)
            + (gemCrushed * gemMultiplier)) + scoreBonusSum.GetValue()) * scoreBonusMultiplier.GetValue();
    }
    
    protected void finalScoreCalculator()
    {
        score.Value -= (((engineBreak + trashBreak + spawnerBreak + driverBreak) * (brokenMachineMultiplier * brokenMachinesDivisor.GetValue())) / (1.0f + (averageSpeed * averageSpeedMultiplier)));
    }

    protected void TopSpeedCalculator()
    {
        if (topSpeed <= actualSpeed.Value)
        {
            topSpeed = actualSpeed.Value;
        }
    }
    

    protected void OnGameEnd()
    {
        if(!isGameOver)
        isGameOver = true;
    }
    #endregion

    #region Resources Stats Methods
    protected void OnResourceGathered(string resourceName)
    {
        switch (resourceName)
        {
            case "Coal":
                coalGathered++;
                s_CoalGathered.SetStat(coalGathered);
                s_CoalOnBoard.SetStat(CoalOnBoard);
                break;
            case "Dirt":
                dirtGathered++;
                s_DirtGathered.SetStat(dirtGathered);
                s_DirtOnBoard.SetStat(DirtOnBoard);
                break;
            case "Gem":
                gemGathered++;
                s_GemGathered.SetStat(gemGathered);
                s_GemOnBoard.SetStat(GemOnBoard);
                break;
            case "Stone":
                stoneGathered++;
                s_StoneGathered.SetStat(stoneGathered);
                s_StoneOnBoard.SetStat(StoneOnBoard);
                break;
            default:
                break;
        }
        ScoreCalculator();
    }

    protected void OnResourceUsed(string resourceName)
    {
        switch (resourceName)
        {
            case "Coal":
                coalUsed++;
                s_CoalUsed.SetStat(coalUsed);
                s_CoalOnBoard.SetStat(CoalOnBoard);
                break;
            case "Dirt":
                dirtUsed++;
                s_DirtUsed.SetStat(dirtUsed);
                s_DirtOnBoard.SetStat(DirtOnBoard);
                break;
            case "Gem":
                gemUsed++;
                s_GemUsed.SetStat(gemUsed);
                s_GemOnBoard.SetStat(GemOnBoard);
                break;
            case "Stone":
                stoneUsed++;
                s_StoneUsed.SetStat(stoneUsed);
                s_StoneOnBoard.SetStat(StoneOnBoard);
                break;
            default:
                break;
        }
        ScoreCalculator();
    }

    protected void OnResourceTrashed(string resourceName)
    {
        switch (resourceName)
        {
            case "Coal":
                coalTrashed++;
                s_CoalTrashed.SetStat(coalTrashed);
                s_CoalOnBoard.SetStat(CoalOnBoard);
                break;
            case "Dirt":
                dirtTrashed++;
                s_DirtTrashed.SetStat(dirtTrashed);
                s_DirtOnBoard.SetStat(DirtOnBoard);
                break;
            case "Gem":
                gemTrashed++;
                s_GemTrashed.SetStat(gemTrashed);
                s_GemOnBoard.SetStat(GemOnBoard);
                break;
            case "Stone":
                stoneTrashed++;
                s_StoneTrashed.SetStat(stoneTrashed);
                s_StoneOnBoard.SetStat(StoneOnBoard);
                break;
            default:
                break;
        }
        ScoreCalculator();
    }

    protected void OnResourceCrushed(string resourceName)
    {
        switch (resourceName)
        {
            case "Coal":
                coalCrushed++;
                s_CoalCrushed.SetStat(coalCrushed);
                s_CoalOnBoard.SetStat(CoalOnBoard);
                break;
            case "Dirt":
                dirtCrushed++;
                s_DirtCrushed.SetStat(dirtCrushed);
                s_DirtOnBoard.SetStat(DirtOnBoard);
                break;
            case "Gem":
                gemCrushed++;
                s_GemCrushed.SetStat(gemCrushed);
                s_GemOnBoard.SetStat(GemOnBoard);
                break;
            case "Stone":
                stoneCrushed++;
                s_StoneCrushed.SetStat(stoneCrushed);
                s_StoneOnBoard.SetStat(StoneOnBoard);
                break;
            default:
                break;
        }
        ScoreCalculator();
    }


    #endregion

}
