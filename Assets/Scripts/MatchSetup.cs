﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchSetup : MonoBehaviour
{
    [SerializeField] private Transform spawnpoint;
    [SerializeField] private Transform resourcesParent;
    [SerializeField] private GameObject gemPrefab;
    [SerializeField] private GameObject coalPrefab;

    public void GiveGemsAtStart(StatModifier modifier)
    {
        StatModifierInt castedModifier = (StatModifierInt)modifier;
        int i;
        for (i = 0; i < castedModifier.ValueToSum; i++)
        {
            PoolManager.GetObject(gemPrefab, spawnpoint.position, resourcesParent);
        }
    }

    public void GiveCoalAtStart(StatModifier modifier)
    {
        StatModifierInt castedModifier = (StatModifierInt)modifier;
        int i;
        for (i = 0; i < castedModifier.ValueToSum; i++)
        {
            PoolManager.GetObject(coalPrefab, spawnpoint.position, resourcesParent);
        }
    }
}
