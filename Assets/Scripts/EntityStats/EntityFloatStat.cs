﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EntityFloatStat : EntityGenericStat<float>
{
    public override void AddModifier(StatModifier _modifier)
    {
        base.AddModifier(_modifier);
        StatModifierFloat modifier = (StatModifierFloat)_modifier;
        valueToSum += modifier.ValueToSum;
        multiplier += modifier.Multiplier;

        UpdateFinalValue();
    }

    public override void SumStat(EntityGenericStat<float> _stat)
    {
        valueToSum += _stat.valueToSum;
        multiplier += _stat.multiplier;

        UpdateFinalValue();
    }

    protected override void UpdateFinalValue()
    {
        float finalMultiplier = (multiplier == 0.0000f) ? 1 : multiplier;
        finalValue = (baseValue + valueToSum) * finalMultiplier;
    }
}
