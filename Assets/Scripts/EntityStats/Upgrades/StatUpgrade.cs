﻿using System.Reflection;
using UnityEngine;

public enum Upgradables
{
    Engine = 0,
    //PressureValve =1,
    Spawner = 2,
    Trash = 3,
    Driver = 4,
    Crusher = 5,
    Player = 6,
    Game = 7,
    Hull = 8,
    StatsManager = 9,
    DefaultGameMode = 10,
    MatchSetup = 11,
    UpgradeHelper = 12
}
public enum UpgradeType {
    Int = 1,
    Float = 2,
    Bool = 3,
    Vector2 = 4,
    
}
public enum CallType
{
    Variable = 1,
    Property = 2,
    Function = 3,
    FloatVariable = 4,
}

[System.Serializable]
public class StatUpgrade
{
    #region VARIABLES
    /// <summary>
    /// Target that must be upgraded
    /// </summary>
    [SerializeField]
    [ShowIf("upgradeCallType", new int[] { 1, 2, 3 })]
    private Upgradables upgradeTarget;
    [SerializeField] 
    private CallType upgradeCallType;
    /// <summary>
    /// Variable of target that must be ugraded
    /// </summary>
    [SerializeField]
    private string upgradeVarName;
    /// <summary>
    /// Type of upgrade
    /// </summary>
    [SerializeField]
    private UpgradeType upgradeVarType;
    /// <summary>
    /// Int upgrade value
    /// </summary>
    [SerializeField][ShowIf("upgradeVarType",1)]
    private StatModifierInt upgradeValueInt;
    /// <summary>
    /// Float upgrade value
    /// </summary>
    [SerializeField][ShowIf("upgradeVarType", 2)]
    private StatModifierFloat upgradeValueFloat;
    /// <summary>
    /// Bool upgrade value
    /// </summary>
    [SerializeField][ShowIf("upgradeVarType", 3)]
    private StatModifierBool upgradeValueBool;
    /// <summary>
    /// Vector2 upgrade value
    /// </summary>
    [SerializeField][ShowIf("upgradeVarType", 4)]
    private StatModifierVector2 upgradeValueVecto2;

    [SerializeField] [ShowIf("upgradeCallType", 4)]
    private GameFramework.FloatVariable upgradeFloatVariable;
    [SerializeField]
    private float upgradeValueFloatVariable;
    #endregion

    #region Methods
    /// <summary>
    /// Apply the stat upgrade to all objects
    /// </summary>
    public void Apply()
    {
        
        switch (upgradeTarget)
        {
            case Upgradables.Crusher:
                Apply(typeof(ResourceBreaker), upgradeVarName);
                break;
            case Upgradables.Driver:
                Apply(typeof(DriverBehaviour), upgradeVarName);
                break;
            case Upgradables.Engine:
                Apply(typeof(EngineBehaviour), upgradeVarName);
                break;
            /*case Upgradables.PressureValve:
                Apply(typeof(PressureValve), upgradeVarName);
                break;*/
            case Upgradables.Spawner:
                Apply(typeof(ResourceSpawner), upgradeVarName);
                break;
            case Upgradables.Trash:
                Apply(typeof(TrashBehaviour), upgradeVarName);
                break;
            case Upgradables.Player:
                //Debug.LogError("ERROR : Player upgrades not implemented yet");
                Apply(typeof(CharacterController2D), upgradeVarName);
                break;
            case Upgradables.Game:
                Apply(typeof(ResourceBreaker), upgradeVarName);
                break;
            case Upgradables.Hull:
                Apply(typeof(HullBehaviour), upgradeVarName);
                break;
            case Upgradables.StatsManager:
                Apply(typeof(StatsManager), upgradeVarName);
                break;
            case Upgradables.DefaultGameMode:
                Apply(typeof(DefaultGameMode), upgradeVarName);
                break;
            case Upgradables.MatchSetup:
                Apply(typeof(MatchSetup), upgradeVarName);
                break;
            case Upgradables.UpgradeHelper:
                Apply(typeof(UpgradeHelper), upgradeVarName);
                break;
        }
    }
    
    /// <summary>
    /// Find all object of chosen type and apply modifier to their stats
    /// </summary>
    /// <param name="targetType">Components to find</param>
    /// <param name="varName">variable to modify</param>
    private void Apply(System.Type targetType,string varName)
    {
        //Get all objects in scene with this specifc type
        var targets = Object.FindObjectsOfType(targetType);
        int i = 0;
        int targetsLen = targets.Length;
        //Loop for each target and call variable/property upgrade or function call , based on upgradeCallType
        for (i = 0; i < targetsLen; i++)
        {
            if (upgradeCallType.Equals(CallType.FloatVariable))
                ApplyFloatVariable(upgradeFloatVariable, upgradeValueFloatVariable);
            if (upgradeCallType.Equals(CallType.Variable))
                ApplyVariable(targets[i], targetType, varName);
            else if (upgradeCallType.Equals(CallType.Property))
                ApplyProperty(targets[i], targetType, varName);
            else if (upgradeCallType.Equals(CallType.Function))
                ApplyMethod(targets[i], targetType, varName);
        }
    }

    #region Apply methods based on call type
    /// <summary>
    ///  Add modifier to given object, using variable
    /// </summary>
    /// <param name="_target">The target where to apply modifier</param>
    /// <param name="targetType">The target type</param>
    /// <param name="varName">the var to modify</param>
    public void ApplyVariable(object _target, System.Type targetType, string varName)
    {
        var target = System.Convert.ChangeType(_target, targetType);
        //Get choosen modifier according with upgradeTarget Value chosen 
        StatModifier modifier = GetModifier();
        //Get variable info from target
        FieldInfo fieldInfo = target.GetType().GetField(varName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        //get variable reference
        var fldVal = fieldInfo.GetValue(target);
        //Get AddModifier method
        MethodInfo method = fldVal.GetType().GetMethod("AddModifier");
        //Call AddModifier method
        method.Invoke(fldVal, new object[] { modifier });
    }
    /// <summary>
    ///  Add modifier to given object, using property
    /// </summary>
    /// <param name="_target">The target where to apply modifier</param>
    /// <param name="targetType">The target type</param>
    /// <param name="varName">the var to modify</param>
    public void ApplyProperty(object _target, System.Type targetType, string varName)
    {
        var target = System.Convert.ChangeType(_target, targetType);
        //Debug.Log("TARGET : " + target);
        //Get choosen modifier according with upgradeTarget Value chosen 
        StatModifier modifier = GetModifier();
        //Get variable info from target
        PropertyInfo propertyInfo = target.GetType().GetProperty(varName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        //get variable reference
        var fieldVal = propertyInfo.GetValue(target);
        //Get AddModifier method
        MethodInfo method = fieldVal.GetType().GetMethod("AddModifier");
        //Call AddModifier method
        method.Invoke(fieldVal, new object[] { modifier });
        //Get property value after modify
        fieldVal = propertyInfo.GetValue(target);
        // Set property as itself , used only to trigger set functionality
        propertyInfo.SetValue(target, fieldVal);
    }
    /// <summary>
    ///  Call desidred method and send modifier to it
    /// </summary>
    /// <param name="_target">The target where to apply modifier</param>
    /// <param name="targetType">The target type</param>
    /// <param name="varName">the var to modify</param>
    public void ApplyMethod(object _target, System.Type targetType, string varName)
    {
        var target = System.Convert.ChangeType(_target, targetType);
        //Get choosen modifier according with upgradeTarget Value chosen 
        StatModifier modifier = GetModifier();
        //Get AddModifier method
        MethodInfo method = target.GetType().GetMethod(varName);
        //Call AddModifier method
        method.Invoke(target, new object[] { modifier });
        
    }

    public void ApplyFloatVariable(GameFramework.FloatVariable floatVar, float value)
    {
        floatVar.Value = value;
    }
    #endregion


    /// <summary>
    /// Return Modifier according with UpgradeType chosen
    /// </summary>
    /// <returns></returns>
    private StatModifier GetModifier()
    {
        StatModifier ret = null;
        switch (upgradeVarType)
        {
            case UpgradeType.Int:
                ret = upgradeValueInt;
                break;
            case UpgradeType.Float:
                ret = upgradeValueFloat;
                break;
            case UpgradeType.Bool:
                ret = upgradeValueBool;
                break;
            case UpgradeType.Vector2:
                ret = upgradeValueVecto2;
                break;
        }
        return ret;
    }
    #endregion

}
