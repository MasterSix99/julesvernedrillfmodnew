﻿using UnityEngine;

/// <summary>
/// Wrapper for upgrades , this is simply a MonoBehaviour Object used to store upgrades 
/// </summary>
public class Upgrade : MonoBehaviour
{
    /// <summary>
    /// The list of upgrades 
    /// </summary>
    public StatUpgrade[] upgrades;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
            Apply();
    }

    /// <summary>
    /// Apply all of the upgrades
    /// </summary>
    public void Apply()
    {
        int i = 0;
        int len = upgrades.Length;
        for(i = 0; i < len; i++)
        {
            upgrades[i].Apply();
        }
    }
}
