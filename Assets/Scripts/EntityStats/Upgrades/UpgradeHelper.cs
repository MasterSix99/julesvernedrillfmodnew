﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeHelper : MonoBehaviour
{
    [System.Serializable]
    private class FloatVariableUpgradeData
    {
        [SerializeField] private GameFramework.FloatVariable floatVariable;
        [SerializeField] private EntityFloatStat floatVarStat = new EntityFloatStat();

        public void SetFloatVar(StatModifierFloat modifier)
        {
            floatVarStat.AddModifier(modifier);
            floatVariable.Value = floatVarStat.GetValue();
        }

        public void ResetFloatVar()
        {
            floatVariable.Value = 0.0000f;
        }
    }


    [SerializeField] private FloatVariableUpgradeData[] floatVariables;
    [SerializeField] private bool resetFloatVarsAtMatchEnd = true;

    private void OnEnable()
    {
        if (resetFloatVarsAtMatchEnd)
            EventManager.StartListening(EventsID.GAMEEND, ResetValuesAtMatchEnd);
    }

    private void OnDisable()
    {
        if (resetFloatVarsAtMatchEnd)
            EventManager.StopListening(EventsID.GAMEEND, ResetValuesAtMatchEnd);
    }

    public void SetFloatVars(StatModifier modifier)
    {
        int i;
        for (i = 0; i < floatVariables.Length; i++)
        {
            floatVariables[i].SetFloatVar((StatModifierFloat)modifier);
        }
    }

    private void ResetValuesAtMatchEnd()
    {
        int i;
        for (i = 0; i < floatVariables.Length; i++)
        {
            floatVariables[i].ResetFloatVar();
        }
    }
}
