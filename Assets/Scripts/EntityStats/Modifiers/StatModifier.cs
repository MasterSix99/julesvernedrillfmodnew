﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class StatModifier
{
    /// <summary>
    /// multiplier value
    /// </summary>
    [SerializeField]
    protected float multiplier;

    public float Multiplier { get { return multiplier; } }
}
