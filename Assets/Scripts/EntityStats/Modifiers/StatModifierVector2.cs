﻿using UnityEngine;

[System.Serializable]
//[CreateAssetMenu(fileName = "StatModifierVector2", menuName = "StatModifier/Vector2")]
public class StatModifierVector2 : StatModifier
{
    [SerializeField]
    protected  Vector2 valueToSum;

    public Vector2 ValueToSum { get { return valueToSum; } }
}
