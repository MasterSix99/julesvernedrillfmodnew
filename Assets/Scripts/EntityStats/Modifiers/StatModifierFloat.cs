﻿using UnityEngine;

[System.Serializable]
//[CreateAssetMenu(fileName = "StatModifierFloat", menuName = "StatModifier/Float")]
public class StatModifierFloat :StatModifier
{
    [SerializeField]
    private float valueToSum;

    public float ValueToSum { get { return valueToSum; } }
}
