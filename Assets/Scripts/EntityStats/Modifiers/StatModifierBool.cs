﻿using UnityEngine;

[System.Serializable]
//[CreateAssetMenu(fileName = "StatModifierBool", menuName = "StatModifier/Bool")]
public class StatModifierBool : StatModifier {

    [SerializeField]
    private bool valueToSum;

    public bool ValueToSum { get { return valueToSum; } }
}
