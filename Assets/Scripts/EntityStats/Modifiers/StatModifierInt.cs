﻿using UnityEngine;

[System.Serializable]
public class StatModifierInt : StatModifier 
{
    [SerializeField]
    private int valueToSum;

    public int ValueToSum { get { return valueToSum; } }
}
