﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EntityGenericStat<T>
{
    [SerializeField] public T baseValue;

    [ReadOnly] public T valueToSum;
    [ReadOnly] public float multiplier = 1;

    private bool init = false;

    protected T finalValue;

    public virtual T GetValue()
    {
        if (!init)
        {
            UpdateFinalValue();
            init = true;
        }

        return finalValue;
    }

    public virtual void SetBaseValue(T newBaseValue)
    {
        baseValue = newBaseValue;
        UpdateFinalValue();
    }

    protected abstract void UpdateFinalValue();
    /// <summary>
    /// Apply the given modifer to stat values
    /// </summary>
    /// <param name="modifier">The modifier to apply</param>
    public virtual void AddModifier(StatModifier _modifier)
    {
        init = true;
    }
    public abstract void SumStat(EntityGenericStat<T>_stat);
}
