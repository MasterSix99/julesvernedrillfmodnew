﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EntityIntStat : EntityGenericStat<int>
{
    public override void AddModifier(StatModifier _modifier)
    {
        base.AddModifier(_modifier);
        StatModifierInt modifier = (StatModifierInt)_modifier;
        valueToSum += modifier.ValueToSum;
        multiplier += modifier.Multiplier;

        UpdateFinalValue();
    }

    public override void SumStat(EntityGenericStat<int> _stat)
    {
        valueToSum += _stat.valueToSum;
        multiplier += _stat.multiplier;
        UpdateFinalValue();
    }

    protected override void UpdateFinalValue()
    {
        int finalMultiplier = (multiplier == 0) ? 1 : Mathf.RoundToInt(multiplier);
        finalValue = Mathf.RoundToInt((baseValue + valueToSum) * finalMultiplier);
    }
}
