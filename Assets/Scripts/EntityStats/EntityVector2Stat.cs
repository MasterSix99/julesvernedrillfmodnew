﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class EntityVector2Stat : EntityGenericStat<Vector2>
{
    public override void AddModifier(StatModifier _modifier)
    {
        base.AddModifier(_modifier);
        StatModifierVector2 modifier = (StatModifierVector2)_modifier;
        valueToSum += modifier.ValueToSum;
        multiplier += modifier.Multiplier;

        UpdateFinalValue();
    }

    public override void SumStat(EntityGenericStat<Vector2> _stat)
    {
        valueToSum += _stat.valueToSum;
        multiplier += _stat.multiplier;

        UpdateFinalValue();
    }

    protected override void UpdateFinalValue()
    {
        finalValue = (baseValue + valueToSum) * multiplier;
    }
}
