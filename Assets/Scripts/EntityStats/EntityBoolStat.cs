﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EntityBoolStat : EntityGenericStat<bool>
{
    public override void AddModifier(StatModifier _modifier)
    {
        base.AddModifier(_modifier);
        StatModifierBool modifier = (StatModifierBool)_modifier;

        baseValue = modifier.ValueToSum;
        UpdateFinalValue();
    }

    public override void SumStat(EntityGenericStat<bool> _stat)
    {
        baseValue = _stat.valueToSum;
        UpdateFinalValue();
    }

    protected override void UpdateFinalValue()
    {
        finalValue = baseValue;
    }
}
