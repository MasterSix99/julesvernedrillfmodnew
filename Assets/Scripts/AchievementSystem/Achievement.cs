﻿using UnityEngine;

[CreateAssetMenu(fileName = "so_ach_1", menuName = "Achievements/Achievement")]
public class Achievement : ScriptableObject
{
    [System.Serializable]
    public struct AchievementSaveInfo
    {
        [SerializeField] private string achievementName;

        public bool unlocked;
        public bool selected;
        public float[] conditionCompletions;
        public float achievementCompletion;

        public string AchievementName { get { return achievementName; } }

        //public void CopyFromSave(AchievementSaveInfo savedInfo)
        //{
        //    unlocked = savedInfo.unlocked;
        //    selected = savedInfo.selected;
        //    conditionCompletions = savedInfo.conditionCompletions;
        //    achievementCompletion = savedInfo.achievementCompletion;
        //}
    }

    [System.Serializable]
    public class AchievementUIInfo
    {
        public Sprite lockedSprite;
        public Sprite unlockedSprite;
        public Sprite selectedSprite;
        public Sprite frameLockedSprite;
        public Sprite frameUnlockedSprite;
        public Sprite frameSelectedSprite;
    }

    public AchievementSaveInfo achievementDataToSave;
    [SerializeField]
    private AchievementConditionAbstract[] conditions;
    [SerializeField]
    private bool unlockAtGameEnd = false;
    [SerializeField]
    private bool allConditionsResolved = true;
    /// <summary>
    /// Upgrades to apply when stat is selected
    /// </summary>
    [SerializeField]
    private StatUpgrade[] upgrades;
    public string description;
    public string effectDescription;
    public string flavourText;
    [Header("UI")]
    public AchievementUIInfo uiInfo;

    public bool UnlockAtEndGame { get { return unlockAtGameEnd; } }

    public void CopyConditions()
    {
        int i;
        for (i = 0; i < conditions.Length; i++)
        {
            conditions[i] = Instantiate(conditions[i]);
            conditions[i].Initialize();
        }
    }

    public void UpdateAchievement(int statId, object valueToCheck)
    {
        if (!achievementDataToSave.unlocked)
        {
            achievementDataToSave.conditionCompletions = new float[conditions.Length];
            achievementDataToSave.achievementCompletion = 0.0000f;
            int i;
            for (i = 0; i < conditions.Length; i++)
            {
                //Debug.Log("CONDITION");
                if (conditions[i].CompareId(statId))
                {
                    conditions[i].UpdateCondition(valueToCheck);
                    achievementDataToSave.conditionCompletions[i] = conditions[i].completion;
                    achievementDataToSave.achievementCompletion += conditions[i].completion;
                    //Debug.Log(">>>>>Updated data to save");
                    //Debug.Log("achievementDataToSave.conditionCompletions -" + i + "- " + achievementDataToSave.conditionCompletions[i]);
                    //Debug.Log("achievementDataToSave.achievementCompletion - " + achievementDataToSave.achievementCompletion);
                }
            }
            if (!unlockAtGameEnd)
            {
                TryResolving();
            }
            achievementDataToSave.achievementCompletion /= conditions.Length;
        }
    }

    public void TryResolving()
    {
        bool canUnlock = allConditionsResolved;

        int i;
        for (i = 0; i < conditions.Length; i++)
        {
            if (allConditionsResolved)
            {
                if (!conditions[i].IsResolved())
                {
                    canUnlock = false;
                    break;
                }
            }
            else
            {
                if (conditions[i].IsResolved())
                {
                    canUnlock = true;
                    break;
                }
            }
        }

        if (canUnlock)
            Unlock();
    }

    private void Unlock()
    {
        achievementDataToSave.unlocked = true;
        EventManager.TriggerEvent(EventsID.ACHIEVEMENTUNLOCKED, this);
    }

    public void ToggleSelection()
    {
        if (achievementDataToSave.unlocked)
        {
            if (achievementDataToSave.selected)
                Deselect();
            else
                Select();
        }
    }

    public void Select()
    {
        if (achievementDataToSave.unlocked)
            achievementDataToSave.selected = true;
    }
    public void Deselect()
    {
        if (achievementDataToSave.unlocked)
            achievementDataToSave.selected = false;
    }

    //Apply the upgrades
    public void Apply()
    {
        int i;
        int len = upgrades.Length;
        for(i = 0; i < len; i++)
        {
            upgrades[i].Apply();
        }
    }

    public void CopyFromSave(AchievementSaveInfo savedInfo)
    {
        achievementDataToSave.unlocked = savedInfo.unlocked;
        achievementDataToSave.selected = savedInfo.selected;

        achievementDataToSave.conditionCompletions = savedInfo.conditionCompletions;
        achievementDataToSave.achievementCompletion = savedInfo.achievementCompletion;

        int i;
        for (i = 0; i < achievementDataToSave.conditionCompletions.Length; i++)
        {
            conditions[i].completion = achievementDataToSave.conditionCompletions[i];
        }
    }

    //public float GetAchievementCompletion()
    //{
    //    float completionSum = 0f;

    //    int i;
    //    for (i = 0; i < conditions.Length; i++)
    //    {
    //        completionSum += conditions[i].GetCompletion();
    //    }

    //    return completionSum / conditions.Length;
    //}
}