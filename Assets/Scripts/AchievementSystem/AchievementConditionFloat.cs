﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "so_ach_cnd_float_1", menuName = "Achievements/Conditions/Float")]
public class AchievementConditionFloat : AchievementCondition<float>
{
    [SerializeField] private bool isMinor = false;
    [SerializeField] private bool isEqual = false;
    [SerializeField] private bool isMajor = false;

    public override void Initialize()
    {
        currentValue = 0.0000f;
    }

    public override void UpdateCondition(object value)
    {
        currentValue = (float)value;

        float major = Mathf.Max(currentValue, goal);
        float minor = Mathf.Min(currentValue, goal);

        completion = minor / major;
    }

    public override bool IsResolved()
    {
        bool result = false;

        if (isMinor)
            result = currentValue < goal;
        else if (isMajor)
            result = currentValue > goal;

        if (!result && isEqual)
            result = currentValue == goal;

        return result;
    }

    //public override float GetCompletion()
    //{
    //    return currentValue / goal;
    //}
}
