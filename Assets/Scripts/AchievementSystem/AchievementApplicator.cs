﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Use this to apply selected achievements on game scenes
/// </summary>
public class AchievementApplicator : MonoBehaviour
{
    private void Start()
    {
        ApplyAchivements();
    }

    private void ApplyAchivements()
    {
        Achievement[] achievements = AchievementManager.Instance.Achievements;
        

        int i = 0;
        int len = achievements.Length;
        for (i = 0; i < len; i++)
        {
            if (achievements[i].achievementDataToSave.unlocked && achievements[i].achievementDataToSave.selected)
            {
                Debug.Log(achievements[i].name);
                achievements[i].Apply();
            }
        }
    }
}
