﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AchievementCondition<T> : AchievementConditionAbstract
{
    [SerializeField] protected T goal;
    [ReadOnly] [SerializeField] protected T currentValue;
}
