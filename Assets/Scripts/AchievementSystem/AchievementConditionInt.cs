﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "so_ach_cnd_int_1", menuName = "Achievements/Conditions/Int")]
public class AchievementConditionInt : AchievementCondition<int>
{
    [SerializeField] private bool isMinor = false;
    [SerializeField] private bool isEqual = false;
    [SerializeField] private bool isMajor = false;

    public override void Initialize()
    {
        currentValue = 0;
    }

    public override void UpdateCondition(object value)
    {
        currentValue = (int)value;
        
        float major = Mathf.Max(currentValue, goal);
        float minor = Mathf.Min(currentValue, goal);

        completion = minor / Mathf.Clamp(major, major, minor);
    }

    public override bool IsResolved()
    {
        bool result = false;

        if (isMinor)
            result = currentValue < goal;
        else if (isMajor)
            result = currentValue > goal;

        if (!result && isEqual)
            result = currentValue == goal;

        return result;
    }

    //public override float GetCompletion()
    //{
    //    return (float)currentValue / goal;
    //}
}
