﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementManager : Singleton<AchievementManager>
{
    #region VARIABLES
    [SerializeField]
    private int maxAchievementSelection = 3;
    [SerializeField] private Achievement[] achievements;
    //private Dictionary<int, List<Achievement>> achievementDictionary = new Dictionary<int, List<Achievement>>();
    #endregion

    #region PROPERTIES
    public Achievement[] Achievements { get { return achievements; } }
    #endregion

    protected override void Awake()
    {
        base.Awake();

        CopyAchievements();
        LoadAchievements();
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventsID.GAMEEND, OngameEnd);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventsID.GAMEEND, OngameEnd);
    }

    private void Update()
    {
        //if (DebugManager.Instance.DebugMode)
        //{
            //if (Input.GetKeyDown(KeyCode.N))
            //    DebugUnlockAndSelectAchievements();
            //else if (Input.GetKeyDown(KeyCode.M))
            //    DebugLockAndDeselectAchievements();
            ///*else */if (Input.GetKeyDown(KeyCode.V))
            //{
            //    UpdateAchievement(21, 30);
            //}
            /*else */if (Input.GetKeyDown(KeyCode.B))
            {
                UpdateAchievement(2, 100);
            }
            //else if (Input.GetKeyDown(KeyCode.N))
            //{
            //    UpdateAchievement(2, 1f);
            //}
        //}
    }

    private void OnApplicationQuit()
    {
        Debug.Log("Quitting application");
        SaveAchievements();
    }

    public void UpdateAchievement(int statConditionId, object valueToCheck)
    {
        int i;
        for (i = 0; i < achievements.Length; i++)
        {
            achievements[i].UpdateAchievement(statConditionId, valueToCheck);
        }

        //if (achievementDictionary.ContainsKey(statId))
        //{
        //    int i;
        //    for (i = 0; i < achievementDictionary[statId].Count; i++)
        //    {
                
        //        achievementDictionary[statId][i].UpdateAchievement(statId, valueToCheck);
        //    }
        //}
    }

    private void OngameEnd()
    {
        int i;
        for (i = 0; i < achievements.Length; i++)
        {
            if (achievements[i].UnlockAtEndGame)
                achievements[i].TryResolving();
        }
    }

    public void SelectAchievement(int index)
    {
        if (!CheckAllAchievementsSelected())
        {
            achievements[index].Select();
        }
    }
    public void SelectAchievement(Achievement achievement)
    {
        if (!CheckAllAchievementsSelected()) { 
            int i = 0;
            int len = achievements.Length;
            for (i = 0; i < len; i++)
            {
                if (achievements[i].Equals(achievement))
                {
                    achievements[i].Select();
                    return;
                }
            }
        }
    }
    public void DeselectAchievement(int index)
    {
        achievements[index].Deselect();
    }
    public void DeselectAchievement(Achievement achievement)
    {
            int i = 0;
            int len = achievements.Length;
            for (i = 0; i < len; i++)
            {
                if (achievements[i].Equals(achievement))
                {
                    achievements[i].Deselect();
                    return;
                }
            }
    }

    public bool CheckAllAchievementsSelected()
    {
        int selectedCount = 0;
        int i = 0;
        int len = achievements.Length;
        for (i = 0; i < len; i++)
        {
            if (achievements[i].achievementDataToSave.selected)
            {
                selectedCount++;
                if (selectedCount == maxAchievementSelection)
                    return true;
            }
        }
        return false;
    }
    #region Methods for saves

    private void SaveAchievements()
    {
        //SaveFile saveFile = new SaveFile();

        int i;
        for(i = 0; i < achievements.Length; i++)
        {
            SaveController.Instance.saveFile.achievementsData.Add(achievements[i].achievementDataToSave);
        }
        //SaveManager.SaveObjectBinary(saveFile, GameGlobals.Saves.SavePath);
    }

    private void CopyAchievements()
    {
        int i;
        for (i = 0; i < achievements.Length; i++)
        {
            achievements[i] = Instantiate(achievements[i]);

            //if (!achievementDictionary.ContainsKey(achievements[i].achievementDataToSave.Id))
            //    achievementDictionary.Add(achievements[i].achievementDataToSave.Id, new List<Achievement>());

            //achievementDictionary[achievements[i].achievementDataToSave.Id].Add(achievements[i]);


            achievements[i].CopyConditions();
        }
    }

    private void LoadAchievements()
    {
        //SaveFile saveFile = (SaveFile)SaveManager.LoadObjectBinary(GameGlobals.Saves.SavePath);
        if (SaveManager.SaveFileExists(GameGlobals.Saves.SavePath))
        {
            if (SaveController.Instance.saveFile.achievementsData != null)
            {
                int i, j;
                for (i = 0; i < SaveController.Instance.saveFile.achievementsData.Count; i++)
                {
                    for (j = 0; j < achievements.Length; j++)
                    {
                        if (string.Equals(SaveController.Instance.saveFile.achievementsData[i].AchievementName, achievements[j].achievementDataToSave.AchievementName, System.StringComparison.OrdinalIgnoreCase))
                        {
                            achievements[j].CopyFromSave(SaveController.Instance.saveFile.achievementsData[i]);
                            break;
                        }
                    }
                }
            }
        }
    }

    #endregion

    #region Debug Methods
    public void DebugUnlockAndSelectAchievements()
    {
        int i;
        for (i = 0; i < achievements.Length; i++)
        {
            achievements[i].achievementDataToSave.unlocked = true;
            achievements[i].achievementDataToSave.selected = true;
        }
    }

    public void DebugLockAndDeselectAchievements()
    {
        int i;
        for (i = 0; i < achievements.Length; i++)
        {
            achievements[i].achievementDataToSave.unlocked = false;
            achievements[i].achievementDataToSave.selected = false;
        }
    }
    #endregion
}
