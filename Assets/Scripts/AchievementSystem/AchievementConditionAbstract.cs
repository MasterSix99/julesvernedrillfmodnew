﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AchievementConditionAbstract : ScriptableObject
{
    [SerializeField] private int id;
    [HideInInspector] public float completion;

    public int Id { get { return id; } }

    public virtual void Initialize()
    {

    }

    public bool CompareId(int in_id)
    {
        return id == in_id;
    }
    public abstract void UpdateCondition(object value);

    public abstract bool IsResolved();

    //public abstract float GetCompletion();
}
