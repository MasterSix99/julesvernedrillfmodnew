﻿using GameFramework;
using UnityEngine;

public class DrillHeadTilingAnimation : MonoBehaviour
{
    [SerializeField]
    private bool isEnabled = true;
    [SerializeField]
    private float minVal = 5.12f;
    [SerializeField]
    private float maxVal = 10.24f;
    [SerializeField]
    private float speed = 1;
    [SerializeField]
    private FloatVariable speedVal;
    
    private SpriteRenderer sprR;

    public float MinVal
    {
        get { return minVal; }
        set { minVal = value; }
    }
    public float MaxVal
    {
        get { return maxVal; }
        set { maxVal = value; }
    }
    public bool IsEnabled
    {
        get { return isEnabled; }
        set { isEnabled = value; }
    }

    private void Awake()
    {
        sprR = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isEnabled)
        {
            UpdateDrillAnimation();
        }
    }


    void UpdateDrillAnimation()
    {
        sprR.size = new Vector2(sprR.size.x,Mathf.MoveTowards(sprR.size.y,MaxVal, Time.deltaTime * speedVal.Value));
        if (sprR.size.y >= maxVal)
        {
            sprR.size = new Vector2(sprR.size.x,minVal);
        }
    }
}
