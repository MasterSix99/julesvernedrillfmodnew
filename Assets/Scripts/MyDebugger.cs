﻿
using GameFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyDebugger : MonoBehaviour
{
    public enum GUIPosition
    {
        Up_Left = 0,
        Up_Center = 1,
        Up_Right = 2,
        Mid_Left = 3,
        Mid_Center = 4,
        Mid_Right = 5,
        Down_Left = 6,
        Down_Center = 7,
        Down_Right = 8
    }

    [System.Serializable]
    public class FloatVarData
    {
        public string prefix;
        public FloatVariable floatVar;

        public GUIPosition guiPosition;
        public Vector2 boxDimensions;
        public Vector2 positionOffset;
    }

    [SerializeField] private bool loadSceneWithKey = true;
    [ShowIfBool("loadSceneWithKey")] [Tooltip("Write 'current' to reload current scene")] [SerializeField] private string loadSceneName;
    [ShowIfBool("loadSceneWithKey")] [SerializeField] private KeyCode reloadKeyCode;
    [ShowIfBool("loadSceneWithKey")] [SerializeField] private GUIPosition guiPosition;
    [ShowIfBool("loadSceneWithKey")] [SerializeField] private Vector2 boxDimensions;
    [ShowIfBool("loadSceneWithKey")] [SerializeField] private Vector2 positionOffset;

    [Header("Float variables")]
    public FloatVarData[] floatVarDatas;

    private GUIStyle style = new GUIStyle();

    private void Start()
    {
        style.fontSize = 30;
    }

    private void Update()
    {
        if (DebugManager.Instance.DebugMode)
        {
            if (loadSceneWithKey)
            {
                if (Input.GetKeyDown(reloadKeyCode))
                    LoadScene();

                if (Input.GetKeyDown(KeyCode.P))
                    Time.timeScale = 0.1f;
                else if (Input.GetKeyDown(KeyCode.O))
                    Time.timeScale = 1f;
            }
        }
    }

    private void OnGUI()
    {
        if (DebugManager.Instance.DebugMode)
        {
            ShowLoadCommandOnUI();
            for (int i = 0; i < floatVarDatas.Length; i++)
            {
                DrawFloatVarData(floatVarDatas[i]);
            }
        }
    }

    private void LoadScene()
    {
        if (loadSceneName.ToLower() == "current")
            SceneController.ReloadScene();
        else
            SceneController.LoadScene(loadSceneName);
    }

    private void DrawFloatVarData(FloatVarData data)
    {
        if (data.floatVar != null)
        {
            Vector2 pos = GetGUIPosition(data.guiPosition, data.boxDimensions, data.positionOffset);
            GUI.Label(new Rect(pos.x, pos.y, data.boxDimensions.x, data.boxDimensions.y), data.prefix + data.floatVar.Value.ToString("#.##"));
        }
    }

    private void ShowLoadCommandOnUI()
    {
        Vector2 pos = GetGUIPosition(guiPosition, boxDimensions, positionOffset);
        GUI.Label(new Rect(pos.x, pos.y, boxDimensions.x, boxDimensions.y), "press " + reloadKeyCode.ToString() + " to load " + loadSceneName + " scene");
    }

    private Vector2 GetGUIPosition(GUIPosition positionEnumValue, Vector2 boxSize, Vector2 offset)
    {
        Vector2 resultPos = Vector2.zero;
        switch (positionEnumValue)
        {
            case GUIPosition.Up_Left:
                resultPos.Set(0, 0);
                break;
            case GUIPosition.Up_Center:
                resultPos.Set(Screen.width / 2, 0);
                break;
            case GUIPosition.Up_Right:
                resultPos.Set(Screen.width, 0);
                break;
            case GUIPosition.Mid_Left:
                resultPos.Set(0, Screen.height / 2);
                break;
            case GUIPosition.Mid_Center:
                resultPos.Set(Screen.width / 2, Screen.height / 2);
                break;
            case GUIPosition.Mid_Right:
                resultPos.Set(Screen.width, Screen.height / 2);
                break;
            case GUIPosition.Down_Left:
                resultPos.Set(0, Screen.height);
                break;
            case GUIPosition.Down_Center:
                resultPos.Set(Screen.width / 2, Screen.height);
                break;
            case GUIPosition.Down_Right:
                resultPos.Set(Screen.width, Screen.height);
                break;
        }

        resultPos.Set(resultPos.x - boxSize.x / 2 * ((int)positionEnumValue % 3) + offset.x, resultPos.y - boxSize.y / 2 * ((int)positionEnumValue / 3) + offset.y);
        return resultPos;
    }
}
