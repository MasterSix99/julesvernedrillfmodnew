﻿/**
 * @author Fabrizio Coppolecchia
 *
 * 
 * This class provide some common functionality for ui Menu/Buttons 
 * 
 * 
 * @date - 2018/03/05
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUtils : MonoBehaviour
{

    public void LoadLevel(string levelName)
    {
        GameFramework.SceneController.LoadScene(levelName);
    }

    public void RestartLevel()
    {
        GameFramework.SceneController.ReloadScene();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
            Debug.Log(MyEventSystem.current.currentSelectedGameObject);
    }
}
