﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TMPBinder : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI UItext;
    [SerializeField] private GameFramework.FloatVariable floatVariable;
    [SerializeField] private string toStringFormat = "#";

    private void Awake()
    {
        if (UItext == null)
            UItext = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        floatVariable.onValueChangeAction += UpdateText;
    }

    private void OnDisable()
    {
        floatVariable.onValueChangeAction -= UpdateText;
    }

    private void UpdateText()
    {
        string stringValue = floatVariable.Value.ToString(toStringFormat);

        if (!string.IsNullOrEmpty(stringValue))
            UItext.text = stringValue;
        else
            UItext.text = "0";
    }
}
