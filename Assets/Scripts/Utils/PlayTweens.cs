﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class PlayTweens : MonoBehaviour
{
    public enum DOTweenPlayMode
    {
        Forward,
        Backwards,
        Rewind,
        Play,
        Pause,
        TogglePause,
        Restart,
        Kill,
    }
    [SerializeField]
    private DOTweenPlayMode playMode;
    [SerializeField]
    private bool playOnEnable;
    [SerializeField]
    private bool PlayOnDisable;
    [SerializeField]
    private DOTweenAnimation[] tweens;
    //[SerializeField]
    private UnityEvent onLastComplete;
    private DOTweenAnimation longest;

    #region MonoBehaviours
    private void Start()
    {
        /*foreach (DOTweenAnimation tween in tweens)
        {
            if(longest == null || tween.duration > longest.duration)
            {
                
                longest = tween;
                Debug.Log(longest.duration);
            }
        }
        if(longest != null) {
            Debug.Log("LONGEST DURATION = " + longest.duration);
            longest.onComplete = new UnityEvent();
            longest.onComplete.AddListener(LastTweenCompleted);
            Debug.Log(longest.onComplete);
        }*/
    }
    // Start is called before the first frame update
    void OnEnable()
    {
        if (playOnEnable) PlayAll();
    }
    private void OnDisable()
    {
        if (PlayOnDisable) PlayAll();
    }
    private void OnDestroy()
    {
        //if (longest != null) longest.onComplete.RemoveListener(LastTweenCompleted);
    }
    #endregion

    #region METHODS
    /// <summary>
    /// Play tweeens in list using local chosen method
    /// </summary>
    public void PlayAll()
    {
        PlayAll(playMode);
    }
    /// <summary>
    /// Play tweeens in list using  chosen method
    /// </summary>
    /// <param name="_playmode">Play method</param>
    public void PlayAll(DOTweenPlayMode _playmode)
    {
        foreach (DOTweenAnimation tween in tweens)
        {
            if (_playmode.Equals(DOTweenPlayMode.Forward))
            {
                tween.DOPlayForward();
            }
            else if (_playmode.Equals(DOTweenPlayMode.Backwards))
            {
                tween.DOPlayBackwards();
            }
            else if (_playmode.Equals(DOTweenPlayMode.Rewind))
            {
                tween.DORewind();
            }
            else if (_playmode.Equals(DOTweenPlayMode.Play))
            {
                tween.DOPlay();
            }
            else if (_playmode.Equals(DOTweenPlayMode.Pause))
            {
                tween.DOPause();
            }
            else if (_playmode.Equals(DOTweenPlayMode.TogglePause))
            {
                tween.DOTogglePause();
            }
            else if (_playmode.Equals(DOTweenPlayMode.Restart))
            {
                tween.DORestart();
            }
            else if (_playmode.Equals(DOTweenPlayMode.Kill))
            {
                tween.DOKill();
            }
        }
    }

    private void LastTweenCompleted()
    {
        Debug.Log("LAST TWEEN COMPLETED");
        onLastComplete?.Invoke();
    }
    #endregion

}
