﻿
/**
 * @author Fabrizio Coppolecchia
 *
 * This Component is used to disable a gameobject when his particle is not playng anymore. It can also Play particle when object is Enabled
 * 
 * @date - 2019/01/03
 */


using UnityEngine;

public class DisableAfterParticleEnd : MonoBehaviour {
    // WARNING!!!!!!!!!!! this can disable particle after play, stay alert for bug
    
    [SerializeField]
    private ParticleSystem particle;
    [SerializeField]
    private bool playParticleOnEnable = true;

    private void Awake()
    {
        if (particle == null)
            particle = GetComponent<ParticleSystem>();
    }

    private void OnEnable()
    {
        if(playParticleOnEnable) particle.Play();
    }

    private void Update()
    {
        if (!particle.isPlaying) {
            gameObject.SetActive(false);
        }
    }

    private void OnValidate()
    {
        if (particle == null)
            particle = GetComponent<ParticleSystem>();
    }
}
