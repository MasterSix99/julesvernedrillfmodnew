﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseHelper : MonoBehaviour
{
    [SerializeField]
    private bool m_disableOnAwake = true;
    [SerializeField]
    private bool m_enableMouseOnMove = false;
    [ShowIf("m_enableMouseOnMove", true)]
    [SerializeField]
    private float inactivityHideTime = 2f;
    [SerializeField]
    private KeyCode toggleMouseKey = KeyCode.M;


    private float timer = 0;

    private void Awake()
    {
        if (m_disableOnAwake)
            DisableCursor();
    }


    // Update is called once per frame
    void Update()
    {
        if (m_enableMouseOnMove)
        {
            if((Mathf.Abs(Input.GetAxis("Mouse X")) > 0 || Mathf.Abs(Input.GetAxis("Mouse Y")) > 0))
            {
                if (!Cursor.visible) { 
                    EnableCursor();
                }
                timer = 0;
            }
            else if(m_enableMouseOnMove)
            {
                timer += Time.deltaTime;
            }
        }
        if (m_enableMouseOnMove && timer > inactivityHideTime)
            DisableCursor();

        if (Input.GetKeyDown(toggleMouseKey))
        {
            if (Cursor.visible)
                DisableCursor();
            else
                EnableCursor();
        }
    }

    /// <summary>
    /// Enable Cursor on screen (NB the cursor will be disablet after time set on variable inactivityHideTime
    /// </summary>
    void EnableCursor()
    {
        //Debug.Log("ENABLE CURSOR");
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
    /// <summary>
    /// Disable cursor on screen
    /// </summary>
    void DisableCursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

}
