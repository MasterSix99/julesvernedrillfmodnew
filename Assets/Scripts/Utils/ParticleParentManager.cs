﻿using System.Collections.Generic;
using UnityEngine;

public class ParticleParentManager : MonoBehaviour
{
    [SerializeField]
    private bool enableParticleOnObjectEnabled = true;
    [SerializeField]
    private bool disableParticleOnObjectDisabled = true;

    List<ParticleSystem> particles;
    private void Awake()
    {
        particles = new List<ParticleSystem>();
        foreach (Transform child in transform)
        {
            ParticleSystem particle = child.GetComponent<ParticleSystem>();
            if (particle != null)
                particles.Add(particle);
        }
    }

    private void OnEnable()
    {
        if (enableParticleOnObjectEnabled)
        {
            foreach(ParticleSystem particle in particles)
            {
                particle.Play();
            }
        }
    }

    private void OnDisable()
    {
        if (disableParticleOnObjectDisabled)
        {
            foreach (ParticleSystem particle in particles)
            {
                particle.Stop();
            }
        }
    }

}
