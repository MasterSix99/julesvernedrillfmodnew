﻿using GameFramework;
using UnityEngine;
using TMPro;
public class Timer : MonoBehaviour
{
    [SerializeField]
    private FloatVariable timer;
    private TextMeshProUGUI textField;
    [SerializeField]
    private string text;

    private void Awake()
    {
        textField = GetComponent<TextMeshProUGUI>();
    }
    // Update is called once per frame
    void Update()
    {
        textField.text = (text+Mathf.Round(timer.Value).ToString());
    }
}
