﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorGroup : MonoBehaviour
{
    [SerializeField] private Animator[] animators;

    public void SetFloat(string floatName, float newValue)
    {
        int i;
        for(i = 0; i < animators.Length; i++)
        {
            animators[i].SetFloat(floatName, newValue);
        }
    }

    public void SetBool(string boolName, bool newValue)
    {
        int i;
        for (i = 0; i < animators.Length; i++)
        {
            animators[i].SetBool(boolName, newValue);
        }
    }

    public void SetInt(string intName, int newValue)
    {
        int i;
        for (i = 0; i < animators.Length; i++)
        {
            animators[i].SetInteger(intName, newValue);
        }
    }
}
