﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnHit2DParentToObject : MonoBehaviour
{
    struct ParentObject {
        private GameObject obj;
        private Transform previousParent;

        public Transform PreviousParent
        {
            get { return previousParent; }
            set { previousParent = value; }
        }
        public GameObject Obj
        {
            get { return obj; }
            set { obj = value; }
        }

    }

    [SerializeField]
    private LayerMask allowedLayers;
    [SerializeField]
    private bool useCollision2D = true;
    [SerializeField]
    private bool useTrigger2D = false;
    //private List<ParentObject> parentedList;
    private Dictionary<Transform, Transform> parentedList;

    private void Awake()
    {
        //parentedList = new List<ParentObject>();
        parentedList = new Dictionary<Transform, Transform>();
    }

    private void AddParent(Transform tr)
    {
        parentedList.Add(tr, tr.parent);
        tr.parent = transform;
    }

    private void RemoveParent(Transform tr)
    {
        if (parentedList.ContainsKey(tr))
        {
            tr.parent = parentedList[tr];
            parentedList.Remove(tr);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (useCollision2D && GameFramework.Utilities.LayerIsInLayerMask(collision.gameObject.layer, allowedLayers))
        {
            AddParent(collision.transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (useCollision2D && GameFramework.Utilities.LayerIsInLayerMask(collision.gameObject.layer, allowedLayers))
        {
            RemoveParent(collision.transform);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (useTrigger2D && GameFramework.Utilities.LayerIsInLayerMask(collision.gameObject.layer, allowedLayers))
        {
            AddParent(collision.transform);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (useTrigger2D && GameFramework.Utilities.LayerIsInLayerMask(collision.gameObject.layer, allowedLayers))
        {
            RemoveParent(collision.transform);
        }
    }


}
