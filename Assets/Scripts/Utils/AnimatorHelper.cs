﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorHelper : MonoBehaviour
{
    private Animator animator;

    [SerializeField] private bool randomPlayOnStart = true;
    [SerializeField] private int maxRandomParameter = 1;
    [SerializeField] private string defaultParameter;
    [SerializeField] private string defaultState;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        if (randomPlayOnStart)
        {
            RandomPlay();
        }
    }

    public void Play()
    {
        animator.Play(defaultState);
    }

    public void RandomPlay()
    {
        SetRandomParameter(defaultParameter);
        Play();
    }

    public void SetRandomParameter()
    {
        SetRandomParameter(defaultParameter);
    }

    public void SetRandomParameter(string parameterName)
    {
        animator.SetInteger(parameterName, Random.Range(0, maxRandomParameter + 1));
    }
}
