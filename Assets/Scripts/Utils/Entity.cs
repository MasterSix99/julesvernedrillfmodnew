﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Entity : MonoBehaviour
{

    #region VARIABLES
    [Space]
    [Header("Entity params")]
    [SerializeField] protected bool isEnabled = true;
    [SerializeField] protected bool setAlsoGameObjectEnabledStatus = false;
    [Space]
    protected int _id = -1;
    #endregion

    #region EVENTS
    [Space]
    [Header("Entity Events")]
    public UnityEvent<bool> StatusChangedEvent;
    #endregion

    #region PROPERTIES
    public bool IsEnabled
    {
        get { return isEnabled; }
        set
        {
            isEnabled = value;
            if (StatusChangedEvent != null)
            {
                StatusChangedEvent.Invoke(value);
            }
            if (setAlsoGameObjectEnabledStatus)
                gameObject.SetActive(value);
        }
    }
    protected int ObjId
    {
        get
        {
            if (_id == -1)
                _id = GetInstanceID();
            return _id;
        }
    }
    #endregion

    #region MONOBEHAVIOR METHODS
    protected virtual void Awake()
    {
        IsEnabled = isEnabled;
        //toggleAudioEnabled.Init(transform);
    }

    protected virtual void OnEnable()
    {
    }
    protected virtual void Start()
    {
    }
    protected virtual void OnDisable()
    {
    }
    #endregion

    #region METHODS
    public virtual void ToggleState()
    {
        IsEnabled = !IsEnabled;
    }
    public virtual void ToggleObjectState()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
    }
    public IEnumerator ExecuteAfterTime(float time, System.Action callback)
    {
        yield return new WaitForSeconds(time);
        callback();
        yield return null;
    }
    #endregion
}