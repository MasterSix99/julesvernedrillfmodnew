﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationUtils : MonoBehaviour
{
    public void DisableParentGameobject()
    {
        transform.parent.gameObject.SetActive(false);
    }
}
