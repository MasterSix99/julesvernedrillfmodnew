﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public static class PlayerUtilities 
{
    /// <summary>
    /// Check if one of players in list own a specific controller
    /// </summary>
    /// <param name="players">list of players to check</param>
    /// <param name="controller">controller to check</param>
    /// <returns>The playerSession that own the controller or null</returns>
    public static PlayerSession GetControllerOwner(ref List<PlayerSession> players, Controller controller)
    {
        foreach (PlayerSession player in players)
        {
            Player rewiredPlayer = ReInput.players.GetPlayer(player.RewiredPlayerId);
            if (rewiredPlayer.controllers.ContainsController(controller))
            {
                return player;
            }
        }
        return null;
    }
    /// <summary>
    /// Check if one of players in game own a specific controller (take player list from DataManager
    /// </summary>
    /// <param name="controller">controller to check</param>
    /// <returns>The playerSession that own the controller or null</returns>
    public static PlayerSession GetControllerOwner(Controller controller)
    {
        foreach (PlayerSession player in DataManager.Instance.Players)
        {
            Player rewiredPlayer = ReInput.players.GetPlayer(player.RewiredPlayerId);
            if (rewiredPlayer.controllers.ContainsController(controller))
            {
                return player;
            }
        }
        return null;
    }


    public static PlayerSession GetPlayer(ref List<PlayerSession> players,Player rewiredPlayer)
    {
        foreach (PlayerSession player in players)
        {
            if (player.RewiredPlayerId == rewiredPlayer.id)
            {
                return player;
            }
        }
        return null;
    }
    public static PlayerSession GetPlayer(ref List<PlayerSession> players, int rewiredPlayerId)
    {
        foreach (PlayerSession player in players)
        {
            if (player.RewiredPlayerId == rewiredPlayerId)
            {
                return player;
            }
        }
        return null;
    }
    public static PlayerSession GetPlayer(Player rewiredPlayer)
    {
        foreach (PlayerSession player in DataManager.Instance.Players)
        {
            if (player.RewiredPlayerId == rewiredPlayer.id)
            {
                return player;
            }
        }
        return null;
    }
    public static PlayerSession GetPlayer(int rewiredPlayerId)
    {
        foreach (PlayerSession player in DataManager.Instance.Players)
        {
            if (player.RewiredPlayerId == rewiredPlayerId)
            {
                return player;
            }
        }
        return null;
    }

 
    



}
