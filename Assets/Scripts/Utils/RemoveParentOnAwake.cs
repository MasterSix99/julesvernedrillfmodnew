﻿using UnityEngine;

public class RemoveParentOnAwake : MonoBehaviour
{
    private void Awake()
    {
        if (transform.parent != null)
            transform.parent = null;
    }
}
