﻿using GameFramework;
using UnityEngine;

public class DebugManager : Singleton<DebugManager>
{
    [SerializeField]
    private bool debugMode = true;
    /// <summary>
    /// If True a gui layout with current state name is shown
    /// </summary>
    [SerializeField]
    private bool debugCurrentGameState = true;

    public int spawnedDirt, spawnedCoal, spawnedStone, spawnedGem, spawnedLava;

    public bool DebugMode { get { return debugMode; } }



    void OnGUI()
    {
        if (debugCurrentGameState)
            GUI.TextArea(new Rect(10, 10, 100, 25), GameState.CurrentGameState.ToString());
    }
    private void Update()
    {
        if (DebugMode)
        {
            //if (Input.GetKeyDown(KeyCode.K))
            //    EventManager.TriggerEvent(EventsID.GAMEEND);
        }
    }
}